============
Icraft XRun
============

新组件XRun将取代旧的Run（在线运行用）、Simulator（离线仿真用），同时具备两者的功能。

XRun基于XRT实现，相当于是XRT的可执行程序外壳。

XRun仅有一个main.cpp组成，不提供头文件，也没有API。

XRun的产物为icraft-run.exe（Windows）或icraft-run（Linux），但都可以使用icraft-run调用


一 命令行参数
=============

.. list-table:: Table.命令行参数
   :widths: 15 30
   :header-rows: 1

   * - 参数
     - 说明
   * - json
     - 待运行的json文件路径
   * - raw
     - 待运行的raw文件路径
   * - jr_path
     - 待运行的json和raw文件路径
   * - network
     - 待运行的json和raw文件名
   * - input
     - 输入文件名：支持图片或ftmp；支持多输入
   * - log_path
     - log存放路径，默认为.logs；完整路径为log_path/network/\*.log
   * - backends
     - 指定后端，默认为Host
   * - dump_format
     - 指定dump特征图使用的格式；支持SFB/SFT/SQB/SQT/HQB/HQT等；默认为空，即不dump
   * - dump_ftmp
     - 指定dump哪些特征图；默认为空，即dump所有特征图
   * - fake_qf
     - 打开fake_qf模式，仅针对HostBackend有效
   * - compress
     - 打开内存压缩模式，仅针对BYBackend有效
   * - log_time
     - 记录每个算子的执行时间信息到屏幕和xrun_io.log文件中
   * - log_io
     - 记录每个算子的输入输出信息到屏幕和xrun_time.log文件中
   * - merge_hardop
     - 打开合并硬算子模式，仅对BYBackend有效
   * - lazy_load
     - 开启惰性加载，仅对输入模型文件为json和raw时有效
   * - precheck
     - 开启device预检，包括dma读写检查、寄存器读写检查、位流是否打patch
   * - frequency
     - 设置ICore的时钟频率，单位为MHz
   * - decode_dll
     - 指定网络输入预处理所使用的动态链接库


二 使用示例
==============

2.1 两种输入方式
^^^^^^^^^^^^^^^^

Xrun支持json+raw和jr_path+network两种输入方式

两种输入方式均支持绝对路径和相对路径

以下是几种输入方式

.. code-block:: bash
   :linenos:

   icraft-run --json "./resnet101_adapted.json" --raw "./resnet101_adapted.raw" --input "./elephant.jpg"
   icraft-run --json "E:/resnet101_adapted.json" --raw "E:/resnet101_adapted.raw" --input "E:/elephant.jpg"
   icraft-run --jr_path "./" --network "resnet101_adapted" --input "./elephant.jpg"
   icraft-run --jr_path "E:/" --network "resnet101_adapted" --input "E:/elephant.jpg"

注意：当同时输入json+raw和jr_path+network两种组合时，Xrun将优先加载json+raw指定的文件

2.2 后端指定
^^^^^^^^^^^^

Xrun目前支持两种后端

后端HostBackend支持CPU、Cuda等多种Device

后端BYBackend支持Socket、AXI等多种Device

不指定后端时，默认为HostCPU

以下是几种指定方式

.. code-block:: bash
   :linenos:

   icraft-run --json "./resnet101_adapted.json" --raw "./resnet101_adapted.raw" --input "./elephant.jpg"
   icraft-run --json "./resnet101_adapted.json" --raw "./resnet101_adapted.raw" --input "./elephant.jpg" --backends "Host"
   icraft-run --json "./resnet101_adapted.json" --raw "./resnet101_adapted.raw" --input "./elephant.jpg" --backends "Host,Cuda"
   icraft-run --json "./resnet101_BY.json" --raw "./resnet101_BY.raw" --input "./elephant.jpg" --backends "BY,socket://ql100aiu@192.168.125.148:9981?npu=0x40000000&dma=0x80000000;Host"
   icraft-run --json "./resnet101_BY.json" --raw "./resnet101_BY.raw" --input "./elephant.jpg" --backends "BY,socket://ql100aiu@192.168.125.148:9981?npu=0x40000000&dma=0x80000000;Host,Cuda"

2.3 多输入
^^^^^^^^^^
input为必填参数

如果输入文件为ftmp，则其格式、大小必须与json中记录的一致

多输入可直接在命令行参数中用分号隔开，也可以统一写入一个文本文件中，用换行符隔开

.. code-block:: bash
   :linenos:

   icraft-run --json "./resnet101_adapted.json" --raw "./resnet101_adapted.raw" --input "./elephant.jpg"
   icraft-run --json "./resnet101_adapted.json" --raw "./resnet101_adapted.raw" --input "./test.ftmp"
   icraft-run --json "./resnet101_adapted.json" --raw "./resnet101_adapted.raw" --input "./test.ftmp;./elephant.jpg"
   icraft-run --json "./resnet101_BY.json" --raw "./resnet101_BY.raw" --input "./img_list.txt"

注意：resnet101仅支持单输入，上面的只是示例


2.4 记录时间
^^^^^^^^^^^^
记录时间功能由各backend负责实现，有问题可向其开发者反馈

目前提供四种时间wall、mem、hard、other


2.5 记录io
^^^^^^^^^^

目前只记录每个算子的所有输入和输出特征图

不记录HardOp这种算子内部隐藏了的特征图


2.6 dump特征图
^^^^^^^^^^^^^^

dump功能由XRT的Tensor提供，有问题可向其开发者反馈

Xrun会在指定的log路径下创建ftmp+dump_format的文件夹，将各特征图保存其中

无论是二进制还是文本文件，后缀名均为ftmp，用户需要根据文件夹名称判断是否可读

.. code-block:: bash
   :linenos:

   icraft-run --json "./resnet101_adapted.json" --raw "./resnet101_adapted.raw" --input "./elephant.jpg" --dump_format SFB
   icraft-run --json "./resnet101_adapted.json" --raw "./resnet101_adapted.raw" --input "./elephant.jpg" --dump_format SFB --dump_ftmp "5,12,75"

注意：指定dump具体特征图时，需要自行保证特征图v_id有效，不存在的v_id不会被dump
