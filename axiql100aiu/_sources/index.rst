========================
Icraft AXIQL100AIUDevice
========================

Icraft XRT是统一前向后的运行时，XRT把不同的计算资源抽象为不同的后端，使得Icraft中的网络能够在任意的计算资源上完成计算。
由于计算需要在具体的设备上进行，因此XRT使用数据结构 ``Device`` 抽象了具体的硬件设备，比如一个板卡。

一个 ``Device`` 中包含若干个 ``RegRegion`` 和 ``MemRegion`` ，其意义分别如下所示：

* ``MemRegion``： 表示设备上的一块独立的内存区域，不同的内存区域的内存访问实现方式可能不相同，比如布衣架构设备上的PLDDR和交换空间的区域。
* ``RegRegion``： 表示设备上的一块独立的寄存器区域，不同的寄存器区域的内存访问实现方式可能不相同。

Icraft中，设备设备采用插件式的方式进行开发，并使用URL进行加载。每个设备都有特定的URL，其规则如下：

``PROTOCOL://DEVICE[@IP:PORT][?key=value&...]``

其中Protocol表示的是设备数据传输的接口或协议，如AXI、PCIE和Socket等；Device表示的是设备的名称，比如7100aiu等；中括号表示可选项，当Protocol为Socket时，IP和PORT分别表示目标设备的地址和端口；而?后的键值对序列则表示传给Device的参数。

一 设备资源
===========

AXIQL100AIUDevice实现了通过AXI总线接口对QL100T开发板上与NPU（布衣架构）相关的资源的管理与控制，具体包含以下资源：

* MemRegion ``udma``:  表示使用一块系统的CMA空间实现的交换空间，用于操作系统和PL DDR之间的数据交换。
* MemRegion ``plddr``: 表示NPU和PL所共同使用的PL DDR内存。
* RegRegion ``npu``:   表示NPU上的寄存器区域。

二 URL和参数
============

AXIQL100AIUDevice的设备URL如下所示：

``axi://ql100aiu[?key=value&...]``

其中?后的键值对序列表示传给Device的参数，该设备支持的参数如下：

* npu: 表示NPU被挂载在GP0的起始地址，默认为0x40000000，即GP0接口的起始地址
* dma: 表示DMA被挂载在GP1的起始地址，默认为0x80000000，即GP1接口的起始地址

以上参数主要是用于做系统集成时，如果不能把NPU和DMA放到GP0和GP1的起始位置，则需要将Device的URL参数设置到其真正的位置，以保证Device能够正确的访问以上模块的寄存器。需要注意的是，URL参数中的npu和dma永远需要指向NPU和DMA的起始位置。比如URL为 ``axi://ql100aiu?npu=0x43c00000&dma=0x83c00000``，则必须保证NPU被挂载到了0x43c00000而且DMA被挂载到了0x83c00000。

.. note:: 

   ``RegRegion`` 的 ``read`` 和 ``write`` 方法有 ``relative`` 参数，其默认为 ``false``。当 ``relative`` 为 ``true`` 时，将以相对地址来访问寄存器。当地址为 ``0x0_XXXX_XXXX`` （X表示16进制占位符）时，访问的是相对URL参数中GP0的地址；当地址为 ``0x1_XXXX_XXXX`` 时，访问的时相对URL参数中GP1的地址。比如， ``regregion.read(0x04, true)`` 访问的是 ``0x40000004`` ， ``regregion.read(0x100000004, true)`` 访问的是 ``0x80000004`` 。

可以通过 ``Device`` 的 ``Open`` 方法来打开设备，获取 ``Device`` 对象：

.. code-block:: cpp
   :linenos:
   :force:
   :caption: C++

   using namespace icraft::xrt;
   
   // 打开设备，URL不带任何参数，默认NPU挂载在0x40000000，DMA挂载在0x80000000
   auto device1 = Device::Open("axi://ql100aiu");

   // 打开设备，URL指定参数，NPU被挂载到了0x43c00000而且DMA被挂载到了0x83c00000
   auto device2 = Device::Open("axi://ql100aiu?npu=0x43c00000&dma=0x83c00000");

.. code-block:: python
   :linenos:
   :force:
   :caption: Python

   from xrt import *
   
   # 打开设备，URL不带任何参数，默认NPU挂载在0x40000000，DMA挂载在0x80000000
   device1 = Device.Open("axi://ql100aiu");

   # 打开设备，URL指定参数，NPU被挂载到了0x43c00000而且DMA被挂载到了0x83c00000
   device2 = Device.Open("axi://ql100aiu?npu=0x43c00000&dma=0x83c00000");

三 复位
=======

``Device`` 提供了 ``reset`` 方法来对设备进行复位，该方法接受一个参数 ``level``，表示复位等级。
不同设备对复位等级有着不同的实现，AXIQL100AIUDevice的复位等级如下：

* 0 : 复位全部状态，包括FPGA, NPU
* 1 : 只复位NPU的计数状态
* 其他： 无效值，不会做任何操作

四 检查
=======

``Device`` 提供了 ``check`` 方法来对设备进行检查，该方法接受一个参数 ``level``，表示检查等级。
不同设备对检查等级有着不同的实现，AXIQL100AIUDevice的检查等级如下：

* 0 : 检查全部功能，包括寄存器读写和DMA读写
* 1 : 只检查寄存器读写
* 2 : 只检查DMA读写
* 其他： 无效值，不会做任何操作， 返回 ``false``

五 状态
=======

``Device`` 提供了 ``showStatus`` 方法来显示设备的状态，该方法接受一个参数 ``level``，表示显示等级。
不同设备对状态显示等级有着不同的实现，AXIQL100AIUDevice的显示等级如下：

* 0 : 显示全部信息，包括状态寄存器、计数寄存器和错误寄存器
* 1 : 只显示状态寄存器
* 2 : 只显示计数寄存器
* 3 : 只显示错误寄存器
* 其他： 无效值，不会做任何操作

六 版本
=======

``Device`` 提供了 ``version`` 方法来显示设备的版本。
AXIQL100AIUDevice的版本信息如下：

* device : 设备的版本，即系统位流的版本，比如 ``101844d2``
* icore  : NPU的版本，即架构设计的版本，比如 ``FMSHBYV1-20200903``

七 其他
=======

AXIQL100AIUDevice派生自 ``icraft::xrt::BuyiDevice`` ，因此除了以上基本的Device接口外，还实现了 ``BuyiDevice`` 中定义的对布衣架构的NPU进行控制的接口。

设备打开后获取到的是基类 ``Device`` 类型的对象，如果要调用 ``BuyiDevice`` 的接口，还需要进行转换：

.. code-block:: cpp
   :linenos:
   :force:
   :caption: C++

   using namespace icraft::xrt;
   
   // 打开设备，URL不带任何参数，默认NPU挂载在0x40000000，DMA挂载在0x80000000
   auto device = Device::Open("axi://ql100aiu");
   // 转为子类 BuyiDevice
   auto buyi_device = device.cast<BuyiDevice>();

   // 调用getRowsCol获取使能的MPE的行数和列数
   auto [rows, cols] = buyi_device.getRowsCols();

.. code-block:: python
   :linenos:
   :force:
   :caption: Python

   from xrt import *
   
   # 打开设备，URL不带任何参数，默认NPU挂载在0x40000000，DMA挂载在0x80000000
   device = Device.Open("axi://ql100aiu");
   # 转为子类 BuyiDevice
   buyi_device = BuyiDevice(device)

   # 调用getRowsCol获取使能的MPE的行数和列数
   rows, cols = buyi_device.getRowsCols()

更多  ``BuyiDevice`` 的接口以及更详细的用法请参考相应的API说明。

三 索引
=======
* :ref:`genindex`