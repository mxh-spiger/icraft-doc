buyi\_codegen.CodeGenOptions
============================

.. currentmodule:: buyi_codegen

.. autoclass:: CodeGenOptions
   :members:
   :show-inheritance:
   :inherited-members:
   :special-members: __call__, __add__, __mul__

   
   
   .. rubric:: Methods

   .. autosummary::
      :nosignatures:
   
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~CodeGenOptions.cols
      ~CodeGenOptions.ddr_base
      ~CodeGenOptions.device_info_config_file 
      ~CodeGenOptions.icropt 
      ~CodeGenOptions.klmopt 
      ~CodeGenOptions.log_path
      ~CodeGenOptions.net_log
      ~CodeGenOptions.ocmopt 
      ~CodeGenOptions.qbits
      ~CodeGenOptions.rows
      ~CodeGenOptions.version
      ~CodeGenOptions.xlmopt 
   
   