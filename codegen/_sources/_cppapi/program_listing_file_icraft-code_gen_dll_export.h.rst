
.. _program_listing_file_icraft-code_gen_dll_export.h:

Program Listing for File dll_export.h
=====================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-code_gen_dll_export.h>` (``icraft-code\gen\dll_export.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   
   #ifdef _WIN32
   #ifdef CG_DLL_EXPORTS
   #define CG_API _declspec(dllexport)
   #else
   #define CG_API _declspec(dllimport)
   #endif
   #else
   #define CG_API
   #endif
