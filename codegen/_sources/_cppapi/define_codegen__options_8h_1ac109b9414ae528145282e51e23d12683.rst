.. _exhale_define_codegen__options_8h_1ac109b9414ae528145282e51e23d12683:

Define INSTR_BYTE_UNIT
======================

- Defined in :ref:`file_icraft-code_gen_codegen_options.h`


Define Documentation
--------------------


.. doxygendefine:: INSTR_BYTE_UNIT
   :project: Icraft CodeGen