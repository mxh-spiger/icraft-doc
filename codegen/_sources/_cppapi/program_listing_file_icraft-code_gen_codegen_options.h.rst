
.. _program_listing_file_icraft-code_gen_codegen_options.h:

Program Listing for File codegen_options.h
==========================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-code_gen_codegen_options.h>` (``icraft-code\gen\codegen_options.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include "dll_export.h"
   #include "icraft-utils/logging.h"
   
   #include <icraft-xir/ops/concat.h>
   #include <icraft-xir/ops/split.h>
   #include <icraft-xir/ops/reshape.h>
   #include <icraft-xir/ops/layernorm.h>
   #include <icraft-xir/ops/softmax.h>
   #include <icraft-xir/ops/normalize.h>
   
   #include <string>
   #include <fstream>
   
   #define DDR_BYTE_UNIT 64
   #define BUYI_ETM_BYTE_UNIT 32
   #define INSTR_BYTE_UNIT 8
   
   
   constexpr std::string_view CONCAT = icraft::xir::Concat::NodeType::type_key;
   constexpr std::string_view SPLIT = icraft::xir::Split::NodeType::type_key;
   constexpr std::string_view RESHAPE = icraft::xir::Reshape::NodeType::type_key;
   constexpr std::string_view LAYERNORM = icraft::xir::Layernorm::NodeType::type_key;
   constexpr std::string_view SOFTMAX = icraft::xir::Softmax::NodeType::type_key;
   constexpr std::string_view NORMALIZE = icraft::xir::Normalize::NodeType::type_key;
   const std::string JSON_SUFFIX = ".json";
   const std::string PARAMS_SUFFIX = ".raw";
   const std::string BIN_SUFFIX = ".imodel";
   const std::string BY = "_BY";
   
   static std::list<std::string_view> no_inst_oplist = {
               CONCAT,
               SPLIT,
               RESHAPE
   };
   
   namespace icraft {
       namespace cg {
           struct CG_API CodeGenOptions {
               CodeGenOptions(const std::string& network_name, const std::filesystem::path& log_dir)
                   : log_path(log_dir.empty() ? (".icraft/logs/" + network_name + "/") : (log_dir / (network_name + "/"))),
                   net_log(FileSink(log_path / (network_name + "_generate.log")))
   
               {}
   
               std::filesystem::path log_path;
               icraft::FileSink net_log;
   
               bool version = false;
   
               uint64_t ddr_base = 64 * DDR_BYTE_UNIT;     // 64个ddr地址
               int qbits = 8;
   
               int rows = 4;
               int cols = 4;
               int xlmopt = 3;
               int klmopt = 3;
               int icropt = 1;
               int ocmopt = -1;
   
               std::string device_info_config_file;
           };
       }
   }
