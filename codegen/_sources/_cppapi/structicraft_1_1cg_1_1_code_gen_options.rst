.. _exhale_struct_structicraft_1_1cg_1_1_code_gen_options:

Struct CodeGenOptions
=====================

- Defined in :ref:`file_icraft-code_gen_codegen_options.h`


Struct Documentation
--------------------


.. doxygenstruct:: icraft::cg::CodeGenOptions
   :project: Icraft CodeGen
   :members:
   :protected-members:
   :undoc-members: