
.. _file_icraft-code_gen_codegen_network.h:

File codegen_network.h
======================

|exhale_lsh| :ref:`Parent directory <dir_icraft-code_gen>` (``icraft-code\gen``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-code\gen\codegen_network.h``)
--------------------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-code_gen_codegen_network.h.rst





Includes
--------


- ``codegen_options.h`` (:ref:`file_icraft-code_gen_codegen_options.h`)

- ``dll_export.h`` (:ref:`file_icraft-code_gen_dll_export.h`)

- ``fstream``

- ``icraft-xir/core/network.h``

- ``iostream``

- ``stdio.h``

- ``stdlib.h``

- ``string``

- ``tchar.h``

- ``vector``






Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__cg`


Functions
---------


- :ref:`exhale_function_codegen__network_8h_1a857721c04b389bb7bcec81c193d1e4e5`

