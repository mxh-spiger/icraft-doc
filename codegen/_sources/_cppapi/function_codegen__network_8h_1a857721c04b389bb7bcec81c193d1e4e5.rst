.. _exhale_function_codegen__network_8h_1a857721c04b389bb7bcec81c193d1e4e5:

Function icraft::cg::MakeNetworkCodeGen
=======================================

- Defined in :ref:`file_icraft-code_gen_codegen_network.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::cg::MakeNetworkCodeGen(icraft::xir::Network, CodeGenOptions&)
   :project: Icraft CodeGen