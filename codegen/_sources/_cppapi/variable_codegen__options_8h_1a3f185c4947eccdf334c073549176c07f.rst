.. _exhale_variable_codegen__options_8h_1a3f185c4947eccdf334c073549176c07f:

Variable CONCAT
===============

- Defined in :ref:`file_icraft-code_gen_codegen_options.h`


Variable Documentation
----------------------


.. doxygenvariable:: CONCAT
   :project: Icraft CodeGen