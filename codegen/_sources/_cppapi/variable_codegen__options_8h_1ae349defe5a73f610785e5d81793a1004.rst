.. _exhale_variable_codegen__options_8h_1ae349defe5a73f610785e5d81793a1004:

Variable RESHAPE
================

- Defined in :ref:`file_icraft-code_gen_codegen_options.h`


Variable Documentation
----------------------


.. doxygenvariable:: RESHAPE
   :project: Icraft CodeGen