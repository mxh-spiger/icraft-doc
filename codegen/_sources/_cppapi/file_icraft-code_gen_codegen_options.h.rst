
.. _file_icraft-code_gen_codegen_options.h:

File codegen_options.h
======================

|exhale_lsh| :ref:`Parent directory <dir_icraft-code_gen>` (``icraft-code\gen``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-code\gen\codegen_options.h``)
--------------------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-code_gen_codegen_options.h.rst





Includes
--------


- ``dll_export.h`` (:ref:`file_icraft-code_gen_dll_export.h`)

- ``fstream``

- ``icraft-utils/logging.h``

- ``icraft-xir/ops/concat.h``

- ``icraft-xir/ops/layernorm.h``

- ``icraft-xir/ops/normalize.h``

- ``icraft-xir/ops/reshape.h``

- ``icraft-xir/ops/softmax.h``

- ``icraft-xir/ops/split.h``

- ``string``



Included By
-----------


- :ref:`file_icraft-code_gen_codegen_network.h`




Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__cg`


Classes
-------


- :ref:`exhale_struct_structicraft_1_1cg_1_1_code_gen_options`


Defines
-------


- :ref:`exhale_define_codegen__options_8h_1abf27f602e2b50b92c4c5ddd6d1225703`

- :ref:`exhale_define_codegen__options_8h_1afff09866f7e501eaed371b52a2d09bd8`

- :ref:`exhale_define_codegen__options_8h_1ac109b9414ae528145282e51e23d12683`


Variables
---------


- :ref:`exhale_variable_codegen__options_8h_1ab0bd9291198e61d1642a841475d14fb0`

- :ref:`exhale_variable_codegen__options_8h_1a819e8b95d2d79be3cff191c37c6f640d`

- :ref:`exhale_variable_codegen__options_8h_1a3f185c4947eccdf334c073549176c07f`

- :ref:`exhale_variable_codegen__options_8h_1aefecb3c2be85b8c46851e8fc8677655d`

- :ref:`exhale_variable_codegen__options_8h_1a49297a6569be798c67c749061a4dc280`

- :ref:`exhale_variable_codegen__options_8h_1a57c7b2987f14c695538e40fd36e299be`

- :ref:`exhale_variable_codegen__options_8h_1a0831676a3301bb5332be3cf6337a251a`

- :ref:`exhale_variable_codegen__options_8h_1a7a3997742b390134bf3a57b4a0d60e8d`

- :ref:`exhale_variable_codegen__options_8h_1ae349defe5a73f610785e5d81793a1004`

- :ref:`exhale_variable_codegen__options_8h_1abcb984dc5d0a0f0d3656c6e318d7a5d8`

- :ref:`exhale_variable_codegen__options_8h_1a696055330a3214772ce99216566117c7`

