.. _dir_icraft-code_gen:


Directory gen
=============


|exhale_lsh| :ref:`Parent directory <dir_icraft-code>` (``icraft-code``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


*Directory path:* ``icraft-code\gen``


Files
-----

- :ref:`file_icraft-code_gen_codegen_network.h`
- :ref:`file_icraft-code_gen_codegen_options.h`
- :ref:`file_icraft-code_gen_dll_export.h`


