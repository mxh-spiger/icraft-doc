.. _exhale_define_codegen__options_8h_1afff09866f7e501eaed371b52a2d09bd8:

Define DDR_BYTE_UNIT
====================

- Defined in :ref:`file_icraft-code_gen_codegen_options.h`


Define Documentation
--------------------


.. doxygendefine:: DDR_BYTE_UNIT
   :project: Icraft CodeGen