.. _exhale_define_dll__export_8h_1ae115b0b180fcfa7267fddf08ddacc791:

Define CG_API
=============

- Defined in :ref:`file_icraft-code_gen_dll_export.h`


Define Documentation
--------------------


.. doxygendefine:: CG_API
   :project: Icraft CodeGen