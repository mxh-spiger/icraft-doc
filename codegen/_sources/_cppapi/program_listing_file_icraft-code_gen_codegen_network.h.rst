
.. _program_listing_file_icraft-code_gen_codegen_network.h:

Program Listing for File codegen_network.h
==========================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-code_gen_codegen_network.h>` (``icraft-code\gen\codegen_network.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <iostream>
   #include <fstream>
   #include <stdio.h>
   #include <tchar.h>
   #include <string>
   #include <vector>
   #include <stdlib.h>
   
   #include "icraft-xir/core/network.h"
   
   #include "codegen_options.h"
   #include "dll_export.h"
   
   
   
   namespace icraft::cg {
       void CG_API MakeNetworkCodeGen(icraft::xir::Network network, CodeGenOptions& options);
   }
