.. _exhale_variable_codegen__options_8h_1a819e8b95d2d79be3cff191c37c6f640d:

Variable BY
===========

- Defined in :ref:`file_icraft-code_gen_codegen_options.h`


Variable Documentation
----------------------


.. doxygenvariable:: BY
   :project: Icraft CodeGen