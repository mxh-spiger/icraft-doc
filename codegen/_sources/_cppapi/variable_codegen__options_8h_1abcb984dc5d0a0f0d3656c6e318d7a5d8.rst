.. _exhale_variable_codegen__options_8h_1abcb984dc5d0a0f0d3656c6e318d7a5d8:

Variable SOFTMAX
================

- Defined in :ref:`file_icraft-code_gen_codegen_options.h`


Variable Documentation
----------------------


.. doxygenvariable:: SOFTMAX
   :project: Icraft CodeGen