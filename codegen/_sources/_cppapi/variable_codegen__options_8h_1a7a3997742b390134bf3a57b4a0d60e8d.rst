.. _exhale_variable_codegen__options_8h_1a7a3997742b390134bf3a57b4a0d60e8d:

Variable PARAMS_SUFFIX
======================

- Defined in :ref:`file_icraft-code_gen_codegen_options.h`


Variable Documentation
----------------------


.. doxygenvariable:: PARAMS_SUFFIX
   :project: Icraft CodeGen