.. _exhale_variable_codegen__options_8h_1a49297a6569be798c67c749061a4dc280:

Variable LAYERNORM
==================

- Defined in :ref:`file_icraft-code_gen_codegen_options.h`


Variable Documentation
----------------------


.. doxygenvariable:: LAYERNORM
   :project: Icraft CodeGen