.. _exhale_variable_codegen__options_8h_1a0831676a3301bb5332be3cf6337a251a:

Variable NORMALIZE
==================

- Defined in :ref:`file_icraft-code_gen_codegen_options.h`


Variable Documentation
----------------------


.. doxygenvariable:: NORMALIZE
   :project: Icraft CodeGen