.. _exhale_variable_codegen__options_8h_1ab0bd9291198e61d1642a841475d14fb0:

Variable BIN_SUFFIX
===================

- Defined in :ref:`file_icraft-code_gen_codegen_options.h`


Variable Documentation
----------------------


.. doxygenvariable:: BIN_SUFFIX
   :project: Icraft CodeGen