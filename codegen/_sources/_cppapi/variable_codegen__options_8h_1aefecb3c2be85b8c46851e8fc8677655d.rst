.. _exhale_variable_codegen__options_8h_1aefecb3c2be85b8c46851e8fc8677655d:

Variable JSON_SUFFIX
====================

- Defined in :ref:`file_icraft-code_gen_codegen_options.h`


Variable Documentation
----------------------


.. doxygenvariable:: JSON_SUFFIX
   :project: Icraft CodeGen