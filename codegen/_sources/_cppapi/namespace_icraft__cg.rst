
.. _namespace_icraft__cg:

Namespace icraft::cg
====================


.. contents:: Contents
   :local:
   :backlinks: none





Classes
-------


- :ref:`exhale_struct_structicraft_1_1cg_1_1_code_gen_options`


Functions
---------


- :ref:`exhale_function_codegen__network_8h_1a857721c04b389bb7bcec81c193d1e4e5`
