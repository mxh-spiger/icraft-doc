.. _exhale_define_codegen__options_8h_1abf27f602e2b50b92c4c5ddd6d1225703:

Define BUYI_ETM_BYTE_UNIT
=========================

- Defined in :ref:`file_icraft-code_gen_codegen_options.h`


Define Documentation
--------------------


.. doxygendefine:: BUYI_ETM_BYTE_UNIT
   :project: Icraft CodeGen