.. _exhale_class_classicraft_1_1xir_1_1_normalized_quantized_type_node:

Class NormalizedQuantizedTypeNode
=================================

- Defined in :ref:`file_icraft-xir_core_data_type.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< NormalizedQuantizedTypeNode, QuantizedTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::NormalizedQuantizedTypeNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: