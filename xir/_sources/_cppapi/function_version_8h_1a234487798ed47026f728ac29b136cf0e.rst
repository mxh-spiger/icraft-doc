.. _exhale_function_version_8h_1a234487798ed47026f728ac29b136cf0e:

Function icraft::xir::IsModified
================================

- Defined in :ref:`file_icraft-xir_base_version.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::IsModified()
   :project: Icraft XIR