.. _exhale_class_classicraft_1_1xir_1_1_s_s_d_output:

Class SSDOutput
===============

- Defined in :ref:`file_icraft-xir_ops_ssd_output.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< SSDOutput, SSDOutputNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::SSDOutput
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: