.. _exhale_enum_resize_8h_1a1ac915a3b32e62d66d8a635bdbeab46f:

Enum ResizeInterpolation
========================

- Defined in :ref:`file_icraft-xir_ops_resize.h`


Enum Documentation
------------------


.. doxygenenum:: icraft::xir::ResizeInterpolation
   :project: Icraft XIR