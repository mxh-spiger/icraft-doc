.. _exhale_class_classicraft_1_1xir_1_1_buyi_target_node:

Class BuyiTargetNode
====================

- Defined in :ref:`file_icraft-xir_core_compile_target.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< BuyiTargetNode, CompileTargetNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::BuyiTargetNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: