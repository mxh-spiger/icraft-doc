.. _exhale_function_string_8h_1ae3cd285a0b3bcf7d5f1b89dba89035d1:

Function icraft::xir::operator+(const char \*, const String&)
=============================================================

- Defined in :ref:`file_icraft-xir_base_string.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::operator+(const char *, const String&)
   :project: Icraft XIR