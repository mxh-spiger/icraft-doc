.. _exhale_define_reflection_8h_1a50e6659d2ac383e62abed02ffe66c6a7:

Define ICRAFT_PASS_REG_VAR_DEF
==============================

- Defined in :ref:`file_icraft-xir_core_reflection.h`


Define Documentation
--------------------


.. doxygendefine:: ICRAFT_PASS_REG_VAR_DEF
   :project: Icraft XIR