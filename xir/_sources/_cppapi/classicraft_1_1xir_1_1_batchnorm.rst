.. _exhale_class_classicraft_1_1xir_1_1_batchnorm:

Class Batchnorm
===============

- Defined in :ref:`file_icraft-xir_ops_batchnorm.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< Batchnorm, BatchnormNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Batchnorm
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: