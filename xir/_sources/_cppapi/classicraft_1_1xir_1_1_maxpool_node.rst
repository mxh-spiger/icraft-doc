.. _exhale_class_classicraft_1_1xir_1_1_maxpool_node:

Class MaxpoolNode
=================

- Defined in :ref:`file_icraft-xir_ops_maxpool.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< MaxpoolNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::MaxpoolNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: