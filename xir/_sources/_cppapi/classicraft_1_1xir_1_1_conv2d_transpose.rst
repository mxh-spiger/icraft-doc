.. _exhale_class_classicraft_1_1xir_1_1_conv2d_transpose:

Class Conv2dTranspose
=====================

- Defined in :ref:`file_icraft-xir_ops_conv2d_transpose.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< Conv2dTranspose, Conv2dTransposeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Conv2dTranspose
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: