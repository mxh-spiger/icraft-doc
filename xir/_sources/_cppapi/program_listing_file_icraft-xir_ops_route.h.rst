
.. _program_listing_file_icraft-xir_ops_route.h:

Program Listing for File route.h
================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_ops_route.h>` (``icraft-xir\ops\route.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/operation.h>
   
   namespace icraft::xir {
       class RouteNode : public OpNodeBase<RouteNode, OneResult> {
       public:
           int64_t groups;         
           int64_t group_id;       
   
           ICRAFT_DECLARE_ATTRS(RouteNode) {
               ICRAFT_ATTR_FIELD(groups).set_default(1).set_lower_bound(1);
               ICRAFT_ATTR_FIELD(group_id).set_default(0).set_lower_bound(0);
           }
   
           XIR_DLL virtual void validate() const override;
       };
   
       class Route : public OpBase<Route, RouteNode> {
       public:
           Route() = default;
   
           XIR_DLL Route(Array<Value> inputs, int64_t groups = 1, int64_t group_id = 0);
       };
   }
