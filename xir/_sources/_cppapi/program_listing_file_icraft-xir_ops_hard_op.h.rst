
.. _program_listing_file_icraft-xir_ops_hard_op.h:

Program Listing for File hard_op.h
==================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_ops_hard_op.h>` (``icraft-xir\ops\hard_op.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/operation.h>
   
   namespace icraft::xir {
       class HardOpNode : public OpNodeBase<HardOpNode> {
       public:
           Params instr;                       
           Params params;                      
           Array<IntImm> origin_ops_id;        
           Map<String, IntImm> device_info;    
   
           ICRAFT_DECLARE_ATTRS(HardOpNode) {
               ICRAFT_ATTR_FIELD(instr);
               ICRAFT_ATTR_FIELD(params);
               ICRAFT_ATTR_FIELD(origin_ops_id);
               ICRAFT_ATTR_FIELD(device_info);
           }
   
           XIR_DLL virtual void validate() const override;
       };
   
       class HardOp : public OpBase<HardOp, HardOpNode> {
       public:
           HardOp() = default;
   
           XIR_DLL HardOp(
               Array<Value> inputs, 
               Params instr, 
               Params params, 
               Array<IntImm> origin_ops_id,
               Map<String, IntImm> device_info
           );
       };
   }
