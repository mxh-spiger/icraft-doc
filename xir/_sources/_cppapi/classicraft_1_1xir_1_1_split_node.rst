.. _exhale_class_classicraft_1_1xir_1_1_split_node:

Class SplitNode
===============

- Defined in :ref:`file_icraft-xir_ops_split.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< SplitNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::SplitNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: