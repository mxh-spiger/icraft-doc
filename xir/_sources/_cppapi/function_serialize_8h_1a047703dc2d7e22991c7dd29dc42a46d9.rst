.. _exhale_function_serialize_8h_1a047703dc2d7e22991c7dd29dc42a46d9:

Function icraft::xir::operator<<(std::ostream&, const ObjectRef&)
=================================================================

- Defined in :ref:`file_icraft-xir_core_serialize.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::operator<<(std::ostream&, const ObjectRef&)
   :project: Icraft XIR