.. _exhale_class_classicraft_1_1xir_1_1_f_p_g_a_target:

Class FPGATarget
================

- Defined in :ref:`file_icraft-xir_core_compile_target.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< FPGATarget, CompileTarget, FPGATargetNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::FPGATarget
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: