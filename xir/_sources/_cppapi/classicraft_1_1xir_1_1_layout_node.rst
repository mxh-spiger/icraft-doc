.. _exhale_class_classicraft_1_1xir_1_1_layout_node:

Class LayoutNode
================

- Defined in :ref:`file_icraft-xir_core_layout.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< LayoutNode, Node >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::LayoutNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: