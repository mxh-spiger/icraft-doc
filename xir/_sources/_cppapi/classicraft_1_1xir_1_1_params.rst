.. _exhale_class_classicraft_1_1xir_1_1_params:

Class Params
============

- Defined in :ref:`file_icraft-xir_core_data.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< Params, Value, ParamsNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Params
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: