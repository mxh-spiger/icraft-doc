.. _exhale_function_string_8h_1ab9706925fd70fabd0427b0dd44ed39a5:

Function icraft::xir::operator>=(const std::string&, const String&)
===================================================================

- Defined in :ref:`file_icraft-xir_base_string.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::operator>=(const std::string&, const String&)
   :project: Icraft XIR