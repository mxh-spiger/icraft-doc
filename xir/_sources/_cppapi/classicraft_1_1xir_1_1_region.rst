.. _exhale_class_classicraft_1_1xir_1_1_region:

Class Region
============

- Defined in :ref:`file_icraft-xir_ops_region.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< Region, RegionNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Region
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: