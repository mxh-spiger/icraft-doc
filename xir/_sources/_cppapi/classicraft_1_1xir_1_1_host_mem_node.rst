.. _exhale_class_classicraft_1_1xir_1_1_host_mem_node:

Class HostMemNode
=================

- Defined in :ref:`file_icraft-xir_core_mem_type.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< HostMemNode, MemTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::HostMemNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: