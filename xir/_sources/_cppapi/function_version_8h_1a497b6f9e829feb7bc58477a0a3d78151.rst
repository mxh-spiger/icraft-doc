.. _exhale_function_version_8h_1a497b6f9e829feb7bc58477a0a3d78151:

Function icraft::xir::CommitID
==============================

- Defined in :ref:`file_icraft-xir_base_version.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::CommitID()
   :project: Icraft XIR