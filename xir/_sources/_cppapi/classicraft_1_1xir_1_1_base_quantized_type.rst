.. _exhale_class_classicraft_1_1xir_1_1_base_quantized_type:

Class BaseQuantizedType
=======================

- Defined in :ref:`file_icraft-xir_core_data_type.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< BaseQuantizedType, ScalarType, BaseQuantizedTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Derived Types
*************

- ``public VirtualBase< CalibratedType, BaseQuantizedType, CalibratedTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< NormalizedType, BaseQuantizedType, NormalizedTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< QuantizedType, BaseQuantizedType, QuantizedTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::BaseQuantizedType
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: