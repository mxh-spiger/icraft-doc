.. _exhale_define_reflection_8h_1a13afd72846c27ef4972a204be15ca40e:

Define ICRAFT_REGISTER_NODE
===========================

- Defined in :ref:`file_icraft-xir_core_reflection.h`


Define Documentation
--------------------


.. doxygendefine:: ICRAFT_REGISTER_NODE
   :project: Icraft XIR