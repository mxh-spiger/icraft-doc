.. _exhale_class_classicraft_1_1xir_1_1_custom_target:

Class CustomTarget
==================

- Defined in :ref:`file_icraft-xir_core_compile_target.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< CustomTarget, CompileTarget, CustomTargetNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::CustomTarget
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: