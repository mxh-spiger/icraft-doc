
.. _file_icraft-xir_ops_prune_axis.h:

File prune_axis.h
=================

|exhale_lsh| :ref:`Parent directory <dir_icraft-xir_ops>` (``icraft-xir\ops``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-xir\ops\prune_axis.h``)
--------------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-xir_ops_prune_axis.h.rst





Includes
--------


- ``icraft-xir/core/operation.h``






Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__xir`


Classes
-------


- :ref:`exhale_class_classicraft_1_1xir_1_1_prune_axis`

- :ref:`exhale_class_classicraft_1_1xir_1_1_prune_axis_node`

