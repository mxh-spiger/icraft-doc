
.. _program_listing_file_icraft-xir_ops_mish.h:

Program Listing for File mish.h
===============================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_ops_mish.h>` (``icraft-xir\ops\mish.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/operation.h>
   
   namespace icraft::xir {
       class MishNode : public OpNodeBase<MishNode, OneResult, IsActivate> {
       public:
           XIR_DLL virtual void validate() const override;
       };
   
       class Mish : public OpBase<Mish, MishNode> {
       public:
           Mish() = default;
   
           XIR_DLL Mish(Value input);
       };
   }
