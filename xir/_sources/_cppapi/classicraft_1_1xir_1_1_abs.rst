.. _exhale_class_classicraft_1_1xir_1_1_abs:

Class Abs
=========

- Defined in :ref:`file_icraft-xir_ops_abs.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< Abs, AbsNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Abs
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: