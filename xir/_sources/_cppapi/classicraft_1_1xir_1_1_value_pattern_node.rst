.. _exhale_class_classicraft_1_1xir_1_1_value_pattern_node:

Class ValuePatternNode
======================

- Defined in :ref:`file_icraft-xir_core_pattern.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< ValuePatternNode, PatternNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::ValuePatternNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: