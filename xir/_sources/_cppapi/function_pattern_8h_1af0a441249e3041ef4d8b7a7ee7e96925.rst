.. _exhale_function_pattern_8h_1af0a441249e3041ef4d8b7a7ee7e96925:

Template Function icraft::xir::operator&&(P, Q)
===============================================

- Defined in :ref:`file_icraft-xir_core_pattern.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::operator&&(P, Q)
   :project: Icraft XIR