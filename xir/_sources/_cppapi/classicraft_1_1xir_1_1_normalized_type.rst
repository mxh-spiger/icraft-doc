.. _exhale_class_classicraft_1_1xir_1_1_normalized_type:

Class NormalizedType
====================

- Defined in :ref:`file_icraft-xir_core_data_type.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< NormalizedType, BaseQuantizedType, NormalizedTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::NormalizedType
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: