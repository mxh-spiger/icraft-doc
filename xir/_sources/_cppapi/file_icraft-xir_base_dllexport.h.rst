
.. _file_icraft-xir_base_dllexport.h:

File dllexport.h
================

|exhale_lsh| :ref:`Parent directory <dir_icraft-xir_base>` (``icraft-xir\base``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-xir\base\dllexport.h``)
--------------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-xir_base_dllexport.h.rst







Included By
-----------


- :ref:`file_icraft-xir_base_object.h`

- :ref:`file_icraft-xir_base_version.h`

- :ref:`file_icraft-xir_core_layout.h`

- :ref:`file_icraft-xir_core_mem_type.h`

- :ref:`file_icraft-xir_core_reflection.h`



