
.. _program_listing_file_icraft-xir_base_version.h:

Program Listing for File version.h
==================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_base_version.h>` (``icraft-xir\base\version.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/base/dllexport.h>
   #include <string>
   
   namespace icraft::xir {
       XIR_DLL std::string_view MainVersion();
   
       XIR_DLL std::string_view FullVersion();
   
       XIR_DLL uint64_t MajorVersionNum();
   
       XIR_DLL uint64_t MinorVersionNum();
   
       XIR_DLL uint64_t PatchVersionNum();
   
       XIR_DLL std::string_view IcraftVersion();
   
       XIR_DLL uint64_t CommitSinceTag();
   
       XIR_DLL std::string_view CommitID();
   
       XIR_DLL bool IsModified();
   
       XIR_DLL std::string_view BuildTime();
   }
