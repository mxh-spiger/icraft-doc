
.. _program_listing_file_icraft-xir_ops_conv2d_transpose.h:

Program Listing for File conv2d_transpose.h
===========================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_ops_conv2d_transpose.h>` (``icraft-xir\ops\conv2d_transpose.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/base/optional.h>
   #include <icraft-xir/core/operation.h>
   
   namespace icraft::xir {
       class Conv2dTransposeNode : public OpNodeBase<Conv2dTransposeNode, OneResult> {
       public:
           int64_t stride_width;           
           int64_t stride_height;          
           int64_t pad_top;                
           int64_t pad_bottom;             
           int64_t pad_left;               
           int64_t pad_right;              
           int64_t dilation_width;         
           int64_t dilation_height;        
           int64_t groups;                 
   
           ICRAFT_DECLARE_ATTRS(Conv2dTransposeNode) {
               ICRAFT_ATTR_FIELD(stride_width).set_default(1).set_lower_bound(1);
               ICRAFT_ATTR_FIELD(stride_height).set_default(1).set_lower_bound(1);
               ICRAFT_ATTR_FIELD(pad_top).set_default(0);
               ICRAFT_ATTR_FIELD(pad_bottom).set_default(0);
               ICRAFT_ATTR_FIELD(pad_left).set_default(0);
               ICRAFT_ATTR_FIELD(pad_right).set_default(0);
               ICRAFT_ATTR_FIELD(dilation_width).set_default(1).set_lower_bound(1);
               ICRAFT_ATTR_FIELD(dilation_height).set_default(1).set_lower_bound(1);
               ICRAFT_ATTR_FIELD(groups).set_default(1).set_lower_bound(1);
           }
   
           XIR_DLL virtual void validate() const override;
       };
   
       class Conv2dTranspose : public OpBase<Conv2dTranspose, Conv2dTransposeNode> {
       public:
           Conv2dTranspose() = default;
   
           XIR_DLL Conv2dTranspose(
               Value input,
               Value weights,
               Optional<Value> bias,
               int64_t stride_width,
               int64_t stride_height,
               int64_t pad_top,
               int64_t pad_bottom,
               int64_t pad_left,
               int64_t pad_right,
               int64_t dilation_width,
               int64_t dilation_height,
               int64_t groups = 1
           );
   
           Conv2dTranspose(
               Value input,
               Value weights,
               Optional<Value> bias,
               int64_t stride,
               int64_t pad,
               int64_t dilation,
               int64_t groups = 1
           ) : Conv2dTranspose(
               input,
               weights,
               bias,
               stride,
               stride,
               pad,
               pad,
               pad,
               pad,
               dilation,
               dilation,
               groups
           ) {}
       };
   }
