.. _exhale_function_string_8h_1ab4a443eed7b8adb78c5aadca1354b49b:

Function icraft::xir::operator>(const String&, const char \*)
=============================================================

- Defined in :ref:`file_icraft-xir_base_string.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::operator>(const String&, const char *)
   :project: Icraft XIR