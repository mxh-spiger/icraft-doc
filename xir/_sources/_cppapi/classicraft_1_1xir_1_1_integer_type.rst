.. _exhale_class_classicraft_1_1xir_1_1_integer_type:

Class IntegerType
=================

- Defined in :ref:`file_icraft-xir_core_data_type.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< IntegerType, ScalarType, IntegerTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::IntegerType
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: