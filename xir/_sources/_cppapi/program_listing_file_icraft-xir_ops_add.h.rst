
.. _program_listing_file_icraft-xir_ops_add.h:

Program Listing for File add.h
==============================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_ops_add.h>` (``icraft-xir\ops\add.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/operation.h>
   
   namespace icraft::xir {
       class AddNode : public OpNodeBase<AddNode, OneResult> {
       public:
           ScalarImm alpha;    
           ScalarImm beta;     
   
           ICRAFT_DECLARE_ATTRS(AddNode) {
               ICRAFT_ATTR_FIELD(alpha).set_default(1.0);
               ICRAFT_ATTR_FIELD(beta).set_default(1.0);
           }
   
           XIR_DLL virtual void validate() const override;
       };
   
       class Add : public OpBase<Add, AddNode> {
       public:
           Add() = default;
   
           XIR_DLL Add(Value lhs, Value rhs, ScalarImm alpha = 1.0, ScalarImm beta = 1.0);
       };
   }
