
.. _program_listing_file_icraft-xir_ops_hardswish.h:

Program Listing for File hardswish.h
====================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_ops_hardswish.h>` (``icraft-xir\ops\hardswish.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/operation.h>
   
   namespace icraft::xir {
       class HardswishNode : public OpNodeBase<HardswishNode, OneResult, IsActivate> {
       public:
           XIR_DLL virtual void validate() const override;
       };
   
       class Hardswish : public OpBase<Hardswish, HardswishNode> {
       public:
           Hardswish() = default;
   
           XIR_DLL Hardswish(Value input);
       };
   }
