.. _exhale_function_string_8h_1ad94401dcf2dfb1476aa89ca9835170b5:

Function icraft::xir::operator<(const String&, const String&)
=============================================================

- Defined in :ref:`file_icraft-xir_base_string.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::operator<(const String&, const String&)
   :project: Icraft XIR