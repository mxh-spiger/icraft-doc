.. _exhale_class_classicraft_1_1xir_1_1_expand_node:

Class ExpandNode
================

- Defined in :ref:`file_icraft-xir_ops_expand.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< ExpandNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::ExpandNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: