.. _exhale_class_classicraft_1_1xir_1_1_swap_order:

Class SwapOrder
===============

- Defined in :ref:`file_icraft-xir_ops_swap_order.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< SwapOrder, SwapOrderNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::SwapOrder
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: