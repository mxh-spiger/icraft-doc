.. _exhale_class_classicraft_1_1xir_1_1_axis_unit:

Class AxisUnit
==============

- Defined in :ref:`file_icraft-xir_core_layout.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< AxisUnit, AxisName, AxisUnitNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::AxisUnit
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: