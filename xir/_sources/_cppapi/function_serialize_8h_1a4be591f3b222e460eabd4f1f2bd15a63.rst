.. _exhale_function_serialize_8h_1a4be591f3b222e460eabd4f1f2bd15a63:

Function icraft::xir::CalcMD5(std::ifstream&)
=============================================

- Defined in :ref:`file_icraft-xir_core_serialize.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::CalcMD5(std::ifstream&)
   :project: Icraft XIR