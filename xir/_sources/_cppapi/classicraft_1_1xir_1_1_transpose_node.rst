.. _exhale_class_classicraft_1_1xir_1_1_transpose_node:

Class TransposeNode
===================

- Defined in :ref:`file_icraft-xir_ops_transpose.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< TransposeNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::TransposeNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: