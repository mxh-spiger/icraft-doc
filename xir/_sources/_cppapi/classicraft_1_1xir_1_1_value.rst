.. _exhale_class_classicraft_1_1xir_1_1_value:

Class Value
===========

- Defined in :ref:`file_icraft-xir_core_data.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< Value, Data, ValueNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Derived Type
************

- ``public VirtualBase< Params, Value, ParamsNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Value
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: