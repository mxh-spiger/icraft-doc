.. _exhale_class_classicraft_1_1xir_1_1_f_p_g_a_target_node:

Class FPGATargetNode
====================

- Defined in :ref:`file_icraft-xir_core_compile_target.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< FPGATargetNode, CompileTargetNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::FPGATargetNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: