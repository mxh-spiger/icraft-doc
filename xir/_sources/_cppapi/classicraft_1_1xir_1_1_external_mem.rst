.. _exhale_class_classicraft_1_1xir_1_1_external_mem:

Class ExternalMem
=================

- Defined in :ref:`file_icraft-xir_core_mem_type.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< ExternalMem, MemType, ExternalMemNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::ExternalMem
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: