.. _exhale_class_classicraft_1_1xir_1_1_string_node:

Class StringNode
================

- Defined in :ref:`file_icraft-xir_base_string.h`


Nested Relationships
--------------------


Nested Types
************

- :ref:`exhale_class_classicraft_1_1xir_1_1_string_node_1_1_from_std`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public Object`` (:ref:`exhale_class_classicraft_1_1xir_1_1_object`)


Derived Type
************

- ``public StringNode::FromStd`` (:ref:`exhale_class_classicraft_1_1xir_1_1_string_node_1_1_from_std`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::StringNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: