.. _exhale_function_data_8h_1a3d2186da2c5fe38cd4cde2ed9fabae42:

Function icraft::xir::operator||(const Bool&, const Bool&)
==========================================================

- Defined in :ref:`file_icraft-xir_core_data.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::operator||(const Bool&, const Bool&)
   :project: Icraft XIR