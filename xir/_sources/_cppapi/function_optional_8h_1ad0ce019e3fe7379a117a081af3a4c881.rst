.. _exhale_function_optional_8h_1ad0ce019e3fe7379a117a081af3a4c881:

Template Function icraft::xir::is_optional(T const&)
====================================================

- Defined in :ref:`file_icraft-xir_base_optional.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::is_optional(T const&)
   :project: Icraft XIR