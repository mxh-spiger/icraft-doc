.. _exhale_class_classicraft_1_1xir_1_1_prelu:

Class Prelu
===========

- Defined in :ref:`file_icraft-xir_ops_prelu.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< Prelu, PreluNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Prelu
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: