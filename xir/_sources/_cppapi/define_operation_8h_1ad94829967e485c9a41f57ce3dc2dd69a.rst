.. _exhale_define_operation_8h_1ad94829967e485c9a41f57ce3dc2dd69a:

Define ICRAFT_REGISTER_OP_NODE
==============================

- Defined in :ref:`file_icraft-xir_core_operation.h`


Define Documentation
--------------------


.. doxygendefine:: ICRAFT_REGISTER_OP_NODE
   :project: Icraft XIR