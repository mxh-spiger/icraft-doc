.. _exhale_function_string_8h_1a522339d5ba8ef44cfee16c46aa6911e0:

Function icraft::xir::operator>(const String&, const std::string&)
==================================================================

- Defined in :ref:`file_icraft-xir_base_string.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::operator>(const String&, const std::string&)
   :project: Icraft XIR