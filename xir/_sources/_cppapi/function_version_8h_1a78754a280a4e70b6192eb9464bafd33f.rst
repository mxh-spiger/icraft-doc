.. _exhale_function_version_8h_1a78754a280a4e70b6192eb9464bafd33f:

Function icraft::xir::FullVersion
=================================

- Defined in :ref:`file_icraft-xir_base_version.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::FullVersion()
   :project: Icraft XIR