.. _exhale_class_classicraft_1_1xir_1_1_pass_info:

Class PassInfo
==============

- Defined in :ref:`file_icraft-xir_core_pass.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< PassInfo, Handle, PassInfoNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::PassInfo
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: