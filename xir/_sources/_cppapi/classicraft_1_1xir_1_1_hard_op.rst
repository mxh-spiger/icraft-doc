.. _exhale_class_classicraft_1_1xir_1_1_hard_op:

Class HardOp
============

- Defined in :ref:`file_icraft-xir_ops_hard_op.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< HardOp, HardOpNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::HardOp
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: