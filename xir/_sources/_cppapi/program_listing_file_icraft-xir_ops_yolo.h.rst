
.. _program_listing_file_icraft-xir_ops_yolo.h:

Program Listing for File yolo.h
===============================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_ops_yolo.h>` (``icraft-xir\ops\yolo.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/operation.h>
   
   namespace icraft::xir {
       class YoloNode : public OpNodeBase<YoloNode, OneResult> {
       public:
           Array<FloatImm> anchors;        
           int64_t class_num;              
           int64_t num;                    
           Array<IntImm> mask;             
           int64_t max;                    
           double scale_x_y;               
           double confidence_thresh;       
           double iou_thresh;              
           double beta_nms;                
           std::string nms_kind;           
   
           ICRAFT_DECLARE_ATTRS(YoloNode) {
               ICRAFT_ATTR_FIELD(anchors);
               ICRAFT_ATTR_FIELD(class_num).set_lower_bound(0);
               ICRAFT_ATTR_FIELD(num).set_lower_bound(0);
               ICRAFT_ATTR_FIELD(mask);
               ICRAFT_ATTR_FIELD(max).set_lower_bound(0);
               ICRAFT_ATTR_FIELD(scale_x_y);
               ICRAFT_ATTR_FIELD(confidence_thresh).set_lower_bound(0);
               ICRAFT_ATTR_FIELD(iou_thresh).set_lower_bound(0);
               ICRAFT_ATTR_FIELD(beta_nms);
               ICRAFT_ATTR_FIELD(nms_kind);
           }
   
           XIR_DLL virtual void validate() const override;
       };
   
       class Yolo : public OpBase<Yolo, YoloNode> {
       public:
           Yolo() = default;
   
           XIR_DLL Yolo(
               Value input,
               Array<FloatImm> anchors,
               int64_t class_num,
               int64_t num,
               Array<IntImm> mask,
               int64_t max,
               double scale_x_y,
               double confidence_thresh,
               double iou_thresh,
               double beta_nms,
               std::string nms_kind
           );
       };
   }
