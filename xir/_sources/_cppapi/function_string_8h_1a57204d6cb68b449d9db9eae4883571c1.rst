.. _exhale_function_string_8h_1a57204d6cb68b449d9db9eae4883571c1:

Function icraft::xir::operator==(const String&, const String&)
==============================================================

- Defined in :ref:`file_icraft-xir_base_string.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::operator==(const String&, const String&)
   :project: Icraft XIR