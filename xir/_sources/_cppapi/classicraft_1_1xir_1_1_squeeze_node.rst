.. _exhale_class_classicraft_1_1xir_1_1_squeeze_node:

Class SqueezeNode
=================

- Defined in :ref:`file_icraft-xir_ops_squeeze.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< SqueezeNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::SqueezeNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: