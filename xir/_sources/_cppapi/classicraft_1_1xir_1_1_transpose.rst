.. _exhale_class_classicraft_1_1xir_1_1_transpose:

Class Transpose
===============

- Defined in :ref:`file_icraft-xir_ops_transpose.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< Transpose, TransposeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Transpose
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: