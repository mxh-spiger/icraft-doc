.. _exhale_function_pattern_8h_1a0ae8a90d05e570283e126b308826ff7f:

Template Function icraft::xir::IsOp
===================================

- Defined in :ref:`file_icraft-xir_core_pattern.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::IsOp(Args...)
   :project: Icraft XIR