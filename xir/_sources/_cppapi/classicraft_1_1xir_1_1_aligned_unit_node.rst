.. _exhale_class_classicraft_1_1xir_1_1_aligned_unit_node:

Class AlignedUnitNode
=====================

- Defined in :ref:`file_icraft-xir_ops_align_axis.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< AlignedUnitNode, Node >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::AlignedUnitNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: