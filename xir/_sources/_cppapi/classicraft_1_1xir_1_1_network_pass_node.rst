.. _exhale_class_classicraft_1_1xir_1_1_network_pass_node:

Class NetworkPassNode
=====================

- Defined in :ref:`file_icraft-xir_core_pass.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< NetworkPassNode, PassNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::NetworkPassNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: