
.. _file_icraft-xir_core_pass.h:

File pass.h
===========

|exhale_lsh| :ref:`Parent directory <dir_icraft-xir_core>` (``icraft-xir\core``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-xir\core\pass.h``)
---------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-xir_core_pass.h.rst





Includes
--------


- ``icraft-xir/base/array.h``

- ``icraft-xir/base/map.h``

- ``icraft-xir/base/string.h``

- ``icraft-xir/core/node.h``

- ``icraft-xir/core/reflection.h``






Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__xir`


Classes
-------


- :ref:`exhale_class_classicraft_1_1xir_1_1_network_pass`

- :ref:`exhale_class_classicraft_1_1xir_1_1_network_pass_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_pass`

- :ref:`exhale_class_classicraft_1_1xir_1_1_pass_context`

- :ref:`exhale_class_classicraft_1_1xir_1_1_pass_context_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_pass_info`

- :ref:`exhale_class_classicraft_1_1xir_1_1_pass_info_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_pass_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_sequential_pass`

- :ref:`exhale_class_classicraft_1_1xir_1_1_sequential_pass_node`

