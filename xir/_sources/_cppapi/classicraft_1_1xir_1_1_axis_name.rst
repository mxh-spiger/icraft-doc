.. _exhale_class_classicraft_1_1xir_1_1_axis_name:

Class AxisName
==============

- Defined in :ref:`file_icraft-xir_core_layout.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< AxisName, Handle, AxisNameNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Derived Type
************

- ``public VirtualBase< AxisUnit, AxisName, AxisUnitNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::AxisName
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: