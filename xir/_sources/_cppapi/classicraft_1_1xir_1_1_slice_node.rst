.. _exhale_class_classicraft_1_1xir_1_1_slice_node:

Class SliceNode
===============

- Defined in :ref:`file_icraft-xir_ops_slice.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< SliceNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::SliceNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: