.. _exhale_class_classicraft_1_1xir_1_1_expand:

Class Expand
============

- Defined in :ref:`file_icraft-xir_ops_expand.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< Expand, ExpandNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Expand
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: