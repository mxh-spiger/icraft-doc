.. _exhale_class_classicraft_1_1xir_1_1_scalar_type_node:

Class ScalarTypeNode
====================

- Defined in :ref:`file_icraft-xir_core_data_type.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public DataTypeNode`` (:ref:`exhale_class_classicraft_1_1xir_1_1_data_type_node`)


Derived Types
*************

- ``public NodeBase< BaseQuantizedTypeNode, ScalarTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)
- ``public NodeBase< FloatTypeNode, ScalarTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)
- ``public NodeBase< IntegerTypeNode, ScalarTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::ScalarTypeNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: