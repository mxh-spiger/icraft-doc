.. _exhale_function_string_8h_1adce2f798f94879309cef872d5c0cf70c:

Function icraft::xir::operator>=(const String&, const char \*)
==============================================================

- Defined in :ref:`file_icraft-xir_base_string.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::operator>=(const String&, const char *)
   :project: Icraft XIR