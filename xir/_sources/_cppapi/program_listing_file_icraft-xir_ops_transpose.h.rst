
.. _program_listing_file_icraft-xir_ops_transpose.h:

Program Listing for File transpose.h
====================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_ops_transpose.h>` (``icraft-xir\ops\transpose.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/operation.h>
   
   namespace icraft::xir {
       class TransposeNode : public OpNodeBase<TransposeNode, OneResult> {
       public:
           Array<IntImm> dims;     
           Layout layout;          
   
           ICRAFT_DECLARE_ATTRS(TransposeNode) {
               ICRAFT_ATTR_FIELD(dims);
               ICRAFT_ATTR_FIELD(layout);
           }
   
           XIR_DLL virtual void validate() const override;
       };
   
       class Transpose : public OpBase<Transpose, TransposeNode> {
       public:
           Transpose() = default;
   
           XIR_DLL Transpose(Value input, Array<IntImm> dims, Layout layout);
   
           XIR_DLL Transpose(Value input, Array<IntImm> dims);
       };
   }
