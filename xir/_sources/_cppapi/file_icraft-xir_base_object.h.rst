
.. _file_icraft-xir_base_object.h:

File object.h
=============

|exhale_lsh| :ref:`Parent directory <dir_icraft-xir_base>` (``icraft-xir\base``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-xir\base\object.h``)
-----------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-xir_base_object.h.rst





Includes
--------


- ``atomic``

- ``icraft-utils/logging.h``

- ``icraft-xir/base/dllexport.h``

- ``type_traits``



Included By
-----------


- :ref:`file_icraft-xir_base_any.h`

- :ref:`file_icraft-xir_base_array.h`

- :ref:`file_icraft-xir_base_optional.h`

- :ref:`file_icraft-xir_base_string.h`

- :ref:`file_icraft-xir_core_node.h`




Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__xir`


Classes
-------


- :ref:`exhale_class_classicraft_1_1xir_1_1_object`

- :ref:`exhale_class_classicraft_1_1xir_1_1_object_ptr`

- :ref:`exhale_class_classicraft_1_1xir_1_1_object_ref`


Functions
---------


- :ref:`exhale_function_object_8h_1a68c32a41efd1aa99afca70837565cced`

- :ref:`exhale_function_object_8h_1a6bb897ef1c79fef652856160d3d825a4`

- :ref:`exhale_function_object_8h_1ae36a387dcf54c696cda7318084dd0d55`

- :ref:`exhale_function_object_8h_1ac798b7092b8c4c6665631be6db1d4327`

- :ref:`exhale_function_object_8h_1a79b91b13ca6d5d54352daa3a7de8de1f`

