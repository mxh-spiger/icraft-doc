.. _exhale_class_classicraft_1_1xir_1_1_upsample:

Class Upsample
==============

- Defined in :ref:`file_icraft-xir_ops_upsample.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< Upsample, UpsampleNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Upsample
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: