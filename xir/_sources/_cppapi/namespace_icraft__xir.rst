
.. _namespace_icraft__xir:

Namespace icraft::xir
=====================


.. contents:: Contents
   :local:
   :backlinks: none





Classes
-------


- :ref:`exhale_struct_structicraft_1_1xir_1_1_null_opt_type`

- :ref:`exhale_class_classicraft_1_1xir_1_1_abs`

- :ref:`exhale_class_classicraft_1_1xir_1_1_abs_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_add`

- :ref:`exhale_class_classicraft_1_1xir_1_1_add_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_align_axis`

- :ref:`exhale_class_classicraft_1_1xir_1_1_align_axis_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_aligned_unit`

- :ref:`exhale_class_classicraft_1_1xir_1_1_aligned_unit_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_and_pattern`

- :ref:`exhale_class_classicraft_1_1xir_1_1_and_pattern_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_any`

- :ref:`exhale_class_classicraft_1_1xir_1_1_anycards_pattern`

- :ref:`exhale_class_classicraft_1_1xir_1_1_anycards_pattern_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_any_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_array`

- :ref:`exhale_class_classicraft_1_1xir_1_1_array_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_attr_field_info`

- :ref:`exhale_class_classicraft_1_1xir_1_1_attr_field_info_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_attr_pattern`

- :ref:`exhale_class_classicraft_1_1xir_1_1_attr_pattern_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_attr_visitor`

- :ref:`exhale_class_classicraft_1_1xir_1_1_avgpool`

- :ref:`exhale_class_classicraft_1_1xir_1_1_avgpool_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_axis_name`

- :ref:`exhale_class_classicraft_1_1xir_1_1_axis_name_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_axis_unit`

- :ref:`exhale_class_classicraft_1_1xir_1_1_axis_unit_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_base_quantized_type`

- :ref:`exhale_class_classicraft_1_1xir_1_1_base_quantized_type_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_batchnorm`

- :ref:`exhale_class_classicraft_1_1xir_1_1_batchnorm_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_bool`

- :ref:`exhale_class_classicraft_1_1xir_1_1_buyi_target`

- :ref:`exhale_class_classicraft_1_1xir_1_1_buyi_target_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_calibrated_type`

- :ref:`exhale_class_classicraft_1_1xir_1_1_calibrated_type_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_cast`

- :ref:`exhale_class_classicraft_1_1xir_1_1_cast_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_compile_target`

- :ref:`exhale_class_classicraft_1_1xir_1_1_compile_target_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_concat`

- :ref:`exhale_class_classicraft_1_1xir_1_1_concat_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_conv2d`

- :ref:`exhale_class_classicraft_1_1xir_1_1_conv2d_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_conv2d_transpose`

- :ref:`exhale_class_classicraft_1_1xir_1_1_conv2d_transpose_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_copy`

- :ref:`exhale_class_classicraft_1_1xir_1_1_copy_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_custom_target`

- :ref:`exhale_class_classicraft_1_1xir_1_1_custom_target_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_data`

- :ref:`exhale_class_classicraft_1_1xir_1_1_data_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_data_type`

- :ref:`exhale_class_classicraft_1_1xir_1_1_data_type_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_divide_scalar`

- :ref:`exhale_class_classicraft_1_1xir_1_1_divide_scalar_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_elu`

- :ref:`exhale_class_classicraft_1_1xir_1_1_elu_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_expand`

- :ref:`exhale_class_classicraft_1_1xir_1_1_expand_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_exp_quantized_scale`

- :ref:`exhale_class_classicraft_1_1xir_1_1_exp_quantized_scale_array`

- :ref:`exhale_class_classicraft_1_1xir_1_1_exp_quantized_scale_array_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_exp_quantized_scale_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_external_mem`

- :ref:`exhale_class_classicraft_1_1xir_1_1_external_mem_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_float_imm`

- :ref:`exhale_class_classicraft_1_1xir_1_1_float_imm_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_float_type`

- :ref:`exhale_class_classicraft_1_1xir_1_1_float_type_1_1_semantics`

- :ref:`exhale_class_classicraft_1_1xir_1_1_float_type_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_f_p_g_a_target`

- :ref:`exhale_class_classicraft_1_1xir_1_1_f_p_g_a_target_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_gelu`

- :ref:`exhale_class_classicraft_1_1xir_1_1_gelu_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_get_attr_nums`

- :ref:`exhale_class_classicraft_1_1xir_1_1_handle`

- :ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`

- :ref:`exhale_class_classicraft_1_1xir_1_1_hard_op`

- :ref:`exhale_class_classicraft_1_1xir_1_1_hard_op_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_hardsigmoid`

- :ref:`exhale_class_classicraft_1_1xir_1_1_hardsigmoid_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_hardswish`

- :ref:`exhale_class_classicraft_1_1xir_1_1_hardswish_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_host_mem`

- :ref:`exhale_class_classicraft_1_1xir_1_1_host_mem_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_host_target`

- :ref:`exhale_class_classicraft_1_1xir_1_1_host_target_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_input`

- :ref:`exhale_class_classicraft_1_1xir_1_1_input_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_integer_type`

- :ref:`exhale_class_classicraft_1_1xir_1_1_integer_type_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_int_imm`

- :ref:`exhale_class_classicraft_1_1xir_1_1_int_imm_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_is_activate`

- :ref:`exhale_class_classicraft_1_1xir_1_1_json_parser`

- :ref:`exhale_class_classicraft_1_1xir_1_1_json_printer`

- :ref:`exhale_class_classicraft_1_1xir_1_1_layernorm`

- :ref:`exhale_class_classicraft_1_1xir_1_1_layernorm_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_layout`

- :ref:`exhale_class_classicraft_1_1xir_1_1_layout_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_map`

- :ref:`exhale_class_classicraft_1_1xir_1_1_map_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_matmul`

- :ref:`exhale_class_classicraft_1_1xir_1_1_matmul_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_maxpool`

- :ref:`exhale_class_classicraft_1_1xir_1_1_maxpool_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_mem_type`

- :ref:`exhale_class_classicraft_1_1xir_1_1_mem_type_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_merged_axis_distr`

- :ref:`exhale_class_classicraft_1_1xir_1_1_merged_axis_distr_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_mish`

- :ref:`exhale_class_classicraft_1_1xir_1_1_mish_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_m_s_g_dumper`

- :ref:`exhale_class_classicraft_1_1xir_1_1_m_s_g_parser`

- :ref:`exhale_class_classicraft_1_1xir_1_1_multiply`

- :ref:`exhale_class_classicraft_1_1xir_1_1_multiply_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_multi_yolo`

- :ref:`exhale_class_classicraft_1_1xir_1_1_multi_yolo_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_network`

- :ref:`exhale_class_classicraft_1_1xir_1_1_network_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_network_pass`

- :ref:`exhale_class_classicraft_1_1xir_1_1_network_pass_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_network_rewriter`

- :ref:`exhale_class_classicraft_1_1xir_1_1_network_view`

- :ref:`exhale_class_classicraft_1_1xir_1_1_network_view_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_node_base`

- :ref:`exhale_class_classicraft_1_1xir_1_1_normalize`

- :ref:`exhale_class_classicraft_1_1xir_1_1_normalized_quantized_type`

- :ref:`exhale_class_classicraft_1_1xir_1_1_normalized_quantized_type_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_normalized_type`

- :ref:`exhale_class_classicraft_1_1xir_1_1_normalized_type_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_normalize_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_object`

- :ref:`exhale_class_classicraft_1_1xir_1_1_object_ptr`

- :ref:`exhale_class_classicraft_1_1xir_1_1_object_ref`

- :ref:`exhale_class_classicraft_1_1xir_1_1_on_chip_mem`

- :ref:`exhale_class_classicraft_1_1xir_1_1_on_chip_mem_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_one_result`

- :ref:`exhale_class_classicraft_1_1xir_1_1_op_base`

- :ref:`exhale_class_classicraft_1_1xir_1_1_operation`

- :ref:`exhale_class_classicraft_1_1xir_1_1_operation_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`

- :ref:`exhale_class_classicraft_1_1xir_1_1_op_pattern`

- :ref:`exhale_class_classicraft_1_1xir_1_1_op_pattern_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_optional`

- :ref:`exhale_class_classicraft_1_1xir_1_1_op_trait`

- :ref:`exhale_class_classicraft_1_1xir_1_1_or_pattern`

- :ref:`exhale_class_classicraft_1_1xir_1_1_or_pattern_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_output`

- :ref:`exhale_class_classicraft_1_1xir_1_1_output_like`

- :ref:`exhale_class_classicraft_1_1xir_1_1_output_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_overide_clone`

- :ref:`exhale_class_classicraft_1_1xir_1_1_overide_equal`

- :ref:`exhale_class_classicraft_1_1xir_1_1_pad`

- :ref:`exhale_class_classicraft_1_1xir_1_1_pad_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_params`

- :ref:`exhale_class_classicraft_1_1xir_1_1_params_dumper`

- :ref:`exhale_class_classicraft_1_1xir_1_1_params_lazy_loader`

- :ref:`exhale_class_classicraft_1_1xir_1_1_params_loader`

- :ref:`exhale_class_classicraft_1_1xir_1_1_params_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_pass`

- :ref:`exhale_class_classicraft_1_1xir_1_1_pass_context`

- :ref:`exhale_class_classicraft_1_1xir_1_1_pass_context_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_pass_info`

- :ref:`exhale_class_classicraft_1_1xir_1_1_pass_info_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_pass_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_pass_registry`

- :ref:`exhale_class_classicraft_1_1xir_1_1_pattern`

- :ref:`exhale_class_classicraft_1_1xir_1_1_pattern_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_pixel_shuffle`

- :ref:`exhale_class_classicraft_1_1xir_1_1_pixel_shuffle_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_prelu`

- :ref:`exhale_class_classicraft_1_1xir_1_1_prelu_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_prior_box`

- :ref:`exhale_class_classicraft_1_1xir_1_1_prior_box_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_prune_axis`

- :ref:`exhale_class_classicraft_1_1xir_1_1_prune_axis_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_quantized_scale`

- :ref:`exhale_class_classicraft_1_1xir_1_1_quantized_scale_array`

- :ref:`exhale_class_classicraft_1_1xir_1_1_quantized_scale_array_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_quantized_scale_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_quantized_type`

- :ref:`exhale_class_classicraft_1_1xir_1_1_quantized_type_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_reduce_visitor`

- :ref:`exhale_class_classicraft_1_1xir_1_1_reflection_table`

- :ref:`exhale_class_classicraft_1_1xir_1_1_reflection_table_1_1_registry`

- :ref:`exhale_class_classicraft_1_1xir_1_1_region`

- :ref:`exhale_class_classicraft_1_1xir_1_1_region_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_relu`

- :ref:`exhale_class_classicraft_1_1xir_1_1_relu_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_reorg`

- :ref:`exhale_class_classicraft_1_1xir_1_1_reorg_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_reshape`

- :ref:`exhale_class_classicraft_1_1xir_1_1_reshape_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_resize`

- :ref:`exhale_class_classicraft_1_1xir_1_1_resize_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_route`

- :ref:`exhale_class_classicraft_1_1xir_1_1_route_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_scalar_imm`

- :ref:`exhale_class_classicraft_1_1xir_1_1_scalar_imm_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_scalar_type`

- :ref:`exhale_class_classicraft_1_1xir_1_1_scalar_type_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_sequential_pass`

- :ref:`exhale_class_classicraft_1_1xir_1_1_sequential_pass_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_sigmoid`

- :ref:`exhale_class_classicraft_1_1xir_1_1_sigmoid_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_silu`

- :ref:`exhale_class_classicraft_1_1xir_1_1_silu_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_slice`

- :ref:`exhale_class_classicraft_1_1xir_1_1_slice_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_softmax`

- :ref:`exhale_class_classicraft_1_1xir_1_1_softmax_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_split`

- :ref:`exhale_class_classicraft_1_1xir_1_1_split_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_squeeze`

- :ref:`exhale_class_classicraft_1_1xir_1_1_squeeze_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_s_s_d_output`

- :ref:`exhale_class_classicraft_1_1xir_1_1_s_s_d_output_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_string`

- :ref:`exhale_class_classicraft_1_1xir_1_1_string_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_string_node_1_1_from_std`

- :ref:`exhale_class_classicraft_1_1xir_1_1_swap_order`

- :ref:`exhale_class_classicraft_1_1xir_1_1_swap_order_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_tanh`

- :ref:`exhale_class_classicraft_1_1xir_1_1_tanh_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_tensor_type`

- :ref:`exhale_class_classicraft_1_1xir_1_1_tensor_type_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_transpose`

- :ref:`exhale_class_classicraft_1_1xir_1_1_transpose_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_upsample`

- :ref:`exhale_class_classicraft_1_1xir_1_1_upsample_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_value`

- :ref:`exhale_class_classicraft_1_1xir_1_1_value_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_value_pattern`

- :ref:`exhale_class_classicraft_1_1xir_1_1_value_pattern_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`

- :ref:`exhale_class_classicraft_1_1xir_1_1_yolo`

- :ref:`exhale_class_classicraft_1_1xir_1_1_yolo_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_zhuge_target`

- :ref:`exhale_class_classicraft_1_1xir_1_1_zhuge_target_node`


Enums
-----


- :ref:`exhale_enum_network_8h_1a52f12ca1331504c9536b043a7d4d2ed7`

- :ref:`exhale_enum_conv2d_8h_1a14d24d90ab4ba2956e92e27890ba4c91`

- :ref:`exhale_enum_resize_8h_1a1ac915a3b32e62d66d8a635bdbeab46f`


Functions
---------


- :ref:`exhale_function_pattern_8h_1a93542529bb000fda0aaf54b90534a76c`

- :ref:`exhale_function_version_8h_1a6abe8cca6a8061bdc0f366bcaeb282fe`

- :ref:`exhale_function_serialize_8h_1a4be591f3b222e460eabd4f1f2bd15a63`

- :ref:`exhale_function_serialize_8h_1a0185d16d44e07b98bc5b409791036be6`

- :ref:`exhale_function_version_8h_1a497b6f9e829feb7bc58477a0a3d78151`

- :ref:`exhale_function_version_8h_1a96c8adafd6a10fbafa925be8d8b51716`

- :ref:`exhale_function_serialize_8h_1a5ebb3b0b58b3fb3e792c71e49b3fbaa7`

- :ref:`exhale_function_object_8h_1a68c32a41efd1aa99afca70837565cced`

- :ref:`exhale_function_version_8h_1a78754a280a4e70b6192eb9464bafd33f`

- :ref:`exhale_function_object_8h_1a6bb897ef1c79fef652856160d3d825a4`

- :ref:`exhale_function_object_8h_1ae36a387dcf54c696cda7318084dd0d55`

- :ref:`exhale_function_object_8h_1ac798b7092b8c4c6665631be6db1d4327`

- :ref:`exhale_function_reflection_8h_1a55e9abf1358f73936d0c260df59e3a48`

- :ref:`exhale_function_reflection_8h_1a3f420755305a0d0bbc43dd197083aafd`

- :ref:`exhale_function_version_8h_1a70608457f1404249247900dd3a5638df`

- :ref:`exhale_function_optional_8h_1ad0ce019e3fe7379a117a081af3a4c881`

- :ref:`exhale_function_optional_8h_1a9daee3d26c5a475c024034ccf4134954`

- :ref:`exhale_function_optional_8h_1a6fc9e3d36fec62302706492a13e206ac`

- :ref:`exhale_function_version_8h_1a234487798ed47026f728ac29b136cf0e`

- :ref:`exhale_function_pattern_8h_1a0ae8a90d05e570283e126b308826ff7f`

- :ref:`exhale_function_version_8h_1aec81871e7ddcd07a5b76e701f54944f2`

- :ref:`exhale_function_version_8h_1a54db24f732df50a1b1ecde8e3eae4431`

- :ref:`exhale_function_object_8h_1a79b91b13ca6d5d54352daa3a7de8de1f`

- :ref:`exhale_function_version_8h_1a70a34ef712f6bee084e34fa3136a2241`

- :ref:`exhale_function_string_8h_1a29475156e730e27c5e1cba18c5177ef6`

- :ref:`exhale_function_string_8h_1af6dfa63e8765ba8f0c05a7d40fa1c584`

- :ref:`exhale_function_string_8h_1ac4e429cee0b2f43d16eca3b016f5185c`

- :ref:`exhale_function_string_8h_1a1cadc42fadabb1c9aaa455d8f857a6db`

- :ref:`exhale_function_string_8h_1acab3e9f8bdf2c08f6cf2fc285558c1ce`

- :ref:`exhale_function_pattern_8h_1af0a441249e3041ef4d8b7a7ee7e96925`

- :ref:`exhale_function_data_8h_1af1ce218b03c384d2aad50a6295fb096d`

- :ref:`exhale_function_data_8h_1a9c0be3c7885099d93fa52204c5e3ff29`

- :ref:`exhale_function_data_8h_1a13f31d0631d8054ec6cbe16e5aa5c246`

- :ref:`exhale_function_string_8h_1a346acdf3143412eaab63e8f3664cd575`

- :ref:`exhale_function_string_8h_1af71ae061ba27c966204712cf13558437`

- :ref:`exhale_function_string_8h_1a4ed702c701164539de5ce6b332c45d07`

- :ref:`exhale_function_string_8h_1ae3cd285a0b3bcf7d5f1b89dba89035d1`

- :ref:`exhale_function_string_8h_1a90015386d12dcad1a1150e0e3c6a0d8a`

- :ref:`exhale_function_string_8h_1aae232265e2908fc6e59999597d6441aa`

- :ref:`exhale_function_string_8h_1abb89216adbfc3f69204c0a4ecbe67458`

- :ref:`exhale_function_string_8h_1ad94401dcf2dfb1476aa89ca9835170b5`

- :ref:`exhale_function_string_8h_1ab6704f416891347d26ebbc5bc9401a3a`

- :ref:`exhale_function_string_8h_1a29ed7eb9bc181d1df7f5c55ca6246b60`

- :ref:`exhale_function_serialize_8h_1a047703dc2d7e22991c7dd29dc42a46d9`

- :ref:`exhale_function_serialize_8h_1a50b7e723472acf1ca28b86c386460d18`

- :ref:`exhale_function_string_8h_1af4c6e48b61b0f04d2b0beda40a19706b`

- :ref:`exhale_function_string_8h_1a721f0ec88d549e1301a0635b695a1a88`

- :ref:`exhale_function_string_8h_1a713de56481110a0598ff3f418b9896d8`

- :ref:`exhale_function_string_8h_1a48545cc6c63142caa4dcce85a1b6feda`

- :ref:`exhale_function_string_8h_1a990a88794f4e1678fd932c84de435e26`

- :ref:`exhale_function_string_8h_1a8849265e14282c47ef7327ce79e14d0e`

- :ref:`exhale_function_string_8h_1a9ae517eb66873b84094560f4a3698609`

- :ref:`exhale_function_string_8h_1a4ff932db1e5a280b017075c85d2856a8`

- :ref:`exhale_function_string_8h_1a57204d6cb68b449d9db9eae4883571c1`

- :ref:`exhale_function_string_8h_1a4cf760d9d1b68bcf1cd43e9f5c54feb1`

- :ref:`exhale_function_string_8h_1a0ccb8b7286ed4a45f31b6c936a9a98c2`

- :ref:`exhale_function_data_8h_1a9bcac01c289f96bdbf3f3d398592098b`

- :ref:`exhale_function_data_8h_1ace1bec728e7401063e26ebc9e83481a0`

- :ref:`exhale_function_data_8h_1aabc2762937ed01fdce00372551019afb`

- :ref:`exhale_function_string_8h_1a522339d5ba8ef44cfee16c46aa6911e0`

- :ref:`exhale_function_string_8h_1acee3f0570836bfe9f9bd769aafda6e75`

- :ref:`exhale_function_string_8h_1a1d1bcc50e73c2538ca16f3aec01eef87`

- :ref:`exhale_function_string_8h_1ab4a443eed7b8adb78c5aadca1354b49b`

- :ref:`exhale_function_string_8h_1a0ac76ec24bc20d38967c451767fa76bd`

- :ref:`exhale_function_string_8h_1aa98dbbb67f5b5fce8330bd3b43993f1b`

- :ref:`exhale_function_string_8h_1ab9706925fd70fabd0427b0dd44ed39a5`

- :ref:`exhale_function_string_8h_1a8aaac987f17f693b727f6f61c82ef59d`

- :ref:`exhale_function_string_8h_1adce2f798f94879309cef872d5c0cf70c`

- :ref:`exhale_function_string_8h_1a5415fd0bce16fb23ec98e50c0b82b42e`

- :ref:`exhale_function_data_8h_1ae841cffb9297b1e03dd333546492082a`

- :ref:`exhale_function_data_8h_1ae7f269258f554b475a1fb22eb0901162`

- :ref:`exhale_function_data_8h_1a3d2186da2c5fe38cd4cde2ed9fabae42`

- :ref:`exhale_function_pattern_8h_1a3402e2566a4fa926faa7d5916b90eec9`

- :ref:`exhale_function_version_8h_1a8ea066b50a4aaa0bc27455912d914c0e`

- :ref:`exhale_function_pattern_8h_1a1280260bbcab517cafd62246c537eb06`


Typedefs
--------


- :ref:`exhale_typedef_reflection_8h_1a119264abacc913e662fe7f5f2b14a813`

- :ref:`exhale_typedef_serialize_8h_1af13a1979f9e8f62c101433fac6511cc6`

- :ref:`exhale_typedef_pattern_8h_1a4d205bdcf25db1c169d605a29aa44357`

- :ref:`exhale_typedef_pattern_8h_1af39b2abf171c61390ccfb385306a70a2`


Variables
---------


- :ref:`exhale_variable_optional_8h_1abf9f9d97c8d614e485dc36b63e62a8d3`
