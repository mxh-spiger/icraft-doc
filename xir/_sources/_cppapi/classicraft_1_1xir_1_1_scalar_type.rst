.. _exhale_class_classicraft_1_1xir_1_1_scalar_type:

Class ScalarType
================

- Defined in :ref:`file_icraft-xir_core_data_type.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public VirtualBase< ScalarType, DataType, ScalarTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)


Derived Types
*************

- ``public VirtualBase< BaseQuantizedType, ScalarType, BaseQuantizedTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< FloatType, ScalarType, FloatTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< IntegerType, ScalarType, IntegerTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::ScalarType
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: