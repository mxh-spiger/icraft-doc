.. _exhale_class_classicraft_1_1xir_1_1_conv2d_node:

Class Conv2dNode
================

- Defined in :ref:`file_icraft-xir_ops_conv2d.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< Conv2dNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Conv2dNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: