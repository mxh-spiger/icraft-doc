.. _exhale_function_serialize_8h_1a5ebb3b0b58b3fb3e792c71e49b3fbaa7:

Function icraft::xir::CreateNetworkFromMSG
==========================================

- Defined in :ref:`file_icraft-xir_core_serialize.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::CreateNetworkFromMSG(const std::filesystem::path&)
   :project: Icraft XIR