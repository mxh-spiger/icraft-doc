
.. _program_listing_file_icraft-xir_core_data.h:

Program Listing for File data.h
===============================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_core_data.h>` (``icraft-xir\core\data.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/base/array.h>
   #include <icraft-xir/core/data_type.h>
   #include <icraft-xir/core/mem_type.h>
   
   #include <memory>
   #include <numeric>
   
   namespace icraft::xir {
       class DataNode : public Node {
       public:
           DataType dtype;     
           MemType mtype;      
       
           virtual void accept(AttrVisitor& visitor) override {
               visitor.visit("dtype", dtype);
               visitor.visit("mtype", mtype);
           }
   
           bool reduceAccept(DataNode* other, const ReduceVisitor& reduce) const {
               return reduce(dtype, other->dtype)
                   && reduce(mtype, other->mtype);
           }
   
       private:
           virtual uint64_t storageBits() const = 0;
   
           friend class Data;
       };
   
       class Data : public VirtualBase<Data, Handle, DataNode> {
       public:
           Data() = default;
   
           XIR_DLL Data& setDType(DataType dtype);
   
           XIR_DLL Data& setMType(MemType mtype);
   
           XIR_DLL uint64_t storageBits() const;
   
           XIR_DLL uint64_t storageBytes() const;
       };
   
       class ScalarImmNode : public DataNode {
       private:
           XIR_DLL virtual uint64_t storageBits() const override;
       };
   
       class ScalarImm : public HandleBase<ScalarImm, Data, ScalarImmNode> {
       public:
           ScalarImm() = default;
   
           XIR_DLL ScalarImm(int64_t value);
   
           XIR_DLL ScalarImm(double value);
   
           ScalarType scalarType() const {
               return Downcast<ScalarType>((*this)->dtype);
           }
       };
   
       class IntImmNode : public NodeBase<IntImmNode, ScalarImmNode> {
       public:
           int64_t value;      
   
           virtual void accept(AttrVisitor& visitor) override {
               visitor.visit("value", value);
               ScalarImmNode::accept(visitor);
           };
   
           bool reduceAccept(IntImmNode* other, const ReduceVisitor& reduce) const {
               return reduce(value, other->value)
                   && ScalarImmNode::reduceAccept(other, reduce);
           }
       };
   
       class IntImm : public HandleBase<IntImm, ScalarImm, IntImmNode> {
       public:
           IntImm() = default;
   
           XIR_DLL IntImm(int64_t value, ScalarType dtype, MemType mtype = HostMem::Init());
   
           IntImm(int64_t value, MemType mtype = HostMem::Init()) 
               : IntImm(value, IntegerType::SInt64(), mtype) {}
   
           operator int64_t() const { return (*this)->value; }
       };
   
       class Bool : public HandleBase<Bool, IntImm, IntImmNode> {
       public:
           Bool() = default;
   
           XIR_DLL Bool(bool value);
   
           Bool operator!() const { return Bool((*this)->value == 0); }
   
           operator bool() const { return (*this)->value != 0; }
       };
   
       inline Bool operator||(const Bool& a, bool b) { return Bool(a.operator bool() || b); }
       inline Bool operator||(bool a, const Bool& b) { return Bool(a || b.operator bool()); }
       inline Bool operator||(const Bool& a, const Bool& b) {
           return Bool(a.operator bool() || b.operator bool());
       }
   
       inline Bool operator&&(const Bool& a, bool b) { return Bool(a.operator bool() && b); }
       inline Bool operator&&(bool a, const Bool& b) { return Bool(a && b.operator bool()); }
       inline Bool operator&&(const Bool& a, const Bool& b) {
           return Bool(a.operator bool() && b.operator bool());
       }
   
       inline bool operator==(const Bool& a, bool b) { return a.operator bool() == b; }
       inline bool operator==(bool a, const Bool& b) { return a == b.operator bool(); }
       inline bool operator==(const Bool& a, const Bool& b) {
           return a.operator bool() == b.operator bool();
       }
   
       class FloatImmNode : public NodeBase<FloatImmNode, ScalarImmNode> {
       public:
           double value;       
   
           virtual void accept(AttrVisitor& visitor) override {
               visitor.visit("value", value);
               ScalarImmNode::accept(visitor);
           };
   
           bool reduceAccept(FloatImmNode* other, const ReduceVisitor& reduce) const {
               return reduce(value, other->value)
                   && ScalarImmNode::reduceAccept(other, reduce);
           }
       };
   
       class FloatImm : public HandleBase<FloatImm, ScalarImm, FloatImmNode> {
       public:
           FloatImm() = default;
   
           XIR_DLL FloatImm(double value, ScalarType dtype, MemType mtype = HostMem::Init());
   
           FloatImm(double value, MemType mtype = HostMem::Init()) : FloatImm(value, FloatType::FP64(), mtype) {}
   
           operator double() { return (*this)->value; }
       };
   
       class OperationNode;
       class Operation;
       class Network;
   
       class ValueNode : public NodeBase<ValueNode, DataNode> {
       public:
           int64_t v_id;               
           std::string name;           
   
           uint64_t _index;
           const OperationNode* _op = nullptr;
   
           virtual void accept(AttrVisitor& visitor) override {
               visitor.visit("v_id", v_id);
               visitor.visit("name", name);
               DataNode::accept(visitor);
           };
   
           bool reduceAccept(ValueNode* other, const ReduceVisitor& reduce) const {
               return reduce(v_id, other->v_id)
                   && reduce(name, other->name)
                   && DataNode::reduceAccept(other, reduce);
           }
   
       private:
           XIR_DLL virtual uint64_t storageBits() const override;
       };
   
       class ParamsNode;
       class Value : public HandleBase<Value, Data, ValueNode> {
       public:
           Value() = default;
   
           XIR_DLL Value(std::string name, TensorType dtype, MemType mtype = HostMem::Init());
   
           Value(TensorType dtype, MemType mtype = HostMem::Init()) : Value("", dtype, mtype) {}
   
           TensorType tensorType() const {
               return Downcast<TensorType>((*this)->dtype);
           }
   
           bool isParams() const {
               return (*this)->isInstance<ParamsNode>();
           }
   
           XIR_DLL Value& setId(int64_t v_id);
   
           XIR_DLL Value& setName(std::string name);
   
           XIR_DLL Value& bindOp(const Operation& op, int64_t index);
   
           XIR_DLL bool isBoundToOp() const;
   
           XIR_DLL Operation op() const;
   
           XIR_DLL uint64_t index() const;
   
           XIR_DLL bool isBoundToNetwork() const;
   
           XIR_DLL Network network() const;
   
           XIR_DLL Array<Operation> getUsesOp() const;
   
           XIR_DLL size_t getUsesNum() const;
   
       protected:
           XIR_DLL static int64_t genId();
       };
   
       class ParamsNode : public NodeBase<ParamsNode, ValueNode> {
       public:
           std::shared_ptr<void> _data = nullptr;
           bool _lazy = false;
           
           bool reduceAccept(ParamsNode* other, const ReduceVisitor& reduce) const {
               return ValueNode::reduceAccept(other, reduce);
           }
       };
   
       class Params : public HandleBase<Params, Value, ParamsNode> {
       public:
           Params() = default;
   
           template <typename T, typename = typename std::enable_if_t<std::is_pod_v<T>>>
           Params(std::shared_ptr<T[]> data, std::string name, TensorType dtype, MemType mtype = HostMem::Init()) {
               auto node = make_object<ParamsNode>();
               node->_data = data;
               node->v_id = genId();
               node->name = std::move(name);
               node->dtype = std::move(dtype);
               node->mtype = std::move(mtype);
               data_ = std::move(node);
           }
   
           Params(std::string name, TensorType dtype, MemType mtype = HostMem::Init())
               : Params(std::shared_ptr<char[]>(), std::move(name), std::move(dtype), std::move(mtype)) {}
   
           Params(TensorType dtype)
               : Params(std::shared_ptr<char[]>(), "", std::move(dtype)) {}
   
           template <typename T, typename = typename std::enable_if_t<std::is_pod_v<T>>>
           void setData(std::shared_ptr<T[]> data) {
               get_mutable()->_data = data;
           }
   
           template <typename T, typename = typename std::enable_if_t<std::is_pod_v<T>>>
           void setData(std::unique_ptr<T[]> data) {
               get_mutable()->_data = std::move(data);
           }
   
           XIR_DLL uint64_t copyData(void* data);
   
           template <typename T, typename = typename std::enable_if_t<std::is_pod_v<T>>>
           std::shared_ptr<T[]> data() const {
               return std::static_pointer_cast<T[]>((*this)->_data);
           }
   
           void setLazy(bool lazy) {
               get_mutable()->_lazy = lazy;
           }
   
           bool isLazyLoaded() const {
               return (*this)->_lazy;
           }
   
           XIR_DLL void load();
   
           template <typename T, typename = typename std::enable_if_t<std::is_pod_v<T>>>
           Params& fill(std::function<T(uint64_t)> f) {
               auto dtype = this->tensorType();
               auto etype = dtype->element_dtype;
               auto tbits = sizeof(T) * 8;
               auto sbits = (*this)->mtype->getStorageBits(etype.bits());
               ICRAFT_CHECK(tbits == sbits)
                   .append("sizeof(T) not match element_dtype, {} vs. {}", tbits, sbits);
               auto num_elements = dtype.numElements();
               auto filled_data = new T[num_elements];
               for (uint64_t i = 0; i < num_elements; i++) {
                   filled_data[i] = f(i);
               }
               get_mutable()->_data = std::shared_ptr<T[]>(filled_data);
               return *this;
           }
   
           bool hasData() const {
               return (*this)->_data != nullptr;
           }
       };
   }
