.. _exhale_class_classicraft_1_1xir_1_1_network_view:

Class NetworkView
=================

- Defined in :ref:`file_icraft-xir_core_network.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< NetworkView, Handle, NetworkViewNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::NetworkView
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: