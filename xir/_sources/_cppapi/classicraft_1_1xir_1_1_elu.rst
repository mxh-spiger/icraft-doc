.. _exhale_class_classicraft_1_1xir_1_1_elu:

Class Elu
=========

- Defined in :ref:`file_icraft-xir_ops_elu.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< Elu, EluNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Elu
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: