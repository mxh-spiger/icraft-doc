.. _exhale_function_optional_8h_1a9daee3d26c5a475c024034ccf4134954:

Template Function icraft::xir::is_optional(std::optional<T> const&)
===================================================================

- Defined in :ref:`file_icraft-xir_base_optional.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::is_optional(std::optional<T> const&)
   :project: Icraft XIR