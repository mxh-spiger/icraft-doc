.. _exhale_class_classicraft_1_1xir_1_1_softmax:

Class Softmax
=============

- Defined in :ref:`file_icraft-xir_ops_softmax.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< Softmax, SoftmaxNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Softmax
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: