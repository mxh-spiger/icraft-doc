.. _exhale_class_classicraft_1_1xir_1_1_reshape:

Class Reshape
=============

- Defined in :ref:`file_icraft-xir_ops_reshape.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< Reshape, ReshapeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Reshape
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: