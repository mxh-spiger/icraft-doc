
.. _file_icraft-xir_base_string.h:

File string.h
=============

|exhale_lsh| :ref:`Parent directory <dir_icraft-xir_base>` (``icraft-xir\base``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-xir\base\string.h``)
-----------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-xir_base_string.h.rst





Includes
--------


- ``cstring``

- ``icraft-xir/base/object.h``

- ``ostream``

- ``string`` (:ref:`file_icraft-xir_base_string.h`)



Included By
-----------


- :ref:`file_icraft-xir_core_attrs.h`

- :ref:`file_icraft-xir_core_network.h`

- :ref:`file_icraft-xir_core_operation.h`

- :ref:`file_icraft-xir_core_pass.h`

- :ref:`file_icraft-xir_core_serialize.h`




Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__xir`


Classes
-------


- :ref:`exhale_class_classicraft_1_1xir_1_1_string`

- :ref:`exhale_class_classicraft_1_1xir_1_1_string_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_string_node_1_1_from_std`


Functions
---------


- :ref:`exhale_function_string_8h_1a29475156e730e27c5e1cba18c5177ef6`

- :ref:`exhale_function_string_8h_1af6dfa63e8765ba8f0c05a7d40fa1c584`

- :ref:`exhale_function_string_8h_1ac4e429cee0b2f43d16eca3b016f5185c`

- :ref:`exhale_function_string_8h_1a1cadc42fadabb1c9aaa455d8f857a6db`

- :ref:`exhale_function_string_8h_1acab3e9f8bdf2c08f6cf2fc285558c1ce`

- :ref:`exhale_function_string_8h_1a346acdf3143412eaab63e8f3664cd575`

- :ref:`exhale_function_string_8h_1af71ae061ba27c966204712cf13558437`

- :ref:`exhale_function_string_8h_1a4ed702c701164539de5ce6b332c45d07`

- :ref:`exhale_function_string_8h_1ae3cd285a0b3bcf7d5f1b89dba89035d1`

- :ref:`exhale_function_string_8h_1a90015386d12dcad1a1150e0e3c6a0d8a`

- :ref:`exhale_function_string_8h_1aae232265e2908fc6e59999597d6441aa`

- :ref:`exhale_function_string_8h_1abb89216adbfc3f69204c0a4ecbe67458`

- :ref:`exhale_function_string_8h_1ad94401dcf2dfb1476aa89ca9835170b5`

- :ref:`exhale_function_string_8h_1ab6704f416891347d26ebbc5bc9401a3a`

- :ref:`exhale_function_string_8h_1a29ed7eb9bc181d1df7f5c55ca6246b60`

- :ref:`exhale_function_string_8h_1af4c6e48b61b0f04d2b0beda40a19706b`

- :ref:`exhale_function_string_8h_1a721f0ec88d549e1301a0635b695a1a88`

- :ref:`exhale_function_string_8h_1a713de56481110a0598ff3f418b9896d8`

- :ref:`exhale_function_string_8h_1a48545cc6c63142caa4dcce85a1b6feda`

- :ref:`exhale_function_string_8h_1a990a88794f4e1678fd932c84de435e26`

- :ref:`exhale_function_string_8h_1a8849265e14282c47ef7327ce79e14d0e`

- :ref:`exhale_function_string_8h_1a9ae517eb66873b84094560f4a3698609`

- :ref:`exhale_function_string_8h_1a4ff932db1e5a280b017075c85d2856a8`

- :ref:`exhale_function_string_8h_1a57204d6cb68b449d9db9eae4883571c1`

- :ref:`exhale_function_string_8h_1a4cf760d9d1b68bcf1cd43e9f5c54feb1`

- :ref:`exhale_function_string_8h_1a0ccb8b7286ed4a45f31b6c936a9a98c2`

- :ref:`exhale_function_string_8h_1a522339d5ba8ef44cfee16c46aa6911e0`

- :ref:`exhale_function_string_8h_1acee3f0570836bfe9f9bd769aafda6e75`

- :ref:`exhale_function_string_8h_1a1d1bcc50e73c2538ca16f3aec01eef87`

- :ref:`exhale_function_string_8h_1ab4a443eed7b8adb78c5aadca1354b49b`

- :ref:`exhale_function_string_8h_1a0ac76ec24bc20d38967c451767fa76bd`

- :ref:`exhale_function_string_8h_1aa98dbbb67f5b5fce8330bd3b43993f1b`

- :ref:`exhale_function_string_8h_1ab9706925fd70fabd0427b0dd44ed39a5`

- :ref:`exhale_function_string_8h_1a8aaac987f17f693b727f6f61c82ef59d`

- :ref:`exhale_function_string_8h_1adce2f798f94879309cef872d5c0cf70c`

- :ref:`exhale_function_string_8h_1a5415fd0bce16fb23ec98e50c0b82b42e`

