.. _exhale_class_classicraft_1_1xir_1_1_reflection_table_1_1_registry:

Class ReflectionTable::Registry
===============================

- Defined in :ref:`file_icraft-xir_core_reflection.h`


Nested Relationships
--------------------

This class is a nested type of :ref:`exhale_class_classicraft_1_1xir_1_1_reflection_table`.


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::ReflectionTable::Registry
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: