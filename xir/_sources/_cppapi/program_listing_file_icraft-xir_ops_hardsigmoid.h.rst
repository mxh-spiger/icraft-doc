
.. _program_listing_file_icraft-xir_ops_hardsigmoid.h:

Program Listing for File hardsigmoid.h
======================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_ops_hardsigmoid.h>` (``icraft-xir\ops\hardsigmoid.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/operation.h>
   
   namespace icraft::xir {
       class HardsigmoidNode : public OpNodeBase<HardsigmoidNode, OneResult, IsActivate> {
       public:
           double alpha;       
   
           ICRAFT_DECLARE_ATTRS(HardsigmoidNode) {
               ICRAFT_ATTR_FIELD(alpha);
           }
       };
   
       class Hardsigmoid : public OpBase<Hardsigmoid, HardsigmoidNode> {
       public:
           Hardsigmoid() = default;
   
           XIR_DLL Hardsigmoid(Value input, double alpha);
       };
   }
