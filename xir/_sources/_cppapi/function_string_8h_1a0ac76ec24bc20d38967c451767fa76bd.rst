.. _exhale_function_string_8h_1a0ac76ec24bc20d38967c451767fa76bd:

Function icraft::xir::operator>(const char \*, const String&)
=============================================================

- Defined in :ref:`file_icraft-xir_base_string.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::operator>(const char *, const String&)
   :project: Icraft XIR