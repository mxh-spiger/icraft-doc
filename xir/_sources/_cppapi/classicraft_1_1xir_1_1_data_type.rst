.. _exhale_class_classicraft_1_1xir_1_1_data_type:

Class DataType
==============

- Defined in :ref:`file_icraft-xir_core_data_type.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public VirtualBase< DataType, Handle, DataTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)


Derived Types
*************

- ``public VirtualBase< ScalarType, DataType, ScalarTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< TensorType, DataType, TensorTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::DataType
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: