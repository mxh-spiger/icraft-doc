.. _exhale_function_serialize_8h_1a0185d16d44e07b98bc5b409791036be6:

Function icraft::xir::CalcMD5(char \*, uint64_t)
================================================

- Defined in :ref:`file_icraft-xir_core_serialize.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::CalcMD5(char *, uint64_t)
   :project: Icraft XIR