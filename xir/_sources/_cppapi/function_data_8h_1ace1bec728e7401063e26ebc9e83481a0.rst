.. _exhale_function_data_8h_1ace1bec728e7401063e26ebc9e83481a0:

Function icraft::xir::operator==(bool, const Bool&)
===================================================

- Defined in :ref:`file_icraft-xir_core_data.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::operator==(bool, const Bool&)
   :project: Icraft XIR