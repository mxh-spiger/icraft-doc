.. _exhale_function_serialize_8h_1a50b7e723472acf1ca28b86c386460d18:

Function icraft::xir::operator<<(std::ostream&, const Object \*)
================================================================

- Defined in :ref:`file_icraft-xir_core_serialize.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::operator<<(std::ostream&, const Object *)
   :project: Icraft XIR