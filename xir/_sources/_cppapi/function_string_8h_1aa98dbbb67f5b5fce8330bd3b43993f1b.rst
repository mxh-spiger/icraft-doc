.. _exhale_function_string_8h_1aa98dbbb67f5b5fce8330bd3b43993f1b:

Function icraft::xir::operator>=(const String&, const std::string&)
===================================================================

- Defined in :ref:`file_icraft-xir_base_string.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::operator>=(const String&, const std::string&)
   :project: Icraft XIR