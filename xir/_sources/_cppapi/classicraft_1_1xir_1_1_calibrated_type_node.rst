.. _exhale_class_classicraft_1_1xir_1_1_calibrated_type_node:

Class CalibratedTypeNode
========================

- Defined in :ref:`file_icraft-xir_core_data_type.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< CalibratedTypeNode, BaseQuantizedTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::CalibratedTypeNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: