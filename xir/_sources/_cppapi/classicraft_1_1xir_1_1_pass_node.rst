.. _exhale_class_classicraft_1_1xir_1_1_pass_node:

Class PassNode
==============

- Defined in :ref:`file_icraft-xir_core_pass.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< PassNode, Node >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Derived Types
*************

- ``public NodeBase< NetworkPassNode, PassNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)
- ``public NodeBase< SequentialPassNode, PassNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::PassNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: