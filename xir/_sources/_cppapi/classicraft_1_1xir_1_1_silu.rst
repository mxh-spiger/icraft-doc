.. _exhale_class_classicraft_1_1xir_1_1_silu:

Class Silu
==========

- Defined in :ref:`file_icraft-xir_ops_silu.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< Silu, SiluNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Silu
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: