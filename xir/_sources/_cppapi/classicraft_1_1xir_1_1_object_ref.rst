.. _exhale_class_classicraft_1_1xir_1_1_object_ref:

Class ObjectRef
===============

- Defined in :ref:`file_icraft-xir_base_object.h`


Inheritance Relationships
-------------------------

Derived Types
*************

- ``public Array< icraft::xir::AlignedUnit >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_array`)
- ``public Array< icraft::xir::IntImm >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_array`)
- ``public Array< icraft::xir::AxisName >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_array`)
- ``public Array< icraft::xir::Bool >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_array`)
- ``public Array< icraft::xir::Operation >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_array`)
- ``public Array< icraft::xir::FloatImm >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_array`)
- ``public Array< icraft::xir::AttrPattern >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_array`)
- ``public Array< icraft::xir::ValuePattern >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_array`)
- ``public Array< icraft::xir::Value >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_array`)
- ``public Array< icraft::xir::String >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_array`)
- ``public Array< icraft::xir::PriorBox >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_array`)
- ``public Array< icraft::xir::Pass >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_array`)
- ``public Array< icraft::xir::MergedAxisDistr >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_array`)
- ``public Map< icraft::xir::String, icraft::xir::IntImm >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_map`)
- ``public Map< icraft::xir::String, icraft::xir::ObjectRef >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_map`)
- ``public Optional< icraft::xir::Params >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_optional`)
- ``public VirtualBase< Handle, ObjectRef, Node >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public Any`` (:ref:`exhale_class_classicraft_1_1xir_1_1_any`)
- ``public Array< T >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_array`)
- ``public Map< K, V, typename, typename >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_map`)
- ``public Optional< T >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_optional`)
- ``public String`` (:ref:`exhale_class_classicraft_1_1xir_1_1_string`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::ObjectRef
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: