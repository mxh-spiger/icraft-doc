.. _exhale_class_classicraft_1_1xir_1_1_zhuge_target:

Class ZhugeTarget
=================

- Defined in :ref:`file_icraft-xir_core_compile_target.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< ZhugeTarget, CompileTarget, ZhugeTargetNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::ZhugeTarget
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: