.. _exhale_class_classicraft_1_1xir_1_1_pad:

Class Pad
=========

- Defined in :ref:`file_icraft-xir_ops_pad.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< Pad, PadNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Pad
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: