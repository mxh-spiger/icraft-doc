.. _exhale_class_classicraft_1_1xir_1_1_base_quantized_type_node:

Class BaseQuantizedTypeNode
===========================

- Defined in :ref:`file_icraft-xir_core_data_type.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< BaseQuantizedTypeNode, ScalarTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Derived Types
*************

- ``public NodeBase< CalibratedTypeNode, BaseQuantizedTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)
- ``public NodeBase< NormalizedTypeNode, BaseQuantizedTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)
- ``public NodeBase< QuantizedTypeNode, BaseQuantizedTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::BaseQuantizedTypeNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: