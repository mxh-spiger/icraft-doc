.. _exhale_class_classicraft_1_1xir_1_1_scalar_imm:

Class ScalarImm
===============

- Defined in :ref:`file_icraft-xir_core_data.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< ScalarImm, Data, ScalarImmNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Derived Types
*************

- ``public VirtualBase< FloatImm, ScalarImm, FloatImmNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< IntImm, ScalarImm, IntImmNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::ScalarImm
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: