.. _exhale_class_classicraft_1_1xir_1_1_virtual_base:

Template Class VirtualBase
==========================

- Defined in :ref:`file_icraft-xir_core_node.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public BaseType``


Derived Types
*************

- ``public HandleBase< Abs, Operation, AbsNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Add, Operation, AddNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< AlignAxis, Operation, AlignAxisNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< AlignedUnit, Handle, AlignedUnitNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< AndPattern, Pattern, AndPatternNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< AnycardsPattern, Pattern, AnycardsPatternNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< AttrFieldInfo, Handle, AttrFieldInfoNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< AttrPattern, Pattern, AttrPatternNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Avgpool, Operation, AvgpoolNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< AxisName, Handle, AxisNameNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< AxisUnit, AxisName, AxisUnitNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< BaseQuantizedType, ScalarType, BaseQuantizedTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Batchnorm, Operation, BatchnormNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Bool, IntImm, IntImmNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< BuyiTarget, CompileTarget, BuyiTargetNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< CalibratedType, BaseQuantizedType, CalibratedTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Cast, Operation, CastNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Concat, Operation, ConcatNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Conv2d, Operation, Conv2dNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Conv2dTranspose, Operation, Conv2dTransposeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Copy, Operation, CopyNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< CustomTarget, CompileTarget, CustomTargetNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< DivideScalar, Operation, DivideScalarNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Elu, Operation, EluNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< ExpQuantizedScale, QuantizedScale, ExpQuantizedScaleNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< ExpQuantizedScaleArray, QuantizedScaleArray, ExpQuantizedScaleArrayNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Expand, Operation, ExpandNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< ExternalMem, MemType, ExternalMemNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< FPGATarget, CompileTarget, FPGATargetNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< FloatImm, ScalarImm, FloatImmNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< FloatType, ScalarType, FloatTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Gelu, Operation, GeluNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< HardOp, Operation, HardOpNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Hardsigmoid, Operation, HardsigmoidNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Hardswish, Operation, HardswishNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< HostMem, MemType, HostMemNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< HostTarget, CompileTarget, HostTargetNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Input, Operation, InputNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< IntImm, ScalarImm, IntImmNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< IntegerType, ScalarType, IntegerTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Layernorm, Operation, LayernormNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Layout, Handle, LayoutNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Matmul, Operation, MatmulNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Maxpool, Operation, MaxpoolNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< MergedAxisDistr, Handle, MergedAxisDistrNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Mish, Operation, MishNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< MultiYolo, Operation, MultiYoloNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Multiply, Operation, MultiplyNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Network, Handle, NetworkNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< NetworkPass, Pass, NetworkPassNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< NetworkView, Handle, NetworkViewNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Normalize, Operation, NormalizeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< NormalizedQuantizedType, QuantizedType, NormalizedQuantizedTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< NormalizedType, BaseQuantizedType, NormalizedTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< OnChipMem, MemType, OnChipMemNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< OpType, Operation, OpNodeType >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< OpPattern, Pattern, OpPatternNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< OrPattern, Pattern, OrPatternNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Output, Operation, OutputNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Pad, Operation, PadNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Params, Value, ParamsNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< PassContext, Handle, PassContextNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< PassInfo, Handle, PassInfoNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< PixelShuffle, Operation, PixelShuffleNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Prelu, Operation, PreluNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< PriorBox, Handle, PriorBoxNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< PruneAxis, Operation, PruneAxisNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< QuantizedScale, Handle, QuantizedScaleNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< QuantizedScaleArray, Handle, QuantizedScaleArrayNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< QuantizedType, BaseQuantizedType, QuantizedTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Region, Operation, RegionNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Relu, Operation, ReluNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Reorg, Operation, ReorgNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Reshape, Operation, ReshapeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Resize, Operation, ResizeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Route, Operation, RouteNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< SSDOutput, Operation, SSDOutputNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< ScalarImm, Data, ScalarImmNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< SequentialPass, Pass, SequentialPassNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Sigmoid, Operation, SigmoidNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Silu, Operation, SiluNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Slice, Operation, SliceNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Softmax, Operation, SoftmaxNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Split, Operation, SplitNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Squeeze, Operation, SqueezeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< SwapOrder, Operation, SwapOrderNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Tanh, Operation, TanhNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< TensorType, DataType, TensorTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Transpose, Operation, TransposeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Upsample, Operation, UpsampleNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Value, Data, ValueNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< ValuePattern, Pattern, ValuePatternNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< Yolo, Operation, YoloNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< ZhugeTarget, CompileTarget, ZhugeTargetNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)
- ``public HandleBase< ConcreteType, BaseType, NodeObject >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::VirtualBase
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: