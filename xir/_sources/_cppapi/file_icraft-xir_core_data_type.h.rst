
.. _file_icraft-xir_core_data_type.h:

File data_type.h
================

|exhale_lsh| :ref:`Parent directory <dir_icraft-xir_core>` (``icraft-xir\core``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-xir\core\data_type.h``)
--------------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-xir_core_data_type.h.rst





Includes
--------


- ``icraft-xir/core/layout.h``



Included By
-----------


- :ref:`file_icraft-xir_core_data.h`




Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__xir`


Classes
-------


- :ref:`exhale_class_classicraft_1_1xir_1_1_base_quantized_type`

- :ref:`exhale_class_classicraft_1_1xir_1_1_base_quantized_type_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_calibrated_type`

- :ref:`exhale_class_classicraft_1_1xir_1_1_calibrated_type_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_data_type`

- :ref:`exhale_class_classicraft_1_1xir_1_1_data_type_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_exp_quantized_scale`

- :ref:`exhale_class_classicraft_1_1xir_1_1_exp_quantized_scale_array`

- :ref:`exhale_class_classicraft_1_1xir_1_1_exp_quantized_scale_array_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_exp_quantized_scale_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_float_type`

- :ref:`exhale_class_classicraft_1_1xir_1_1_float_type_1_1_semantics`

- :ref:`exhale_class_classicraft_1_1xir_1_1_float_type_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_integer_type`

- :ref:`exhale_class_classicraft_1_1xir_1_1_integer_type_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_merged_axis_distr`

- :ref:`exhale_class_classicraft_1_1xir_1_1_merged_axis_distr_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_normalized_quantized_type`

- :ref:`exhale_class_classicraft_1_1xir_1_1_normalized_quantized_type_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_normalized_type`

- :ref:`exhale_class_classicraft_1_1xir_1_1_normalized_type_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_quantized_scale`

- :ref:`exhale_class_classicraft_1_1xir_1_1_quantized_scale_array`

- :ref:`exhale_class_classicraft_1_1xir_1_1_quantized_scale_array_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_quantized_scale_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_quantized_type`

- :ref:`exhale_class_classicraft_1_1xir_1_1_quantized_type_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_scalar_type`

- :ref:`exhale_class_classicraft_1_1xir_1_1_scalar_type_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_tensor_type`

- :ref:`exhale_class_classicraft_1_1xir_1_1_tensor_type_node`

