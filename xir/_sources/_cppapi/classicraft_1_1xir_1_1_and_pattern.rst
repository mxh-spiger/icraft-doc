.. _exhale_class_classicraft_1_1xir_1_1_and_pattern:

Class AndPattern
================

- Defined in :ref:`file_icraft-xir_core_pattern.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< AndPattern, Pattern, AndPatternNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::AndPattern
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: