
.. _file_icraft-xir_ops_hardswish.h:

File hardswish.h
================

|exhale_lsh| :ref:`Parent directory <dir_icraft-xir_ops>` (``icraft-xir\ops``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-xir\ops\hardswish.h``)
-------------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-xir_ops_hardswish.h.rst





Includes
--------


- ``icraft-xir/core/operation.h``






Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__xir`


Classes
-------


- :ref:`exhale_class_classicraft_1_1xir_1_1_hardswish`

- :ref:`exhale_class_classicraft_1_1xir_1_1_hardswish_node`

