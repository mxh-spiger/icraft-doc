.. _exhale_class_classicraft_1_1xir_1_1_s_s_d_output_node:

Class SSDOutputNode
===================

- Defined in :ref:`file_icraft-xir_ops_ssd_output.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< SSDOutputNode, OutputLike >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::SSDOutputNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: