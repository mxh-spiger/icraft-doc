.. _exhale_class_classicraft_1_1xir_1_1_squeeze:

Class Squeeze
=============

- Defined in :ref:`file_icraft-xir_ops_squeeze.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< Squeeze, SqueezeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Squeeze
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: