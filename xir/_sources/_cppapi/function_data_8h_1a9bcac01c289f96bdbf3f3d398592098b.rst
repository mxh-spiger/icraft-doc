.. _exhale_function_data_8h_1a9bcac01c289f96bdbf3f3d398592098b:

Function icraft::xir::operator==(const Bool&, bool)
===================================================

- Defined in :ref:`file_icraft-xir_core_data.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::operator==(const Bool&, bool)
   :project: Icraft XIR