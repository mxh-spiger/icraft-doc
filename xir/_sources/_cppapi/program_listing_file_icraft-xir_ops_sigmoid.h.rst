
.. _program_listing_file_icraft-xir_ops_sigmoid.h:

Program Listing for File sigmoid.h
==================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_ops_sigmoid.h>` (``icraft-xir\ops\sigmoid.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/operation.h>
   
   namespace icraft::xir {
       class SigmoidNode : public OpNodeBase<SigmoidNode, OneResult, IsActivate> {
       public:
           XIR_DLL virtual void validate() const override;
       };
   
       class Sigmoid : public OpBase<Sigmoid, SigmoidNode> {
       public:
           Sigmoid() = default;
   
           XIR_DLL Sigmoid(Value input);
       };
   }
