.. _exhale_class_classicraft_1_1xir_1_1_reorg_node:

Class ReorgNode
===============

- Defined in :ref:`file_icraft-xir_ops_reorg.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< ReorgNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::ReorgNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: