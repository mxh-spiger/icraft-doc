.. _exhale_class_classicraft_1_1xir_1_1_calibrated_type:

Class CalibratedType
====================

- Defined in :ref:`file_icraft-xir_core_data_type.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< CalibratedType, BaseQuantizedType, CalibratedTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::CalibratedType
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: