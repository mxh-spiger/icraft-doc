
.. _file_icraft-xir_ops_ssd_output.h:

File ssd_output.h
=================

|exhale_lsh| :ref:`Parent directory <dir_icraft-xir_ops>` (``icraft-xir\ops``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-xir\ops\ssd_output.h``)
--------------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-xir_ops_ssd_output.h.rst





Includes
--------


- ``icraft-xir/core/operation.h``






Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__xir`


Classes
-------


- :ref:`exhale_class_classicraft_1_1xir_1_1_prior_box`

- :ref:`exhale_class_classicraft_1_1xir_1_1_prior_box_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_s_s_d_output`

- :ref:`exhale_class_classicraft_1_1xir_1_1_s_s_d_output_node`

