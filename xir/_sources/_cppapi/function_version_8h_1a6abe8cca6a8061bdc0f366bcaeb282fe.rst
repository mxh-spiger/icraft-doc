.. _exhale_function_version_8h_1a6abe8cca6a8061bdc0f366bcaeb282fe:

Function icraft::xir::BuildTime
===============================

- Defined in :ref:`file_icraft-xir_base_version.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::BuildTime()
   :project: Icraft XIR