.. _exhale_class_classicraft_1_1xir_1_1_data_node:

Class DataNode
==============

- Defined in :ref:`file_icraft-xir_core_data.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public Node`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node`)


Derived Types
*************

- ``public NodeBase< ValueNode, DataNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)
- ``public ScalarImmNode`` (:ref:`exhale_class_classicraft_1_1xir_1_1_scalar_imm_node`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::DataNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: