.. _exhale_class_classicraft_1_1xir_1_1_external_mem_node:

Class ExternalMemNode
=====================

- Defined in :ref:`file_icraft-xir_core_mem_type.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< ExternalMemNode, MemTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::ExternalMemNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: