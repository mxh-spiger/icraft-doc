.. _exhale_class_classicraft_1_1xir_1_1_pass:

Class Pass
==========

- Defined in :ref:`file_icraft-xir_core_pass.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public VirtualBase< Pass, Handle, PassNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)


Derived Types
*************

- ``public VirtualBase< NetworkPass, Pass, NetworkPassNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< SequentialPass, Pass, SequentialPassNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Pass
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: