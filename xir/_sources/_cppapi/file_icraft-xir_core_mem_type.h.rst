
.. _file_icraft-xir_core_mem_type.h:

File mem_type.h
===============

|exhale_lsh| :ref:`Parent directory <dir_icraft-xir_core>` (``icraft-xir\core``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-xir\core\mem_type.h``)
-------------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-xir_core_mem_type.h.rst





Includes
--------


- ``icraft-xir/base/dllexport.h``

- ``icraft-xir/core/node.h``

- ``icraft-xir/core/reflection.h``

- ``regex``



Included By
-----------


- :ref:`file_icraft-xir_core_data.h`




Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__xir`


Classes
-------


- :ref:`exhale_class_classicraft_1_1xir_1_1_external_mem`

- :ref:`exhale_class_classicraft_1_1xir_1_1_external_mem_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_host_mem`

- :ref:`exhale_class_classicraft_1_1xir_1_1_host_mem_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_mem_type`

- :ref:`exhale_class_classicraft_1_1xir_1_1_mem_type_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_on_chip_mem`

- :ref:`exhale_class_classicraft_1_1xir_1_1_on_chip_mem_node`

