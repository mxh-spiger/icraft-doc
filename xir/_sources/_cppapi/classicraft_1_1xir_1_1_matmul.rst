.. _exhale_class_classicraft_1_1xir_1_1_matmul:

Class Matmul
============

- Defined in :ref:`file_icraft-xir_ops_matmul.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< Matmul, MatmulNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Matmul
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: