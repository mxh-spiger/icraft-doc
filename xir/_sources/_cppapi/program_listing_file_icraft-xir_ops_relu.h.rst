
.. _program_listing_file_icraft-xir_ops_relu.h:

Program Listing for File relu.h
===============================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_ops_relu.h>` (``icraft-xir\ops\relu.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/operation.h>
   
   namespace icraft::xir {
       class ReluNode : public OpNodeBase<ReluNode, OneResult, IsActivate> {
       public:
           double alpha;           
           double max_value;       
           double threshold;       
   
           ICRAFT_DECLARE_ATTRS(ReluNode) {
               ICRAFT_ATTR_FIELD(alpha).set_default(0);
               ICRAFT_ATTR_FIELD(max_value).set_default(std::numeric_limits<double>::infinity());
               ICRAFT_ATTR_FIELD(threshold).set_default(0);
           }
   
           XIR_DLL virtual void validate() const override;
       };
   
       class Relu : public OpBase<Relu, ReluNode> {
       public:
           Relu() = default;
   
           XIR_DLL Relu(
               Value input, 
               double alpha = 0, 
               double max_value = std::numeric_limits<double>::infinity(), 
               double threshold = 0
           );
       };
   }
