.. _exhale_class_classicraft_1_1xir_1_1_pixel_shuffle_node:

Class PixelShuffleNode
======================

- Defined in :ref:`file_icraft-xir_ops_pixel_shuffle.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< PixelShuffleNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::PixelShuffleNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: