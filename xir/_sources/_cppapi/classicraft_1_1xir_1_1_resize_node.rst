.. _exhale_class_classicraft_1_1xir_1_1_resize_node:

Class ResizeNode
================

- Defined in :ref:`file_icraft-xir_ops_resize.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< ResizeNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::ResizeNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: