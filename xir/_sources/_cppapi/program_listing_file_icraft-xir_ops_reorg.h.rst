
.. _program_listing_file_icraft-xir_ops_reorg.h:

Program Listing for File reorg.h
================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_ops_reorg.h>` (``icraft-xir\ops\reorg.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/operation.h>
   
   namespace icraft::xir {
       class ReorgNode : public OpNodeBase<ReorgNode, OneResult> {
       public:
           int64_t stride;     
   
           ICRAFT_DECLARE_ATTRS(ReorgNode) {
               ICRAFT_ATTR_FIELD(stride).set_default(1).set_lower_bound(1);
           }
   
           XIR_DLL virtual void validate() const override;
       };
   
       class Reorg : public OpBase<Reorg, ReorgNode> {
       public:
           Reorg() = default;
   
           XIR_DLL Reorg(Value input, int64_t stride);
       };
   }
