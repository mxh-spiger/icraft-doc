
.. _program_listing_file_icraft-xir_ops_ssd_output.h:

Program Listing for File ssd_output.h
=====================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_ops_ssd_output.h>` (``icraft-xir\ops\ssd_output.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/operation.h>
   
   namespace icraft::xir {
       class PriorBoxNode : public NodeBase<PriorBoxNode, Node> {
       public:
           int64_t class_num;
           double min_size;
           double max_size;
           Array<FloatImm> aspect_ratio;
           bool flip;
           bool clip;
           Array<FloatImm> variance;
           double step;
           int64_t bottom_num;
           double valid_thresh;
           double nms_thresh;
           int64_t valid_num;
   
           virtual void accept(AttrVisitor& visitor) override {
               visitor.visit("class_num", class_num);
               visitor.visit("min_size", min_size);
               visitor.visit("max_size", max_size);
               visitor.visit("aspect_ratio", aspect_ratio);
               visitor.visit("flip", flip);
               visitor.visit("clip", clip);
               visitor.visit("variance", variance);
               visitor.visit("step", step);
               visitor.visit("bottom_num", bottom_num);
               visitor.visit("valid_thresh", valid_thresh);
               visitor.visit("nms_thresh", nms_thresh);
               visitor.visit("valid_num", valid_num);
           };
       };
   
       class PriorBox : public HandleBase<PriorBox, Handle, PriorBoxNode> {
       public:
           PriorBox() = default;
   
           XIR_DLL PriorBox(
               int64_t class_num,
               double min_size,
               double max_size,
               Array<FloatImm> aspect_ratio,
               bool flip,
               bool clip,
               Array<FloatImm> variance,
               double step,
               int64_t bottom_num,
               double valid_thresh,
               double nms_thresh,
               int64_t valid_num
           );
       };
   
       class SSDOutputNode : public OpNodeBase<SSDOutputNode, OutputLike> {
       public:
           Array<PriorBox> prior_boxes;    //< 先验框
   
           ICRAFT_DECLARE_ATTRS(SSDOutputNode) {
               ICRAFT_ATTR_FIELD(prior_boxes);
           }
   
           XIR_DLL virtual void validate() const override;
       };
   
       class SSDOutput : public OpBase<SSDOutput, SSDOutputNode> {
       public:
           SSDOutput() = default;
   
           XIR_DLL SSDOutput(Array<Value> inputs, Array<PriorBox> prior_boxes);
       };
   }
