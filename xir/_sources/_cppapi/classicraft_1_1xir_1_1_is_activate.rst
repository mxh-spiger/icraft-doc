.. _exhale_class_classicraft_1_1xir_1_1_is_activate:

Template Class IsActivate
=========================

- Defined in :ref:`file_icraft-xir_core_operation.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::OpTrait< OpType >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_trait`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::IsActivate
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: