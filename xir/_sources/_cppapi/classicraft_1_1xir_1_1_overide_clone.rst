.. _exhale_class_classicraft_1_1xir_1_1_overide_clone:

Class OverideClone
==================

- Defined in :ref:`file_icraft-xir_core_reflection.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleTraitFunctor< OverideClone, Handle(const Handle &)>``


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::OverideClone
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: