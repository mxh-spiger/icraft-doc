
.. _file_icraft-xir_ops_slice.h:

File slice.h
============

|exhale_lsh| :ref:`Parent directory <dir_icraft-xir_ops>` (``icraft-xir\ops``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-xir\ops\slice.h``)
---------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-xir_ops_slice.h.rst





Includes
--------


- ``icraft-xir/core/operation.h``






Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__xir`


Classes
-------


- :ref:`exhale_class_classicraft_1_1xir_1_1_slice`

- :ref:`exhale_class_classicraft_1_1xir_1_1_slice_node`

