.. _exhale_class_classicraft_1_1xir_1_1_align_axis_node:

Class AlignAxisNode
===================

- Defined in :ref:`file_icraft-xir_ops_align_axis.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< AlignAxisNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::AlignAxisNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: