.. _exhale_class_classicraft_1_1xir_1_1_buyi_target:

Class BuyiTarget
================

- Defined in :ref:`file_icraft-xir_core_compile_target.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< BuyiTarget, CompileTarget, BuyiTargetNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::BuyiTarget
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: