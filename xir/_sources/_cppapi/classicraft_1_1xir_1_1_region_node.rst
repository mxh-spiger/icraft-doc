.. _exhale_class_classicraft_1_1xir_1_1_region_node:

Class RegionNode
================

- Defined in :ref:`file_icraft-xir_ops_region.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< RegionNode, OutputLike >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::RegionNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: