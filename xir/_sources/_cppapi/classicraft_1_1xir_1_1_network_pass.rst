.. _exhale_class_classicraft_1_1xir_1_1_network_pass:

Class NetworkPass
=================

- Defined in :ref:`file_icraft-xir_core_pass.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< NetworkPass, Pass, NetworkPassNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::NetworkPass
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: