.. _exhale_class_classicraft_1_1xir_1_1_reduce_visitor:

Class ReduceVisitor
===================

- Defined in :ref:`file_icraft-xir_core_reflection.h`


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::ReduceVisitor
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: