.. _exhale_class_classicraft_1_1xir_1_1_custom_target_node:

Class CustomTargetNode
======================

- Defined in :ref:`file_icraft-xir_core_compile_target.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< CustomTargetNode, CompileTargetNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::CustomTargetNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: