.. _exhale_class_classicraft_1_1xir_1_1_op_trait:

Template Class OpTrait
======================

- Defined in :ref:`file_icraft-xir_core_operation.h`


Inheritance Relationships
-------------------------

Derived Types
*************

- ``public IsActivate< OpType >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_is_activate`)
- ``public OneResult< OpType >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_one_result`)
- ``public OutputLike< OpType >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_output_like`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::OpTrait
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: