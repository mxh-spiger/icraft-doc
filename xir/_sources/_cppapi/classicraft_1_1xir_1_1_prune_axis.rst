.. _exhale_class_classicraft_1_1xir_1_1_prune_axis:

Class PruneAxis
===============

- Defined in :ref:`file_icraft-xir_ops_prune_axis.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< PruneAxis, PruneAxisNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::PruneAxis
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: