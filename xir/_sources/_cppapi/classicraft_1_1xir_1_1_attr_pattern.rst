.. _exhale_class_classicraft_1_1xir_1_1_attr_pattern:

Class AttrPattern
=================

- Defined in :ref:`file_icraft-xir_core_pattern.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< AttrPattern, Pattern, AttrPatternNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::AttrPattern
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: