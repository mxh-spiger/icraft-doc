.. _exhale_class_classicraft_1_1xir_1_1_layernorm:

Class Layernorm
===============

- Defined in :ref:`file_icraft-xir_ops_layernorm.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< Layernorm, LayernormNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Layernorm
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: