.. _exhale_class_classicraft_1_1xir_1_1_handle:

Class Handle
============

- Defined in :ref:`file_icraft-xir_core_node.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public VirtualBase< Handle, ObjectRef, Node >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)


Derived Types
*************

- ``public VirtualBase< AlignedUnit, Handle, AlignedUnitNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< AttrFieldInfo, Handle, AttrFieldInfoNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< AxisName, Handle, AxisNameNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< CompileTarget, Handle, CompileTargetNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Data, Handle, DataNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< DataType, Handle, DataTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Layout, Handle, LayoutNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< MemType, Handle, MemTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< MergedAxisDistr, Handle, MergedAxisDistrNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Network, Handle, NetworkNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< NetworkView, Handle, NetworkViewNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Operation, Handle, OperationNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Pass, Handle, PassNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< PassContext, Handle, PassContextNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< PassInfo, Handle, PassInfoNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Pattern, Handle, PatternNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< PriorBox, Handle, PriorBoxNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< QuantizedScale, Handle, QuantizedScaleNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< QuantizedScaleArray, Handle, QuantizedScaleArrayNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Handle
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: