.. _exhale_class_classicraft_1_1xir_1_1_add_node:

Class AddNode
=============

- Defined in :ref:`file_icraft-xir_ops_add.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< AddNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::AddNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: