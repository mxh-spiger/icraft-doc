.. _exhale_function_string_8h_1acee3f0570836bfe9f9bd769aafda6e75:

Function icraft::xir::operator>(const std::string&, const String&)
==================================================================

- Defined in :ref:`file_icraft-xir_base_string.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::operator>(const std::string&, const String&)
   :project: Icraft XIR