
.. _program_listing_file_icraft-xir_ops_copy.h:

Program Listing for File copy.h
===============================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_ops_copy.h>` (``icraft-xir\ops\copy.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/operation.h>
   
   namespace icraft::xir {
       class CopyNode : public OpNodeBase<CopyNode, OneResult> {
       public:
           XIR_DLL virtual void validate() const override;
       };
   
       class Copy : public OpBase<Copy, CopyNode> {
       public:
           Copy() = default;
   
           XIR_DLL Copy(Value input);
       };
   }
