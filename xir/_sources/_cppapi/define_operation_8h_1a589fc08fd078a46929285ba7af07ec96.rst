.. _exhale_define_operation_8h_1a589fc08fd078a46929285ba7af07ec96:

Define ICRAFT_ATTR_FIELD
========================

- Defined in :ref:`file_icraft-xir_core_operation.h`


Define Documentation
--------------------


.. doxygendefine:: ICRAFT_ATTR_FIELD
   :project: Icraft XIR