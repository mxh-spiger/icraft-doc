.. _exhale_function_string_8h_1a29ed7eb9bc181d1df7f5c55ca6246b60:

Function icraft::xir::operator<(const char \*, const String&)
=============================================================

- Defined in :ref:`file_icraft-xir_base_string.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::operator<(const char *, const String&)
   :project: Icraft XIR