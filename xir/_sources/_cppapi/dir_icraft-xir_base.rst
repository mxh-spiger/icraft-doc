.. _dir_icraft-xir_base:


Directory base
==============


|exhale_lsh| :ref:`Parent directory <dir_icraft-xir>` (``icraft-xir``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


*Directory path:* ``icraft-xir\base``


Files
-----

- :ref:`file_icraft-xir_base_any.h`
- :ref:`file_icraft-xir_base_array.h`
- :ref:`file_icraft-xir_base_dllexport.h`
- :ref:`file_icraft-xir_base_map.h`
- :ref:`file_icraft-xir_base_object.h`
- :ref:`file_icraft-xir_base_optional.h`
- :ref:`file_icraft-xir_base_string.h`
- :ref:`file_icraft-xir_base_version.h`


