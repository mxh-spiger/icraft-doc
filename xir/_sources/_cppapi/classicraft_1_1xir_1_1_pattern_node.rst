.. _exhale_class_classicraft_1_1xir_1_1_pattern_node:

Class PatternNode
=================

- Defined in :ref:`file_icraft-xir_core_pattern.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public Node`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node`)


Derived Types
*************

- ``public NodeBase< AndPatternNode, PatternNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)
- ``public NodeBase< AnycardsPatternNode, PatternNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)
- ``public NodeBase< AttrPatternNode, PatternNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)
- ``public NodeBase< OpPatternNode, PatternNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)
- ``public NodeBase< OrPatternNode, PatternNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)
- ``public NodeBase< ValuePatternNode, PatternNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::PatternNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: