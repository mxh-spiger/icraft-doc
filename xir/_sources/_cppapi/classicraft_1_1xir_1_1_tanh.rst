.. _exhale_class_classicraft_1_1xir_1_1_tanh:

Class Tanh
==========

- Defined in :ref:`file_icraft-xir_ops_tanh.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< Tanh, TanhNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Tanh
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: