.. _exhale_typedef_reflection_8h_1a119264abacc913e662fe7f5f2b14a813:

Typedef icraft::xir::AttrVistorOptions
======================================

- Defined in :ref:`file_icraft-xir_core_reflection.h`


Typedef Documentation
---------------------


.. doxygentypedef:: icraft::xir::AttrVistorOptions
   :project: Icraft XIR