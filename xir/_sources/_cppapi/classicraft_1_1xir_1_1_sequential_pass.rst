.. _exhale_class_classicraft_1_1xir_1_1_sequential_pass:

Class SequentialPass
====================

- Defined in :ref:`file_icraft-xir_core_pass.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< SequentialPass, Pass, SequentialPassNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::SequentialPass
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: