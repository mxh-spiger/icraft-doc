
.. _file_icraft-xir_core_attrs.h:

File attrs.h
============

|exhale_lsh| :ref:`Parent directory <dir_icraft-xir_core>` (``icraft-xir\core``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-xir\core\attrs.h``)
----------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-xir_core_attrs.h.rst





Includes
--------


- ``icraft-xir/base/any.h``

- ``icraft-xir/base/array.h``

- ``icraft-xir/base/map.h``

- ``icraft-xir/base/optional.h``

- ``icraft-xir/base/string.h``

- ``icraft-xir/core/data.h``

- ``icraft-xir/core/node.h``

- ``icraft-xir/utils/magic_enum.hpp``



Included By
-----------


- :ref:`file_icraft-xir_core_operation.h`




Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__xir`


Classes
-------


- :ref:`exhale_class_classicraft_1_1xir_1_1_attr_field_info`

- :ref:`exhale_class_classicraft_1_1xir_1_1_attr_field_info_node`

