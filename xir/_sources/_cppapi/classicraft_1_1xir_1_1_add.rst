.. _exhale_class_classicraft_1_1xir_1_1_add:

Class Add
=========

- Defined in :ref:`file_icraft-xir_ops_add.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< Add, AddNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Add
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: