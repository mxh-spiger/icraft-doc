.. _exhale_class_classicraft_1_1xir_1_1_pattern:

Class Pattern
=============

- Defined in :ref:`file_icraft-xir_core_pattern.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public VirtualBase< Pattern, Handle, PatternNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)


Derived Types
*************

- ``public VirtualBase< AndPattern, Pattern, AndPatternNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< AnycardsPattern, Pattern, AnycardsPatternNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< AttrPattern, Pattern, AttrPatternNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< OpPattern, Pattern, OpPatternNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< OrPattern, Pattern, OrPatternNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< ValuePattern, Pattern, ValuePatternNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Pattern
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: