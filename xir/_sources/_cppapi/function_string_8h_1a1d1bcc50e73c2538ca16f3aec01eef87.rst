.. _exhale_function_string_8h_1a1d1bcc50e73c2538ca16f3aec01eef87:

Function icraft::xir::operator>(const String&, const String&)
=============================================================

- Defined in :ref:`file_icraft-xir_base_string.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::operator>(const String&, const String&)
   :project: Icraft XIR