
.. _file_icraft-xir_core_node.h:

File node.h
===========

|exhale_lsh| :ref:`Parent directory <dir_icraft-xir_core>` (``icraft-xir\core``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-xir\core\node.h``)
---------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-xir_core_node.h.rst





Includes
--------


- ``icraft-xir/base/object.h``

- ``string_view``



Included By
-----------


- :ref:`file_icraft-xir_core_attrs.h`

- :ref:`file_icraft-xir_core_compile_target.h`

- :ref:`file_icraft-xir_core_layout.h`

- :ref:`file_icraft-xir_core_mem_type.h`

- :ref:`file_icraft-xir_core_pass.h`

- :ref:`file_icraft-xir_core_reflection.h`




Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__xir`


Classes
-------


- :ref:`exhale_class_classicraft_1_1xir_1_1_handle`

- :ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`

- :ref:`exhale_class_classicraft_1_1xir_1_1_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_node_base`

- :ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`

