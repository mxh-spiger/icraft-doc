.. _exhale_function_object_8h_1ac798b7092b8c4c6665631be6db1d4327:

Template Function icraft::xir::GetRef(const ObjType \*)
=======================================================

- Defined in :ref:`file_icraft-xir_base_object.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::GetRef(const ObjType *)
   :project: Icraft XIR