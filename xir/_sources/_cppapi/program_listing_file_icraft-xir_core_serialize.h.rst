
.. _program_listing_file_icraft-xir_core_serialize.h:

Program Listing for File serialize.h
====================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_core_serialize.h>` (``icraft-xir\core\serialize.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <sstream>
   #include <icraft-xir/base/map.h>
   #include <icraft-xir/core/data.h>
   #include <icraft-xir/base/array.h>
   #include <icraft-xir/base/string.h>
   #include <icraft-xir/utils/json.hpp>
   
   
   namespace icraft::xir {
   
       class GetAttrNums : AttrVisitor {
       public:
           virtual void visit(std::string_view key, double& value) { attrCount++; }
           virtual void visit(std::string_view key, int64_t& value) { attrCount++; }
           virtual void visit(std::string_view key, uint64_t& value) { attrCount++; }
           virtual void visit(std::string_view key, int& value) { attrCount++; }
           virtual void visit(std::string_view key, bool& value) { attrCount++; }
           virtual void visit(std::string_view key, std::string& value) { attrCount++; }
           virtual void visit(std::string_view key, ObjectRef& value, const AttrVistorOptions& options) { attrCount++; }
   
           int getAttrNums(const ObjectRef& ref) {
               auto node = ref.as_mutable<Node>();
               node->accept(*this);
               return this->attrCount;
           }
   
       private:
           int attrCount = 0;
       };
   
       class MSGDumper : public AttrVisitor {
       public:
           MSGDumper();
   
           XIR_DLL virtual void visit(std::string_view key, double& value);
           XIR_DLL virtual void visit(std::string_view key, int64_t& value);
           XIR_DLL virtual void visit(std::string_view key, uint64_t& value);
           XIR_DLL virtual void visit(std::string_view key, int& value);
           XIR_DLL virtual void visit(std::string_view key, bool& value);
           XIR_DLL virtual void visit(std::string_view key, std::string& value);
   
           virtual void visit(std::string_view key, ObjectRef& value, const AttrVistorOptions& options);
   
           XIR_DLL void dump(const ObjectRef& ref);
   
           XIR_DLL void write(std::ostream& stream);
   
           struct Impl_MSGD;
           std::shared_ptr<Impl_MSGD> impl_msgd;
       };
   
       class MSGParser : public AttrVisitor {
       public:
           MSGParser();
           XIR_DLL virtual void visit(std::string_view key, double& value);
           virtual void visit(std::string_view key, int64_t& value);
           virtual void visit(std::string_view key, uint64_t& value);
           virtual void visit(std::string_view key, int& value);
           virtual void visit(std::string_view key, bool& value);
           virtual void visit(std::string_view key, std::string& value);
   
           virtual void visit(std::string_view key, ObjectRef& value, const AttrVistorOptions& options);
   
           struct Impl_MSGP;
           std::shared_ptr<Impl_MSGP> impl_msgp;
       };
   
       Network CreateNetworkFromMSG(const std::filesystem::path& file_path);
   
       using Json = nlohmann::json;
   
       class JsonPrinter : public AttrVisitor {
       public:
           XIR_DLL virtual void visit(std::string_view key, double& value);
           virtual void visit(std::string_view key, int64_t& value) {
               visit_(key, value);
           }
           virtual void visit(std::string_view key, uint64_t& value) {
               visit_(key, value);
           }
           virtual void visit(std::string_view key, int& value) {
               visit_(key, value);
           }
           virtual void visit(std::string_view key, bool& value) {
               visit_(key, value);
           }
           virtual void visit(std::string_view key, std::string& value) {
               visit_(key, value);
           }
   
           virtual void visit(std::string_view key, ObjectRef& value, const AttrVistorOptions& options) {
               json_[key] = dump(value, options);
           }
           
           XIR_DLL Json dump(const ObjectRef& ref, const AttrVistorOptions& options = {});
   
       private:
           template <typename T>
           void visit_(std::string_view key, T& value) {
               json_[key] = value;
           }
   
           Json json_;
       };
   
       class Network;
       class JsonParser : public AttrVisitor {
       public:
           XIR_DLL virtual void visit(std::string_view key, double& value);
           virtual void visit(std::string_view key, int64_t& value) {
               visit_(key, value);
           }
           virtual void visit(std::string_view key, uint64_t& value) {
               visit_(key, value);
           }
           virtual void visit(std::string_view key, int& value) {
               visit_(key, value);
           }
           virtual void visit(std::string_view key, bool& value) {
               visit_(key, value);
           }
           virtual void visit(std::string_view key, std::string& value) {
               visit_(key, value);
           }
   
           virtual void visit(std::string_view key, ObjectRef& value, const AttrVistorOptions& options) {
               value = parse(json_[key], options);
           }
           
           XIR_DLL ObjectRef parse(Json json, const AttrVistorOptions& options = {});
   
       private:
           template <typename T>
           void visit_(std::string_view key, T& value) {
               value = json_[key];
           }
   
           Json json_;
   
           XIR_DLL ObjectRef parseNetwork(Network network, Json json);
       };
   
       class ParamsDumper : public AttrVisitor {
       public:
           virtual void visit(std::string_view key, double& value) {}
           virtual void visit(std::string_view key, int64_t& value) {}
           virtual void visit(std::string_view key, uint64_t& value) {}
           virtual void visit(std::string_view key, int& value) {}
           virtual void visit(std::string_view key, bool& value) {}
           virtual void visit(std::string_view key, std::string& value) {}
   
           virtual void visit(std::string_view key, ObjectRef& value, const AttrVistorOptions& options) {
               genParamsTable(value);
           }
   
           ParamsDumper(std::ostream& stream) : stream_(stream) {}
   
           XIR_DLL uint64_t dump(const ObjectRef& ref);
   
           XIR_DLL uint64_t calcMd5(const ObjectRef& ref);
   
       private:
           XIR_DLL void genParamsTable(const ObjectRef& ref);
   
           std::vector<std::tuple<int, uint64_t, const char*>> params_table_;
           std::ostream& stream_;
       };
   
       class ParamsLoader : public AttrVisitor {
       public:
           virtual void visit(std::string_view key, double& value) {}
           virtual void visit(std::string_view key, int64_t& value) {}
           virtual void visit(std::string_view key, uint64_t& value) {}
           virtual void visit(std::string_view key, int& value) {}
           virtual void visit(std::string_view key, bool& value) {}
           virtual void visit(std::string_view key, std::string& value) {}
   
           virtual void visit(std::string_view key, ObjectRef& value, const AttrVistorOptions& options) {
               loadParams(value);
           }
   
           ParamsLoader(std::istream& stream) : stream_(stream) {}
   
           XIR_DLL void load(ObjectRef& ref);
   
       private:
           XIR_DLL void loadParams(ObjectRef& ref);
   
           std::unordered_map<int, uint64_t> offset_map_;
           std::istream& stream_;
       };
   
       class ParamsLazyLoader : public AttrVisitor {
       public:
           virtual void visit(std::string_view key, double& value) {}
           virtual void visit(std::string_view key, int64_t& value) {}
           virtual void visit(std::string_view key, uint64_t& value) {}
           virtual void visit(std::string_view key, int& value) {}
           virtual void visit(std::string_view key, bool& value) {}
           virtual void visit(std::string_view key, std::string& value) {}
   
           virtual void visit(std::string_view key, ObjectRef& value, const AttrVistorOptions& options) {
               loadParams(value);
           }
           
           ParamsLazyLoader(char* mapped_ptr) : mapped_ptr_(mapped_ptr) {}
   
           XIR_DLL void load(ObjectRef& ref);
   
       private:
           XIR_DLL void loadParams(ObjectRef& ref);
   
           static void null_deleter(const char*) {}
   
           std::unordered_map<int, uint64_t> offset_map_;
           char* mapped_ptr_;
       };
   
       XIR_DLL std::string CalcMD5(std::ifstream& stream);
   
       XIR_DLL std::string CalcMD5(char* ptr, uint64_t byte_size);
   
       XIR_DLL std::ostream& operator<<(std::ostream& os, const ObjectRef& ref);
       XIR_DLL std::ostream& operator<<(std::ostream& os, const Object* obj);
   }
