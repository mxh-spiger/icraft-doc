.. _exhale_class_classicraft_1_1xir_1_1_bool:

Class Bool
==========

- Defined in :ref:`file_icraft-xir_core_data.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< Bool, IntImm, IntImmNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Bool
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: