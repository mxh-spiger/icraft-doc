
.. _program_listing_file_icraft-xir_ops_multi_yolo.h:

Program Listing for File multi_yolo.h
=====================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_ops_multi_yolo.h>` (``icraft-xir\ops\multi_yolo.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/operation.h>
   
   namespace icraft::xir {
       class MultiYoloNode : public OpNodeBase<MultiYoloNode, OutputLike> {
       public:
           int64_t class_num;      
           double thresh_value;    
           double nms_thresh;      
           double hier_thresh;     
   
           ICRAFT_DECLARE_ATTRS(MultiYoloNode) {
               ICRAFT_ATTR_FIELD(class_num);
               ICRAFT_ATTR_FIELD(thresh_value);
               ICRAFT_ATTR_FIELD(nms_thresh);
               ICRAFT_ATTR_FIELD(hier_thresh);
           }
   
           XIR_DLL virtual void validate() const override;
       };
   
       class MultiYolo : public OpBase<MultiYolo, MultiYoloNode> {
       public:
           MultiYolo() = default;
   
           XIR_DLL MultiYolo(
               Array<Value> inputs,
               int64_t class_num, 
               double thresh_value, 
               double nms_thresh, 
               double hier_thresh
           );
       };
   }
