.. _exhale_class_classicraft_1_1xir_1_1_or_pattern:

Class OrPattern
===============

- Defined in :ref:`file_icraft-xir_core_pattern.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< OrPattern, Pattern, OrPatternNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::OrPattern
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: