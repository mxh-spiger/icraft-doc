.. _exhale_class_classicraft_1_1xir_1_1_and_pattern_node:

Class AndPatternNode
====================

- Defined in :ref:`file_icraft-xir_core_pattern.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< AndPatternNode, PatternNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::AndPatternNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: