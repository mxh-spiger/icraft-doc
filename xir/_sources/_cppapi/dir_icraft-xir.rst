.. _dir_icraft-xir:


Directory icraft-xir
====================


*Directory path:* ``icraft-xir``

Subdirectories
--------------

- :ref:`dir_icraft-xir_base`
- :ref:`dir_icraft-xir_core`
- :ref:`dir_icraft-xir_ops`



