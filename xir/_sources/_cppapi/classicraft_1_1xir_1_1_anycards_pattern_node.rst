.. _exhale_class_classicraft_1_1xir_1_1_anycards_pattern_node:

Class AnycardsPatternNode
=========================

- Defined in :ref:`file_icraft-xir_core_pattern.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< AnycardsPatternNode, PatternNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::AnycardsPatternNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: