.. _exhale_class_classicraft_1_1xir_1_1_softmax_node:

Class SoftmaxNode
=================

- Defined in :ref:`file_icraft-xir_ops_softmax.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< SoftmaxNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::SoftmaxNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: