
.. _program_listing_file_icraft-xir_ops_expand.h:

Program Listing for File expand.h
=================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_ops_expand.h>` (``icraft-xir\ops\expand.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/operation.h>
   
   namespace icraft::xir {
       class ExpandNode : public OpNodeBase<ExpandNode, OneResult> {
       public:
           Array<IntImm> sizes;            
   
           ICRAFT_DECLARE_ATTRS(ExpandNode) {
               ICRAFT_ATTR_FIELD(sizes);
           }
   
           XIR_DLL virtual void validate() const override;
       };
   
       class Expand : public OpBase<Expand, ExpandNode> {
       public:
           Expand() = default;
   
           XIR_DLL Expand(Value input, Array<IntImm> axis);
       };
   }
