.. _exhale_class_classicraft_1_1xir_1_1_int_imm_node:

Class IntImmNode
================

- Defined in :ref:`file_icraft-xir_core_data.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< IntImmNode, ScalarImmNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::IntImmNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: