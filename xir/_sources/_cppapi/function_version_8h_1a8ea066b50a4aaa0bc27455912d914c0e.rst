.. _exhale_function_version_8h_1a8ea066b50a4aaa0bc27455912d914c0e:

Function icraft::xir::PatchVersionNum
=====================================

- Defined in :ref:`file_icraft-xir_base_version.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::PatchVersionNum()
   :project: Icraft XIR