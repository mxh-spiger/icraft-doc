.. _exhale_class_classicraft_1_1xir_1_1_prior_box:

Class PriorBox
==============

- Defined in :ref:`file_icraft-xir_ops_ssd_output.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< PriorBox, Handle, PriorBoxNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::PriorBox
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: