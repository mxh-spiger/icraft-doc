.. _exhale_class_classicraft_1_1xir_1_1_input:

Class Input
===========

- Defined in :ref:`file_icraft-xir_core_operation.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< Input, InputNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Input
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: