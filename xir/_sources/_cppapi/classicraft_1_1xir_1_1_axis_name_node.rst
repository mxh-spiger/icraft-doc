.. _exhale_class_classicraft_1_1xir_1_1_axis_name_node:

Class AxisNameNode
==================

- Defined in :ref:`file_icraft-xir_core_layout.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< AxisNameNode, Node >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Derived Type
************

- ``public NodeBase< AxisUnitNode, AxisNameNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::AxisNameNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: