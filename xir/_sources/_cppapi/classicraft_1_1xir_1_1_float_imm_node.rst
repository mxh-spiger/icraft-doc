.. _exhale_class_classicraft_1_1xir_1_1_float_imm_node:

Class FloatImmNode
==================

- Defined in :ref:`file_icraft-xir_core_data.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< FloatImmNode, ScalarImmNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::FloatImmNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: