.. _exhale_class_classicraft_1_1xir_1_1_pad_node:

Class PadNode
=============

- Defined in :ref:`file_icraft-xir_ops_pad.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< PadNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::PadNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: