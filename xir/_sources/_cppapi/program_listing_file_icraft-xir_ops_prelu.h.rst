
.. _program_listing_file_icraft-xir_ops_prelu.h:

Program Listing for File prelu.h
================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_ops_prelu.h>` (``icraft-xir\ops\prelu.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/operation.h>
   
   namespace icraft::xir {
       class PreluNode : public OpNodeBase<PreluNode, OneResult, IsActivate> {
       public:
           Params alpha;       
   
           ICRAFT_DECLARE_ATTRS(PreluNode) {
               ICRAFT_ATTR_FIELD(alpha);
           }
   
           XIR_DLL virtual void validate() const override;
       };
   
       class Prelu : public OpBase<Prelu, PreluNode> {
       public:
           Prelu() = default;
   
           XIR_DLL Prelu(Value input, Params alpha);
       };
   }
