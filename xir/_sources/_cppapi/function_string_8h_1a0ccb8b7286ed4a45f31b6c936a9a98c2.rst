.. _exhale_function_string_8h_1a0ccb8b7286ed4a45f31b6c936a9a98c2:

Function icraft::xir::operator==(const char \*, const String&)
==============================================================

- Defined in :ref:`file_icraft-xir_base_string.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::operator==(const char *, const String&)
   :project: Icraft XIR