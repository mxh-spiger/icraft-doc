.. _exhale_class_classicraft_1_1xir_1_1_on_chip_mem:

Class OnChipMem
===============

- Defined in :ref:`file_icraft-xir_core_mem_type.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< OnChipMem, MemType, OnChipMemNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::OnChipMem
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: