.. _exhale_function_string_8h_1a346acdf3143412eaab63e8f3664cd575:

Function icraft::xir::operator+(const String&, const String&)
=============================================================

- Defined in :ref:`file_icraft-xir_base_string.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::operator+(const String&, const String&)
   :project: Icraft XIR