.. _exhale_class_classicraft_1_1xir_1_1_elu_node:

Class EluNode
=============

- Defined in :ref:`file_icraft-xir_ops_elu.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< EluNode, OneResult, IsActivate >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::EluNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: