.. _exhale_variable_optional_8h_1abf9f9d97c8d614e485dc36b63e62a8d3:

Variable icraft::xir::NullOpt
=============================

- Defined in :ref:`file_icraft-xir_base_optional.h`


Variable Documentation
----------------------


.. doxygenvariable:: icraft::xir::NullOpt
   :project: Icraft XIR