.. _exhale_class_classicraft_1_1xir_1_1_node_base:

Template Class NodeBase
=======================

- Defined in :ref:`file_icraft-xir_core_node.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public BaseNode``


Derived Types
*************

- ``public OpNodeBase< AbsNode, OneResult, IsActivate >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< AddNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< AlignAxisNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< AvgpoolNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< BatchnormNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< CastNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< ConcatNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< Conv2dNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< Conv2dTransposeNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< CopyNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< DivideScalarNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< EluNode, OneResult, IsActivate >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< ExpandNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< GeluNode, OneResult, IsActivate >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< HardOpNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< HardsigmoidNode, OneResult, IsActivate >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< HardswishNode, OneResult, IsActivate >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< InputNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< LayernormNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< MatmulNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< MaxpoolNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< MishNode, OneResult, IsActivate >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< MultiYoloNode, OutputLike >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< MultiplyNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< NormalizeNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< OutputNode, OutputLike >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< PadNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< PixelShuffleNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< PreluNode, OneResult, IsActivate >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< PruneAxisNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< RegionNode, OutputLike >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< ReluNode, OneResult, IsActivate >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< ReorgNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< ReshapeNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< ResizeNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< RouteNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< SSDOutputNode, OutputLike >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< SigmoidNode, OneResult, IsActivate >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< SiluNode, OneResult, IsActivate >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< SliceNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< SoftmaxNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< SplitNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< SqueezeNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< SwapOrderNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< TanhNode, OneResult, IsActivate >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< TransposeNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< UpsampleNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)
- ``public OpNodeBase< YoloNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::NodeBase
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: