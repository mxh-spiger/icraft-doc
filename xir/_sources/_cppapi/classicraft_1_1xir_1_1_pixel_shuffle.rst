.. _exhale_class_classicraft_1_1xir_1_1_pixel_shuffle:

Class PixelShuffle
==================

- Defined in :ref:`file_icraft-xir_ops_pixel_shuffle.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< PixelShuffle, PixelShuffleNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::PixelShuffle
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: