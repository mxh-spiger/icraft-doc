.. _exhale_class_classicraft_1_1xir_1_1_hardsigmoid_node:

Class HardsigmoidNode
=====================

- Defined in :ref:`file_icraft-xir_ops_hardsigmoid.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< HardsigmoidNode, OneResult, IsActivate >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::HardsigmoidNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: