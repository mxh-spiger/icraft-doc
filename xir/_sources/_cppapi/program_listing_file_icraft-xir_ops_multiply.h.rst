
.. _program_listing_file_icraft-xir_ops_multiply.h:

Program Listing for File multiply.h
===================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_ops_multiply.h>` (``icraft-xir\ops\multiply.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/operation.h>
   
   namespace icraft::xir {
       class MultiplyNode : public OpNodeBase<MultiplyNode, OneResult> {
       public:
           QuantizedScaleArray cut_scale;  
   
           ICRAFT_DECLARE_ATTRS(MultiplyNode) {
               ICRAFT_ATTR_FIELD(cut_scale);
           };
   
           XIR_DLL virtual void validate() const override;
       };
   
       class Multiply : public OpBase<Multiply, MultiplyNode> {
       public:
           Multiply() = default;
   
           XIR_DLL Multiply(Value lhs, Value rhs);
       };
   }
