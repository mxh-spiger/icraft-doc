.. _exhale_class_classicraft_1_1xir_1_1_exp_quantized_scale_node:

Class ExpQuantizedScaleNode
===========================

- Defined in :ref:`file_icraft-xir_core_data_type.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< ExpQuantizedScaleNode, QuantizedScaleNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::ExpQuantizedScaleNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: