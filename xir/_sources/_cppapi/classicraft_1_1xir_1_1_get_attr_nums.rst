.. _exhale_class_classicraft_1_1xir_1_1_get_attr_nums:

Class GetAttrNums
=================

- Defined in :ref:`file_icraft-xir_core_serialize.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``private AttrVisitor`` (:ref:`exhale_class_classicraft_1_1xir_1_1_attr_visitor`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::GetAttrNums
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: