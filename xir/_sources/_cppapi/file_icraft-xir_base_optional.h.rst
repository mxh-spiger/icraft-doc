
.. _file_icraft-xir_base_optional.h:

File optional.h
===============

|exhale_lsh| :ref:`Parent directory <dir_icraft-xir_base>` (``icraft-xir\base``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-xir\base\optional.h``)
-------------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-xir_base_optional.h.rst





Includes
--------


- ``icraft-xir/base/object.h``

- ``optional`` (:ref:`file_icraft-xir_base_optional.h`)



Included By
-----------


- :ref:`file_icraft-xir_base_array.h`

- :ref:`file_icraft-xir_base_map.h`

- :ref:`file_icraft-xir_core_attrs.h`

- :ref:`file_icraft-xir_core_layout.h`

- :ref:`file_icraft-xir_ops_conv2d_transpose.h`




Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__xir`


Classes
-------


- :ref:`exhale_struct_structicraft_1_1xir_1_1_null_opt_type`

- :ref:`exhale_class_classicraft_1_1xir_1_1_optional`


Functions
---------


- :ref:`exhale_function_optional_8h_1ad0ce019e3fe7379a117a081af3a4c881`

- :ref:`exhale_function_optional_8h_1a6fc9e3d36fec62302706492a13e206ac`

- :ref:`exhale_function_optional_8h_1a9daee3d26c5a475c024034ccf4134954`


Variables
---------


- :ref:`exhale_variable_optional_8h_1abf9f9d97c8d614e485dc36b63e62a8d3`

