.. _exhale_class_classicraft_1_1xir_1_1_prior_box_node:

Class PriorBoxNode
==================

- Defined in :ref:`file_icraft-xir_ops_ssd_output.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< PriorBoxNode, Node >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::PriorBoxNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: