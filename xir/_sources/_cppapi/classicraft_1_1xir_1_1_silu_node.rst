.. _exhale_class_classicraft_1_1xir_1_1_silu_node:

Class SiluNode
==============

- Defined in :ref:`file_icraft-xir_ops_silu.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< SiluNode, OneResult, IsActivate >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::SiluNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: