.. _exhale_class_classicraft_1_1xir_1_1_one_result:

Template Class OneResult
========================

- Defined in :ref:`file_icraft-xir_core_operation.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::OpTrait< OpType >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_trait`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::OneResult
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: