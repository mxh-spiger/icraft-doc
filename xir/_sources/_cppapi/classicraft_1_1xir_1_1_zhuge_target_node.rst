.. _exhale_class_classicraft_1_1xir_1_1_zhuge_target_node:

Class ZhugeTargetNode
=====================

- Defined in :ref:`file_icraft-xir_core_compile_target.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< ZhugeTargetNode, CompileTargetNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::ZhugeTargetNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: