.. _exhale_class_classicraft_1_1xir_1_1_reflection_table:

Class ReflectionTable
=====================

- Defined in :ref:`file_icraft-xir_core_reflection.h`


Nested Relationships
--------------------


Nested Types
************

- :ref:`exhale_class_classicraft_1_1xir_1_1_reflection_table_1_1_registry`


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::ReflectionTable
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: