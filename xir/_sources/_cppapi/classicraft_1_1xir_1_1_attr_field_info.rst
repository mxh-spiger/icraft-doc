.. _exhale_class_classicraft_1_1xir_1_1_attr_field_info:

Class AttrFieldInfo
===================

- Defined in :ref:`file_icraft-xir_core_attrs.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< AttrFieldInfo, Handle, AttrFieldInfoNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::AttrFieldInfo
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: