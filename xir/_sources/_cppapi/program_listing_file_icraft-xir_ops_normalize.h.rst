
.. _program_listing_file_icraft-xir_ops_normalize.h:

Program Listing for File normalize.h
====================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_ops_normalize.h>` (``icraft-xir\ops\normalize.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/operation.h>
   
   namespace icraft::xir {
       class NormalizeNode : public OpNodeBase<NormalizeNode, OneResult> {
       public:
           Array<IntImm> axis;         
           double p;                   
           double eps;                 
   
           ICRAFT_DECLARE_ATTRS(NormalizeNode) {
               ICRAFT_ATTR_FIELD(axis);
               ICRAFT_ATTR_FIELD(p).set_default(-2);
               ICRAFT_ATTR_FIELD(eps).set_default(1e-12);
           }
   
           XIR_DLL virtual void validate() const override;
       };
   
       class Normalize : public OpBase<Normalize, NormalizeNode> {
       public:
           Normalize() = default;
   
           XIR_DLL Normalize(Value input, Array<IntImm> axis, double p = 2, double eps = 1e-5);
       };
   }
