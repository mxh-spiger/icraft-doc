.. _exhale_function_string_8h_1a8aaac987f17f693b727f6f61c82ef59d:

Function icraft::xir::operator>=(const String&, const String&)
==============================================================

- Defined in :ref:`file_icraft-xir_base_string.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::operator>=(const String&, const String&)
   :project: Icraft XIR