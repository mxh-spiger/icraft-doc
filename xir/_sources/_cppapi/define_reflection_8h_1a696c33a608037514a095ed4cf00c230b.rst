.. _exhale_define_reflection_8h_1a696c33a608037514a095ed4cf00c230b:

Define ICRAFT_REGISTER_PASS
===========================

- Defined in :ref:`file_icraft-xir_core_reflection.h`


Define Documentation
--------------------


.. doxygendefine:: ICRAFT_REGISTER_PASS
   :project: Icraft XIR