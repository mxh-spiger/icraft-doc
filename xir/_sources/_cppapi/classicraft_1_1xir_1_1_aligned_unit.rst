.. _exhale_class_classicraft_1_1xir_1_1_aligned_unit:

Class AlignedUnit
=================

- Defined in :ref:`file_icraft-xir_ops_align_axis.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< AlignedUnit, Handle, AlignedUnitNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::AlignedUnit
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: