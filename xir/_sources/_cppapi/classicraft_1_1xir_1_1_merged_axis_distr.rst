.. _exhale_class_classicraft_1_1xir_1_1_merged_axis_distr:

Class MergedAxisDistr
=====================

- Defined in :ref:`file_icraft-xir_core_data_type.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< MergedAxisDistr, Handle, MergedAxisDistrNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::MergedAxisDistr
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: