
.. _program_listing_file_icraft-xir_ops_tanh.h:

Program Listing for File tanh.h
===============================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_ops_tanh.h>` (``icraft-xir\ops\tanh.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/operation.h>
   
   namespace icraft::xir {
       class TanhNode : public OpNodeBase<TanhNode, OneResult, IsActivate> {
       public:
           XIR_DLL virtual void validate() const override;
       };
   
       class Tanh : public OpBase<Tanh, TanhNode> {
       public:
           Tanh() = default;
   
           XIR_DLL Tanh(Value input);
       };
   }
