
.. _program_listing_file_icraft-xir_core_pass.h:

Program Listing for File pass.h
===============================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_core_pass.h>` (``icraft-xir\core\pass.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/node.h>
   #include <icraft-xir/core/reflection.h>
   #include <icraft-xir/base/array.h>
   #include <icraft-xir/base/map.h>
   #include <icraft-xir/base/string.h>
   
   namespace icraft::xir {
       class PassContextNode : public NodeBase<PassContextNode, Node> {
       public:
           uint64_t top_level{ LLONG_MAX };    
           Array<String> required_passes;      
           Array<String> disabled_passes;      
           Map<String, ObjectRef> config;      
   
           virtual void accept(AttrVisitor& visitor) override {
               visitor.visit("opt_level", top_level);
               visitor.visit("required_passes", required_passes);
               visitor.visit("disabled_passes", disabled_passes);
               visitor.visit("config", config);
           };
       };
   
       class PassInfo;
       /*@brief 表示Pass上下文的引用类*/
       class PassContext : public HandleBase<PassContext, Handle, PassContextNode> {
       public:
           PassContext() = default;
   
           XIR_DLL ~PassContext();
   
           XIR_DLL explicit PassContext(
               uint64_t top_level,
               Array<String> required_passes = {},
               Array<String> disabled_passes = {},
               Map<String, ObjectRef> config = {}
           );
   
           XIR_DLL static PassContext Init();
   
           XIR_DLL static PassContext& Current();
   
           XIR_DLL PassContext& requirePass(const std::string& pass_name);
   
           XIR_DLL PassContext& removeRequiredPass(const std::string& pass_name);
   
           XIR_DLL PassContext& disablePass(const std::string& pass_name);
   
           XIR_DLL PassContext& removeDisabledPass(const std::string& pass_name);
           
           XIR_DLL PassContext& setConfig(const std::string& key, ObjectRef value);
   
           template <typename T,
               typename = typename std::enable_if_t<(std::is_base_of_v<ObjectRef, T>)>>
           Optional<T> getConfig(
                   const std::string& key,
                   Optional<T> default_value = NullOpt) const {
               auto& config = (*this)->config;
               if (!config.defined()) return default_value;
               if (config.count(key)) return Downcast<Optional<T>>(config[key]);
               return default_value;
           }
   
           template <typename T,
               typename = typename std::enable_if_t<(std::is_base_of_v<ObjectRef, T>)>>
           Optional<T> getConfig(const std::string& key, T default_value) const {
               return getConfig<T>(key, Optional<T>(default_value));
           }
   
           XIR_DLL bool isPassEnabled(const PassInfo& info) const;
       };
   
       class PassInfoNode : public NodeBase<PassInfoNode, Node> {
       public:
           String name;                    
           uint64_t opt_level;             
           Array<String> required_passes;  
   
           virtual void accept(AttrVisitor& visitor) override {
               visitor.visit("name", name);
               visitor.visit("opt_level", opt_level);
               visitor.visit("required_passes", required_passes);
           };
       };
   
       class PassInfo : public HandleBase<PassInfo, Handle, PassInfoNode> {
       public:
           PassInfo() = default;
   
           XIR_DLL PassInfo(String name, uint64_t opt_level = 0, Array<String> required_passes = {});
       };
   
       class Network;
       class PassNode : public NodeBase<PassNode, Node> {
       public:
           PassInfo pass_info;     
   
           virtual void accept(AttrVisitor& visitor) override {
               visitor.visit("pass_info", pass_info);
           };
   
           virtual Network operator()(Network network, const PassContext& pass_ctx) const = 0;
       };
   
       class Pass : public VirtualBase<Pass, Handle, PassNode> {
       public:
           XIR_DLL Network operator()(Network network, const PassContext& pass_ctx = PassContext::Current()) const;
   
           XIR_DLL static Pass Create(const std::string& name, const std::filesystem::path& dll_path = {});
       };
   
       class NetworkPassNode : public NodeBase<NetworkPassNode, PassNode> {
       public:
           std::function<Network(Network, const PassContext&)> pass_func;  
   
           XIR_DLL virtual Network operator()(Network network, const PassContext& pass_ctx) const override final;
       };
   
       class NetworkPass : public HandleBase<NetworkPass, Pass, NetworkPassNode> {
       public:
           NetworkPass() = default;
   
           XIR_DLL NetworkPass(std::function<Network(Network, const PassContext&)> pass_func, PassInfo pass_info);
       };
   
       class SequentialPassNode : public NodeBase<SequentialPassNode, PassNode> {
       public:
           Array<Pass> passes;     
   
           virtual void accept(AttrVisitor& visitor) override {
               visitor.visit("passes", passes);
               PassNode::accept(visitor);
           };
   
           XIR_DLL virtual Network operator()(Network network, const PassContext& pass_ctx) const override final;
       };
   
       class SequentialPass : public HandleBase<SequentialPass, Pass, SequentialPassNode> {
       public:
           SequentialPass() = default;
   
           XIR_DLL SequentialPass(Array<Pass> passes, PassInfo pass_info = PassInfo("sequential"));
   
       };
   }
