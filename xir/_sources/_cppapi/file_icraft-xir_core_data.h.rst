
.. _file_icraft-xir_core_data.h:

File data.h
===========

|exhale_lsh| :ref:`Parent directory <dir_icraft-xir_core>` (``icraft-xir\core``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-xir\core\data.h``)
---------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-xir_core_data.h.rst





Includes
--------


- ``icraft-xir/base/array.h``

- ``icraft-xir/core/data_type.h``

- ``icraft-xir/core/mem_type.h``

- ``memory``

- ``numeric``



Included By
-----------


- :ref:`file_icraft-xir_core_attrs.h`

- :ref:`file_icraft-xir_core_operation.h`

- :ref:`file_icraft-xir_core_pattern.h`

- :ref:`file_icraft-xir_core_serialize.h`




Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__xir`


Classes
-------


- :ref:`exhale_class_classicraft_1_1xir_1_1_bool`

- :ref:`exhale_class_classicraft_1_1xir_1_1_data`

- :ref:`exhale_class_classicraft_1_1xir_1_1_data_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_float_imm`

- :ref:`exhale_class_classicraft_1_1xir_1_1_float_imm_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_int_imm`

- :ref:`exhale_class_classicraft_1_1xir_1_1_int_imm_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_params`

- :ref:`exhale_class_classicraft_1_1xir_1_1_params_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_scalar_imm`

- :ref:`exhale_class_classicraft_1_1xir_1_1_scalar_imm_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_value`

- :ref:`exhale_class_classicraft_1_1xir_1_1_value_node`


Functions
---------


- :ref:`exhale_function_data_8h_1af1ce218b03c384d2aad50a6295fb096d`

- :ref:`exhale_function_data_8h_1a9c0be3c7885099d93fa52204c5e3ff29`

- :ref:`exhale_function_data_8h_1a13f31d0631d8054ec6cbe16e5aa5c246`

- :ref:`exhale_function_data_8h_1a9bcac01c289f96bdbf3f3d398592098b`

- :ref:`exhale_function_data_8h_1ace1bec728e7401063e26ebc9e83481a0`

- :ref:`exhale_function_data_8h_1aabc2762937ed01fdce00372551019afb`

- :ref:`exhale_function_data_8h_1ae7f269258f554b475a1fb22eb0901162`

- :ref:`exhale_function_data_8h_1a3d2186da2c5fe38cd4cde2ed9fabae42`

- :ref:`exhale_function_data_8h_1ae841cffb9297b1e03dd333546492082a`

