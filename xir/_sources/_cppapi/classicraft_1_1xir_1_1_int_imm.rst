.. _exhale_class_classicraft_1_1xir_1_1_int_imm:

Class IntImm
============

- Defined in :ref:`file_icraft-xir_core_data.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< IntImm, ScalarImm, IntImmNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Derived Type
************

- ``public VirtualBase< Bool, IntImm, IntImmNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::IntImm
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: