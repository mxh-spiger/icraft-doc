.. _exhale_function_version_8h_1aec81871e7ddcd07a5b76e701f54944f2:

Function icraft::xir::MainVersion
=================================

- Defined in :ref:`file_icraft-xir_base_version.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::MainVersion()
   :project: Icraft XIR