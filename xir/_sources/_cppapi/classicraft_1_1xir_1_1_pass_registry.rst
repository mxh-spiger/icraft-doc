.. _exhale_class_classicraft_1_1xir_1_1_pass_registry:

Class PassRegistry
==================

- Defined in :ref:`file_icraft-xir_core_reflection.h`


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::PassRegistry
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: