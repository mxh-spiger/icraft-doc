.. _exhale_class_classicraft_1_1xir_1_1_tanh_node:

Class TanhNode
==============

- Defined in :ref:`file_icraft-xir_ops_tanh.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< TanhNode, OneResult, IsActivate >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::TanhNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: