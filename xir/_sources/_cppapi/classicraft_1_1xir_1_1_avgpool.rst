.. _exhale_class_classicraft_1_1xir_1_1_avgpool:

Class Avgpool
=============

- Defined in :ref:`file_icraft-xir_ops_avgpool.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< Avgpool, AvgpoolNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Avgpool
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: