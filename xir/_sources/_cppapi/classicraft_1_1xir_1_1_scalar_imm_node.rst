.. _exhale_class_classicraft_1_1xir_1_1_scalar_imm_node:

Class ScalarImmNode
===================

- Defined in :ref:`file_icraft-xir_core_data.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public DataNode`` (:ref:`exhale_class_classicraft_1_1xir_1_1_data_node`)


Derived Types
*************

- ``public NodeBase< FloatImmNode, ScalarImmNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)
- ``public NodeBase< IntImmNode, ScalarImmNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::ScalarImmNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: