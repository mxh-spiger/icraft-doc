
.. _program_listing_file_icraft-xir_core_data_type.h:

Program Listing for File data_type.h
====================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_core_data_type.h>` (``icraft-xir\core\data_type.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/layout.h>
   
   namespace icraft::xir {
       class DataTypeNode : public Node {
       private:
           virtual uint64_t bits() const = 0;
   
           virtual uint64_t numElements() const = 0;
   
           friend class DataType;
       };
   
       class FloatImm;
       class QuantizedScale;
       class QuantizedScaleArray;
       class ScalarType;
   
       class DataType : public VirtualBase<DataType, Handle, DataTypeNode> {
       public:
           XIR_DLL uint64_t bits() const;
   
           XIR_DLL uint64_t bytes() const;
   
           XIR_DLL uint64_t numElements() const;
   
           XIR_DLL Optional<Array<FloatImm>> getNormratio() const;
   
           XIR_DLL bool setNormratio(Array<FloatImm> normratio);
   
           XIR_DLL Optional<QuantizedScaleArray> getScale() const;
   
           XIR_DLL bool setScale(QuantizedScaleArray scale);
   
           XIR_DLL ScalarType getStorageType() const;
       };
   
       class ScalarTypeNode : public DataTypeNode {
       public:
           virtual uint64_t numElements() const override {
               return 1;
           }
       };
   
       class ScalarType : public VirtualBase<ScalarType, DataType, ScalarTypeNode> {};
   
       class IntegerTypeNode : public NodeBase<IntegerTypeNode, ScalarTypeNode> {
       private:
           bool sign;          
           uint64_t ibits;     
   
           friend class IntegerType;
       public:
           IntegerTypeNode() = default;
           IntegerTypeNode(bool sign, uint64_t bits) : sign(sign), ibits(bits){}
   
           virtual void accept(AttrVisitor& visitor) override {
               visitor.visit("sign", sign);
               visitor.visit("bits", ibits);
           };
   
           bool reduceAccept(IntegerTypeNode* other, const ReduceVisitor& reduce) const {
               return reduce(sign, other->sign)
                   && reduce(ibits, other->ibits);
           }
   
       private:
           virtual uint64_t bits() const override {
               return this->ibits;
           }
       };
   
       class IntegerType : public HandleBase<IntegerType, ScalarType, IntegerTypeNode> {
       public:
           IntegerType() = default;
   
           XIR_DLL IntegerType(bool sign, int bits);
   
           bool isSigned() const { return (*this)->sign; }
   
           bool isSInt(uint64_t N) const { return (*this)->sign && (*this).bits() == N; }
   
           bool isUInt(uint64_t N) const { return !(*this)->sign && (*this).bits() == N; }
   
           bool isSInt8() const { return isSInt(8); }
   
           bool isUInt8() const { return isUInt(8); }
   
           bool isSInt16() const { return isSInt(16); }
   
           bool isUInt16() const { return isUInt(16); }
   
           bool isSInt32() const { return isSInt(32); }
   
           bool isUInt32() const { return isUInt(32); }
   
           bool isSInt64() const { return isSInt(64); }
   
           bool isUInt64() const { return isUInt(64); }
   
           bool isBool() const { return isUInt(1); }
   
           static IntegerType SInt(uint64_t N) {
               return IntegerType(true, N);
           }
   
           static IntegerType UInt(uint64_t N) {
               return IntegerType(false, N);
           }
   
           static IntegerType SInt8() {
               return SInt(8);
           }
   
           static IntegerType UInt8() {
               return UInt(8);
           }
   
           static IntegerType SInt16() {
               return SInt(16);
           }
   
           static IntegerType UInt16() {
               return UInt(16);
           }
   
           static IntegerType SInt32() {
               return SInt(32);
           }
   
           static IntegerType UInt32() {
               return UInt(32);
           }
   
           static IntegerType SInt64() {
               return SInt(64);
           }
   
           static IntegerType UInt64() {
               return UInt(64);
           }
   
           static IntegerType Bool() {
               return UInt(1);
           }
       };
   
       class FloatTypeNode : public NodeBase<FloatTypeNode, ScalarTypeNode> {
       private:
           int64_t emax;           
           int64_t emin;           
           uint64_t precision;     
           uint64_t fbits;         
   
           friend class FloatType;
   
       public:
           virtual void accept(AttrVisitor& visitor) override {
               visitor.visit("emax", emax);
               visitor.visit("emin", emin);
               visitor.visit("precision", precision);
               visitor.visit("fbits", fbits);
           };
   
           bool reduceAccept(FloatTypeNode* other, const ReduceVisitor& reduce) const {
               return reduce(emax, other->emax)
                   && reduce(emin, other->emin)
                   && reduce(precision, other->precision)
                   && reduce(fbits, other->fbits);
           }
   
       private:
           virtual uint64_t bits() const override {
               return this->fbits;
           }
       };
   
       class FloatType : public HandleBase<FloatType, ScalarType, FloatTypeNode> {
       public:
           class Semantics {
           public:
               int64_t emax;           
               int64_t emin;           
               uint64_t precision;     
               uint64_t bits;          
   
               bool operator==(const Semantics& other) {
                   return other.emax == emax &&
                       other.emin == emin &&
                       other.precision == precision &&
                       other.bits == bits;
               }
           };
   
           FloatType() = default;
   
           XIR_DLL FloatType(int emax, int emin, int precision, int bits);
   
           FloatType(const Semantics& sem)
               : FloatType(sem.emax, sem.emin, sem.precision, sem.bits) {}
   
           Semantics semantics() const {
               return { (*this)->emax, (*this)->emin, (*this)->precision, (*this)->fbits };
           }
   
           bool isBF16() const {
               return this->semantics() == BF16_SEM;
           }
   
           bool isFP16() const {
               return this->semantics() == FP16_SEM;
           }
   
           bool isFP32() const {
               return this->semantics() == FP32_SEM;
           }
   
           bool isFP64() const {
               return this->semantics() == FP64_SEM;
           }
   
           static FloatType BF16() {
               return FloatType(BF16_SEM);
           }
   
           static FloatType FP16() {
               return FloatType(FP16_SEM);
           }
   
           static FloatType FP32() {
               return FloatType(FP32_SEM);
           }
   
           static FloatType FP64() {
               return FloatType(FP64_SEM);
           }
   
           inline static const Semantics BF16_SEM = { 127, -126, 8, 16 };
           inline static const Semantics FP16_SEM = { 15, -14, 11, 16 };
           inline static const Semantics FP32_SEM = { 127, -126, 24, 32 };
           inline static const Semantics FP64_SEM = { 1023, -1022, 53, 64 };
       };
   
       class Bool;
       class IntImm;
       class MergedAxisDistrNode : public NodeBase<MergedAxisDistrNode, Node> {
       public:
           Array<IntImm> merged_axis;      
           // TODO: 使用bitset优化内存
           Array<Bool> valid_mask;         
   
           virtual void accept(AttrVisitor& visitor) override {
               visitor.visit("merged_axis", merged_axis);
               visitor.visit("valid_mask", valid_mask);
           };
   
           bool reduceAccept(MergedAxisDistrNode* other, const ReduceVisitor& reduce) const {
               return reduce(merged_axis, other->merged_axis)
                   && reduce(valid_mask, other->valid_mask);
           }
       };
   
       class MergedAxisDistr : public HandleBase<MergedAxisDistr, Handle, MergedAxisDistrNode> {
       public:
           MergedAxisDistr() = default;
   
           XIR_DLL MergedAxisDistr(Array<IntImm> merged_axis, uint64_t element_num = 0, bool init = true);
   
           XIR_DLL MergedAxisDistr& setMergedAxis(Array<IntImm> merged_axis);
   
           XIR_DLL MergedAxisDistr& append(uint64_t element_num, bool valid);
   
           XIR_DLL MergedAxisDistr& append(const Array<Bool>& other_valid_mask);
   
           XIR_DLL MergedAxisDistr& setValid(uint64_t start, uint64_t end);
   
           XIR_DLL MergedAxisDistr& setUnvalid(uint64_t start, uint64_t end);
   
           XIR_DLL std::vector<std::tuple<uint64_t, uint64_t, bool>> allSections() const;
   
           XIR_DLL std::vector<std::pair<uint64_t, uint64_t>> validSections() const;
   
           XIR_DLL std::vector<std::pair<uint64_t, uint64_t>> unvalidSections() const;
   
           XIR_DLL uint64_t totalValidNum() const;
   
           XIR_DLL uint64_t totalUnvalidNum() const;
   
           XIR_DLL uint64_t totalNum() const;
   
           XIR_DLL bool isValid(uint64_t index) const;
       };
   
       class TensorTypeNode : public NodeBase<TensorTypeNode, DataTypeNode> {
       public:
           ScalarType element_dtype;               
           Array<IntImm> shape;                    
           Layout layout;                          
           Array<MergedAxisDistr> merged_distrs;   
   
           XIR_DLL virtual void accept(AttrVisitor& visitor) override;
   
           bool reduceAccept(TensorTypeNode* other, const ReduceVisitor& reduce) const {
               return reduce(element_dtype, other->element_dtype)
                   && reduce(shape, other->shape)
                   && reduce(layout, other->layout)
                   && reduce(merged_distrs, other->merged_distrs);
           }
   
       private:
           XIR_DLL virtual uint64_t bits() const override;
   
           XIR_DLL virtual uint64_t numElements() const override;
       };
   
       class TensorType : public HandleBase<TensorType, DataType, TensorTypeNode> {
       public:
           TensorType() = default;
   
           XIR_DLL TensorType(
               ScalarType element_type, 
               Array<IntImm> shape, 
               Layout layout, 
               Array<MergedAxisDistr> merged_distrs
           );
   
           XIR_DLL TensorType(ScalarType element_type, Array<IntImm> shape, Layout layout);
   
           XIR_DLL int64_t getDimByAxis(char c) const;
   
           XIR_DLL void setDimByAxis(char c, int64_t dim);
   
           uint64_t dimSize() const {
               return (*this)->shape.size();
           }
   
           XIR_DLL int64_t getDim(int64_t index) const;
   
           XIR_DLL TensorType& setElementDtype(ScalarType dtype);
   
           XIR_DLL TensorType& setShape(Array<IntImm> shape);
   
           XIR_DLL TensorType& setLayout(Layout layout);
   
           XIR_DLL TensorType& setMergedDistrs(Array<MergedAxisDistr> merged_distrs);
   
           XIR_DLL MergedAxisDistr createAddMergedDistr(Array<IntImm> merged_axis);
   
           XIR_DLL Optional<MergedAxisDistr> getMergedDistr(const Array<IntImm>& merged_axis) const;
   
           XIR_DLL TensorType& mutateMergedDistr(
               const Array<IntImm>& merged_axis, 
               int64_t start, 
               int64_t end, 
               bool value
           );
   
           XIR_DLL bool hasNegativeDim() const;
       };
   
       /* ---- 以下数据类型量化相关 ----*/
   
       class BaseQuantizedTypeNode : public NodeBase<BaseQuantizedTypeNode, ScalarTypeNode> {
       public:
           ScalarType storage_dtype;       
           ScalarType expressed_dtype;     
   
           XIR_DLL virtual void accept(AttrVisitor& visitor) override;
   
           bool reduceAccept(BaseQuantizedTypeNode* other, const ReduceVisitor& reduce) const {
               return reduce(storage_dtype, other->storage_dtype)
                   && reduce(expressed_dtype, other->expressed_dtype);
           }
       };
   
       class BaseQuantizedType : public HandleBase<BaseQuantizedType, ScalarType, BaseQuantizedTypeNode> {};
   
       class CalibratedTypeNode : public NodeBase<CalibratedTypeNode, BaseQuantizedTypeNode> {
       public:
           double min;     
           double max;     
           double sat;     
   
           XIR_DLL virtual void accept(AttrVisitor& visitor) override;
   
           bool reduceAccept(CalibratedTypeNode* other, const ReduceVisitor& reduce) const {
               return reduce(min, other->min)
                   && reduce(max, other->max)
                   && reduce(sat, other->sat)
                   && BaseQuantizedTypeNode::reduceAccept(other, reduce);
           }
   
           virtual uint64_t bits() const override {
               return expressed_dtype.bits();
           };
       };
   
       class CalibratedType : public HandleBase<CalibratedType, BaseQuantizedType, CalibratedTypeNode> {
       public:
           CalibratedType() = default;
   
           XIR_DLL CalibratedType(
               double min,
               double max,
               double sat,
               ScalarType storage_dtype,
               ScalarType expressed_dtype = FloatType::FP32()
           );
       };
   
       class FloatImm;
       class NormalizedTypeNode : public NodeBase<NormalizedTypeNode, BaseQuantizedTypeNode> {
       public:
           Array<FloatImm> normratio;      
   
           XIR_DLL virtual void accept(AttrVisitor& visitor) override;
   
           bool reduceAccept(NormalizedTypeNode* other, const ReduceVisitor& reduce) const {
               return reduce(normratio, other->normratio)
                   && BaseQuantizedTypeNode::reduceAccept(other, reduce);
           }
   
           virtual uint64_t bits() const override {
               return expressed_dtype.bits();
           };
       };
   
       class NormalizedType : public HandleBase<NormalizedType, BaseQuantizedType, NormalizedTypeNode> {
       public:
           NormalizedType() = default;
   
           XIR_DLL NormalizedType(
               Array<FloatImm> normratio,
               ScalarType storage_dtype,
               ScalarType expressed_dtype = FloatType::FP32()
           );
       };
   
       class QuantizedScaleNode : public NodeBase<QuantizedScaleNode, Node> {
       public:
           double origin_scale;        
   
           XIR_DLL virtual void accept(AttrVisitor& visitor) override;
   
           bool reduceAccept(QuantizedScaleNode* other, const ReduceVisitor& reduce) const {
               return reduce(origin_scale, other->origin_scale);
           }
       };
   
       class QuantizedScale : public HandleBase<QuantizedScale, Handle, QuantizedScaleNode> {
       public:
           QuantizedScale() = default;
   
           XIR_DLL explicit QuantizedScale(double origin_scale);
       };
   
       class QuantizedScaleArrayNode : public NodeBase<QuantizedScaleArrayNode, Node> {
   
           friend class QuantizedScaleArray;
   
       public:
           Array<FloatImm> origin_scale;       
   
           XIR_DLL virtual void accept(AttrVisitor& visitor) override;
   
           bool reduceAccept(QuantizedScaleArrayNode* other, const ReduceVisitor& reduce) const {
               return reduce(origin_scale, other->origin_scale);
           }
   
       private:
           XIR_DLL virtual const QuantizedScale operator[](size_t idx) const;
   
           XIR_DLL virtual const QuantizedScale front() const;
   
           XIR_DLL virtual const QuantizedScale back() const;
   
           XIR_DLL virtual QuantizedScaleArray split(int begin, int end) const;
       };
   
       class QuantizedScaleArray : public HandleBase<QuantizedScaleArray, Handle, QuantizedScaleArrayNode> {
       public:
           QuantizedScaleArray() {
               auto node = make_object<QuantizedScaleArrayNode>();
               data_ = std::move(node);
           };
   
           XIR_DLL explicit QuantizedScaleArray(Array<FloatImm> origin_scale);
   
           XIR_DLL QuantizedScaleArray(QuantizedScaleArray lhs, QuantizedScaleArray rhs);
   
           XIR_DLL QuantizedScaleArray(const size_t n, const QuantizedScale& val);
   
           XIR_DLL const size_t size() const;
   
           XIR_DLL bool empty() const;
   
           const QuantizedScale operator[](size_t idx) const { return (*this)->operator[](idx); };
   
           const QuantizedScale front() const { return (*this)->front(); };
   
           const QuantizedScale back() const { return (*this)->back(); };
   
           QuantizedScaleArray split(int begin, int end) const { return (*this)->split(begin, end); };
       };
   
       class ExpQuantizedScaleNode : public NodeBase<ExpQuantizedScaleNode, QuantizedScaleNode> {
       public:
           int64_t m;      
           int64_t exp;    
   
           XIR_DLL virtual void accept(AttrVisitor& visitor) override;
   
           bool reduceAccept(ExpQuantizedScaleNode* other, const ReduceVisitor& reduce) const {
               return reduce(m, other->m)
                   && reduce(exp, other->exp)
                   && QuantizedScaleNode::reduceAccept(other, reduce);
           }
       };
   
       class ExpQuantizedScale : public HandleBase<ExpQuantizedScale, QuantizedScale, ExpQuantizedScaleNode> {
       public:
           ExpQuantizedScale() = default;
   
           XIR_DLL ExpQuantizedScale(int64_t m, int64_t exp, double origin_scale);
       };
   
       class ExpQuantizedScaleArrayNode : public NodeBase<ExpQuantizedScaleArrayNode, QuantizedScaleArrayNode> {
       public:
           Array<IntImm> m;        
           Array<IntImm> exp;      
   
           friend class QuantizedScaleArrayNode;
   
           XIR_DLL virtual void accept(AttrVisitor& visitor) override;
   
           bool reduceAccept(ExpQuantizedScaleArrayNode* other, const ReduceVisitor& reduce) const {
               return reduce(m, other->m)
                   && reduce(exp, other->exp)
                   && QuantizedScaleArrayNode::reduceAccept(other, reduce);
           }
   
           XIR_DLL virtual const QuantizedScale operator[](size_t idx) const override;
   
           XIR_DLL virtual const QuantizedScale front() const override;
   
           XIR_DLL virtual const QuantizedScale back() const override;
   
           XIR_DLL virtual QuantizedScaleArray split(int begin, int end) const override;
       };
   
       class ExpQuantizedScaleArray : public HandleBase<ExpQuantizedScaleArray, QuantizedScaleArray, ExpQuantizedScaleArrayNode> {
       public:
           ExpQuantizedScaleArray() {
               auto node = make_object<ExpQuantizedScaleArrayNode>();
               data_ = std::move(node);
           };
   
           XIR_DLL ExpQuantizedScaleArray(Array<IntImm> m, Array<IntImm> exp, Array<FloatImm> origin_scale);
   
           XIR_DLL ExpQuantizedScaleArray(QuantizedScaleArray lhs, QuantizedScaleArray rhs);
   
           XIR_DLL ExpQuantizedScaleArray(const size_t n, QuantizedScale val);
       };
   
       class QuantizedTypeNode : public NodeBase<QuantizedTypeNode, BaseQuantizedTypeNode> {
       public:
           QuantizedScaleArray scale;  
   
           XIR_DLL virtual void accept(AttrVisitor& visitor) override;
   
           bool reduceAccept(QuantizedTypeNode* other, const ReduceVisitor& reduce) const {
               return reduce(scale, other->scale)
                   && BaseQuantizedTypeNode::reduceAccept(other, reduce);
           }
   
           virtual uint64_t bits() const override {
               return this->storage_dtype.bits();
           };
       };
   
       class QuantizedType : public HandleBase<QuantizedType, BaseQuantizedType, QuantizedTypeNode> {
       public:
           QuantizedType() = default;
   
           XIR_DLL QuantizedType(
               QuantizedScaleArray scale,
               ScalarType storage_dtype,
               ScalarType expressed_dtype = FloatType::FP32()
           );
       };
   
   
       class NormalizedQuantizedTypeNode : public NodeBase<NormalizedQuantizedTypeNode, QuantizedTypeNode> {
       public:
           Array<FloatImm> normratio;      
   
           XIR_DLL virtual void accept(AttrVisitor& visitor) override;
   
           bool reduceAccept(NormalizedQuantizedTypeNode* other, const ReduceVisitor& reduce) const {
               return reduce(normratio, other->normratio)
                   && QuantizedTypeNode::reduceAccept(other, reduce);
           }
       };
   
       class NormalizedQuantizedType :
           public HandleBase<NormalizedQuantizedType, QuantizedType, NormalizedQuantizedTypeNode> {
       public:
           NormalizedQuantizedType() = default;
   
           XIR_DLL NormalizedQuantizedType(
               Array<FloatImm> normratio,
               QuantizedScaleArray scale,
               ScalarType storage_dtype,
               ScalarType expressed_dtype = FloatType::FP32()
           );
   
           XIR_DLL NormalizedQuantizedType(
               NormalizedType nomalized,
               QuantizedScaleArray scale
           );
   
           XIR_DLL operator NormalizedType();
       };
   }
