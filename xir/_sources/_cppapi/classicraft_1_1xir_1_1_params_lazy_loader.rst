.. _exhale_class_classicraft_1_1xir_1_1_params_lazy_loader:

Class ParamsLazyLoader
======================

- Defined in :ref:`file_icraft-xir_core_serialize.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public AttrVisitor`` (:ref:`exhale_class_classicraft_1_1xir_1_1_attr_visitor`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::ParamsLazyLoader
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: