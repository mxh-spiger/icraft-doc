
.. _program_listing_file_icraft-xir_core_node.h:

Program Listing for File node.h
===============================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_core_node.h>` (``icraft-xir\core\node.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/base/object.h>
   
   #include <string_view>
   
   namespace icraft::xir {
       namespace detail {
           // 获取类型字符串的辅助函数
           constexpr auto is_pretty(char ch) noexcept {
               return (ch >= 'a' && ch <= 'z')
                   || (ch >= 'A' && ch <= 'Z')
                   || (ch >= '0' && ch <= '9')
                   || (ch == ':');
           }
   
           constexpr auto pretty_name(std::string_view sv) noexcept {
               for (std::size_t n = sv.size() - 1; n > 0; --n) {
                   if (!is_pretty(sv[n])) {
                       sv.remove_prefix(n + 1);
                       break;
                   }
               }
               return sv;
           }
   
           template<typename E>
           constexpr auto n() noexcept {
   #if defined(__GNUC__) || defined(__clang__)
               return pretty_name({ __PRETTY_FUNCTION__, sizeof(__PRETTY_FUNCTION__) - 2 });
   #elif defined(_MSC_VER)
               return pretty_name({ __FUNCSIG__, sizeof(__FUNCSIG__) - 17 });
   #endif
           }
       }
       
       class ReduceVisitor;
       class AttrVisitor;
   
       class Node : public Object {
       public:
           static constexpr std::string_view type_key = "Node";
   
           virtual std::string_view typeKey() const override {
               return type_key;
           }
   
           virtual void accept(AttrVisitor& visitor) = 0;
   
           virtual bool reduceAccept(Node* other, const ReduceVisitor& reduce) const = 0;
       };
   
       template <typename ConcreteNode, typename BaseNode>
       class NodeBase : public BaseNode {
       public:
           using TNodeParent = NodeBase<ConcreteNode, BaseNode>;
   
           static constexpr std::string_view type_key = detail::n<ConcreteNode>();
   
           virtual std::string_view typeKey() const override {
               return type_key;
           }
   
           bool reduceAccept(ConcreteNode* other, const ReduceVisitor& reduce) const {
               ICRAFT_LOG(EXCEPT) << "reduceAccept is not implemented yet for " << type_key;
               return false;
           }
   
           virtual bool reduceAccept(Node* other, const ReduceVisitor& reduce) const override {
               return static_cast<const ConcreteNode*>(this)->
                   reduceAccept(static_cast<ConcreteNode*>(other), reduce);
           }
       };
   
       template <typename ConcreteType, typename BaseType, typename NodeObject>
       class VirtualBase : public BaseType {
       public:
           using NodeType = NodeObject;
   
           static ConcreteType Get(ObjectPtr<Object> node) {
               auto handle = ConcreteType();
               handle.data_ = std::move(node);
               return handle;
           }
   
           const NodeType* operator->() const { return static_cast<const NodeType*>(ObjectRef::get()); }
   
           const NodeType* get() const { return operator->(); }
   
           template <typename ChildType,
               typename = typename std::enable_if_t<std::is_base_of_v<ConcreteType, ChildType>>>
           bool is() const {
               return (*this)->template isInstance<typename ChildType::NodeType>();
           }
   
           template <typename ChildType,
               typename = typename std::enable_if_t<std::is_base_of_v<ConcreteType, ChildType>>>
           ChildType cast() const {
               return Downcast<ChildType>(*this);
           }
   
           ConcreteType clone(int64_t depth = 1) const {
               return Downcast<ConcreteType>(ObjectRef::clone(depth));
           }
   
       protected:
   
           NodeType* get_mutable() const { return static_cast<NodeType*>(ObjectRef::get_mutable()); }
       };
   
       template <typename ConcreteType, typename BaseType, typename NodeObject>
       class HandleBase : public VirtualBase<ConcreteType, BaseType, NodeObject> {
       public:
           static ConcreteType Init() {
               auto node = make_object<NodeObject>();
               return VirtualBase<ConcreteType, BaseType, NodeObject>::Get(std::move(node));
           }
       };
   
       class Handle : public VirtualBase<Handle, ObjectRef, Node> {};
   
   }
