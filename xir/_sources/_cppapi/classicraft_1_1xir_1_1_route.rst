.. _exhale_class_classicraft_1_1xir_1_1_route:

Class Route
===========

- Defined in :ref:`file_icraft-xir_ops_route.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< Route, RouteNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Route
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: