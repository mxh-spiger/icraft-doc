.. _dir_icraft-xir_core:


Directory core
==============


|exhale_lsh| :ref:`Parent directory <dir_icraft-xir>` (``icraft-xir``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


*Directory path:* ``icraft-xir\core``


Files
-----

- :ref:`file_icraft-xir_core_attrs.h`
- :ref:`file_icraft-xir_core_compile_target.h`
- :ref:`file_icraft-xir_core_data.h`
- :ref:`file_icraft-xir_core_data_type.h`
- :ref:`file_icraft-xir_core_functor.h`
- :ref:`file_icraft-xir_core_layout.h`
- :ref:`file_icraft-xir_core_mem_type.h`
- :ref:`file_icraft-xir_core_network.h`
- :ref:`file_icraft-xir_core_node.h`
- :ref:`file_icraft-xir_core_operation.h`
- :ref:`file_icraft-xir_core_pass.h`
- :ref:`file_icraft-xir_core_pattern.h`
- :ref:`file_icraft-xir_core_reflection.h`
- :ref:`file_icraft-xir_core_serialize.h`


