
.. _program_listing_file_icraft-xir_core_network.h:

Program Listing for File network.h
==================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_core_network.h>` (``icraft-xir\core\network.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/base/map.h>
   #include <icraft-xir/base/string.h>
   #include <icraft-xir/core/operation.h>
   #include <icraft-xir/core/pattern.h>
   #include <icraft-xir/core/serialize.h>
   #include <icraft-xir/base/version.h>
   #include <unordered_set>
   
   
   namespace icraft::ir { class Network; }
   
   
   namespace icraft::xir {
       enum class Framework {
           PYTORCH,        
           CAFFE,          
           TENSORFLOW,     
           DARKNET,        
           ONNX            
       };
   
       class NetworkRewriter;
       class NetworkView;
       class NetworkNode : public NodeBase<NetworkNode, Node> {
       public:
           std::string name;                                               
           Framework framework_kind;                                       
           std::string framework_version;                                  
           std::string icraft_xir_version;                                 
           std::string icraft_version;                                     
           Array<Operation> ops;                                           
           uint64_t params_bytes = 0;                                      
           std::string params_md5 = "d41d8cd98f00b204e9800998ecf8427e";    
   
           Map<String, ObjectRef> tags;
   
       private:
           std::unordered_map<int64_t, int64_t> op_index_map_;
           std::unordered_map<int64_t, Value> value_map_;
           std::unordered_map<int64_t, std::unordered_set<int64_t>> value_uses_map_;
   
           NetworkRewriter* rewriter_;
   
           int64_t values_id_ = 0;
           int64_t ops_id_ = 0;
           void* mapped_addr_ = nullptr;
           uint64_t mapped_byte_size_;
           friend Network;
           friend bool _OverrideNetworkEqual(const NetworkNode*, const NetworkNode*);
           friend Network _OverrideNetworkClone(const Network&);
   
       public:
           XIR_DLL virtual void accept(AttrVisitor& visitor) override;
   
           XIR_DLL bool reduceAccept(NetworkNode* other, const ReduceVisitor& reduce) const;
   
           XIR_DLL ~NetworkNode();
       };
   
       class Network : public HandleBase<Network, Handle, NetworkNode> {
       public:
           Network() = default;
   
           XIR_DLL Network(
               const std::string& name, 
               Framework framework_kind, 
               const std::string& framework_version
           );
   
           XIR_DLL Network& setTag(const std::string& key, ObjectRef value);
   
           XIR_DLL std::optional<ObjectRef> getTag(const std::string& key) const;
   
           XIR_DLL bool removeTag(const std::string& key) const;
   
           XIR_DLL Network& insertOp(const Array<Operation>::const_iterator& it, Operation op);
   
   
           XIR_DLL Network& insertOp(uint64_t op_index, Operation op);
   
           XIR_DLL Network& insertOpById(int64_t op_id, Operation op);
   
           XIR_DLL Network& addOp(Operation op);
   
           XIR_DLL Network& removeOpById(int64_t op_id);
   
           template <typename F,
               typename = std::enable_if_t<std::is_same_v<bool, std::invoke_result_t<F, Operation>>>>
           Network& removeOp(F f) {
               for (auto op : (*this)->ops) {
                   if (f(op)) removeOpById(op->op_id);
               }
               return *this;
           }
   
           XIR_DLL Network& replaceOpById(int64_t op_id, Operation op);
   
           XIR_DLL Network& replaceOpByIdKeepUses(int64_t op_id, Operation op);
   
           Network& replaceOp(Operation src, Operation dest) {
               return replaceOpById(src->op_id, dest);
           }
   
           Network& replaceOpKeepUses(Operation src, Operation dest) {
               return replaceOpByIdKeepUses(src->op_id, dest);
           }
   
           XIR_DLL Operation operator[](uint64_t index) const;
   
           XIR_DLL std::vector<Value> inputs() const;
   
           XIR_DLL std::vector<Value> outputs() const;
   
           XIR_DLL Operation getOpById(int64_t op_id) const;
   
           XIR_DLL Array<Operation> getOpsById(const Array<IntImm>& op_ids) const;
   
           XIR_DLL const Value& getValueById(int64_t v_id) const;
   
           XIR_DLL Array<Value> getValuesById(const Array<IntImm>& value_ids) const;
   
           XIR_DLL Array<Operation> getUsesOpByValueId(int64_t v_id) const;
   
           XIR_DLL size_t getUsesNumByValueId(int64_t v_id) const;
   
           XIR_DLL uint64_t getOpIndexById(int64_t op_id) const;
   
           XIR_DLL Operation getPreOpById(int64_t op_id) const;
           XIR_DLL Operation getNextOpById(int64_t op_id) const;
   
           XIR_DLL Array<Operation> getProducersByOpId(int64_t op_id) const;
   
           XIR_DLL Array<Operation> getConsumersByOpId(int64_t op_id) const;
   
           XIR_DLL Json toJson(bool calc_md5 = true) const;
   
           void dumpJson(std::ostream& stream, bool calc_md5 = true) const {
               stream << toJson(calc_md5);
           }
   
           XIR_DLL void dumpMSG(std::ostream& stream, bool calc_md5 = true) const;
   
           XIR_DLL void dumpJsonToFile(const std::filesystem::path& file_path, bool calc_md5 = true) const;
   
           XIR_DLL void dumpMSGToFile(const std::filesystem::path& file_path, bool calc_md5 = true) const;
   
           static Network CreateFromJson(Json json) {
               return Downcast<Network>(JsonParser().parse(std::move(json)));
           }
   
           XIR_DLL static Network CreateFromJsonFile(const std::filesystem::path& file_path);
   
           XIR_DLL static Network CreateFromMSGFile(const std::filesystem::path& file_path);
   
           void dumpParams(std::ostream& stream) const {
               ParamsDumper(stream).dump(*this);
           }
   
           XIR_DLL void dumpParamsToFile(const std::filesystem::path& file_path) const;
   
           XIR_DLL void loadParams(std::ifstream& stream);
   
           XIR_DLL void loadParamsFromFile(const std::filesystem::path& file_path);
   
           XIR_DLL void lazyLoadParams(char* mapped_ptr, uint64_t byte_size);
   
           XIR_DLL void lazyLoadParamsFromFile(const std::filesystem::path& file_path);
   
           XIR_DLL MatchResults search(const Pattern& pattern) const;
   
           XIR_DLL MatchGroup searchOnce(const Pattern& pattern) const;
   
           using PatternCallback = std::function<void(Network& network, const MatchGroup& result)>;
   
           XIR_DLL void rewrite(const Pattern& pattern, PatternCallback callback, uint64_t times = 1000);
   
           XIR_DLL Network& replaceGroup(const MatchGroup& src, Operation dest);
   
           using ValueUsesInfo = std::vector<std::tuple<Operation, uint64_t>>;
   
           XIR_DLL ValueUsesInfo getUsesInfo(const Value& v) const;
   
           XIR_DLL ValueUsesInfo getUsesInfoExceptMatch(const Value& v, const MatchGroup& match) const;
   
           XIR_DLL Network& connect(const Value& from, Operation to, uint64_t index);
   
           Network& connect(const Value& from, Operation to) {
               return connect(from, to, to->inputs.size());
           }
   
           XIR_DLL Network& connect(const Value& from, const ValueUsesInfo& to);
   
           XIR_DLL NetworkRewriter& rewriter() const;
   
           XIR_DLL static Network CreateFromLegacyNetwork(icraft::ir::Network& network_legacy);
   
           XIR_DLL NetworkView view(Array<Operation> ops) const;
   
           XIR_DLL NetworkView view(const std::vector<int64_t>& op_ids) const;
   
           XIR_DLL NetworkView view(uint64_t start_index, uint64_t end_index) const;
   
           XIR_DLL NetworkView view(uint64_t start_index = 0) const;
   
           XIR_DLL operator NetworkView() const;
   
       private:
           void removeValueUses(const Value& v, int64_t op_id);
           void removeValueUsesById(int64_t v_id, int64_t op_id);
           void addValueUses(const Value& v, int64_t op_id);
           void addValueUsesById(int64_t v_id, int64_t op_id);
           void changeOpIdById(int64_t old_id, int64_t new_id);
           void changeValueIdById(int64_t old_id, int64_t new_id);
           int64_t genValueId();
           int64_t genOpId();
           bool checkIsFirstOpById(int64_t op_id) const;
           bool checkIsLastOpById(int64_t op_id) const;
   
           friend class Operation;
           friend class Value;
           friend Network _OverrideNetworkClone(const Network&);
       };
   
       class NetworkRewriter {
       public:
           void Continue() {is_continue = true;}
   
           void Break() { is_break = true; }
   
       private:
           bool is_continue = false;
           bool is_break = false;
           uint64_t current = 0;
   
           void rewrite(
               Network& network,
               const Pattern& pattern,
               Network::PatternCallback callback,
               uint64_t times
           );
           friend class Network;
       };
   
       class NetworkViewNode : public NodeBase<NetworkViewNode, Node> {
       public:
           Array<Operation> ops;   
   
           virtual void accept(AttrVisitor& visitor) override {}
   
       private:
           std::vector<Value> inputs_;
           std::vector<Value> outputs_;
           friend class NetworkView;
       };
   
       class NetworkView : public HandleBase<NetworkView, Handle, NetworkViewNode> {
       public:
           NetworkView() = default;
   
       private:
           NetworkView(Array<Operation> ops);
           friend class Network;
   
       public:
           XIR_DLL const std::vector<Value>& inputs() const;
   
           XIR_DLL const std::vector<Value>& outputs() const;
   
           XIR_DLL Operation operator[](uint64_t index) const;
   
           XIR_DLL Network network() const;
   
           XIR_DLL NetworkView view(Array<Operation> ops) const;
   
           XIR_DLL NetworkView view(const std::vector<int64_t>& op_ids) const;
   
           XIR_DLL NetworkView view(uint64_t start_index, uint64_t end_index) const;
   
           XIR_DLL NetworkView view(uint64_t start_index) const;
       };
   }
