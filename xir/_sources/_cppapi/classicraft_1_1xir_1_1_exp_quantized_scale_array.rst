.. _exhale_class_classicraft_1_1xir_1_1_exp_quantized_scale_array:

Class ExpQuantizedScaleArray
============================

- Defined in :ref:`file_icraft-xir_core_data_type.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< ExpQuantizedScaleArray, QuantizedScaleArray, ExpQuantizedScaleArrayNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::ExpQuantizedScaleArray
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: