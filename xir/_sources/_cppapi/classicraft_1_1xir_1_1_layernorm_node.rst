.. _exhale_class_classicraft_1_1xir_1_1_layernorm_node:

Class LayernormNode
===================

- Defined in :ref:`file_icraft-xir_ops_layernorm.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< LayernormNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::LayernormNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: