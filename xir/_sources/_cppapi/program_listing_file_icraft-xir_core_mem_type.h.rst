
.. _program_listing_file_icraft-xir_core_mem_type.h:

Program Listing for File mem_type.h
===================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_core_mem_type.h>` (``icraft-xir\core\mem_type.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/node.h>
   #include <icraft-xir/core/reflection.h>
   #include <icraft-xir/base/dllexport.h>
   
   #include <regex>
   
   namespace icraft::xir {
       class MemTypeNode : public Node {
       public:
           virtual uint64_t getStorageBits(uint64_t bits) const = 0;
       };
   
       class HostMemNode;
       class OnChipMemNode;
       class ExternalMemNode;
       class MemType : public VirtualBase<MemType, Handle, MemTypeNode> {};
   
       class HostMemNode : public NodeBase<HostMemNode, MemTypeNode> {
       public:
           virtual void accept(AttrVisitor& visitor) override {};
   
           bool reduceAccept(HostMemNode* other, const ReduceVisitor& reduce) const {
               return true;
           }
   
           XIR_DLL virtual uint64_t getStorageBits(uint64_t bits) const override;
       };
   
       class HostMem : public HandleBase<HostMem, MemType, HostMemNode> {};
   
       class OnChipMemNode : public NodeBase<OnChipMemNode, MemTypeNode> {
       public:
           uint64_t addr;      
   
           virtual void accept(AttrVisitor& visitor) override {
               visitor.visit("addr", addr);
           };
   
           bool reduceAccept(OnChipMemNode* other, const ReduceVisitor& reduce) const {
               return reduce(addr, other->addr);
           }
   
           virtual uint64_t getStorageBits(uint64_t bits) const override {
               return bits;
           }
       };
   
       class OnChipMem : public HandleBase<OnChipMem, MemType, OnChipMemNode> {
       public:
           OnChipMem() = default;
   
           XIR_DLL explicit OnChipMem(uint64_t addr);
   
           XIR_DLL OnChipMem& setAddr(uint64_t addr);
       };
   
       class ExternalMemNode : public NodeBase<ExternalMemNode, MemTypeNode> {
       public:
           uint64_t addr;      
   
           virtual void accept(AttrVisitor& visitor) override {
               visitor.visit("addr", addr);
           };
   
           bool reduceAccept(ExternalMemNode* other, const ReduceVisitor& reduce) const {
               return reduce(addr, other->addr);
           }
   
           virtual uint64_t getStorageBits(uint64_t bits) const override {
               return bits;
           }
       };
       
       class ExternalMem : public HandleBase<ExternalMem, MemType, ExternalMemNode> {
       public:
           ExternalMem() = default;
   
           XIR_DLL explicit ExternalMem(uint64_t addr);
   
           XIR_DLL ExternalMem& setAddr(uint64_t addr);
       };
   }
