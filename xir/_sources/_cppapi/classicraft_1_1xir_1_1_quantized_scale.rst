.. _exhale_class_classicraft_1_1xir_1_1_quantized_scale:

Class QuantizedScale
====================

- Defined in :ref:`file_icraft-xir_core_data_type.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< QuantizedScale, Handle, QuantizedScaleNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Derived Type
************

- ``public VirtualBase< ExpQuantizedScale, QuantizedScale, ExpQuantizedScaleNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::QuantizedScale
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: