.. _exhale_class_classicraft_1_1xir_1_1_handle_base:

Template Class HandleBase
=========================

- Defined in :ref:`file_icraft-xir_core_node.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::VirtualBase< ConcreteType, BaseType, NodeObject >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)


Derived Types
*************

- ``public OpBase< Abs, AbsNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< Add, AddNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< AlignAxis, AlignAxisNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< Avgpool, AvgpoolNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< Batchnorm, BatchnormNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< Cast, CastNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< Concat, ConcatNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< Conv2d, Conv2dNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< Conv2dTranspose, Conv2dTransposeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< Copy, CopyNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< DivideScalar, DivideScalarNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< Elu, EluNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< Expand, ExpandNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< Gelu, GeluNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< HardOp, HardOpNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< Hardsigmoid, HardsigmoidNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< Hardswish, HardswishNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< Input, InputNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< Layernorm, LayernormNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< Matmul, MatmulNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< Maxpool, MaxpoolNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< Mish, MishNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< MultiYolo, MultiYoloNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< Multiply, MultiplyNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< Normalize, NormalizeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< Output, OutputNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< Pad, PadNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< PixelShuffle, PixelShuffleNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< Prelu, PreluNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< PruneAxis, PruneAxisNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< Region, RegionNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< Relu, ReluNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< Reorg, ReorgNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< Reshape, ReshapeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< Resize, ResizeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< Route, RouteNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< SSDOutput, SSDOutputNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< Sigmoid, SigmoidNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< Silu, SiluNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< Slice, SliceNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< Softmax, SoftmaxNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< Split, SplitNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< Squeeze, SqueezeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< SwapOrder, SwapOrderNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< Tanh, TanhNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< Transpose, TransposeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< Upsample, UpsampleNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)
- ``public OpBase< Yolo, YoloNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::HandleBase
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: