
.. _file_icraft-xir_base_any.h:

File any.h
==========

|exhale_lsh| :ref:`Parent directory <dir_icraft-xir_base>` (``icraft-xir\base``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-xir\base\any.h``)
--------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-xir_base_any.h.rst





Includes
--------


- ``any`` (:ref:`file_icraft-xir_base_any.h`)

- ``icraft-xir/base/object.h``

- ``icraft-xir/utils/magic_enum.hpp``

- ``optional`` (:ref:`file_icraft-xir_base_optional.h`)



Included By
-----------


- :ref:`file_icraft-xir_core_attrs.h`

- :ref:`file_icraft-xir_core_pattern.h`




Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__xir`


Classes
-------


- :ref:`exhale_class_classicraft_1_1xir_1_1_any`

- :ref:`exhale_class_classicraft_1_1xir_1_1_any_node`

