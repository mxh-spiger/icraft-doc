.. _exhale_class_classicraft_1_1xir_1_1_string_node_1_1_from_std:

Class StringNode::FromStd
=========================

- Defined in :ref:`file_icraft-xir_base_string.h`


Nested Relationships
--------------------

This class is a nested type of :ref:`exhale_class_classicraft_1_1xir_1_1_string_node`.


Inheritance Relationships
-------------------------

Base Type
*********

- ``public StringNode`` (:ref:`exhale_class_classicraft_1_1xir_1_1_string_node`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::StringNode::FromStd
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: