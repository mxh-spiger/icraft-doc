.. _exhale_class_classicraft_1_1xir_1_1_mish_node:

Class MishNode
==============

- Defined in :ref:`file_icraft-xir_ops_mish.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< MishNode, OneResult, IsActivate >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::MishNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: