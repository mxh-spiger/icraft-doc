.. _exhale_class_classicraft_1_1xir_1_1_pass_info_node:

Class PassInfoNode
==================

- Defined in :ref:`file_icraft-xir_core_pass.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< PassInfoNode, Node >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::PassInfoNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: