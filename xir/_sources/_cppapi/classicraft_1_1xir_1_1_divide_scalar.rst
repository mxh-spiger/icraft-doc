.. _exhale_class_classicraft_1_1xir_1_1_divide_scalar:

Class DivideScalar
==================

- Defined in :ref:`file_icraft-xir_ops_divide_scalar.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< DivideScalar, DivideScalarNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::DivideScalar
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: