
.. _file_icraft-xir_core_layout.h:

File layout.h
=============

|exhale_lsh| :ref:`Parent directory <dir_icraft-xir_core>` (``icraft-xir\core``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-xir\core\layout.h``)
-----------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-xir_core_layout.h.rst





Includes
--------


- ``icraft-xir/base/dllexport.h``

- ``icraft-xir/base/optional.h``

- ``icraft-xir/core/node.h``

- ``icraft-xir/core/reflection.h``

- ``regex``



Included By
-----------


- :ref:`file_icraft-xir_core_data_type.h`




Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__xir`


Classes
-------


- :ref:`exhale_class_classicraft_1_1xir_1_1_axis_name`

- :ref:`exhale_class_classicraft_1_1xir_1_1_axis_name_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_axis_unit`

- :ref:`exhale_class_classicraft_1_1xir_1_1_axis_unit_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_layout`

- :ref:`exhale_class_classicraft_1_1xir_1_1_layout_node`

