.. _exhale_function_string_8h_1ab6704f416891347d26ebbc5bc9401a3a:

Function icraft::xir::operator<(const String&, const char \*)
=============================================================

- Defined in :ref:`file_icraft-xir_base_string.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::operator<(const String&, const char *)
   :project: Icraft XIR