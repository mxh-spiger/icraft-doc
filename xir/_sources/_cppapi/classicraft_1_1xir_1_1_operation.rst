.. _exhale_class_classicraft_1_1xir_1_1_operation:

Class Operation
===============

- Defined in :ref:`file_icraft-xir_core_operation.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public VirtualBase< Operation, Handle, OperationNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)


Derived Types
*************

- ``public VirtualBase< Abs, Operation, AbsNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Add, Operation, AddNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< AlignAxis, Operation, AlignAxisNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Avgpool, Operation, AvgpoolNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Batchnorm, Operation, BatchnormNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Cast, Operation, CastNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Concat, Operation, ConcatNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Conv2d, Operation, Conv2dNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Conv2dTranspose, Operation, Conv2dTransposeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Copy, Operation, CopyNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< DivideScalar, Operation, DivideScalarNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Elu, Operation, EluNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Expand, Operation, ExpandNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Gelu, Operation, GeluNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< HardOp, Operation, HardOpNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Hardsigmoid, Operation, HardsigmoidNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Hardswish, Operation, HardswishNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Input, Operation, InputNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Layernorm, Operation, LayernormNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Matmul, Operation, MatmulNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Maxpool, Operation, MaxpoolNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Mish, Operation, MishNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< MultiYolo, Operation, MultiYoloNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Multiply, Operation, MultiplyNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Normalize, Operation, NormalizeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< OpType, Operation, OpNodeType >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Output, Operation, OutputNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Pad, Operation, PadNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< PixelShuffle, Operation, PixelShuffleNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Prelu, Operation, PreluNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< PruneAxis, Operation, PruneAxisNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Region, Operation, RegionNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Relu, Operation, ReluNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Reorg, Operation, ReorgNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Reshape, Operation, ReshapeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Resize, Operation, ResizeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Route, Operation, RouteNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< SSDOutput, Operation, SSDOutputNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Sigmoid, Operation, SigmoidNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Silu, Operation, SiluNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Slice, Operation, SliceNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Softmax, Operation, SoftmaxNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Split, Operation, SplitNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Squeeze, Operation, SqueezeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< SwapOrder, Operation, SwapOrderNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Tanh, Operation, TanhNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Transpose, Operation, TransposeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Upsample, Operation, UpsampleNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Yolo, Operation, YoloNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Operation
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: