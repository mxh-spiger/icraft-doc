.. _exhale_class_classicraft_1_1xir_1_1_integer_type_node:

Class IntegerTypeNode
=====================

- Defined in :ref:`file_icraft-xir_core_data_type.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< IntegerTypeNode, ScalarTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::IntegerTypeNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: