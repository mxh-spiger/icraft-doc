
.. _program_listing_file_icraft-xir_base_dllexport.h:

Program Listing for File dllexport.h
====================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_base_dllexport.h>` (``icraft-xir\base\dllexport.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   
   #ifdef _WIN32
   #ifdef XIR_DLL_EXPORTS
   #define XIR_DLL _declspec(dllexport)
   #else
   #define XIR_DLL _declspec(dllimport)
   #endif
   #else
   #define XIR_DLL
   #endif
   
