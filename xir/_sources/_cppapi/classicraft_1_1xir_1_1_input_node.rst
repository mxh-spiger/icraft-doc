.. _exhale_class_classicraft_1_1xir_1_1_input_node:

Class InputNode
===============

- Defined in :ref:`file_icraft-xir_core_operation.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< InputNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::InputNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: