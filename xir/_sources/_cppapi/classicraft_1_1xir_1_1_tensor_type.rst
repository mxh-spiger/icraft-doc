.. _exhale_class_classicraft_1_1xir_1_1_tensor_type:

Class TensorType
================

- Defined in :ref:`file_icraft-xir_core_data_type.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< TensorType, DataType, TensorTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::TensorType
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: