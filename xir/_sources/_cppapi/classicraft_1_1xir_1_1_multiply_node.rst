.. _exhale_class_classicraft_1_1xir_1_1_multiply_node:

Class MultiplyNode
==================

- Defined in :ref:`file_icraft-xir_ops_multiply.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< MultiplyNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::MultiplyNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: