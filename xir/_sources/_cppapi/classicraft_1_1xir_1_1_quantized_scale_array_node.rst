.. _exhale_class_classicraft_1_1xir_1_1_quantized_scale_array_node:

Class QuantizedScaleArrayNode
=============================

- Defined in :ref:`file_icraft-xir_core_data_type.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< QuantizedScaleArrayNode, Node >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Derived Type
************

- ``public NodeBase< ExpQuantizedScaleArrayNode, QuantizedScaleArrayNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::QuantizedScaleArrayNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: