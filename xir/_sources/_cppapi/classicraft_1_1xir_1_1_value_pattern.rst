.. _exhale_class_classicraft_1_1xir_1_1_value_pattern:

Class ValuePattern
==================

- Defined in :ref:`file_icraft-xir_core_pattern.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< ValuePattern, Pattern, ValuePatternNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::ValuePattern
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: