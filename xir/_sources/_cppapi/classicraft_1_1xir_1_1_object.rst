.. _exhale_class_classicraft_1_1xir_1_1_object:

Class Object
============

- Defined in :ref:`file_icraft-xir_base_object.h`


Inheritance Relationships
-------------------------

Derived Types
*************

- ``public AnyNode`` (:ref:`exhale_class_classicraft_1_1xir_1_1_any_node`)
- ``public ArrayNode`` (:ref:`exhale_class_classicraft_1_1xir_1_1_array_node`)
- ``public MapNode`` (:ref:`exhale_class_classicraft_1_1xir_1_1_map_node`)
- ``public Node`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node`)
- ``public StringNode`` (:ref:`exhale_class_classicraft_1_1xir_1_1_string_node`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Object
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: