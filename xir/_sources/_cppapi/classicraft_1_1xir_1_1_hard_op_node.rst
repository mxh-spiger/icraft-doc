.. _exhale_class_classicraft_1_1xir_1_1_hard_op_node:

Class HardOpNode
================

- Defined in :ref:`file_icraft-xir_ops_hard_op.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< HardOpNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::HardOpNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: