.. _exhale_class_classicraft_1_1xir_1_1_map:

Template Class Map
==================

- Defined in :ref:`file_icraft-xir_base_map.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public ObjectRef`` (:ref:`exhale_class_classicraft_1_1xir_1_1_object_ref`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Map
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: