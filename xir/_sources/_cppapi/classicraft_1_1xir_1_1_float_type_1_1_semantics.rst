.. _exhale_class_classicraft_1_1xir_1_1_float_type_1_1_semantics:

Class FloatType::Semantics
==========================

- Defined in :ref:`file_icraft-xir_core_data_type.h`


Nested Relationships
--------------------

This class is a nested type of :ref:`exhale_class_classicraft_1_1xir_1_1_float_type`.


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::FloatType::Semantics
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: