.. _exhale_class_classicraft_1_1xir_1_1_map_node:

Class MapNode
=============

- Defined in :ref:`file_icraft-xir_base_map.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public Object`` (:ref:`exhale_class_classicraft_1_1xir_1_1_object`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::MapNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: