.. _exhale_class_classicraft_1_1xir_1_1_layout:

Class Layout
============

- Defined in :ref:`file_icraft-xir_core_layout.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< Layout, Handle, LayoutNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Layout
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: