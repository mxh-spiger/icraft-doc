.. _exhale_class_classicraft_1_1xir_1_1_swap_order_node:

Class SwapOrderNode
===================

- Defined in :ref:`file_icraft-xir_ops_swap_order.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< SwapOrderNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::SwapOrderNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: