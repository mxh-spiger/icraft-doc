.. _exhale_function_object_8h_1a68c32a41efd1aa99afca70837565cced:

Template Function icraft::xir::Downcast
=======================================

- Defined in :ref:`file_icraft-xir_base_object.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::Downcast(BaseRef)
   :project: Icraft XIR