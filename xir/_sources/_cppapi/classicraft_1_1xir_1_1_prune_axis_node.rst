.. _exhale_class_classicraft_1_1xir_1_1_prune_axis_node:

Class PruneAxisNode
===================

- Defined in :ref:`file_icraft-xir_ops_prune_axis.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< PruneAxisNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::PruneAxisNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: