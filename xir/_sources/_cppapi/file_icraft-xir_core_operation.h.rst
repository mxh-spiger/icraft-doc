
.. _file_icraft-xir_core_operation.h:

File operation.h
================

|exhale_lsh| :ref:`Parent directory <dir_icraft-xir_core>` (``icraft-xir\core``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-xir\core\operation.h``)
--------------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-xir_core_operation.h.rst





Includes
--------


- ``icraft-xir/base/map.h``

- ``icraft-xir/base/string.h``

- ``icraft-xir/core/attrs.h``

- ``icraft-xir/core/compile_target.h``

- ``icraft-xir/core/data.h``

- ``map`` (:ref:`file_icraft-xir_base_map.h`)



Included By
-----------


- :ref:`file_icraft-xir_core_network.h`

- :ref:`file_icraft-xir_ops_abs.h`

- :ref:`file_icraft-xir_ops_add.h`

- :ref:`file_icraft-xir_ops_align_axis.h`

- :ref:`file_icraft-xir_ops_avgpool.h`

- :ref:`file_icraft-xir_ops_batchnorm.h`

- :ref:`file_icraft-xir_ops_cast.h`

- :ref:`file_icraft-xir_ops_concat.h`

- :ref:`file_icraft-xir_ops_conv2d.h`

- :ref:`file_icraft-xir_ops_conv2d_transpose.h`

- :ref:`file_icraft-xir_ops_copy.h`

- :ref:`file_icraft-xir_ops_divide_scalar.h`

- :ref:`file_icraft-xir_ops_elu.h`

- :ref:`file_icraft-xir_ops_expand.h`

- :ref:`file_icraft-xir_ops_gelu.h`

- :ref:`file_icraft-xir_ops_hard_op.h`

- :ref:`file_icraft-xir_ops_hardsigmoid.h`

- :ref:`file_icraft-xir_ops_hardswish.h`

- :ref:`file_icraft-xir_ops_layernorm.h`

- :ref:`file_icraft-xir_ops_matmul.h`

- :ref:`file_icraft-xir_ops_maxpool.h`

- :ref:`file_icraft-xir_ops_mish.h`

- :ref:`file_icraft-xir_ops_multi_yolo.h`

- :ref:`file_icraft-xir_ops_multiply.h`

- :ref:`file_icraft-xir_ops_normalize.h`

- :ref:`file_icraft-xir_ops_pad.h`

- :ref:`file_icraft-xir_ops_pixel_shuffle.h`

- :ref:`file_icraft-xir_ops_prelu.h`

- :ref:`file_icraft-xir_ops_prune_axis.h`

- :ref:`file_icraft-xir_ops_region.h`

- :ref:`file_icraft-xir_ops_relu.h`

- :ref:`file_icraft-xir_ops_reorg.h`

- :ref:`file_icraft-xir_ops_reshape.h`

- :ref:`file_icraft-xir_ops_resize.h`

- :ref:`file_icraft-xir_ops_route.h`

- :ref:`file_icraft-xir_ops_sigmoid.h`

- :ref:`file_icraft-xir_ops_silu.h`

- :ref:`file_icraft-xir_ops_slice.h`

- :ref:`file_icraft-xir_ops_softmax.h`

- :ref:`file_icraft-xir_ops_split.h`

- :ref:`file_icraft-xir_ops_squeeze.h`

- :ref:`file_icraft-xir_ops_ssd_output.h`

- :ref:`file_icraft-xir_ops_swap_order.h`

- :ref:`file_icraft-xir_ops_tanh.h`

- :ref:`file_icraft-xir_ops_transpose.h`

- :ref:`file_icraft-xir_ops_upsample.h`

- :ref:`file_icraft-xir_ops_yolo.h`




Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__ir`

- :ref:`namespace_icraft__xir`


Classes
-------


- :ref:`exhale_class_classicraft_1_1xir_1_1_input`

- :ref:`exhale_class_classicraft_1_1xir_1_1_input_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_is_activate`

- :ref:`exhale_class_classicraft_1_1xir_1_1_one_result`

- :ref:`exhale_class_classicraft_1_1xir_1_1_op_base`

- :ref:`exhale_class_classicraft_1_1xir_1_1_operation`

- :ref:`exhale_class_classicraft_1_1xir_1_1_operation_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`

- :ref:`exhale_class_classicraft_1_1xir_1_1_op_trait`

- :ref:`exhale_class_classicraft_1_1xir_1_1_output`

- :ref:`exhale_class_classicraft_1_1xir_1_1_output_like`

- :ref:`exhale_class_classicraft_1_1xir_1_1_output_node`


Defines
-------


- :ref:`exhale_define_operation_8h_1a589fc08fd078a46929285ba7af07ec96`

- :ref:`exhale_define_operation_8h_1a0587adc4fa59710015e27ce34a6d4f20`

- :ref:`exhale_define_operation_8h_1ad94829967e485c9a41f57ce3dc2dd69a`

- :ref:`exhale_define_operation_8h_1ab6c5eefe51f60d0617b743d3bdf95b1f`

