.. _exhale_define_pattern_8h_1a34e9a7009fa4c7b0bbf683e280c52fa2:

Define PATTERN_REQUIRE
======================

- Defined in :ref:`file_icraft-xir_core_pattern.h`


Define Documentation
--------------------


.. doxygendefine:: PATTERN_REQUIRE
   :project: Icraft XIR