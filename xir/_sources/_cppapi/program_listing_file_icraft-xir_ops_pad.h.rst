
.. _program_listing_file_icraft-xir_ops_pad.h:

Program Listing for File pad.h
==============================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_ops_pad.h>` (``icraft-xir\ops\pad.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/operation.h>
   
   namespace icraft::xir {
       class PadNode : public OpNodeBase<PadNode, OneResult> {
       public:
           enum class Mode {
               CONSTANT,       
               REFLECTION,     
               REPLICATION,    
               SYMMETRIC       
           };
   
           Array<IntImm> padding;      
           Mode mode;                  
           ScalarImm value;            
   
           ICRAFT_DECLARE_ATTRS(PadNode) {
               ICRAFT_ATTR_FIELD(padding);
               ICRAFT_ATTR_FIELD(mode).set_default(Mode::CONSTANT);
               ICRAFT_ATTR_FIELD(value).set_default(IntImm(0));
           }
   
           XIR_DLL virtual void validate() const override;
       };
   
       class Pad : public OpBase<Pad, PadNode> {
       public:
           using Mode = PadNode::Mode;
   
           Pad() = default;
   
           XIR_DLL Pad(Value input, Array<IntImm> padding, Mode mode = Mode::CONSTANT, ScalarImm value = IntImm(0));
       };
   }
