.. _exhale_enum_network_8h_1a52f12ca1331504c9536b043a7d4d2ed7:

Enum Framework
==============

- Defined in :ref:`file_icraft-xir_core_network.h`


Enum Documentation
------------------


.. doxygenenum:: icraft::xir::Framework
   :project: Icraft XIR