.. _exhale_function_data_8h_1ae7f269258f554b475a1fb22eb0901162:

Function icraft::xir::operator||(bool, const Bool&)
===================================================

- Defined in :ref:`file_icraft-xir_core_data.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::operator||(bool, const Bool&)
   :project: Icraft XIR