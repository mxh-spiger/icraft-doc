.. _exhale_class_classicraft_1_1xir_1_1_divide_scalar_node:

Class DivideScalarNode
======================

- Defined in :ref:`file_icraft-xir_ops_divide_scalar.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< DivideScalarNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::DivideScalarNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: