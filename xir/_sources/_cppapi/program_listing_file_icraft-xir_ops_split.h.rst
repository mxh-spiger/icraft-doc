
.. _program_listing_file_icraft-xir_ops_split.h:

Program Listing for File split.h
================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_ops_split.h>` (``icraft-xir\ops\split.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/operation.h>
   
   namespace icraft::xir {
       class SplitNode : public OpNodeBase<SplitNode> {
       public:
           Array<IntImm> sections;     
           int64_t axis;               
   
           ICRAFT_DECLARE_ATTRS(SplitNode) {
               ICRAFT_ATTR_FIELD(sections);
               ICRAFT_ATTR_FIELD(axis).set_default(-1).set_lower_bound(-1);
           };
   
           XIR_DLL virtual void validate() const override;
       };
   
       class Split : public OpBase<Split, SplitNode> {
       public:
           Split() = default;
   
           XIR_DLL Split(Value input, Array<IntImm> sections, int64_t axis = -1);
       };
   }
