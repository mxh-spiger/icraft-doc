
.. _program_listing_file_icraft-xir_ops_divide_scalar.h:

Program Listing for File divide_scalar.h
========================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_ops_divide_scalar.h>` (``icraft-xir\ops\divide_scalar.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/operation.h>
   
   namespace icraft::xir {
       class DivideScalarNode : public OpNodeBase<DivideScalarNode, OneResult> {
       public:
           enum class RoundingMode {
               NONE,       
               TRUNC,      
               FLOOR       
           };
   
           double divisor;                 
           RoundingMode rounding_mode;     
   
           ICRAFT_DECLARE_ATTRS(DivideScalarNode) {
               ICRAFT_ATTR_FIELD(divisor);
               ICRAFT_ATTR_FIELD(rounding_mode);
           }
   
           XIR_DLL virtual void validate() const override;
       };
   
       class DivideScalar : public OpBase<DivideScalar, DivideScalarNode> {
       public:
           using RoundingMode = DivideScalarNode::RoundingMode;
   
           DivideScalar() = default;
   
           XIR_DLL DivideScalar(Value input, double divisor, RoundingMode rounding_mode);
       };
   }
