.. _exhale_class_classicraft_1_1xir_1_1_conv2d_transpose_node:

Class Conv2dTransposeNode
=========================

- Defined in :ref:`file_icraft-xir_ops_conv2d_transpose.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< Conv2dTransposeNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Conv2dTransposeNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: