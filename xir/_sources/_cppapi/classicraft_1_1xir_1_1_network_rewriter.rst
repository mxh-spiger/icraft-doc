.. _exhale_class_classicraft_1_1xir_1_1_network_rewriter:

Class NetworkRewriter
=====================

- Defined in :ref:`file_icraft-xir_core_network.h`


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::NetworkRewriter
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: