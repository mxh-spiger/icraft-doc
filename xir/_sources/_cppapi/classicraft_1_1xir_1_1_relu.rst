.. _exhale_class_classicraft_1_1xir_1_1_relu:

Class Relu
==========

- Defined in :ref:`file_icraft-xir_ops_relu.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< Relu, ReluNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Relu
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: