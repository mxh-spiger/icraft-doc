.. _exhale_class_classicraft_1_1xir_1_1_object_ptr:

Template Class ObjectPtr
========================

- Defined in :ref:`file_icraft-xir_base_object.h`


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::ObjectPtr
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: