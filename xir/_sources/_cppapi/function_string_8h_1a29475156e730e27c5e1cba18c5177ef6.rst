.. _exhale_function_string_8h_1a29475156e730e27c5e1cba18c5177ef6:

Function icraft::xir::operator!=(const String&, const std::string&)
===================================================================

- Defined in :ref:`file_icraft-xir_base_string.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::operator!=(const String&, const std::string&)
   :project: Icraft XIR