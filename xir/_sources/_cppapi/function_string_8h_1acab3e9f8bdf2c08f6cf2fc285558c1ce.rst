.. _exhale_function_string_8h_1acab3e9f8bdf2c08f6cf2fc285558c1ce:

Function icraft::xir::operator!=(const char \*, const String&)
==============================================================

- Defined in :ref:`file_icraft-xir_base_string.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::operator!=(const char *, const String&)
   :project: Icraft XIR