.. _exhale_class_classicraft_1_1xir_1_1_attr_visitor:

Class AttrVisitor
=================

- Defined in :ref:`file_icraft-xir_core_reflection.h`


Inheritance Relationships
-------------------------

Derived Types
*************

- ``private GetAttrNums`` (:ref:`exhale_class_classicraft_1_1xir_1_1_get_attr_nums`)
- ``public JsonParser`` (:ref:`exhale_class_classicraft_1_1xir_1_1_json_parser`)
- ``public JsonPrinter`` (:ref:`exhale_class_classicraft_1_1xir_1_1_json_printer`)
- ``public MSGDumper`` (:ref:`exhale_class_classicraft_1_1xir_1_1_m_s_g_dumper`)
- ``public MSGParser`` (:ref:`exhale_class_classicraft_1_1xir_1_1_m_s_g_parser`)
- ``public ParamsDumper`` (:ref:`exhale_class_classicraft_1_1xir_1_1_params_dumper`)
- ``public ParamsLazyLoader`` (:ref:`exhale_class_classicraft_1_1xir_1_1_params_lazy_loader`)
- ``public ParamsLoader`` (:ref:`exhale_class_classicraft_1_1xir_1_1_params_loader`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::AttrVisitor
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: