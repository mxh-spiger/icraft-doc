
.. _program_listing_file_icraft-xir_ops_abs.h:

Program Listing for File abs.h
==============================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_ops_abs.h>` (``icraft-xir\ops\abs.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/operation.h>
   
   namespace icraft::xir {
       class AbsNode : public OpNodeBase<AbsNode, OneResult, IsActivate> {
       public:
           XIR_DLL virtual void validate() const override;
       };
   
       class Abs : public OpBase<Abs, AbsNode> {
       public:
           Abs() = default;
   
           XIR_DLL Abs(Value input);
       };
   }
