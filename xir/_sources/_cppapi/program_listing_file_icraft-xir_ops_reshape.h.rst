
.. _program_listing_file_icraft-xir_ops_reshape.h:

Program Listing for File reshape.h
==================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_ops_reshape.h>` (``icraft-xir\ops\reshape.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/operation.h>
   
   namespace icraft::xir {
       class ReshapeNode : public OpNodeBase<ReshapeNode, OneResult> {
       public:
           Array<IntImm> shape;    
           Layout layout;          
   
           ICRAFT_DECLARE_ATTRS(ReshapeNode) {
               ICRAFT_ATTR_FIELD(shape);
               ICRAFT_ATTR_FIELD(layout);
           }
   
           XIR_DLL virtual void validate() const override;
       };
   
       class Reshape : public OpBase<Reshape, ReshapeNode> {
       public:
           Reshape() = default;
   
           XIR_DLL Reshape(Value input, Array<IntImm> shape, Layout layout);
   
           XIR_DLL Reshape(Value input, Array<IntImm> shape);
       };
   }
