:orphan:


Full API
========

Directories
***********


.. toctree::
   :maxdepth: 5

   dir_icraft-xir.rst

.. toctree::
   :maxdepth: 5

   dir_icraft-xir_base.rst

.. toctree::
   :maxdepth: 5

   dir_icraft-xir_core.rst

.. toctree::
   :maxdepth: 5

   dir_icraft-xir_ops.rst

Files
*****


.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_abs.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_add.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_align_axis.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_base_any.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_base_array.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_core_attrs.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_avgpool.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_batchnorm.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_cast.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_core_compile_target.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_concat.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_conv2d.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_conv2d_transpose.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_copy.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_core_data.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_core_data_type.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_divide_scalar.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_base_dllexport.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_elu.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_expand.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_core_functor.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_gelu.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_hard_op.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_hardsigmoid.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_hardswish.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_layernorm.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_core_layout.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_base_map.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_matmul.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_maxpool.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_core_mem_type.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_mish.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_multi_yolo.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_multiply.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_core_network.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_core_node.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_normalize.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_base_object.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_core_operation.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_base_optional.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_pad.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_core_pass.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_core_pattern.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_pixel_shuffle.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_prelu.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_prune_axis.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_core_reflection.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_region.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_relu.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_reorg.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_reshape.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_resize.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_route.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_core_serialize.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_sigmoid.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_silu.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_slice.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_softmax.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_split.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_squeeze.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_ssd_output.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_base_string.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_swap_order.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_tanh.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_transpose.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_upsample.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_base_version.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xir_ops_yolo.h.rst
