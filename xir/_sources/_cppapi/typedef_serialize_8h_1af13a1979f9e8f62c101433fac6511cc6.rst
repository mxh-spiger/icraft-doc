.. _exhale_typedef_serialize_8h_1af13a1979f9e8f62c101433fac6511cc6:

Typedef icraft::xir::Json
=========================

- Defined in :ref:`file_icraft-xir_core_serialize.h`


Typedef Documentation
---------------------


.. doxygentypedef:: icraft::xir::Json
   :project: Icraft XIR