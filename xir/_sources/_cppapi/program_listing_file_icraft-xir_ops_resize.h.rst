
.. _program_listing_file_icraft-xir_ops_resize.h:

Program Listing for File resize.h
=================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_ops_resize.h>` (``icraft-xir\ops\resize.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/operation.h>
   
   namespace icraft::xir {
   
       enum class ResizeInterpolation {
           INTER_NEAREST,      
           INTER_LINEAR        
       };
       class ResizeNode : public OpNodeBase<ResizeNode, OneResult> {
       public:
           Array<IntImm> dsize;                
           ResizeInterpolation interpolation;  
   
   
           ICRAFT_DECLARE_ATTRS(ResizeNode) {
               ICRAFT_ATTR_FIELD(dsize);
               ICRAFT_ATTR_FIELD(interpolation).set_default(ResizeInterpolation::INTER_LINEAR);
           }
   
           XIR_DLL virtual void validate() const override;
       };
   
       class Resize : public OpBase<Resize, ResizeNode> {
       public:
           Resize() = default;
   
           XIR_DLL Resize(
               Value input, 
               Array<IntImm> dsize, 
               ResizeInterpolation interpolation = ResizeInterpolation::INTER_LINEAR
           );
       };
   }
