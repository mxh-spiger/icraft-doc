.. _exhale_class_classicraft_1_1xir_1_1_node:

Class Node
==========

- Defined in :ref:`file_icraft-xir_core_node.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public Object`` (:ref:`exhale_class_classicraft_1_1xir_1_1_object`)


Derived Types
*************

- ``public NodeBase< AlignedUnitNode, Node >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)
- ``public NodeBase< AttrFieldInfoNode, Node >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)
- ``public NodeBase< AxisNameNode, Node >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)
- ``public NodeBase< LayoutNode, Node >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)
- ``public NodeBase< MergedAxisDistrNode, Node >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)
- ``public NodeBase< NetworkNode, Node >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)
- ``public NodeBase< NetworkViewNode, Node >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)
- ``public NodeBase< PassContextNode, Node >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)
- ``public NodeBase< PassInfoNode, Node >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)
- ``public NodeBase< PassNode, Node >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)
- ``public NodeBase< PriorBoxNode, Node >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)
- ``public NodeBase< QuantizedScaleArrayNode, Node >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)
- ``public NodeBase< QuantizedScaleNode, Node >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)
- ``public CompileTargetNode`` (:ref:`exhale_class_classicraft_1_1xir_1_1_compile_target_node`)
- ``public DataNode`` (:ref:`exhale_class_classicraft_1_1xir_1_1_data_node`)
- ``public DataTypeNode`` (:ref:`exhale_class_classicraft_1_1xir_1_1_data_type_node`)
- ``public MemTypeNode`` (:ref:`exhale_class_classicraft_1_1xir_1_1_mem_type_node`)
- ``public OperationNode`` (:ref:`exhale_class_classicraft_1_1xir_1_1_operation_node`)
- ``public PatternNode`` (:ref:`exhale_class_classicraft_1_1xir_1_1_pattern_node`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Node
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: