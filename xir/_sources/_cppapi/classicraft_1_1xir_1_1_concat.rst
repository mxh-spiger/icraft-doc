.. _exhale_class_classicraft_1_1xir_1_1_concat:

Class Concat
============

- Defined in :ref:`file_icraft-xir_ops_concat.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< Concat, ConcatNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Concat
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: