
.. _program_listing_file_icraft-xir_core_compile_target.h:

Program Listing for File compile_target.h
=========================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_core_compile_target.h>` (``icraft-xir\core\compile_target.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/node.h>
   
   namespace icraft::xir {
       class CompileTargetNode : public Node {
       public:
           virtual void accept(AttrVisitor& visitor) override {}
       };
   
       class CompileTarget : public VirtualBase<CompileTarget, Handle, CompileTargetNode> {};
   
       class HostTargetNode : public NodeBase<HostTargetNode, CompileTargetNode> {
       public:
           bool reduceAccept(CompileTargetNode* other, const ReduceVisitor& reduce) const {
               return true;
           }
       };
   
       class HostTarget : public HandleBase<HostTarget, CompileTarget, HostTargetNode> {};
   
       class FPGATargetNode : public NodeBase<FPGATargetNode, CompileTargetNode> {
       public:
           bool reduceAccept(FPGATargetNode* other, const ReduceVisitor& reduce) const {
               return true;
           }
       };
   
       class FPGATarget : public HandleBase<FPGATarget, CompileTarget, FPGATargetNode> {};
   
       class CustomTargetNode : public NodeBase<CustomTargetNode, CompileTargetNode> {
       public:
           bool reduceAccept(CustomTargetNode* other, const ReduceVisitor& reduce) const {
               return true;
           }
       };
   
       class CustomTarget : public HandleBase<CustomTarget, CompileTarget, CustomTargetNode> {};
   
       class BuyiTargetNode : public NodeBase<BuyiTargetNode, CompileTargetNode> {
       public:
           bool reduceAccept(BuyiTargetNode* other, const ReduceVisitor& reduce) const {
               return true;
           }
       };
   
       class BuyiTarget : public HandleBase<BuyiTarget, CompileTarget, BuyiTargetNode> {};
   
       class ZhugeTargetNode : public NodeBase<ZhugeTargetNode, CompileTargetNode> {
       public:
           bool reduceAccept(ZhugeTargetNode* other, const ReduceVisitor& reduce) const {
               return true;
           }
       };
   
       class ZhugeTarget : public HandleBase<ZhugeTarget, CompileTarget, ZhugeTargetNode> {};
   
   
   }
