.. _exhale_class_classicraft_1_1xir_1_1_float_type:

Class FloatType
===============

- Defined in :ref:`file_icraft-xir_core_data_type.h`


Nested Relationships
--------------------


Nested Types
************

- :ref:`exhale_class_classicraft_1_1xir_1_1_float_type_1_1_semantics`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< FloatType, ScalarType, FloatTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::FloatType
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: