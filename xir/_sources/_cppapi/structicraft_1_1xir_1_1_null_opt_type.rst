.. _exhale_struct_structicraft_1_1xir_1_1_null_opt_type:

Struct NullOptType
==================

- Defined in :ref:`file_icraft-xir_base_optional.h`


Struct Documentation
--------------------


.. doxygenstruct:: icraft::xir::NullOptType
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: