.. _exhale_function_data_8h_1af1ce218b03c384d2aad50a6295fb096d:

Function icraft::xir::operator&&(const Bool&, bool)
===================================================

- Defined in :ref:`file_icraft-xir_core_data.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::operator&&(const Bool&, bool)
   :project: Icraft XIR