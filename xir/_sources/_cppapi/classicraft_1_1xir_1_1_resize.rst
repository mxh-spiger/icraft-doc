.. _exhale_class_classicraft_1_1xir_1_1_resize:

Class Resize
============

- Defined in :ref:`file_icraft-xir_ops_resize.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< Resize, ResizeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Resize
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: