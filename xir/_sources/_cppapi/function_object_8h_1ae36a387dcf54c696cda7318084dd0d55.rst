.. _exhale_function_object_8h_1ae36a387dcf54c696cda7318084dd0d55:

Template Function icraft::xir::GetRef(const ObjType \*, bool)
=============================================================

- Defined in :ref:`file_icraft-xir_base_object.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::GetRef(const ObjType *, bool)
   :project: Icraft XIR