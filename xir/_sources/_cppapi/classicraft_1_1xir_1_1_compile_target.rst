.. _exhale_class_classicraft_1_1xir_1_1_compile_target:

Class CompileTarget
===================

- Defined in :ref:`file_icraft-xir_core_compile_target.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public VirtualBase< CompileTarget, Handle, CompileTargetNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)


Derived Types
*************

- ``public VirtualBase< BuyiTarget, CompileTarget, BuyiTargetNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< CustomTarget, CompileTarget, CustomTargetNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< FPGATarget, CompileTarget, FPGATargetNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< HostTarget, CompileTarget, HostTargetNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< ZhugeTarget, CompileTarget, ZhugeTargetNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::CompileTarget
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: