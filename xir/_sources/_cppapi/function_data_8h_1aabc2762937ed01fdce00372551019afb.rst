.. _exhale_function_data_8h_1aabc2762937ed01fdce00372551019afb:

Function icraft::xir::operator==(const Bool&, const Bool&)
==========================================================

- Defined in :ref:`file_icraft-xir_core_data.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::operator==(const Bool&, const Bool&)
   :project: Icraft XIR