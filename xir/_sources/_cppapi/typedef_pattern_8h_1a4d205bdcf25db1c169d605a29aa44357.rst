.. _exhale_typedef_pattern_8h_1a4d205bdcf25db1c169d605a29aa44357:

Typedef icraft::xir::MatchGroup
===============================

- Defined in :ref:`file_icraft-xir_core_pattern.h`


Typedef Documentation
---------------------


.. doxygentypedef:: icraft::xir::MatchGroup
   :project: Icraft XIR