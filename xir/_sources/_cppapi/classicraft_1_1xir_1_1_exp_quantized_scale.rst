.. _exhale_class_classicraft_1_1xir_1_1_exp_quantized_scale:

Class ExpQuantizedScale
=======================

- Defined in :ref:`file_icraft-xir_core_data_type.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< ExpQuantizedScale, QuantizedScale, ExpQuantizedScaleNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::ExpQuantizedScale
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: