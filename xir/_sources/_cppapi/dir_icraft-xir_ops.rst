.. _dir_icraft-xir_ops:


Directory ops
=============


|exhale_lsh| :ref:`Parent directory <dir_icraft-xir>` (``icraft-xir``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


*Directory path:* ``icraft-xir\ops``


Files
-----

- :ref:`file_icraft-xir_ops_abs.h`
- :ref:`file_icraft-xir_ops_add.h`
- :ref:`file_icraft-xir_ops_align_axis.h`
- :ref:`file_icraft-xir_ops_avgpool.h`
- :ref:`file_icraft-xir_ops_batchnorm.h`
- :ref:`file_icraft-xir_ops_cast.h`
- :ref:`file_icraft-xir_ops_concat.h`
- :ref:`file_icraft-xir_ops_conv2d.h`
- :ref:`file_icraft-xir_ops_conv2d_transpose.h`
- :ref:`file_icraft-xir_ops_copy.h`
- :ref:`file_icraft-xir_ops_divide_scalar.h`
- :ref:`file_icraft-xir_ops_elu.h`
- :ref:`file_icraft-xir_ops_expand.h`
- :ref:`file_icraft-xir_ops_gelu.h`
- :ref:`file_icraft-xir_ops_hard_op.h`
- :ref:`file_icraft-xir_ops_hardsigmoid.h`
- :ref:`file_icraft-xir_ops_hardswish.h`
- :ref:`file_icraft-xir_ops_layernorm.h`
- :ref:`file_icraft-xir_ops_matmul.h`
- :ref:`file_icraft-xir_ops_maxpool.h`
- :ref:`file_icraft-xir_ops_mish.h`
- :ref:`file_icraft-xir_ops_multi_yolo.h`
- :ref:`file_icraft-xir_ops_multiply.h`
- :ref:`file_icraft-xir_ops_normalize.h`
- :ref:`file_icraft-xir_ops_pad.h`
- :ref:`file_icraft-xir_ops_pixel_shuffle.h`
- :ref:`file_icraft-xir_ops_prelu.h`
- :ref:`file_icraft-xir_ops_prune_axis.h`
- :ref:`file_icraft-xir_ops_region.h`
- :ref:`file_icraft-xir_ops_relu.h`
- :ref:`file_icraft-xir_ops_reorg.h`
- :ref:`file_icraft-xir_ops_reshape.h`
- :ref:`file_icraft-xir_ops_resize.h`
- :ref:`file_icraft-xir_ops_route.h`
- :ref:`file_icraft-xir_ops_sigmoid.h`
- :ref:`file_icraft-xir_ops_silu.h`
- :ref:`file_icraft-xir_ops_slice.h`
- :ref:`file_icraft-xir_ops_softmax.h`
- :ref:`file_icraft-xir_ops_split.h`
- :ref:`file_icraft-xir_ops_squeeze.h`
- :ref:`file_icraft-xir_ops_ssd_output.h`
- :ref:`file_icraft-xir_ops_swap_order.h`
- :ref:`file_icraft-xir_ops_tanh.h`
- :ref:`file_icraft-xir_ops_transpose.h`
- :ref:`file_icraft-xir_ops_upsample.h`
- :ref:`file_icraft-xir_ops_yolo.h`


