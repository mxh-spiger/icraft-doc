.. _exhale_class_classicraft_1_1xir_1_1_network_node:

Class NetworkNode
=================

- Defined in :ref:`file_icraft-xir_core_network.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< NetworkNode, Node >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::NetworkNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: