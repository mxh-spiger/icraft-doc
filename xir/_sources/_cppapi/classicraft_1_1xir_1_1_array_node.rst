.. _exhale_class_classicraft_1_1xir_1_1_array_node:

Class ArrayNode
===============

- Defined in :ref:`file_icraft-xir_base_array.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public Object`` (:ref:`exhale_class_classicraft_1_1xir_1_1_object`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::ArrayNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: