.. _exhale_function_string_8h_1af6dfa63e8765ba8f0c05a7d40fa1c584:

Function icraft::xir::operator!=(const std::string&, const String&)
===================================================================

- Defined in :ref:`file_icraft-xir_base_string.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::operator!=(const std::string&, const String&)
   :project: Icraft XIR