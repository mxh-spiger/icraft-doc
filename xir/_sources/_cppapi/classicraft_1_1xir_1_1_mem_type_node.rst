.. _exhale_class_classicraft_1_1xir_1_1_mem_type_node:

Class MemTypeNode
=================

- Defined in :ref:`file_icraft-xir_core_mem_type.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public Node`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node`)


Derived Types
*************

- ``public NodeBase< ExternalMemNode, MemTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)
- ``public NodeBase< HostMemNode, MemTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)
- ``public NodeBase< OnChipMemNode, MemTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::MemTypeNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: