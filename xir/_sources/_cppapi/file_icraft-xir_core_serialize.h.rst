
.. _file_icraft-xir_core_serialize.h:

File serialize.h
================

|exhale_lsh| :ref:`Parent directory <dir_icraft-xir_core>` (``icraft-xir\core``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-xir\core\serialize.h``)
--------------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-xir_core_serialize.h.rst





Includes
--------


- ``icraft-xir/base/array.h``

- ``icraft-xir/base/map.h``

- ``icraft-xir/base/string.h``

- ``icraft-xir/core/data.h``

- ``icraft-xir/utils/json.hpp``

- ``sstream``



Included By
-----------


- :ref:`file_icraft-xir_core_network.h`




Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__xir`


Classes
-------


- :ref:`exhale_class_classicraft_1_1xir_1_1_get_attr_nums`

- :ref:`exhale_class_classicraft_1_1xir_1_1_json_parser`

- :ref:`exhale_class_classicraft_1_1xir_1_1_json_printer`

- :ref:`exhale_class_classicraft_1_1xir_1_1_m_s_g_dumper`

- :ref:`exhale_class_classicraft_1_1xir_1_1_m_s_g_parser`

- :ref:`exhale_class_classicraft_1_1xir_1_1_params_dumper`

- :ref:`exhale_class_classicraft_1_1xir_1_1_params_lazy_loader`

- :ref:`exhale_class_classicraft_1_1xir_1_1_params_loader`


Functions
---------


- :ref:`exhale_function_serialize_8h_1a4be591f3b222e460eabd4f1f2bd15a63`

- :ref:`exhale_function_serialize_8h_1a0185d16d44e07b98bc5b409791036be6`

- :ref:`exhale_function_serialize_8h_1a5ebb3b0b58b3fb3e792c71e49b3fbaa7`

- :ref:`exhale_function_serialize_8h_1a047703dc2d7e22991c7dd29dc42a46d9`

- :ref:`exhale_function_serialize_8h_1a50b7e723472acf1ca28b86c386460d18`


Typedefs
--------


- :ref:`exhale_typedef_serialize_8h_1af13a1979f9e8f62c101433fac6511cc6`

