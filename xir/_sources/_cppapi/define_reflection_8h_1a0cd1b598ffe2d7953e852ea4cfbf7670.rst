.. _exhale_define_reflection_8h_1a0cd1b598ffe2d7953e852ea4cfbf7670:

Define ICRAFT_REGISTER_REFLECTION_VTABLE
========================================

- Defined in :ref:`file_icraft-xir_core_reflection.h`


Define Documentation
--------------------


.. doxygendefine:: ICRAFT_REGISTER_REFLECTION_VTABLE
   :project: Icraft XIR