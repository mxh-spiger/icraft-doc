.. _exhale_function_data_8h_1a13f31d0631d8054ec6cbe16e5aa5c246:

Function icraft::xir::operator&&(const Bool&, const Bool&)
==========================================================

- Defined in :ref:`file_icraft-xir_core_data.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::operator&&(const Bool&, const Bool&)
   :project: Icraft XIR