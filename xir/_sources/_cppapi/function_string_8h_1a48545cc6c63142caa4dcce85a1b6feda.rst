.. _exhale_function_string_8h_1a48545cc6c63142caa4dcce85a1b6feda:

Function icraft::xir::operator<=(const String&, const String&)
==============================================================

- Defined in :ref:`file_icraft-xir_base_string.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::operator<=(const String&, const String&)
   :project: Icraft XIR