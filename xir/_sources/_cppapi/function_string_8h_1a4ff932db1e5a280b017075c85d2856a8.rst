.. _exhale_function_string_8h_1a4ff932db1e5a280b017075c85d2856a8:

Function icraft::xir::operator==(const std::string&, const String&)
===================================================================

- Defined in :ref:`file_icraft-xir_base_string.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::operator==(const std::string&, const String&)
   :project: Icraft XIR