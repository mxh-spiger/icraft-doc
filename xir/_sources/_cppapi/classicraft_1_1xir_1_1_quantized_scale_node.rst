.. _exhale_class_classicraft_1_1xir_1_1_quantized_scale_node:

Class QuantizedScaleNode
========================

- Defined in :ref:`file_icraft-xir_core_data_type.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< QuantizedScaleNode, Node >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Derived Type
************

- ``public NodeBase< ExpQuantizedScaleNode, QuantizedScaleNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::QuantizedScaleNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: