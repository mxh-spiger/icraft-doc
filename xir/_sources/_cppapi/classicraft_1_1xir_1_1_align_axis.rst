.. _exhale_class_classicraft_1_1xir_1_1_align_axis:

Class AlignAxis
===============

- Defined in :ref:`file_icraft-xir_ops_align_axis.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< AlignAxis, AlignAxisNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::AlignAxis
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: