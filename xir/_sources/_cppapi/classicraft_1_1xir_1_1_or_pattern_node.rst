.. _exhale_class_classicraft_1_1xir_1_1_or_pattern_node:

Class OrPatternNode
===================

- Defined in :ref:`file_icraft-xir_core_pattern.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< OrPatternNode, PatternNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::OrPatternNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: