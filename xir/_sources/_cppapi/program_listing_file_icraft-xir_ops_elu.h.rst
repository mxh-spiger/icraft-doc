
.. _program_listing_file_icraft-xir_ops_elu.h:

Program Listing for File elu.h
==============================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_ops_elu.h>` (``icraft-xir\ops\elu.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/operation.h>
   
   namespace icraft::xir {
       class EluNode : public OpNodeBase<EluNode, OneResult, IsActivate> {
       public:
           double alpha;   
   
           ICRAFT_DECLARE_ATTRS(EluNode) {
               ICRAFT_ATTR_FIELD(alpha);
           }
   
           XIR_DLL virtual void validate() const override;
       };
   
       class Elu : public OpBase<Elu, EluNode> {
       public:
           Elu() = default;
   
           XIR_DLL Elu(Value input, double alpha);
       };
   }
