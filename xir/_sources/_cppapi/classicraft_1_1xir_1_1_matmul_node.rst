.. _exhale_class_classicraft_1_1xir_1_1_matmul_node:

Class MatmulNode
================

- Defined in :ref:`file_icraft-xir_ops_matmul.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< MatmulNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::MatmulNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: