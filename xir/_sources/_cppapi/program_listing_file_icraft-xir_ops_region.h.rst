
.. _program_listing_file_icraft-xir_ops_region.h:

Program Listing for File region.h
=================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_ops_region.h>` (``icraft-xir\ops\region.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/operation.h>
   
   namespace icraft::xir {
       class RegionNode : public OpNodeBase<RegionNode, OutputLike> {
       public:
           int64_t class_num;              
           int64_t coords;                 
           int64_t softmax;                
           int64_t num;                    
           int64_t max;                    
           double confidence_thresh;       
           double iou_thresh;              
           Array<FloatImm> anchors;        
   
           ICRAFT_DECLARE_ATTRS(RegionNode) {
               ICRAFT_ATTR_FIELD(class_num);
               ICRAFT_ATTR_FIELD(coords).set_lower_bound(0);
               ICRAFT_ATTR_FIELD(softmax);
               ICRAFT_ATTR_FIELD(num).set_lower_bound(0);
               ICRAFT_ATTR_FIELD(max).set_lower_bound(0);
               ICRAFT_ATTR_FIELD(confidence_thresh).set_lower_bound(0);
               ICRAFT_ATTR_FIELD(iou_thresh).set_lower_bound(0);
               ICRAFT_ATTR_FIELD(anchors);
           }
   
           XIR_DLL virtual void validate() const override;
       };
   
       class Region : public OpBase<Region, RegionNode> {
       public:
           Region() = default;
   
           XIR_DLL Region(
               Value input,
               int64_t class_num,
               int64_t coords,
               int64_t softmax,
               int64_t num,
               int64_t max,
               double confidence_thresh,
               double iou_thresh,
               Array<FloatImm> anchors
           );
       };
   }
