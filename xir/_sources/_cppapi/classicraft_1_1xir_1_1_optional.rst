.. _exhale_class_classicraft_1_1xir_1_1_optional:

Template Class Optional
=======================

- Defined in :ref:`file_icraft-xir_base_optional.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public ObjectRef`` (:ref:`exhale_class_classicraft_1_1xir_1_1_object_ref`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Optional
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: