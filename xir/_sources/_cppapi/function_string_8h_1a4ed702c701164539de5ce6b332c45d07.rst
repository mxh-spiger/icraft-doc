.. _exhale_function_string_8h_1a4ed702c701164539de5ce6b332c45d07:

Function icraft::xir::operator+(const std::string&, const String&)
==================================================================

- Defined in :ref:`file_icraft-xir_base_string.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::operator+(const std::string&, const String&)
   :project: Icraft XIR