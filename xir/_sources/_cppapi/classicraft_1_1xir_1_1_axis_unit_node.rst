.. _exhale_class_classicraft_1_1xir_1_1_axis_unit_node:

Class AxisUnitNode
==================

- Defined in :ref:`file_icraft-xir_core_layout.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< AxisUnitNode, AxisNameNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::AxisUnitNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: