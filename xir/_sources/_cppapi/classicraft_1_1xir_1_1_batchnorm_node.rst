.. _exhale_class_classicraft_1_1xir_1_1_batchnorm_node:

Class BatchnormNode
===================

- Defined in :ref:`file_icraft-xir_ops_batchnorm.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< BatchnormNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::BatchnormNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: