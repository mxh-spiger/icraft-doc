
.. _file_icraft-xir_core_reflection.h:

File reflection.h
=================

|exhale_lsh| :ref:`Parent directory <dir_icraft-xir_core>` (``icraft-xir\core``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-xir\core\reflection.h``)
---------------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-xir_core_reflection.h.rst





Includes
--------


- ``cstdint``

- ``icraft-xir/base/array.h``

- ``icraft-xir/base/dllexport.h``

- ``icraft-xir/base/map.h``

- ``icraft-xir/core/functor.h``

- ``icraft-xir/core/node.h``

- ``icraft-xir/utils/magic_enum.hpp``

- ``string`` (:ref:`file_icraft-xir_base_string.h`)



Included By
-----------


- :ref:`file_icraft-xir_core_layout.h`

- :ref:`file_icraft-xir_core_mem_type.h`

- :ref:`file_icraft-xir_core_pass.h`




Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__xir`


Classes
-------


- :ref:`exhale_class_classicraft_1_1xir_1_1_attr_visitor`

- :ref:`exhale_class_classicraft_1_1xir_1_1_overide_clone`

- :ref:`exhale_class_classicraft_1_1xir_1_1_overide_equal`

- :ref:`exhale_class_classicraft_1_1xir_1_1_pass_registry`

- :ref:`exhale_class_classicraft_1_1xir_1_1_reduce_visitor`

- :ref:`exhale_class_classicraft_1_1xir_1_1_reflection_table`

- :ref:`exhale_class_classicraft_1_1xir_1_1_reflection_table_1_1_registry`


Functions
---------


- :ref:`exhale_function_reflection_8h_1a55e9abf1358f73936d0c260df59e3a48`

- :ref:`exhale_function_reflection_8h_1a3f420755305a0d0bbc43dd197083aafd`


Defines
-------


- :ref:`exhale_define_reflection_8h_1a50e6659d2ac383e62abed02ffe66c6a7`

- :ref:`exhale_define_reflection_8h_1a53097a5ba6b07fccef71a666acbbb533`

- :ref:`exhale_define_reflection_8h_1a13afd72846c27ef4972a204be15ca40e`

- :ref:`exhale_define_reflection_8h_1a696c33a608037514a095ed4cf00c230b`

- :ref:`exhale_define_reflection_8h_1a0cd1b598ffe2d7953e852ea4cfbf7670`


Typedefs
--------


- :ref:`exhale_typedef_reflection_8h_1a119264abacc913e662fe7f5f2b14a813`

