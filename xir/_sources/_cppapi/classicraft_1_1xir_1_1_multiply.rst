.. _exhale_class_classicraft_1_1xir_1_1_multiply:

Class Multiply
==============

- Defined in :ref:`file_icraft-xir_ops_multiply.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< Multiply, MultiplyNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Multiply
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: