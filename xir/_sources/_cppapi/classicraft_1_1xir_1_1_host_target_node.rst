.. _exhale_class_classicraft_1_1xir_1_1_host_target_node:

Class HostTargetNode
====================

- Defined in :ref:`file_icraft-xir_core_compile_target.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< HostTargetNode, CompileTargetNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::HostTargetNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: