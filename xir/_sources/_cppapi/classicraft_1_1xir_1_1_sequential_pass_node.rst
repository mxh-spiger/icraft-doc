.. _exhale_class_classicraft_1_1xir_1_1_sequential_pass_node:

Class SequentialPassNode
========================

- Defined in :ref:`file_icraft-xir_core_pass.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< SequentialPassNode, PassNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::SequentialPassNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: