.. _exhale_function_object_8h_1a6bb897ef1c79fef652856160d3d825a4:

Template Function icraft::xir::GetObjectPtr
===========================================

- Defined in :ref:`file_icraft-xir_base_object.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::GetObjectPtr(ObjType *)
   :project: Icraft XIR