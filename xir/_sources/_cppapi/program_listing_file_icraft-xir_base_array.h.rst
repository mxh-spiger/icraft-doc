
.. _program_listing_file_icraft-xir_base_array.h:

Program Listing for File array.h
================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_base_array.h>` (``icraft-xir\base\array.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/base/object.h>
   #include <icraft-xir/base/optional.h>
   
   #include <memory>
   #include <unordered_set>
   
   namespace icraft::xir {
       class ArrayNode : public Object {
       public:
           using ContainerType = std::vector<ObjectRef>;
           using iterator = ContainerType::iterator;
           using const_iterator = ContainerType::const_iterator;
   
           ArrayNode() = default;
   
           ArrayNode(uint64_t n) : data_(ContainerType(n)) {}
   
           ArrayNode(uint64_t n, const ObjectRef& val) : data_(ContainerType(n, val)) {}
   
           const ObjectRef& operator[](size_t idx) const {
               size_t size = this->size();
               ICRAFT_CHECK_LT(idx, size).append("Index {} out of bounds {}", idx, size);
               return *(this->begin() + idx);
           }
   
           ObjectRef& operator[](size_t idx) {
               size_t size = this->size();
               ICRAFT_CHECK_LT(idx, size).append("Index {} out of bounds {}", idx, size);
               return *(this->begin() + idx);
           }
   
           size_t size() const { return data_.size(); }
   
           size_t capacity() const { return data_.capacity(); }
   
           const ObjectRef& at(int64_t i) const { return data_[i]; }
   
           ObjectRef& at(int64_t i) { return data_[i]; }
   
           const_iterator begin() const { return data_.begin(); }
   
           iterator begin() { return data_.begin(); }
   
           const_iterator end() const { return data_.end(); }
   
           iterator end() { return data_.end(); }
   
           void clear() { data_.clear(); }
   
           void setItem(int64_t i, ObjectRef item) { data_[i] = std::move(item); }
   
           static ObjectPtr<ArrayNode> CopyFrom(int64_t cap, const ArrayNode* from) {
               int64_t size = from->size();
               ICRAFT_CHECK_GE(cap, size).append("ValueError: not enough capacity");
               ObjectPtr<ArrayNode> p = make_object<ArrayNode>(cap);
               std::copy_n(from->begin(), size, p->begin());
               return p;
           }
   
           static ObjectPtr<ArrayNode> MoveFrom(int64_t cap, ArrayNode* from) {
               int64_t size = from->size();
               ICRAFT_CHECK_GE(cap, size).append("ValueError: not enough capacity");
               ObjectPtr<ArrayNode> p = make_object<ArrayNode>(cap);
               std::copy_n(std::make_move_iterator(from->begin()), size, p->begin());
               from->clear();
               return p;
           }
   
           static ObjectPtr<ArrayNode> CreateRepeated(int64_t n, const ObjectRef& val) {
               return make_object<ArrayNode>(n, val);
           }
   
       private:
           static ObjectPtr<ArrayNode> Empty(uint64_t n = kInitSize) {
               auto p = make_object<ArrayNode>();
               p->data_.reserve(n);
               return p;
           }
   
           template <typename IterType>
           ArrayNode* initRange(int64_t idx, IterType first, IterType last) {
               auto itr = this->begin() + idx;
               std::copy(first, last, itr);
               return this;
           }
   
           ArrayNode* moveElementsLeft(int64_t dst, int64_t src_begin, int64_t src_end) {
               auto from = this->begin() + src_begin;
               auto to = this->end() + dst;
               while (src_begin++ != src_end) {
                   *to++ = std::move(*from++);
               }
               return this;
           }
   
           ArrayNode* moveElementsRight(int64_t dst, int64_t src_begin, int64_t src_end) {
               auto from = this->begin() + src_end;
               auto to = this->end() + (src_end - src_begin + dst);
               while (src_begin++ != src_end) {
                   *--to = std::move(*--from);
               }
               return this;
           }
   
           ArrayNode* enlargeBy(int64_t delta, const ObjectRef& val = ObjectRef(nullptr)) {
               while (delta-- > 0) {
                   data_.push_back(val);
               }
               return this;
           }
   
           ArrayNode* shrinkBy(int64_t delta) {
               while (delta-- > 0) {
                   data_.pop_back();
               }
               return this;
           }
   
           static constexpr int64_t kInitSize = 4;
           static constexpr int64_t kIncFactor = 2;
   
           ContainerType data_;
   
           template <typename>
           friend class Array;
       };
   
       template <typename T, typename IterType>
       struct is_valid_iterator
           : std::bool_constant<std::is_base_of_v<
           T, std::remove_cv_t<std::remove_reference_t<decltype(*std::declval<IterType>())>>>> {};
   
       template <typename T, typename IterType>
       struct is_valid_iterator<Optional<T>, IterType> : is_valid_iterator<T, IterType> {};
   
       template <typename T, typename IterType>
       inline constexpr bool is_valid_iterator_v = is_valid_iterator<T, IterType>::value;
   
       template <typename Converter, typename TIter>
       class ConstIterAdapter {
       public:
           using difference_type = typename std::iterator_traits<TIter>::difference_type;
           using value_type = const typename Converter::ResultType;
           using pointer = const typename Converter::ResultType*;
           using reference = const typename Converter::ResultType&;
           using iterator_category = typename std::iterator_traits<TIter>::iterator_category;
   
           explicit ConstIterAdapter() : iter_() {}
           explicit ConstIterAdapter(TIter iter) : iter_(iter) {}
           ConstIterAdapter& operator++() {
               ++iter_;
               return *this;
           }
           ConstIterAdapter& operator--() {
               --iter_;
               return *this;
           }
           ConstIterAdapter operator++(int) {
               auto copy = *this;
               ++iter_;
               return copy;
           }
           ConstIterAdapter operator--(int) {
               auto copy = *this;
               --iter_;
               return copy;
           }
   
           ConstIterAdapter& operator+=(difference_type offset) {
               iter_ += offset;
               return *this;
           }
   
           ConstIterAdapter operator+(difference_type offset) const {
               auto copy = *this;
               copy += offset;
               return copy;
           }
   
           ConstIterAdapter& operator-=(difference_type offset) {
               iter_ -= offset;
               return *this;
           }
   
           ConstIterAdapter operator-(difference_type offset) const {
               auto copy = *this;
               copy -= offset;
               return copy;
           }
   
           inline difference_type operator-(const ConstIterAdapter& rhs) const {
               return iter_ - rhs.iter_;
           }
   
           bool operator==(const ConstIterAdapter& other) const { return iter_ == other.iter_; }
           bool operator!=(const ConstIterAdapter& other) const { return !(*this == other); }
           value_type operator*() const { return Converter::convert(*iter_); }
   
           const TIter& iter() const { return iter_; }
   
       protected:
           TIter iter_;
       };
   
       template <typename Converter, typename TIter>
       class IterAdapter : public ConstIterAdapter<Converter, TIter> {
       public:
           using difference_type = typename std::iterator_traits<TIter>::difference_type;
           using value_type = typename Converter::ResultType;
           using pointer = typename Converter::ResultType*;
           using reference = typename Converter::ResultType&;
           using iterator_category = typename std::iterator_traits<TIter>::iterator_category;
   
           using _BaseType = ConstIterAdapter<Converter, TIter>;
   
           explicit IterAdapter() : _BaseType() {}
           explicit IterAdapter(TIter iter) : _BaseType(iter) {}
           explicit IterAdapter(_BaseType const_iter) : _BaseType(const_iter) {}
   
           IterAdapter& operator++() {
               _BaseType::operator++();
               return *this;
           }
           IterAdapter& operator--() {
               _BaseType::operator--();
               return *this;
           }
           IterAdapter operator++(int) {
               auto copy = *this;
               _BaseType::operator++();
               return copy;
           }
           IterAdapter operator--(int) {
               auto copy = *this;
               _BaseType::operator--();
               return copy;
           }
   
           IterAdapter& operator+=(difference_type offset) {
               _BaseType::operator+=(offset);
               return *this;
           }
   
           IterAdapter operator+(difference_type offset) const {
               auto copy = *this;
               copy += offset;
               return copy;
           }
   
           IterAdapter& operator-=(difference_type offset) {
               _BaseType::operator-=(offset);
               return *this;
           }
   
           IterAdapter operator-(difference_type offset) const {
               auto copy = *this;
               copy -= offset;
               return copy;
           }
   
           value_type operator*() const { return Converter::convert(*this->iter_); }
   
           inline difference_type operator-(const IterAdapter& rhs) const {
               return _BaseType::operator-(rhs);
           }
       };
   
       template <typename Converter, typename TIter>
       class ConstReverseIterAdapter {
       public:
           using difference_type = typename std::iterator_traits<TIter>::difference_type;
           using value_type = const typename Converter::ResultType;
           using pointer = const typename Converter::ResultType*;
           using reference = const typename Converter::ResultType&;  // NOLINT(*)
           using iterator_category = typename std::iterator_traits<TIter>::iterator_category;
   
           explicit ConstReverseIterAdapter() : iter_() {}
           explicit ConstReverseIterAdapter(const TIter iter) : iter_(iter) {}
           ConstReverseIterAdapter& operator++() {
               --iter_;
               return *this;
           }
           ConstReverseIterAdapter& operator--() {
               ++iter_;
               return *this;
           }
           ConstReverseIterAdapter& operator++(int) {
               auto copy = *this;
               --iter_;
               return copy;
           }
           ConstReverseIterAdapter& operator--(int) {
               auto copy = *this;
               ++iter_;
               return copy;
           }
   
           ConstReverseIterAdapter& operator+=(difference_type offset) {
               iter_ += offset;
               return *this;
           }
   
           ConstReverseIterAdapter operator+(difference_type offset) const {
               auto copy = *this;
               copy += offset;
               return copy;
           }
   
           ConstReverseIterAdapter& operator-=(difference_type offset) {
               iter_ -= offset;
               return *this;
           }
   
           ConstReverseIterAdapter operator-(difference_type offset) const {
               auto copy = *this;
               copy -= offset;
               return copy;
           }
   
           inline difference_type operator-(const ConstReverseIterAdapter& rhs) const {
               return rhs.iter_ - iter_;
           }
   
           bool operator==(const ConstReverseIterAdapter& other) const { return iter_ == other.iter_; }
           bool operator!=(const ConstReverseIterAdapter& other) const { return !(*this == other); }
           value_type operator*() const { return Converter::convert(*iter_); }
   
           const TIter& iter() const { return iter_; }
   
       protected:
           TIter iter_;
       };
   
       template <typename Converter, typename TIter>
       class ReverseIterAdapter : public ConstReverseIterAdapter<Converter, TIter> {
       public:
           using difference_type = typename std::iterator_traits<TIter>::difference_type;
           using value_type = typename Converter::ResultType;
           using pointer = typename Converter::ResultType*;
           using reference = typename Converter::ResultType&;
           using iterator_category = typename std::iterator_traits<TIter>::iterator_category;
   
           using _BaseType = ConstReverseIterAdapter<Converter, TIter>;
   
           explicit ReverseIterAdapter() : _BaseType() {}
           explicit ReverseIterAdapter(TIter iter) : _BaseType(iter) {}
           explicit ReverseIterAdapter(_BaseType const_iter) : _BaseType(const_iter) {}
   
           ReverseIterAdapter& operator++() {
               _BaseType::operator++();
               return *this;
           }
           ReverseIterAdapter& operator--() {
               _BaseType::operator--();
               return *this;
           }
           ReverseIterAdapter operator++(int) {
               auto copy = *this;
               _BaseType::operator++();
               return copy;
           }
           ReverseIterAdapter operator--(int) {
               auto copy = *this;
               _BaseType::operator--();
               return copy;
           }
   
           ReverseIterAdapter& operator+=(difference_type offset) {
               _BaseType::operator+=(offset);
               return *this;
           }
   
           ReverseIterAdapter operator+(difference_type offset) const {
               auto copy = *this;
               copy += offset;
               return copy;
           }
   
           ReverseIterAdapter& operator-=(difference_type offset) {
               _BaseType::operator-=(offset);
               return *this;
           }
   
           ReverseIterAdapter operator-(difference_type offset) const {
               auto copy = *this;
               copy -= offset;
               return copy;
           }
   
           value_type operator*() const { return Converter::convert(*this->iter_); }
   
           inline difference_type operator-(const ReverseIterAdapter& rhs) const {
               return _BaseType::operator-(rhs);
           }
       };
   
       template <typename T>
       class Array : public ObjectRef {
       public:
           using value_type = T;
           
           Array(ObjectPtr<Object> n) {
               static_assert(std::is_base_of_v<ObjectRef, T>, "T must inherit from ObjectRef");
               data_ = std::move(n);
           }
   
           Array() : Array(ArrayNode::Empty()) {}
   
           Array(Array<T>&& other) {  // NOLINT(*)
               data_ = std::move(other.data_);
           }
   
           Array(const Array<T>& other) {  // NOLINT(*)
               data_ = other.data_;
           }
   
           template <typename U,
               typename = typename std::enable_if_t<std::is_convertible_v<U, T>>>
           Array(const Array<U>& init) : Array() {  // NOLINT(*)
               assign(init.begin(), init.end());
           }
   
           template <typename IterType>
           Array(IterType first, IterType last) : Array() {
               static_assert(is_valid_iterator_v<T, IterType>,
                   "IterType cannot be inserted into a Array<T>");
               assign(first, last);
           }
   
           Array(std::initializer_list<T> init) : Array() {  // NOLINT(*)
               assign(init.begin(), init.end());
           }
   
           Array(const std::vector<T>& init) : Array() {  // NOLINT(*)
               assign(init.begin(), init.end());
           }
   
           template <typename U,
               typename = typename std::enable_if_t<std::is_convertible_v<U, T>>>
           Array(const std::vector<U>& init) : Array() {  // NOLINT(*)
               assign(init.begin(), init.end());
           }
   
           Array(const std::unordered_set<T>& init) : Array() {  // NOLINT(*)
               assign(init.begin(), init.end());
           }
   
           template <typename U,
               typename = typename std::enable_if_t<std::is_convertible_v<U, T>>>
           Array(const std::unordered_set<U>& init) : Array() {  // NOLINT(*)
               assign(init.begin(), init.end());
           }
   
           explicit Array(const size_t n, const T& val) : Array(ArrayNode::CreateRepeated(n, val)) {}
   
           Array<T>& operator=(Array<T>&& other) {
               data_ = std::move(other.data_);
               return *this;
           }
   
           Array<T>& operator=(const Array<T>& other) {
               data_ = other.data_;
               return *this;
           }
   
           static Array Get(ObjectPtr<Object> n) {
               return Array(n);
           }
   
           static Array Init() {
               return {};
           }
   
           operator std::vector<T>() const {
               return std::vector<T>(this->begin(), this->end());
           }
   
           template <typename U,
               typename = typename std::enable_if_t<std::is_convertible_v<T, U>>>
           operator std::vector<U>() const {
               return std::vector<U>(this->begin(), this->end());
           }
   
           Array<T> clone(int64_t depth = 1) const {
               return Downcast<Array<T>>(ObjectRef::clone(depth));
           }
   
       public:
           struct ValueConverter {
               using ResultType = T;
               static T convert(const ObjectRef& n) { return DowncastNoCheck<T>(n); }
           };
   
           using iterator = IterAdapter<ValueConverter, ArrayNode::iterator>;
           using const_iterator = ConstIterAdapter<ValueConverter, ArrayNode::iterator>;
           using reverse_iterator = ReverseIterAdapter<ValueConverter, ArrayNode::iterator>;
           using const_reverse_iterator = ConstReverseIterAdapter<ValueConverter, ArrayNode::iterator>;
   
           iterator begin() { return iterator(getArrayNode()->begin()); }
           const_iterator begin() const { return const_iterator(getArrayNode()->begin()); }
   
           iterator end() { return iterator(getArrayNode()->end()); }
           const_iterator end() const { return const_iterator(getArrayNode()->end()); }
   
           reverse_iterator rbegin() {
               return reverse_iterator(getArrayNode()->end() - 1);
           }
           const_reverse_iterator rbegin() const {
               return const_reverse_iterator(getArrayNode()->end() - 1);
           }
   
           reverse_iterator rend() {
               return reverse_iterator(getArrayNode()->begin() - 1);
           }
   
           const_reverse_iterator rend() const {
               return const_reverse_iterator(getArrayNode()->begin() - 1);
           }
   
       public:
           T operator[](int64_t i) const {
               auto array_size = this->size();
               ICRAFT_CHECK(i >= -(int64_t)array_size && i < (int64_t)array_size)
                   .append("index out of range, index = {} vs. size = {}", i, array_size);
               if (i < 0) i = array_size + i;
               ArrayNode* p = getArrayNode();
               ICRAFT_CHECK(p != nullptr).append("ValueError: cannot index a null array");
               return DowncastNoCheck<T>(*(p->begin() + i));
           }
   
           size_t size() const {
               ArrayNode* p = getArrayNode();
               return p == nullptr ? 0 : getArrayNode()->size();
           }
   
           size_t capacity() const {
               ArrayNode* p = getArrayNode();
               return p == nullptr ? 0 : getArrayNode()->capacity();
           }
   
           bool empty() const { return size() == 0; }
   
           T front() const {
               ArrayNode* p = getArrayNode();
               ICRAFT_CHECK(p != nullptr).append("ValueError: cannot index a null array");
               ICRAFT_CHECK_GT(p->size(), 0).append("IndexError: cannot index an empty array");
               return DowncastNoCheck<T>(*(p->begin()));
           }
   
           T back() const {
               ArrayNode* p = getArrayNode();
               ICRAFT_CHECK(p != nullptr).append("ValueError: cannot index a null array");
               ICRAFT_CHECK_GT(p->size(), 0).append("IndexError: cannot index an empty array");
               return DowncastNoCheck<T>(*(p->end() - 1));
           }
   
       public:
           void push_back(const T& item) {
               getArrayNode()->data_.push_back(item);
           }
   
           void insert(const_iterator position, const T& val) {
               ICRAFT_CHECK(data_ != nullptr).append("ValueError: cannot insert a null array");
               getArrayNode()->data_.insert(position.iter(), val);
           }
   
           void insert(int64_t index, const T& val) {
               auto i = getUIndex(index, true);
               insert(this->begin() + i, val);
           }
   
           template <typename IterType>
           void insert(const_iterator position, IterType first, IterType last) {
               static_assert(
                   is_valid_iterator_v<T, IterType>,
                   "IterType cannot be inserted into a Array<T>"
               );
   
               if (first == last) {
                   return;
               }
               ICRAFT_CHECK(data_ != nullptr).append("ValueError: cannot insert a null array");
               getArrayNode()->data_.insert(position.iter(), first, last);
           }
   
           void pop_back() {
               ICRAFT_CHECK(data_ != nullptr).append("ValueError: cannot pop_back because array is null");
               ICRAFT_CHECK_GT(getArrayNode()->size(), 0).append("ValueError: cannot pop_back because array is empty");
               getArrayNode()->data_.pop_back();
           }
   
           iterator erase(const_iterator position) {
               ICRAFT_CHECK(data_ != nullptr).append("ValueError: cannot erase a null array");
               int64_t st = position - begin();
               int64_t size = getArrayNode()->size();
               ICRAFT_CHECK(0 <= st && st < size).append("ValueError: cannot erase at index {}, because Array size is {}", st, size);
               return iterator(getArrayNode()->data_.erase(position.iter()));
           }
   
           uint64_t erase(int64_t index) {
               auto i = getUIndex(index);
               return erase(this->begin() + index) - begin();
           }
   
           iterator erase(const_iterator first, const_iterator last) {
               if (first == last) {
                   return iterator(last);
               }
               ICRAFT_CHECK(data_ != nullptr).append("ValueError: cannot erase a null array");
               int64_t size = getArrayNode()->size();
               int64_t st = first - begin();
               int64_t ed = last - begin();
               ICRAFT_CHECK_LT(st, ed).append("ValueError: cannot erase array in range [{},{})", st, ed);
               ICRAFT_CHECK(0 <= st && st <= size && 0 <= ed && ed <= size).append(
                   "ValueError: cannot erase array in range [{},{}), because array size is {}", 
                   st, 
                   ed, 
                   size
               );
               return iterator(getArrayNode()->data_.erase(first.iter(), last.iter()));
           }
   
           void resize(int64_t n) {
               ICRAFT_CHECK_GE(n, 0).append("ValueError: cannot resize an Array to negative size");
               if (data_ == nullptr) {
                   data_ = ArrayNode::Empty(capacity);
                   return;
               }
               getArrayNode()->data_.resize(n);
           }
   
           void reserve(int64_t n) {
               if (data_ == nullptr) {
                   data_ = ArrayNode::Empty(capacity);
                   return;
               }
               getArrayNode()->data_.reserve(n);
           }
   
           void clear() {
               if (data_ != nullptr) {
                   getArrayNode()->data_.clear();
               }
           }
   
       public:
           void set(int64_t i, T value) {
               auto array_size = this->size();
               ICRAFT_CHECK(i >= -(int64_t)array_size && i < (int64_t)array_size)
                   .append("index out of range, index = {} vs. size = {}", i, array_size);
               if (i < 0) i = array_size + i;
               getArrayNode()->data_[i] = std::move(value);
           }
   
           ArrayNode* getArrayNode() const { return static_cast<ArrayNode*>(data_.get()); }
   
           template <typename F, typename U = std::invoke_result_t<F, T>>
           Array<U> map(F fmap) const {
               return Array<U>::Get(MapHelper(data_, fmap));
           }
   
           template <typename F, typename = std::enable_if_t<std::is_same_v<bool, std::invoke_result_t<F, T>>>>
           Array<T> filter(F ffilter) const {
               auto node = getArrayNode();
               auto data = ArrayNode::CopyFrom(node->size(), node);
               FilterHelper(data, ffilter);
               return Array<T>::Get(std::move(data));
           }
   
           template <typename F, typename = std::enable_if_t<std::is_same_v<bool, std::invoke_result_t<F, T>>>>
           void remove(F fremove) {
               auto ffilter = [&fremove](T t) {
                   return !fremove(std::move(t));
               };
               FilterHelper(data_, ffilter);
           }
   
           bool contains(T value) const {
               return std::any_of(begin(), end(), [&](auto i) {return i == value; });
           }
   
           bool exactContains(T value) const {
               return std::any_of(begin(), end(), [&](auto i) {return i.same_as(value); });
           }
   
           void reverse() {
               auto array_node = getArrayNode();
               std::reverse(array_node->begin(), array_node->end());
           }
   
           void swap(int64_t lhs_index, int64_t rhs_index) {
               auto array_node = getArrayNode();
               std::swap(array_node->data_[getUIndex(lhs_index)], array_node->data_[getUIndex(rhs_index)]);
           }
   
           template <typename IterType>
           void assign(IterType first, IterType last) {
               int64_t cap = std::distance(first, last);
               ICRAFT_CHECK_GE(cap, 0).append("ValueError: cannot construct an Array of negative size");
               ArrayNode* p = getArrayNode();
               if (p != nullptr && data_.unique() && p->capacity() >= cap) {
                   // do not have to make new space
                   p->clear();
               }
               else {
                   // create new space
                   data_ = ArrayNode::Empty(cap);
                   p = getArrayNode();
               }
               
               for (auto it = first; it != last; it++) {
                   p->data_.push_back(ObjectRef(T(*it)));
               }
           }
   
           void concat(const Array<T>& rhs) {
               for (const auto& x : rhs) {
                   this->push_back(x);
               }
           }
   
           using ContainerType = ArrayNode;
   
       private:
           uint64_t getUIndex(int64_t i, bool insert = false) const {
               auto array_size = this->size();
               if (insert) array_size++;
               ICRAFT_CHECK(i >= -(int64_t)array_size && i < (int64_t)array_size)
                   .append("index out of range, index = {} vs. size = {}", i, array_size);
               if (i < 0) i = array_size + i;
               return i;
           }
   
           template <typename F, typename U = std::invoke_result_t<F, T>>
           static ObjectPtr<Object> MapHelper(ObjectPtr<Object> data, F fmap) {
               if (data == nullptr) {
                   return nullptr;
               }
   
               ICRAFT_CHECK(data->isInstance<ArrayNode>());
   
               constexpr bool compatible_types = is_valid_iterator_v<T, U*> || is_valid_iterator_v<U, T*>;
   
               ObjectPtr<ArrayNode> output = nullptr;
               auto arr = static_cast<ArrayNode*>(data.get());
   
               auto it = arr->begin();
               if constexpr (compatible_types) {
                   bool all_identical = true;
                   for (; it != arr->end(); it++) {
                       U mapped = fmap(DowncastNoCheck<T>(*it));
                       if (!mapped.same_as(*it)) {
                           all_identical = false;
                           output = ArrayNode::CreateRepeated(arr->size(), U());
                           output->initRange(0, arr->begin(), it);
                           output->setItem(it - arr->begin(), std::move(mapped));
                           it++;
                           break;
                       }
                   }
                   if (all_identical) {
                       return data;
                   }
               }
               else {
                   output = ArrayNode::CreateRepeated(arr->size(), U());
               }
   
               for (; it != arr->end(); it++) {
                   U mapped = fmap(DowncastNoCheck<T>(*it));
                   output->setItem(it - arr->begin(), std::move(mapped));
               }
   
               return output;
           }
   
           template <typename F, typename = std::enable_if_t<std::is_same_v<bool, std::invoke_result_t<F, T>>>>
           static void FilterHelper(ObjectPtr<Object> data, F ffilter) {
               if (data == nullptr) {
                   return;
               }
   
               ICRAFT_CHECK(data->isInstance<ArrayNode>());
   
               auto arr = static_cast<ArrayNode*>(data.get());
               auto arr_size = arr->size();
               auto keep_map = std::vector<bool>(arr_size, false);
               size_t keep_num = 0;
               for (auto it = arr->begin(); it != arr->end(); it++) {
                   auto keep = ffilter(DowncastNoCheck<T>(*it));
                   if (keep) {
                       keep_map[it - arr->begin()] = true;
                       keep_num++;
                   }
               }
   
               auto all_identical = keep_num == arr_size;
   
               if (all_identical) return;
   
               size_t keep_count = 0;
               for (auto i = 0; i < keep_map.size(); i++) {
                   auto keep = keep_map[i];
                   if (keep) {
                       if (keep_count != i) arr->setItem(keep_count, std::move(arr->at(i)));
                       keep_count++;
                   }
               }
               arr->shrinkBy(arr_size - keep_num);
               return;
           }
       };
   }
