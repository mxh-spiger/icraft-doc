.. _exhale_class_classicraft_1_1xir_1_1_attr_pattern_node:

Class AttrPatternNode
=====================

- Defined in :ref:`file_icraft-xir_core_pattern.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< AttrPatternNode, PatternNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::AttrPatternNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: