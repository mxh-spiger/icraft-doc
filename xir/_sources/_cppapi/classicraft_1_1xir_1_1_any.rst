.. _exhale_class_classicraft_1_1xir_1_1_any:

Class Any
=========

- Defined in :ref:`file_icraft-xir_base_any.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public ObjectRef`` (:ref:`exhale_class_classicraft_1_1xir_1_1_object_ref`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Any
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: