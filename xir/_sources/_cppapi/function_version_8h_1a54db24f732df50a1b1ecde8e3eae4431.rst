.. _exhale_function_version_8h_1a54db24f732df50a1b1ecde8e3eae4431:

Function icraft::xir::MajorVersionNum
=====================================

- Defined in :ref:`file_icraft-xir_base_version.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::MajorVersionNum()
   :project: Icraft XIR