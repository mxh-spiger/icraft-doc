.. _exhale_function_data_8h_1a9c0be3c7885099d93fa52204c5e3ff29:

Function icraft::xir::operator&&(bool, const Bool&)
===================================================

- Defined in :ref:`file_icraft-xir_core_data.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::operator&&(bool, const Bool&)
   :project: Icraft XIR