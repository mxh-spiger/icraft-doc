
.. _file_icraft-xir_core_network.h:

File network.h
==============

|exhale_lsh| :ref:`Parent directory <dir_icraft-xir_core>` (``icraft-xir\core``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-xir\core\network.h``)
------------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-xir_core_network.h.rst





Includes
--------


- ``icraft-xir/base/map.h``

- ``icraft-xir/base/string.h``

- ``icraft-xir/base/version.h``

- ``icraft-xir/core/operation.h``

- ``icraft-xir/core/pattern.h``

- ``icraft-xir/core/serialize.h``

- ``unordered_set``






Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__ir`

- :ref:`namespace_icraft__xir`


Classes
-------


- :ref:`exhale_class_classicraft_1_1xir_1_1_network`

- :ref:`exhale_class_classicraft_1_1xir_1_1_network_node`

- :ref:`exhale_class_classicraft_1_1xir_1_1_network_rewriter`

- :ref:`exhale_class_classicraft_1_1xir_1_1_network_view`

- :ref:`exhale_class_classicraft_1_1xir_1_1_network_view_node`


Enums
-----


- :ref:`exhale_enum_network_8h_1a52f12ca1331504c9536b043a7d4d2ed7`

