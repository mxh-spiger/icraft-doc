.. _exhale_class_classicraft_1_1xir_1_1_conv2d:

Class Conv2d
============

- Defined in :ref:`file_icraft-xir_ops_conv2d.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< Conv2d, Conv2dNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Conv2d
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: