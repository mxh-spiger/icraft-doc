.. _exhale_class_classicraft_1_1xir_1_1_value_node:

Class ValueNode
===============

- Defined in :ref:`file_icraft-xir_core_data.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< ValueNode, DataNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Derived Type
************

- ``public NodeBase< ParamsNode, ValueNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::ValueNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: