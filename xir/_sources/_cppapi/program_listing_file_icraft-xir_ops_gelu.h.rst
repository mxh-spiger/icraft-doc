
.. _program_listing_file_icraft-xir_ops_gelu.h:

Program Listing for File gelu.h
===============================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_ops_gelu.h>` (``icraft-xir\ops\gelu.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/operation.h>
   
   namespace icraft::xir {
       class GeluNode : public OpNodeBase<GeluNode, OneResult, IsActivate> {
       public:
           XIR_DLL virtual void validate() const override;
       };
   
       class Gelu : public OpBase<Gelu, GeluNode> {
       public:
           Gelu() = default;
   
           XIR_DLL Gelu(Value input);
       };
   }
