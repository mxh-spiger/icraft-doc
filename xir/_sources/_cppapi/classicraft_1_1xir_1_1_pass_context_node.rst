.. _exhale_class_classicraft_1_1xir_1_1_pass_context_node:

Class PassContextNode
=====================

- Defined in :ref:`file_icraft-xir_core_pass.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< PassContextNode, Node >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::PassContextNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: