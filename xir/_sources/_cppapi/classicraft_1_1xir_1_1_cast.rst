.. _exhale_class_classicraft_1_1xir_1_1_cast:

Class Cast
==========

- Defined in :ref:`file_icraft-xir_ops_cast.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< Cast, CastNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Cast
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: