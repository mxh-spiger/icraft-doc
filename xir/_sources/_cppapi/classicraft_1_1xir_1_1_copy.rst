.. _exhale_class_classicraft_1_1xir_1_1_copy:

Class Copy
==========

- Defined in :ref:`file_icraft-xir_ops_copy.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< Copy, CopyNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Copy
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: