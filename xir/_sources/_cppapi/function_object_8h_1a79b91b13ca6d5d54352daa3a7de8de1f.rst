.. _exhale_function_object_8h_1a79b91b13ca6d5d54352daa3a7de8de1f:

Template Function icraft::xir::make_object
==========================================

- Defined in :ref:`file_icraft-xir_base_object.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::make_object(Args&&...)
   :project: Icraft XIR