.. _exhale_class_classicraft_1_1xir_1_1_attr_field_info_node:

Class AttrFieldInfoNode
=======================

- Defined in :ref:`file_icraft-xir_core_attrs.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< AttrFieldInfoNode, Node >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::AttrFieldInfoNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: