.. _exhale_class_classicraft_1_1xir_1_1_reshape_node:

Class ReshapeNode
=================

- Defined in :ref:`file_icraft-xir_ops_reshape.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< ReshapeNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::ReshapeNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: