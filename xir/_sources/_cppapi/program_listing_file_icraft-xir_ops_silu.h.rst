
.. _program_listing_file_icraft-xir_ops_silu.h:

Program Listing for File silu.h
===============================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_ops_silu.h>` (``icraft-xir\ops\silu.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/operation.h>
   
   namespace icraft::xir {
       class SiluNode : public OpNodeBase<SiluNode, OneResult, IsActivate> {
       public:
           XIR_DLL virtual void validate() const override;
       };
   
       class Silu : public OpBase<Silu, SiluNode> {
       public:
           Silu() = default;
   
           XIR_DLL Silu(Value input);
       };
   }
