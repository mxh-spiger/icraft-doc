.. _exhale_class_classicraft_1_1xir_1_1_float_type_node:

Class FloatTypeNode
===================

- Defined in :ref:`file_icraft-xir_core_data_type.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< FloatTypeNode, ScalarTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::FloatTypeNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: