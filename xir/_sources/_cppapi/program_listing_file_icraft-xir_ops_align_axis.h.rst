
.. _program_listing_file_icraft-xir_ops_align_axis.h:

Program Listing for File align_axis.h
=====================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_ops_align_axis.h>` (``icraft-xir\ops\align_axis.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/operation.h>
   
   namespace icraft::xir {
       class AlignedUnitNode : public NodeBase<AlignedUnitNode, Node> {
       public:
           uint64_t index;     
           uint64_t unit;      
   
           virtual void accept(AttrVisitor& visitor) override {
               visitor.visit("index", index);
               visitor.visit("unit", unit);
           }
   
           virtual bool reduceAccept(AlignedUnitNode* other, const ReduceVisitor& reduce) const {
               return reduce(index, other->index) && reduce(unit, other->unit);
           }
       };
   
       class AlignedUnit : public HandleBase<AlignedUnit, Handle, AlignedUnitNode> {
       public:
           AlignedUnit() = default;
   
           XIR_DLL AlignedUnit(uint64_t index, uint64_t unit);
       };
   
       class AlignAxisNode : public OpNodeBase<AlignAxisNode, OneResult> {
       public:
           Array<AlignedUnit> aligned_units;       
   
           ICRAFT_DECLARE_ATTRS(AlignAxisNode) {
               ICRAFT_ATTR_FIELD(aligned_units);
           }
       };
   
       class AlignAxis : public OpBase<AlignAxis, AlignAxisNode> {
       public:
           AlignAxis() = default;
   
           XIR_DLL AlignAxis(Value input, Array<AlignedUnit> aligned_units);
       };
   }
