.. _exhale_function_string_8h_1a5415fd0bce16fb23ec98e50c0b82b42e:

Function icraft::xir::operator>=(const char \*, const String&)
==============================================================

- Defined in :ref:`file_icraft-xir_base_string.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::operator>=(const char *, const String&)
   :project: Icraft XIR