.. _exhale_function_reflection_8h_1a3f420755305a0d0bbc43dd197083aafd:

Function icraft::xir::HasReprOf
===============================

- Defined in :ref:`file_icraft-xir_core_reflection.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::HasReprOf(const Handle&)
   :project: Icraft XIR