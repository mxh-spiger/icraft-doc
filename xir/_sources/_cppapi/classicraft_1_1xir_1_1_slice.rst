.. _exhale_class_classicraft_1_1xir_1_1_slice:

Class Slice
===========

- Defined in :ref:`file_icraft-xir_ops_slice.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< Slice, SliceNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Slice
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: