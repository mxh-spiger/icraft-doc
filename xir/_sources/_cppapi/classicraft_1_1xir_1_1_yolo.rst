.. _exhale_class_classicraft_1_1xir_1_1_yolo:

Class Yolo
==========

- Defined in :ref:`file_icraft-xir_ops_yolo.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< Yolo, YoloNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Yolo
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: