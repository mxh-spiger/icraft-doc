.. _exhale_class_classicraft_1_1xir_1_1_data_type_node:

Class DataTypeNode
==================

- Defined in :ref:`file_icraft-xir_core_data_type.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public Node`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node`)


Derived Types
*************

- ``public NodeBase< TensorTypeNode, DataTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)
- ``public ScalarTypeNode`` (:ref:`exhale_class_classicraft_1_1xir_1_1_scalar_type_node`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::DataTypeNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: