.. _exhale_define_operation_8h_1a0587adc4fa59710015e27ce34a6d4f20:

Define ICRAFT_DECLARE_ATTRS
===========================

- Defined in :ref:`file_icraft-xir_core_operation.h`


Define Documentation
--------------------


.. doxygendefine:: ICRAFT_DECLARE_ATTRS
   :project: Icraft XIR