.. _exhale_class_classicraft_1_1xir_1_1_output:

Class Output
============

- Defined in :ref:`file_icraft-xir_core_operation.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< Output, OutputNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Output
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: