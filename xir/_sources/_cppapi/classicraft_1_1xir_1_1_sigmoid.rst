.. _exhale_class_classicraft_1_1xir_1_1_sigmoid:

Class Sigmoid
=============

- Defined in :ref:`file_icraft-xir_ops_sigmoid.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< Sigmoid, SigmoidNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Sigmoid
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: