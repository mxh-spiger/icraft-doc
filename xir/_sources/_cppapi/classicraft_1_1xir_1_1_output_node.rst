.. _exhale_class_classicraft_1_1xir_1_1_output_node:

Class OutputNode
================

- Defined in :ref:`file_icraft-xir_core_operation.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< OutputNode, OutputLike >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::OutputNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: