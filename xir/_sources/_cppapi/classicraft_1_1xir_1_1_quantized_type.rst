.. _exhale_class_classicraft_1_1xir_1_1_quantized_type:

Class QuantizedType
===================

- Defined in :ref:`file_icraft-xir_core_data_type.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< QuantizedType, BaseQuantizedType, QuantizedTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Derived Type
************

- ``public VirtualBase< NormalizedQuantizedType, QuantizedType, NormalizedQuantizedTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::QuantizedType
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: