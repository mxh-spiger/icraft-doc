.. _exhale_class_classicraft_1_1xir_1_1_copy_node:

Class CopyNode
==============

- Defined in :ref:`file_icraft-xir_ops_copy.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< CopyNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::CopyNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: