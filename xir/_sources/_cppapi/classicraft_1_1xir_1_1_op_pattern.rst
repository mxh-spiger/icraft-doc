.. _exhale_class_classicraft_1_1xir_1_1_op_pattern:

Class OpPattern
===============

- Defined in :ref:`file_icraft-xir_core_pattern.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< OpPattern, Pattern, OpPatternNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::OpPattern
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: