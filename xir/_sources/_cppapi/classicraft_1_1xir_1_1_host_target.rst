.. _exhale_class_classicraft_1_1xir_1_1_host_target:

Class HostTarget
================

- Defined in :ref:`file_icraft-xir_core_compile_target.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< HostTarget, CompileTarget, HostTargetNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::HostTarget
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: