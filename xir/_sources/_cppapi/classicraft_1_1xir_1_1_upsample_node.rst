.. _exhale_class_classicraft_1_1xir_1_1_upsample_node:

Class UpsampleNode
==================

- Defined in :ref:`file_icraft-xir_ops_upsample.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< UpsampleNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::UpsampleNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: