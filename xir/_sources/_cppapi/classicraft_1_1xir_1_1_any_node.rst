.. _exhale_class_classicraft_1_1xir_1_1_any_node:

Class AnyNode
=============

- Defined in :ref:`file_icraft-xir_base_any.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public Object`` (:ref:`exhale_class_classicraft_1_1xir_1_1_object`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::AnyNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: