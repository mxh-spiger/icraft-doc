.. _exhale_function_string_8h_1ac4e429cee0b2f43d16eca3b016f5185c:

Function icraft::xir::operator!=(const String&, const String&)
==============================================================

- Defined in :ref:`file_icraft-xir_base_string.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::operator!=(const String&, const String&)
   :project: Icraft XIR