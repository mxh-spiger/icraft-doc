
.. _program_listing_file_icraft-xir_ops_layernorm.h:

Program Listing for File layernorm.h
====================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_ops_layernorm.h>` (``icraft-xir\ops\layernorm.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/operation.h>
   
   namespace icraft::xir {
       class LayernormNode : public OpNodeBase<LayernormNode, OneResult> {
       public:
           Array<IntImm> axis;         
           double eps;                 
           Optional<Params> gamma;     
           Optional<Params> beta;      
   
           ICRAFT_DECLARE_ATTRS(LayernormNode) {
               ICRAFT_ATTR_FIELD(axis);
               ICRAFT_ATTR_FIELD(eps).set_default(1e-5);
               ICRAFT_ATTR_FIELD(gamma);
               ICRAFT_ATTR_FIELD(beta);
           }
   
           XIR_DLL virtual void validate() const override;
       };
   
       class Layernorm : public OpBase<Layernorm, LayernormNode> {
       public:
           Layernorm() = default;
   
           XIR_DLL Layernorm(
               Value input, 
               Array<IntImm> axis, 
               double eps = 1e-5, 
               Optional<Params> gamma = NullOpt,
               Optional<Params> beta = NullOpt
           );
       };
   }
   #pragma once
