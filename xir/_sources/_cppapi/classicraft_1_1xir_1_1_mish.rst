.. _exhale_class_classicraft_1_1xir_1_1_mish:

Class Mish
==========

- Defined in :ref:`file_icraft-xir_ops_mish.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< Mish, MishNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Mish
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: