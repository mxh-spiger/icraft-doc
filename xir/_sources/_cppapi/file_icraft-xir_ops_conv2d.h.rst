
.. _file_icraft-xir_ops_conv2d.h:

File conv2d.h
=============

|exhale_lsh| :ref:`Parent directory <dir_icraft-xir_ops>` (``icraft-xir\ops``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-xir\ops\conv2d.h``)
----------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-xir_ops_conv2d.h.rst





Includes
--------


- ``icraft-xir/core/operation.h``






Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__xir`


Classes
-------


- :ref:`exhale_class_classicraft_1_1xir_1_1_conv2d`

- :ref:`exhale_class_classicraft_1_1xir_1_1_conv2d_node`


Enums
-----


- :ref:`exhale_enum_conv2d_8h_1a14d24d90ab4ba2956e92e27890ba4c91`

