.. _exhale_function_pattern_8h_1a1280260bbcab517cafd62246c537eb06:

Function icraft::xir::Wildcard
==============================

- Defined in :ref:`file_icraft-xir_core_pattern.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::Wildcard()
   :project: Icraft XIR