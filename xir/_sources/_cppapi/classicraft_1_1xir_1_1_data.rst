.. _exhale_class_classicraft_1_1xir_1_1_data:

Class Data
==========

- Defined in :ref:`file_icraft-xir_core_data.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public VirtualBase< Data, Handle, DataNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)


Derived Types
*************

- ``public VirtualBase< ScalarImm, Data, ScalarImmNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< Value, Data, ValueNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Data
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: