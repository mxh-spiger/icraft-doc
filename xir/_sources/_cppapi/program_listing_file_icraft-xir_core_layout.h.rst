
.. _program_listing_file_icraft-xir_core_layout.h:

Program Listing for File layout.h
=================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_core_layout.h>` (``icraft-xir\core\layout.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/base/optional.h>
   #include <icraft-xir/core/reflection.h>
   #include <icraft-xir/core/node.h>
   #include <icraft-xir/base/dllexport.h>
   
   #include <regex>
   
   namespace icraft::xir {
       class AxisNameNode : public NodeBase<AxisNameNode, Node> {
       public:
           std::string name;       
   
           virtual void accept(AttrVisitor& visitor) override {
               visitor.visit("name", name);
           }
   
           bool reduceAccept(AxisNameNode* other, const ReduceVisitor& reduce) const {
               return reduce(name, other->name);
           }
       };
   
       class AxisName : public HandleBase<AxisName, Handle, AxisNameNode> {
       public:
           AxisName() = default;
   
           XIR_DLL explicit AxisName(char name);
   
           bool operator==(const AxisName& other) const {
               return (*this)->name == other->name;
           }
       };
   
       class AxisUnitNode : public NodeBase<AxisUnitNode, AxisNameNode> {
       public:
           uint64_t unit;      
   
           virtual void accept(AttrVisitor& visitor) override {
               visitor.visit("unit", unit);
               AxisNameNode::accept(visitor);
           }
   
           bool reduceAccept(AxisUnitNode* other, const ReduceVisitor& reduce) const {
               return reduce(unit, other->unit)
                   && AxisNameNode::reduceAccept(other, reduce);
           }
       };
   
       class AxisUnit : public HandleBase<AxisUnit, AxisName, AxisUnitNode> {
       public:
           AxisUnit() = default;
           XIR_DLL AxisUnit(char name, uint64_t unit);
   
           explicit AxisUnit(char name) : AxisUnit(name, 0) {}
   
           void setUnit(uint64_t unit) {
               get_mutable()->unit = unit;
           }
   
           bool operator==(const AxisUnit& other) const {
               return (*this)->name == other->name && (*this)->unit == other->unit;
           }
       };
   
       class LayoutNode : public NodeBase<LayoutNode, Node> {
       public:
           Array<AxisName> axis_names;     
   
           virtual void accept(AttrVisitor& visitor) override {
               visitor.visit("axis_names", axis_names);
           }
   
           bool reduceAccept(LayoutNode* other, const ReduceVisitor& reduce) const {
               return reduce(axis_names, other->axis_names);
           }
       };
   
       class Layout : public HandleBase<Layout, Handle, LayoutNode> {
       public:
           Layout() = default;
   
           XIR_DLL Layout(Array<AxisName> axis);
   
           XIR_DLL explicit Layout(const std::string& layout);
   
           static Layout Default() {
               return Layout::Init();
           }
   
           static Layout NHWC() {
               return Layout::Init().N().H().W().C();
           }
   
           static Layout NCHW() {
               return Layout::Init().N().C().H().W();
           }
   
           static Layout HWIO() {
               return Layout::Init().H().W().I().O();
           }
   
           static Layout OIHW() {
               return Layout::Init().O().I().H().W();
           }
   
           bool has(char c) const {
               return get(c).has_value();
           }
   
           XIR_DLL Optional<AxisName> get(char c) const;
   
           XIR_DLL Layout& N();
   
           XIR_DLL Layout& H();
   
           XIR_DLL Layout& W();
   
           XIR_DLL Layout& D();
   
           XIR_DLL Layout& C();
   
           XIR_DLL Layout& I();
   
           XIR_DLL Layout& O();
   
           XIR_DLL Layout& X(uint32_t num = 1);
   
           XIR_DLL Layout& n(int64_t unit);
   
           XIR_DLL uint64_t n() const;
   
           XIR_DLL Layout& h(int64_t unit);
   
           XIR_DLL uint64_t h() const;
   
           XIR_DLL Layout& w(int64_t unit);
   
           XIR_DLL uint64_t w() const;
   
           XIR_DLL Layout& d(int64_t unit);
   
           XIR_DLL uint64_t d() const;
   
           XIR_DLL Layout& c(int64_t unit);
   
           XIR_DLL uint64_t c() const;
   
           XIR_DLL Layout& i(int64_t unit);
   
           XIR_DLL uint64_t i() const;
   
           XIR_DLL Layout& o(int64_t unit);
   
           XIR_DLL uint64_t o() const;
   
           bool isDefault() const {
               return (*this)->axis_names.empty();
           }
   
           uint64_t numAxis() const {
               return (*this)->axis_names.size();
           }
   
           XIR_DLL uint64_t getIndexOf(char c) const;
   
           char getAxisCharName(int64_t index) const {
               return operator[](index)->name[0];
           }
   
           XIR_DLL AxisName operator[](int64_t index) const;
   
           XIR_DLL void swapAxis(int64_t left_index, int64_t right_index);
   
           XIR_DLL void insertAxis(int64_t index, AxisName axis_name);
   
           XIR_DLL void eraseAxis(int64_t index);
   
           XIR_DLL static bool CheckAxisNameUpper(char c);
   
       private:
           inline static std::string_view valid_names = "NHWDCIO*";
       };
   }
