.. _exhale_class_classicraft_1_1xir_1_1_gelu_node:

Class GeluNode
==============

- Defined in :ref:`file_icraft-xir_ops_gelu.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< GeluNode, OneResult, IsActivate >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::GeluNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: