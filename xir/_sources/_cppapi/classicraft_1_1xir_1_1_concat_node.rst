.. _exhale_class_classicraft_1_1xir_1_1_concat_node:

Class ConcatNode
================

- Defined in :ref:`file_icraft-xir_ops_concat.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< ConcatNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::ConcatNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: