
.. _program_listing_file_icraft-xir_ops_avgpool.h:

Program Listing for File avgpool.h
==================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_ops_avgpool.h>` (``icraft-xir\ops\avgpool.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/operation.h>
   
   namespace icraft::xir {
       class AvgpoolNode : public OpNodeBase<AvgpoolNode, OneResult> {
       public:
           int64_t pool_width;             
           int64_t pool_height;            
           int64_t stride_width;           
           int64_t stride_height;          
           int64_t pad_top;                
           int64_t pad_bottom;             
           int64_t pad_left;               
           int64_t pad_right;              
           int64_t divisor_override;       
           bool count_include_pad;         
   
           ICRAFT_DECLARE_ATTRS(AvgpoolNode) {
               ICRAFT_ATTR_FIELD(pool_width).set_lower_bound(1);
               ICRAFT_ATTR_FIELD(pool_height).set_lower_bound(1);
               ICRAFT_ATTR_FIELD(stride_width).set_default(1).set_lower_bound(1);
               ICRAFT_ATTR_FIELD(stride_height).set_default(1).set_lower_bound(1);
               ICRAFT_ATTR_FIELD(pad_top).set_default(0);
               ICRAFT_ATTR_FIELD(pad_bottom).set_default(0);
               ICRAFT_ATTR_FIELD(pad_left).set_default(0);
               ICRAFT_ATTR_FIELD(pad_right).set_default(0);
               ICRAFT_ATTR_FIELD(divisor_override).set_default(0);
               ICRAFT_ATTR_FIELD(count_include_pad).set_default(false);
           }
   
           XIR_DLL virtual void validate() const override;
       };
   
       class Avgpool : public OpBase<Avgpool, AvgpoolNode> {
       public:
           Avgpool() = default;
   
           XIR_DLL Avgpool(
               Value input,
               int64_t pool_width,
               int64_t pool_height,
               int64_t stride_width,
               int64_t stride_height,
               int64_t pad_top,
               int64_t pad_bottom,
               int64_t pad_left,
               int64_t pad_right,
               bool count_include_pad = false,
               int64_t divisor_override = 0
           );
   
           Avgpool(
               Value input,
               int64_t pool_size,
               int64_t stride,
               int64_t pad_top_bottom,
               int64_t pad_left_right,
               bool count_include_pad = false,
               int64_t divisor_override = 0
           ) : Avgpool(
               input,
               pool_size,
               pool_size,
               stride,
               stride,
               pad_top_bottom,
               pad_top_bottom,
               pad_left_right,
               pad_left_right,
               count_include_pad,
               divisor_override
           ) {}
   
           Avgpool(
               Value input,
               int64_t pool_size,
               int64_t stride,
               int64_t pad,
               bool count_include_pad = false,
               int64_t divisor_override = 0
           ) : Avgpool(
               input,
               pool_size,
               stride,
               pad,
               pad,
               divisor_override,
               count_include_pad
           ) {}
       };
   }
