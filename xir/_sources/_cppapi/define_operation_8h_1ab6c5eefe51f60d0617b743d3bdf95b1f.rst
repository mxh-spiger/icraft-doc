.. _exhale_define_operation_8h_1ab6c5eefe51f60d0617b743d3bdf95b1f:

Define ICRAFT_REGISTER_TINFER
=============================

- Defined in :ref:`file_icraft-xir_core_operation.h`


Define Documentation
--------------------


.. doxygendefine:: ICRAFT_REGISTER_TINFER
   :project: Icraft XIR