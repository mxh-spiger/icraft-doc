.. _exhale_class_classicraft_1_1xir_1_1_pass_context:

Class PassContext
=================

- Defined in :ref:`file_icraft-xir_core_pass.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< PassContext, Handle, PassContextNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::PassContext
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: