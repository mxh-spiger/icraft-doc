.. _exhale_class_classicraft_1_1xir_1_1_float_imm:

Class FloatImm
==============

- Defined in :ref:`file_icraft-xir_core_data.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< FloatImm, ScalarImm, FloatImmNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::FloatImm
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: