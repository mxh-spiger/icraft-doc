.. _exhale_class_classicraft_1_1xir_1_1_normalized_quantized_type:

Class NormalizedQuantizedType
=============================

- Defined in :ref:`file_icraft-xir_core_data_type.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< NormalizedQuantizedType, QuantizedType, NormalizedQuantizedTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::NormalizedQuantizedType
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: