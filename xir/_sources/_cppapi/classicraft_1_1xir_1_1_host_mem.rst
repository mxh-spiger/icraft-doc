.. _exhale_class_classicraft_1_1xir_1_1_host_mem:

Class HostMem
=============

- Defined in :ref:`file_icraft-xir_core_mem_type.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< HostMem, MemType, HostMemNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::HostMem
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: