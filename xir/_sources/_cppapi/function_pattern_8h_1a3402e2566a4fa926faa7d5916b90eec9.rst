.. _exhale_function_pattern_8h_1a3402e2566a4fa926faa7d5916b90eec9:

Template Function icraft::xir::operator||(P, Q)
===============================================

- Defined in :ref:`file_icraft-xir_core_pattern.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::operator||(P, Q)
   :project: Icraft XIR