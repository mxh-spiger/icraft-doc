
.. _file_icraft-xir_base_array.h:

File array.h
============

|exhale_lsh| :ref:`Parent directory <dir_icraft-xir_base>` (``icraft-xir\base``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-xir\base\array.h``)
----------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-xir_base_array.h.rst





Includes
--------


- ``icraft-xir/base/object.h``

- ``icraft-xir/base/optional.h``

- ``memory``

- ``unordered_set``



Included By
-----------


- :ref:`file_icraft-xir_core_attrs.h`

- :ref:`file_icraft-xir_core_data.h`

- :ref:`file_icraft-xir_core_pass.h`

- :ref:`file_icraft-xir_core_reflection.h`

- :ref:`file_icraft-xir_core_serialize.h`




Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__xir`


Classes
-------


- :ref:`exhale_class_classicraft_1_1xir_1_1_array`

- :ref:`exhale_class_classicraft_1_1xir_1_1_array_node`

