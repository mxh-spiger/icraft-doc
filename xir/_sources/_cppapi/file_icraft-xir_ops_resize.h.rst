
.. _file_icraft-xir_ops_resize.h:

File resize.h
=============

|exhale_lsh| :ref:`Parent directory <dir_icraft-xir_ops>` (``icraft-xir\ops``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-xir\ops\resize.h``)
----------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-xir_ops_resize.h.rst





Includes
--------


- ``icraft-xir/core/operation.h``






Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__xir`


Classes
-------


- :ref:`exhale_class_classicraft_1_1xir_1_1_resize`

- :ref:`exhale_class_classicraft_1_1xir_1_1_resize_node`


Enums
-----


- :ref:`exhale_enum_resize_8h_1a1ac915a3b32e62d66d8a635bdbeab46f`

