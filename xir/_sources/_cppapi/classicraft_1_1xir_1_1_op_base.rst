.. _exhale_class_classicraft_1_1xir_1_1_op_base:

Template Class OpBase
=====================

- Defined in :ref:`file_icraft-xir_core_operation.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< OpType, Operation, OpNodeType >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::OpBase
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: