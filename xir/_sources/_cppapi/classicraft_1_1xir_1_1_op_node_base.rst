.. _exhale_class_classicraft_1_1xir_1_1_op_node_base:

Template Class OpNodeBase
=========================

- Defined in :ref:`file_icraft-xir_core_operation.h`


Inheritance Relationships
-------------------------

Base Types
**********

- ``public NodeBase< ConcreteType, OperationNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)
- ``public Traits< ConcreteType >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::OpNodeBase
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: