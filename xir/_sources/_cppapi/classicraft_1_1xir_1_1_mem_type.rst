.. _exhale_class_classicraft_1_1xir_1_1_mem_type:

Class MemType
=============

- Defined in :ref:`file_icraft-xir_core_mem_type.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public VirtualBase< MemType, Handle, MemTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)


Derived Types
*************

- ``public VirtualBase< ExternalMem, MemType, ExternalMemNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< HostMem, MemType, HostMemNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)
- ``public VirtualBase< OnChipMem, MemType, OnChipMemNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_virtual_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::MemType
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: