.. _exhale_function_optional_8h_1a6fc9e3d36fec62302706492a13e206ac:

Template Function icraft::xir::is_optional(Optional<T> const&)
==============================================================

- Defined in :ref:`file_icraft-xir_base_optional.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::is_optional(Optional<T> const&)
   :project: Icraft XIR