.. _exhale_function_pattern_8h_1a93542529bb000fda0aaf54b90534a76c:

Function icraft::xir::Anycards
==============================

- Defined in :ref:`file_icraft-xir_core_pattern.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::Anycards()
   :project: Icraft XIR