
.. _program_listing_file_icraft-xir_ops_maxpool.h:

Program Listing for File maxpool.h
==================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_ops_maxpool.h>` (``icraft-xir\ops\maxpool.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/operation.h>
   
   namespace icraft::xir {
       class MaxpoolNode : public OpNodeBase<MaxpoolNode, OneResult> {
       public:
           int64_t pool_width;             
           int64_t pool_height;            
           int64_t stride_width;           
           int64_t stride_height;          
           int64_t pad_top;                
           int64_t pad_bottom;             
           int64_t pad_left;               
           int64_t pad_right;              
           int64_t dilation_width;         
           int64_t dilation_height;        
   
           ICRAFT_DECLARE_ATTRS(MaxpoolNode) {
               ICRAFT_ATTR_FIELD(pool_width).set_lower_bound(1);
               ICRAFT_ATTR_FIELD(pool_height).set_lower_bound(1);
               ICRAFT_ATTR_FIELD(stride_width).set_default(1).set_lower_bound(1);
               ICRAFT_ATTR_FIELD(stride_height).set_default(1).set_lower_bound(1);
               ICRAFT_ATTR_FIELD(pad_top).set_default(0);
               ICRAFT_ATTR_FIELD(pad_bottom).set_default(0);
               ICRAFT_ATTR_FIELD(pad_left).set_default(0);
               ICRAFT_ATTR_FIELD(pad_right).set_default(0);
               ICRAFT_ATTR_FIELD(dilation_width).set_default(1).set_lower_bound(1);
               ICRAFT_ATTR_FIELD(dilation_height).set_default(1).set_lower_bound(1);
           }
   
           XIR_DLL virtual void validate() const override;
       };
   
       class Maxpool : public OpBase<Maxpool, MaxpoolNode> {
       public:
           Maxpool() = default;
   
           XIR_DLL Maxpool(
               Value input,
               int64_t pool_width,
               int64_t pool_height,
               int64_t stride_width,
               int64_t stride_height,
               int64_t pad_top,
               int64_t pad_bottom,
               int64_t pad_left,
               int64_t pad_right,
               int64_t dilation_width,
               int64_t dilation_height
           );
   
           Maxpool(
               Value input,
               int64_t pool_size,
               int64_t stride,
               int64_t pad_top_bottom,
               int64_t pad_left_right,
               int64_t dilation
           ) : Maxpool(
               input,
               pool_size,
               pool_size,
               stride,
               stride,
               pad_top_bottom,
               pad_top_bottom,
               pad_left_right,
               pad_left_right,
               dilation,
               dilation
           ) {}
   
           Maxpool(
               Value input,
               int64_t pool_size,
               int64_t stride,
               int64_t pad,
               int64_t dilation
           ) : Maxpool(
               input,
               pool_size,
               stride,
               pad,
               pad,
               dilation
           ) {}
       };
   }
