
.. _program_listing_file_icraft-xir_ops_batchnorm.h:

Program Listing for File batchnorm.h
====================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_ops_batchnorm.h>` (``icraft-xir\ops\batchnorm.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/operation.h>
   
   namespace icraft::xir {
       class BatchnormNode : public OpNodeBase<BatchnormNode, OneResult>{
       public:
           Params variance;        
           Params mean;            
   
           ICRAFT_DECLARE_ATTRS(BatchnormNode) {
               ICRAFT_ATTR_FIELD(variance);
               ICRAFT_ATTR_FIELD(mean);
           }
   
           XIR_DLL virtual void validate() const override;
       };
   
       class Batchnorm : public OpBase<Batchnorm, BatchnormNode> {
       public:
           Batchnorm() = default;
   
           XIR_DLL Batchnorm(Value input, Params variance, Params mean);
       };
   }
