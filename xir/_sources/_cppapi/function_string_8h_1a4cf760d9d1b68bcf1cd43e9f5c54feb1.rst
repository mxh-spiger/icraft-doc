.. _exhale_function_string_8h_1a4cf760d9d1b68bcf1cd43e9f5c54feb1:

Function icraft::xir::operator==(const String&, const char \*)
==============================================================

- Defined in :ref:`file_icraft-xir_base_string.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::operator==(const String&, const char *)
   :project: Icraft XIR