.. _exhale_class_classicraft_1_1xir_1_1_normalize_node:

Class NormalizeNode
===================

- Defined in :ref:`file_icraft-xir_ops_normalize.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< NormalizeNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::NormalizeNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: