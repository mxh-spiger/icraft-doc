.. _exhale_function_string_8h_1a90015386d12dcad1a1150e0e3c6a0d8a:

Function icraft::xir::operator+(const String&, const char \*)
=============================================================

- Defined in :ref:`file_icraft-xir_base_string.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::operator+(const String&, const char *)
   :project: Icraft XIR