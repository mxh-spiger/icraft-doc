.. _exhale_class_classicraft_1_1xir_1_1_normalize:

Class Normalize
===============

- Defined in :ref:`file_icraft-xir_ops_normalize.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< Normalize, NormalizeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Normalize
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: