.. _exhale_class_classicraft_1_1xir_1_1_route_node:

Class RouteNode
===============

- Defined in :ref:`file_icraft-xir_ops_route.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< RouteNode, OneResult >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::RouteNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: