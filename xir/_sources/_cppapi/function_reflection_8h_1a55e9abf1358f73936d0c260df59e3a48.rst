.. _exhale_function_reflection_8h_1a55e9abf1358f73936d0c260df59e3a48:

Function icraft::xir::GetReprBytes
==================================

- Defined in :ref:`file_icraft-xir_core_reflection.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::GetReprBytes(const Handle&)
   :project: Icraft XIR