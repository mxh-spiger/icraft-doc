.. _exhale_class_classicraft_1_1xir_1_1_maxpool:

Class Maxpool
=============

- Defined in :ref:`file_icraft-xir_ops_maxpool.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< Maxpool, MaxpoolNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Maxpool
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: