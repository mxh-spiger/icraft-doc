.. _exhale_class_classicraft_1_1xir_1_1_abs_node:

Class AbsNode
=============

- Defined in :ref:`file_icraft-xir_ops_abs.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< AbsNode, OneResult, IsActivate >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::AbsNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: