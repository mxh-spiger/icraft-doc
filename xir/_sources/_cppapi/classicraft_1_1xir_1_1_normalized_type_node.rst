.. _exhale_class_classicraft_1_1xir_1_1_normalized_type_node:

Class NormalizedTypeNode
========================

- Defined in :ref:`file_icraft-xir_core_data_type.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< NormalizedTypeNode, BaseQuantizedTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::NormalizedTypeNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: