
.. _file_icraft-xir_ops_divide_scalar.h:

File divide_scalar.h
====================

|exhale_lsh| :ref:`Parent directory <dir_icraft-xir_ops>` (``icraft-xir\ops``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-xir\ops\divide_scalar.h``)
-----------------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-xir_ops_divide_scalar.h.rst





Includes
--------


- ``icraft-xir/core/operation.h``






Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__xir`


Classes
-------


- :ref:`exhale_class_classicraft_1_1xir_1_1_divide_scalar`

- :ref:`exhale_class_classicraft_1_1xir_1_1_divide_scalar_node`

