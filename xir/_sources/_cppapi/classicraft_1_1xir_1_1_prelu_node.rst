.. _exhale_class_classicraft_1_1xir_1_1_prelu_node:

Class PreluNode
===============

- Defined in :ref:`file_icraft-xir_ops_prelu.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< PreluNode, OneResult, IsActivate >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::PreluNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: