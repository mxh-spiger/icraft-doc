
.. _file_icraft-xir_core_functor.h:

File functor.h
==============

|exhale_lsh| :ref:`Parent directory <dir_icraft-xir_core>` (``icraft-xir\core``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-xir\core\functor.h``)
------------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-xir_core_functor.h.rst





Includes
--------


- ``functional``

- ``icraft-utils/logging.h``

- ``unordered_map``



Included By
-----------


- :ref:`file_icraft-xir_core_reflection.h`



