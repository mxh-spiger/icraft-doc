.. _exhale_class_classicraft_1_1xir_1_1_multi_yolo_node:

Class MultiYoloNode
===================

- Defined in :ref:`file_icraft-xir_ops_multi_yolo.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpNodeBase< MultiYoloNode, OutputLike >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::MultiYoloNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: