.. _exhale_class_classicraft_1_1xir_1_1_params_node:

Class ParamsNode
================

- Defined in :ref:`file_icraft-xir_core_data.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< ParamsNode, ValueNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::ParamsNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: