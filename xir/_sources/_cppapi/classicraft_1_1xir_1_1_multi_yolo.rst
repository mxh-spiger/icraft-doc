.. _exhale_class_classicraft_1_1xir_1_1_multi_yolo:

Class MultiYolo
===============

- Defined in :ref:`file_icraft-xir_ops_multi_yolo.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< MultiYolo, MultiYoloNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::MultiYolo
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: