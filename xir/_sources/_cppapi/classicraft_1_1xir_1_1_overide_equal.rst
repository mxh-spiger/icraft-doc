.. _exhale_class_classicraft_1_1xir_1_1_overide_equal:

Class OverideEqual
==================

- Defined in :ref:`file_icraft-xir_core_reflection.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleTraitFunctor< OverideEqual, bool(const Handle &, const Handle &)>``


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::OverideEqual
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: