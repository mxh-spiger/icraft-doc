.. _exhale_class_classicraft_1_1xir_1_1_reorg:

Class Reorg
===========

- Defined in :ref:`file_icraft-xir_ops_reorg.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< Reorg, ReorgNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Reorg
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: