.. _exhale_class_classicraft_1_1xir_1_1_split:

Class Split
===========

- Defined in :ref:`file_icraft-xir_ops_split.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< Split, SplitNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Split
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: