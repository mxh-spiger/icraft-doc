.. _exhale_class_classicraft_1_1xir_1_1_exp_quantized_scale_array_node:

Class ExpQuantizedScaleArrayNode
================================

- Defined in :ref:`file_icraft-xir_core_data_type.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< ExpQuantizedScaleArrayNode, QuantizedScaleArrayNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::ExpQuantizedScaleArrayNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: