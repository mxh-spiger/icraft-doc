.. _exhale_class_classicraft_1_1xir_1_1_on_chip_mem_node:

Class OnChipMemNode
===================

- Defined in :ref:`file_icraft-xir_core_mem_type.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< OnChipMemNode, MemTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::OnChipMemNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: