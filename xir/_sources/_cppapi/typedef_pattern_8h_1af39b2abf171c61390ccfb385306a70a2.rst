.. _exhale_typedef_pattern_8h_1af39b2abf171c61390ccfb385306a70a2:

Typedef icraft::xir::MatchResults
=================================

- Defined in :ref:`file_icraft-xir_core_pattern.h`


Typedef Documentation
---------------------


.. doxygentypedef:: icraft::xir::MatchResults
   :project: Icraft XIR