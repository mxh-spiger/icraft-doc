
.. _program_listing_file_icraft-xir_ops_slice.h:

Program Listing for File slice.h
================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_ops_slice.h>` (``icraft-xir\ops\slice.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/operation.h>
   
   namespace icraft::xir {
       class SliceNode : public OpNodeBase<SliceNode, OneResult> {
       public:
           Array<IntImm> begin;    
           Array<IntImm> end;      
           Array<IntImm> stride;   
   
           ICRAFT_DECLARE_ATTRS(SliceNode) {
               ICRAFT_ATTR_FIELD(begin);
               ICRAFT_ATTR_FIELD(end);
               ICRAFT_ATTR_FIELD(stride);
           }
   
           XIR_DLL virtual void validate() const override;
       };
   
       class Slice : public OpBase<Slice, SliceNode> {
       public:
           Slice() = default;
   
           XIR_DLL Slice(Value input, Array<IntImm> begin, Array<IntImm> end, Array<IntImm> stride);
       };
   }
