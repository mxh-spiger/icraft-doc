.. _exhale_class_classicraft_1_1xir_1_1_tensor_type_node:

Class TensorTypeNode
====================

- Defined in :ref:`file_icraft-xir_core_data_type.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< TensorTypeNode, DataTypeNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::TensorTypeNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: