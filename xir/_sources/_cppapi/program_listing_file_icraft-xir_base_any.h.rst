
.. _program_listing_file_icraft-xir_base_any.h:

Program Listing for File any.h
==============================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_base_any.h>` (``icraft-xir\base\any.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/base/object.h>
   #include <icraft-xir/utils/magic_enum.hpp>
   
   #include <any>
   #include <optional>
   
   namespace icraft::xir {
   
       template <typename T, typename = void>
       struct TypeName {
           static constexpr std::string_view value = "unknown";
       };
   
       template <typename T>
       struct TypeName<T, std::enable_if_t<std::is_base_of_v<ObjectRef, T>>> {
           static constexpr std::string_view value = std::remove_reference_t<T>::NodeType::type_key;
       };
   
       template <typename T>
       struct TypeName<T, std::enable_if_t<std::is_enum_v<T>>> {
           static constexpr std::string_view value = magic_enum::enum_type_name<T>();
       };
   
       template <>
       struct TypeName<int> {
           static constexpr std::string_view value = "int";
       };
   
       template <>
       struct TypeName<int64_t> {
           static constexpr std::string_view value = "int64";
       };
   
       template <>
       struct TypeName<uint64_t> {
           static constexpr std::string_view value = "uint64_t";
       };
   
       template <>
       struct TypeName<std::string> {
           static constexpr std::string_view value = "string";
       };
   
       template <>
       struct TypeName<bool> {
           static constexpr std::string_view value = "bool";
       };
   
       template <>
       struct TypeName<void*> {
           static constexpr std::string_view value = "handle";
       };
   
       template <>
       struct TypeName<double> {
           static constexpr std::string_view value = "double";
       };
   
       class AnyNode : public Object {
       public:
           using ContainerType = std::any;
   
           std::string_view type_name() const {
               return data_.type().name();
           }
   
           template <typename T>
           void emplace(T&& v) {
               data_ = v;
           }
   
           template <typename U>
           U cast() {
               try {
                   return std::any_cast<U>(data_);
               }
               catch (std::exception& e) {
                   ICRAFT_LOG(EXCEPT).append(
                       "cast type mismatch, source type is {}, but target type is {}",
                       type_name(),
                       TypeName<U>::value
                   );
               }
           }
   
           template <typename U>
           std::optional<U> cast_opt() {
               try {
                   return std::any_cast<U>(data_);
               }
               catch (std::exception& e) {
                   return std::nullopt;
               }
           }
   
           bool has_value() const {
               return data_.has_value();
           }
   
       private:
           ContainerType data_;
           friend class Any;
       };
   
       class Any : public ObjectRef {
       public:
           using NodeType = AnyNode;
   
           Any() {
               data_ = make_object<AnyNode>();
           }
   
           template <typename T>
           Any(T v) : Any() {
               getAnyNode()->emplace(std::move(v));
           }
   
           explicit Any(std::any v) {
               getAnyNode()->data_ = std::move(v);
           }
   
           std::string_view type_name() const {
               return getAnyNode()->type_name();
           }
   
           template <typename U>
           U cast() const {
               return getAnyNode()->cast<U>();
           }
   
           template <typename U>
           U cast() {
               return getAnyNode()->cast<U>();
           }
   
           template <typename U>
           std::optional<U> cast_opt() const {
               return getAnyNode()->cast_opt<U>();
           }
   
           template <typename U>
           std::optional<U> cast_opt() {
               return getAnyNode()->cast_opt<U>();
           }
   
           bool has_value() const {
               return getAnyNode()->has_value();
           }
   
           template <typename U>
           U value_or(U v) const {
               if (has_value()) return cast<U>();
               return v;
           }
   
       private:
           AnyNode* getAnyNode() const { return static_cast<AnyNode*>(data_.get()); }
       };
   }
