.. _exhale_function_string_8h_1af4c6e48b61b0f04d2b0beda40a19706b:

Function icraft::xir::operator<<(std::ostream&, const String&)
==============================================================

- Defined in :ref:`file_icraft-xir_base_string.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::operator<<(std::ostream&, const String&)
   :project: Icraft XIR