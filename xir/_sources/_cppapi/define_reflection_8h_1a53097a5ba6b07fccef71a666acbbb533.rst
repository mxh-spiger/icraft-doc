.. _exhale_define_reflection_8h_1a53097a5ba6b07fccef71a666acbbb533:

Define ICRAFT_REFLECTION_REG_VAR_DEF
====================================

- Defined in :ref:`file_icraft-xir_core_reflection.h`


Define Documentation
--------------------


.. doxygendefine:: ICRAFT_REFLECTION_REG_VAR_DEF
   :project: Icraft XIR