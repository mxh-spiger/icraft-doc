.. _exhale_class_classicraft_1_1xir_1_1_merged_axis_distr_node:

Class MergedAxisDistrNode
=========================

- Defined in :ref:`file_icraft-xir_core_data_type.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< MergedAxisDistrNode, Node >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_node_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::MergedAxisDistrNode
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: