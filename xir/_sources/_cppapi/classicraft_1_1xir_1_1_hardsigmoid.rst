.. _exhale_class_classicraft_1_1xir_1_1_hardsigmoid:

Class Hardsigmoid
=================

- Defined in :ref:`file_icraft-xir_ops_hardsigmoid.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpBase< Hardsigmoid, HardsigmoidNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_op_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::Hardsigmoid
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: