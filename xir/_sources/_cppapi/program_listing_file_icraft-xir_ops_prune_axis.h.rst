
.. _program_listing_file_icraft-xir_ops_prune_axis.h:

Program Listing for File prune_axis.h
=====================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_ops_prune_axis.h>` (``icraft-xir\ops\prune_axis.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/operation.h>
   
   namespace icraft::xir {
   
       class PruneAxisNode : public OpNodeBase<PruneAxisNode, OneResult> {};
   
       class PruneAxis : public OpBase<PruneAxis, PruneAxisNode> {
       public:
           PruneAxis() = default;
   
           XIR_DLL explicit PruneAxis(Value input);
       };
   }
