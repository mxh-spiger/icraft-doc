
.. _program_listing_file_icraft-xir_ops_squeeze.h:

Program Listing for File squeeze.h
==================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_ops_squeeze.h>` (``icraft-xir\ops\squeeze.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/operation.h>
   
   namespace icraft::xir {
       class SqueezeNode : public OpNodeBase<SqueezeNode, OneResult> {
       public:
           Array<IntImm> axis;     
           Layout layout;          
   
           ICRAFT_DECLARE_ATTRS(SqueezeNode) {
               ICRAFT_ATTR_FIELD(axis);
               ICRAFT_ATTR_FIELD(layout);
           }
   
           XIR_DLL virtual void validate() const override;
       };
   
       class Squeeze : public OpBase<Squeeze, SqueezeNode> {
       public:
           Squeeze() = default;
   
           XIR_DLL Squeeze(Value input, Array<IntImm> axis, Layout layout);
   
           XIR_DLL Squeeze(Value input, Array<IntImm> axis);
       };
   }
