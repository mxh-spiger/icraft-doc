.. _exhale_class_classicraft_1_1xir_1_1_anycards_pattern:

Class AnycardsPattern
=====================

- Defined in :ref:`file_icraft-xir_core_pattern.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< AnycardsPattern, Pattern, AnycardsPatternNode >`` (:ref:`exhale_class_classicraft_1_1xir_1_1_handle_base`)


Class Documentation
-------------------


.. doxygenclass:: icraft::xir::AnycardsPattern
   :project: Icraft XIR
   :members:
   :protected-members:
   :undoc-members: