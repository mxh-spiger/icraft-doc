.. _exhale_function_string_8h_1a1cadc42fadabb1c9aaa455d8f857a6db:

Function icraft::xir::operator!=(const String&, const char \*)
==============================================================

- Defined in :ref:`file_icraft-xir_base_string.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xir::operator!=(const String&, const char *)
   :project: Icraft XIR