
.. _program_listing_file_icraft-xir_core_pattern.h:

Program Listing for File pattern.h
==================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_core_pattern.h>` (``icraft-xir\core\pattern.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   
   #include <icraft-xir/core/data.h>
   #include <icraft-xir/core/compile_target.h>
   #include <icraft-xir/base/any.h>
   
   namespace icraft::xir {
       class PatternNode : public Node {
       public:
           virtual void accept(AttrVisitor& visitor) override {};
       };
   
       class Pattern : public VirtualBase<Pattern, Handle, PatternNode> {};
   
       class AttrPatternNode : public NodeBase<AttrPatternNode, PatternNode> {
       public:
           std::string key;        
           Any value;              
   
           virtual void accept(AttrVisitor& visitor) override {
               visitor.visit("key", key);
               visitor.visit("value", value);
           }
       };
       class AttrPattern : public HandleBase<AttrPattern, Pattern, AttrPatternNode> {
       public:
           AttrPattern() = default;
   
           XIR_DLL AttrPattern(std::string key, Any value);
       };
   
       class ValuePattern;
       class OpPatternNode : public NodeBase<OpPatternNode, PatternNode> {
       public:
           std::string op_type;                            
           Array<AttrPattern> attrs;                       
           Array<ValuePattern> inputs;                     
           std::string target_type;                        
           std::function<bool(const Operation& op)> cfunc; 
   
           virtual void accept(AttrVisitor& visitor) override {
               visitor.visit("op_type", op_type);
               visitor.visit("attrs", attrs);
               visitor.visit("inputs", inputs);
               visitor.visit("target_type", target_type);
           }
       };
   
       class OpPattern : public HandleBase<OpPattern, Pattern, OpPatternNode> {
       public:
           OpPattern() = default;
   
           XIR_DLL explicit OpPattern(std::string op_type);
   
           XIR_DLL OpPattern& operator()(Array<ValuePattern> inputs);
   
           template <typename... Args>
           OpPattern& operator()(Args... args) {
               return operator()({std::forward<Args>(args)...});
           }
   
           XIR_DLL ValuePattern operator[](uint64_t index) const;
   
           OpPattern& hasAttr(std::string key, Any value = Any()) {
               get_mutable()->attrs.push_back({std::move(key), std::move(value)});
               return *this;
           }
   
           template <typename T, 
               typename = typename std::enable_if_t<std::is_base_of_v<CompileTarget, T>>>
           OpPattern& onTarget() {
               get_mutable()->target_type = T::NodeType::type_key;
               return *this;
           }
   
           OpPattern& onTarget(std::string type_key) {
               get_mutable()->target_type = type_key;
               return *this;
           }
   
           OpPattern& setConstraint(std::function<bool(const Operation& op)> cfunc) {
               get_mutable()->cfunc = cfunc;
               return *this;
           }
       };
   
       class AnycardsPatternNode : public NodeBase<AnycardsPatternNode, PatternNode> {};
   
       class AnycardsPattern : public HandleBase<AnycardsPattern, Pattern, AnycardsPatternNode> {};
   
       class ValuePatternNode : public NodeBase<ValuePatternNode, PatternNode> {
       public:
           OpPattern op_pattern;       
           uint64_t index;             
           int64_t uses = -1;          
           TensorType dtype;           
   
           XIR_DLL virtual void accept(AttrVisitor& visitor) override;
       };
   
       class ValuePattern : public HandleBase<ValuePattern, Pattern, ValuePatternNode> {
       public:
           ValuePattern() = default;
   
           XIR_DLL ValuePattern(
               OpPattern op_pattern, 
               uint64_t index, 
               int64_t uses = -1, 
               TensorType dtype = TensorType()
           );
   
           ValuePattern(AnycardsPattern) {}
   
           XIR_DLL ValuePattern hasUses(uint32_t uses);
   
           XIR_DLL ValuePattern hasDtype(TensorType dtype);
   
           bool isWildcard() const { return !(*this)->op_pattern.defined(); }
   
           bool isAnycards() const { return !this->defined(); }
       };
   
       class OrPatternNode : public NodeBase<OrPatternNode, PatternNode> {
       public:
           Pattern left;       
           Pattern right;      
   
           virtual void accept(AttrVisitor& visitor) override {
               visitor.visit("left", left);
               visitor.visit("right", right);
           }
       };
   
       class OrPattern : public HandleBase<OrPattern, Pattern, OrPatternNode> {
       public:
           OrPattern() = default;
   
           XIR_DLL OrPattern(Pattern left, Pattern right);
       };
   
       class AndPatternNode : public NodeBase<AndPatternNode, PatternNode> {
       public:
           Pattern left;       
           Pattern right;      
   
           virtual void accept(AttrVisitor& visitor) override {
               visitor.visit("left", left);
               visitor.visit("right", right);
           }
       };
   
       class AndPattern : public HandleBase<AndPattern, Pattern, AndPatternNode> {
       public:
           AndPattern() = default;
   
           XIR_DLL AndPattern(Pattern left, Pattern right);
       };
   
       template<typename T>
       struct IsOneOf {
           template<typename... Args>
           static constexpr bool value = std::disjunction_v<std::is_same<T, Args>...>;
       };
       
       template <typename P, typename = typename std::enable_if_t<
           IsOneOf<P>::template value<OpPattern, OrPattern, AndPattern>>,
           typename Q, typename = typename std::enable_if_t<
           IsOneOf<Q>::template value<OpPattern, OrPattern, AndPattern>>>
       OrPattern operator||(P left, Q right) { return OrPattern(left, right); }
   
       template <typename P, typename = typename std::enable_if_t<
           IsOneOf<P>::template value<OpPattern, OrPattern, AndPattern>>,
           typename Q, typename = typename std::enable_if_t<
           IsOneOf<Q>::template value<OpPattern, OrPattern, AndPattern>>>
       AndPattern operator&&(P left, Q right) { return AndPattern(left, right); }
   
       inline ValuePattern Wildcard() { return ValuePattern::Init(); }
   
       inline AnycardsPattern Anycards() { return AnycardsPattern::Init(); }
   
       template <typename T, 
           typename = typename std::enable_if_t<std::is_base_of_v<Operation, T>>,
           typename... Args>
       OpPattern IsOp(Args... args) { 
           return OpPattern(std::string(T::NodeType::type_key))({std::forward<Args>(args)...});
       }
   
   #if !defined(DOXYGEN_SKIP_THIS)
       template <typename... Args>
       OpPattern IsOp(Args... args) {
           return OpPattern(std::string())({ std::forward<Args>(args)... });
       }
   #endif // DOXYGEN_SKIP_THIS
   
       using MatchGroup = std::unordered_map<OpPattern, Operation, ObjectPtrHash, ObjectPtrEqual>;
       using MatchResults = std::vector<MatchGroup>;
   }
   
   #define PATTERN_REQUIRE(x) if (!(x)) return false
