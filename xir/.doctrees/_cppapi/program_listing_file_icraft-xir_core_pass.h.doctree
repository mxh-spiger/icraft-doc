���7      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]�(�docutils.nodes��target���)��}�(h�0.. _program_listing_file_icraft-xir_core_pass.h:�h]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��refid��+program-listing-file-icraft-xir-core-pass-h�u�tagname�h
�line�K�parent�h�	_document�h�source���C:\Icraft-Build\build\windows-x64-release\_deps\icraft-xir-src\docs\source\_cppapi\program_listing_file_icraft-xir_core_pass.h.rst�ubh	�section���)��}�(hhh]�(h	�title���)��}�(h�Program Listing for File pass.h�h]�h	�Text����Program Listing for File pass.h�����}�(h h+h!hh"NhNubah}�(h]�h]�h]�h]�h]�uhh)h h&h!hh"h#hKubh	�	paragraph���)��}�(h�o|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xir_core_pass.h>` (``icraft-xir\core\pass.h``)�h]�(h0�↰�����}�(h h=h!hh"NhNubh0� �����}�(h h=h!hh"NhNubh �pending_xref���)��}�(h�E:ref:`Return to documentation for file <file_icraft-xir_core_pass.h>`�h]�h	�inline���)��}�(hhMh]�h0� Return to documentation for file�����}�(h hQh!hh"NhNubah}�(h]�h]�(�xref��std��std-ref�eh]�h]�h]�uhhOh hKubah}�(h]�h]�h]�h]�h]��refdoc��3_cppapi/program_listing_file_icraft-xir_core_pass.h��	refdomain�h\�reftype��ref��refexplicit���refwarn���	reftarget��file_icraft-xir_core_pass.h�uhhIh"h#hKh h=ubh0� (�����}�(h h=h!hh"NhNubh	�literal���)��}�(h�``icraft-xir\core\pass.h``�h]�h0�icraft-xir\core\pass.h�����}�(h hvh!hh"NhNubah}�(h]�h]�h]�h]�h]�uhhth h=ubh0�)�����}�(h h=h!hh"NhNubeh}�(h]�h]�h]�h]�h]�uhh;h"h#hKh h&h!hubh	�substitution_definition���)��}�(h�F.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS
�h]�h0�↰�����}�h h�sbah}�(h]�h]�h]��
exhale_lsh�ah]�h]�uhh�h"h#hK	h h&h!hubh	�literal_block���)��}�(hXA  #pragma once
#include <icraft-xir/core/node.h>
#include <icraft-xir/core/reflection.h>
#include <icraft-xir/base/array.h>
#include <icraft-xir/base/map.h>
#include <icraft-xir/base/string.h>

namespace icraft::xir {
    class PassContextNode : public NodeBase<PassContextNode, Node> {
    public:
        uint64_t top_level{ LLONG_MAX };
        Array<String> required_passes;
        Array<String> disabled_passes;
        Map<String, ObjectRef> config;

        virtual void accept(AttrVisitor& visitor) override {
            visitor.visit("opt_level", top_level);
            visitor.visit("required_passes", required_passes);
            visitor.visit("disabled_passes", disabled_passes);
            visitor.visit("config", config);
        };
    };

    class PassInfo;
    /*@brief 表示Pass上下文的引用类*/
    class PassContext : public HandleBase<PassContext, Handle, PassContextNode> {
    public:
        PassContext() = default;

        XIR_DLL ~PassContext();

        XIR_DLL explicit PassContext(
            uint64_t top_level,
            Array<String> required_passes = {},
            Array<String> disabled_passes = {},
            Map<String, ObjectRef> config = {}
        );

        XIR_DLL static PassContext Init();

        XIR_DLL static PassContext& Current();

        XIR_DLL PassContext& requirePass(const std::string& pass_name);

        XIR_DLL PassContext& removeRequiredPass(const std::string& pass_name);

        XIR_DLL PassContext& disablePass(const std::string& pass_name);

        XIR_DLL PassContext& removeDisabledPass(const std::string& pass_name);

        XIR_DLL PassContext& setConfig(const std::string& key, ObjectRef value);

        template <typename T,
            typename = typename std::enable_if_t<(std::is_base_of_v<ObjectRef, T>)>>
        Optional<T> getConfig(
                const std::string& key,
                Optional<T> default_value = NullOpt) const {
            auto& config = (*this)->config;
            if (!config.defined()) return default_value;
            if (config.count(key)) return Downcast<Optional<T>>(config[key]);
            return default_value;
        }

        template <typename T,
            typename = typename std::enable_if_t<(std::is_base_of_v<ObjectRef, T>)>>
        Optional<T> getConfig(const std::string& key, T default_value) const {
            return getConfig<T>(key, Optional<T>(default_value));
        }

        XIR_DLL bool isPassEnabled(const PassInfo& info) const;
    };

    class PassInfoNode : public NodeBase<PassInfoNode, Node> {
    public:
        String name;
        uint64_t opt_level;
        Array<String> required_passes;

        virtual void accept(AttrVisitor& visitor) override {
            visitor.visit("name", name);
            visitor.visit("opt_level", opt_level);
            visitor.visit("required_passes", required_passes);
        };
    };

    class PassInfo : public HandleBase<PassInfo, Handle, PassInfoNode> {
    public:
        PassInfo() = default;

        XIR_DLL PassInfo(String name, uint64_t opt_level = 0, Array<String> required_passes = {});
    };

    class Network;
    class PassNode : public NodeBase<PassNode, Node> {
    public:
        PassInfo pass_info;

        virtual void accept(AttrVisitor& visitor) override {
            visitor.visit("pass_info", pass_info);
        };

        virtual Network operator()(Network network, const PassContext& pass_ctx) const = 0;
    };

    class Pass : public VirtualBase<Pass, Handle, PassNode> {
    public:
        XIR_DLL Network operator()(Network network, const PassContext& pass_ctx = PassContext::Current()) const;

        XIR_DLL static Pass Create(const std::string& name, const std::filesystem::path& dll_path = {});
    };

    class NetworkPassNode : public NodeBase<NetworkPassNode, PassNode> {
    public:
        std::function<Network(Network, const PassContext&)> pass_func;

        XIR_DLL virtual Network operator()(Network network, const PassContext& pass_ctx) const override final;
    };

    class NetworkPass : public HandleBase<NetworkPass, Pass, NetworkPassNode> {
    public:
        NetworkPass() = default;

        XIR_DLL NetworkPass(std::function<Network(Network, const PassContext&)> pass_func, PassInfo pass_info);
    };

    class SequentialPassNode : public NodeBase<SequentialPassNode, PassNode> {
    public:
        Array<Pass> passes;

        virtual void accept(AttrVisitor& visitor) override {
            visitor.visit("passes", passes);
            PassNode::accept(visitor);
        };

        XIR_DLL virtual Network operator()(Network network, const PassContext& pass_ctx) const override final;
    };

    class SequentialPass : public HandleBase<SequentialPass, Pass, SequentialPassNode> {
    public:
        SequentialPass() = default;

        XIR_DLL SequentialPass(Array<Pass> passes, PassInfo pass_info = PassInfo("sequential"));

    };
}�h]�h0XA  #pragma once
#include <icraft-xir/core/node.h>
#include <icraft-xir/core/reflection.h>
#include <icraft-xir/base/array.h>
#include <icraft-xir/base/map.h>
#include <icraft-xir/base/string.h>

namespace icraft::xir {
    class PassContextNode : public NodeBase<PassContextNode, Node> {
    public:
        uint64_t top_level{ LLONG_MAX };
        Array<String> required_passes;
        Array<String> disabled_passes;
        Map<String, ObjectRef> config;

        virtual void accept(AttrVisitor& visitor) override {
            visitor.visit("opt_level", top_level);
            visitor.visit("required_passes", required_passes);
            visitor.visit("disabled_passes", disabled_passes);
            visitor.visit("config", config);
        };
    };

    class PassInfo;
    /*@brief 表示Pass上下文的引用类*/
    class PassContext : public HandleBase<PassContext, Handle, PassContextNode> {
    public:
        PassContext() = default;

        XIR_DLL ~PassContext();

        XIR_DLL explicit PassContext(
            uint64_t top_level,
            Array<String> required_passes = {},
            Array<String> disabled_passes = {},
            Map<String, ObjectRef> config = {}
        );

        XIR_DLL static PassContext Init();

        XIR_DLL static PassContext& Current();

        XIR_DLL PassContext& requirePass(const std::string& pass_name);

        XIR_DLL PassContext& removeRequiredPass(const std::string& pass_name);

        XIR_DLL PassContext& disablePass(const std::string& pass_name);

        XIR_DLL PassContext& removeDisabledPass(const std::string& pass_name);

        XIR_DLL PassContext& setConfig(const std::string& key, ObjectRef value);

        template <typename T,
            typename = typename std::enable_if_t<(std::is_base_of_v<ObjectRef, T>)>>
        Optional<T> getConfig(
                const std::string& key,
                Optional<T> default_value = NullOpt) const {
            auto& config = (*this)->config;
            if (!config.defined()) return default_value;
            if (config.count(key)) return Downcast<Optional<T>>(config[key]);
            return default_value;
        }

        template <typename T,
            typename = typename std::enable_if_t<(std::is_base_of_v<ObjectRef, T>)>>
        Optional<T> getConfig(const std::string& key, T default_value) const {
            return getConfig<T>(key, Optional<T>(default_value));
        }

        XIR_DLL bool isPassEnabled(const PassInfo& info) const;
    };

    class PassInfoNode : public NodeBase<PassInfoNode, Node> {
    public:
        String name;
        uint64_t opt_level;
        Array<String> required_passes;

        virtual void accept(AttrVisitor& visitor) override {
            visitor.visit("name", name);
            visitor.visit("opt_level", opt_level);
            visitor.visit("required_passes", required_passes);
        };
    };

    class PassInfo : public HandleBase<PassInfo, Handle, PassInfoNode> {
    public:
        PassInfo() = default;

        XIR_DLL PassInfo(String name, uint64_t opt_level = 0, Array<String> required_passes = {});
    };

    class Network;
    class PassNode : public NodeBase<PassNode, Node> {
    public:
        PassInfo pass_info;

        virtual void accept(AttrVisitor& visitor) override {
            visitor.visit("pass_info", pass_info);
        };

        virtual Network operator()(Network network, const PassContext& pass_ctx) const = 0;
    };

    class Pass : public VirtualBase<Pass, Handle, PassNode> {
    public:
        XIR_DLL Network operator()(Network network, const PassContext& pass_ctx = PassContext::Current()) const;

        XIR_DLL static Pass Create(const std::string& name, const std::filesystem::path& dll_path = {});
    };

    class NetworkPassNode : public NodeBase<NetworkPassNode, PassNode> {
    public:
        std::function<Network(Network, const PassContext&)> pass_func;

        XIR_DLL virtual Network operator()(Network network, const PassContext& pass_ctx) const override final;
    };

    class NetworkPass : public HandleBase<NetworkPass, Pass, NetworkPassNode> {
    public:
        NetworkPass() = default;

        XIR_DLL NetworkPass(std::function<Network(Network, const PassContext&)> pass_func, PassInfo pass_info);
    };

    class SequentialPassNode : public NodeBase<SequentialPassNode, PassNode> {
    public:
        Array<Pass> passes;

        virtual void accept(AttrVisitor& visitor) override {
            visitor.visit("passes", passes);
            PassNode::accept(visitor);
        };

        XIR_DLL virtual Network operator()(Network network, const PassContext& pass_ctx) const override final;
    };

    class SequentialPass : public HandleBase<SequentialPass, Pass, SequentialPassNode> {
    public:
        SequentialPass() = default;

        XIR_DLL SequentialPass(Array<Pass> passes, PassInfo pass_info = PassInfo("sequential"));

    };
}�����}�h h�sbah}�(h]�h]�h]�h]�h]��	xml:space��preserve��force���language��cpp��highlight_args�}�uhh�h"h#hKh h&h!hubeh}�(h]�(�program-listing-for-file-pass-h�heh]�h]�(�program listing for file pass.h��+program_listing_file_icraft-xir_core_pass.h�eh]�h]�uhh$h hh!hh"h#hK�expect_referenced_by_name�}�h�hs�expect_referenced_by_id�}�hhsubeh}�(h]�h]�h]�h]�h]��source�h#uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h)N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�h�error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��zh_CN��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h#�_destination�N�_config_files�]��file_insertion_enabled���raw_enabled�K�line_length_limit�M'�pep_references�N�pep_base_url��https://peps.python.org/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��&https://datatracker.ietf.org/doc/html/��	tab_width�K�trim_footnote_reference_space���syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���image_loading��link��embed_stylesheet���cloak_email_addresses���section_self_link���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}�h�h�s�substitution_names�}��
exhale_lsh�h�s�refnames�}��refids�}�h]�has�nameids�}�(h�hh�h�u�	nametypes�}�(h��h��uh}�(hh&h�h&u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]��transform_messages�]�h	�system_message���)��}�(hhh]�h<)��}�(hhh]�h0�QHyperlink target "program-listing-file-icraft-xir-core-pass-h" is not referenced.�����}�h jP  sbah}�(h]�h]�h]�h]�h]�uhh;h jM  ubah}�(h]�h]�h]�h]�h]��level�K�type��INFO��source�h#�line�KuhjK  uba�transformer�N�include_log�]��
decoration�Nh!hub.