buyibackend.HardOpInfo
======================

.. currentmodule:: buyibackend

.. autoclass:: HardOpInfo
   :members:
   :show-inheritance:
   :inherited-members:
   :special-members: __call__, __add__, __mul__

   
   
   .. rubric:: Methods

   .. autosummary::
      :nosignatures:
   
      ~HardOpInfo.clone
      ~HardOpInfo.defined
      ~HardOpInfo.same_as
      ~HardOpInfo.typeKey
      ~HardOpInfo.type_is
      ~HardOpInfo.unique
      ~HardOpInfo.use_count
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~HardOpInfo.instr_data
      ~HardOpInfo.instr_logic_addr
      ~HardOpInfo.instr_logic_size
      ~HardOpInfo.instr_phy_addr
      ~HardOpInfo.instr_phy_size
      ~HardOpInfo.merge_from
      ~HardOpInfo.net_hardop
      ~HardOpInfo.sync_idx
      ~HardOpInfo.type_key
      ~HardOpInfo.user_used
      ~HardOpInfo.weight_phy_addr
      ~HardOpInfo.weights_logic_addr
      ~HardOpInfo.weights_size
   
   