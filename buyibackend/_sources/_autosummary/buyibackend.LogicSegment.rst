buyibackend.LogicSegment
========================

.. currentmodule:: buyibackend

.. autoclass:: LogicSegment
   :members:
   :show-inheritance:
   :inherited-members:
   :special-members: __call__, __add__, __mul__

   
   
   .. rubric:: Methods

   .. autosummary::
      :nosignatures:
   
      ~LogicSegment.clone
      ~LogicSegment.defined
      ~LogicSegment.same_as
      ~LogicSegment.typeKey
      ~LogicSegment.type_is
      ~LogicSegment.unique
      ~LogicSegment.use_count
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~LogicSegment.byte_size
      ~LogicSegment.hardop_map
      ~LogicSegment.info_map
      ~LogicSegment.logic_addr
      ~LogicSegment.segment_type
      ~LogicSegment.type_key
   
   