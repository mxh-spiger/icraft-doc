buyibackend.BuyiBackend
=======================

.. currentmodule:: buyibackend

.. autoclass:: BuyiBackend
   :members:
   :show-inheritance:
   :inherited-members:
   :special-members: __call__, __add__, __mul__

   
   
   .. rubric:: Methods

   .. autosummary::
      :nosignatures:
   
      ~BuyiBackend.Init
      ~BuyiBackend.clone
      ~BuyiBackend.compressFtmp
      ~BuyiBackend.defined
      ~BuyiBackend.getDeploySize
      ~BuyiBackend.log
      ~BuyiBackend.precheck
      ~BuyiBackend.same_as
      ~BuyiBackend.setUserSegment
      ~BuyiBackend.speedMode
      ~BuyiBackend.typeKey
      ~BuyiBackend.type_is
      ~BuyiBackend.unique
      ~BuyiBackend.use_count
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~BuyiBackend.buyibackend_id
      ~BuyiBackend.forward_info
      ~BuyiBackend.is_applied_
      ~BuyiBackend.is_compressftmp_
      ~BuyiBackend.logic_segment_map
      ~BuyiBackend.phy_segment_map
      ~BuyiBackend.type_key
      ~BuyiBackend.value_info
      ~BuyiBackend.value_list
   
   