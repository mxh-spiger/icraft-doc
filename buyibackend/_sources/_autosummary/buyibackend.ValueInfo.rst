buyibackend.ValueInfo
=====================

.. currentmodule:: buyibackend

.. autoclass:: ValueInfo
   :members:
   :show-inheritance:
   :inherited-members:
   :special-members: __call__, __add__, __mul__

   
   
   .. rubric:: Methods

   .. autosummary::
      :nosignatures:
   
      ~ValueInfo.clone
      ~ValueInfo.defined
      ~ValueInfo.same_as
      ~ValueInfo.typeKey
      ~ValueInfo.type_is
      ~ValueInfo.unique
      ~ValueInfo.use_count
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~ValueInfo.byte_size
      ~ValueInfo.consumer
      ~ValueInfo.fake_from
      ~ValueInfo.is_host
      ~ValueInfo.is_ocm
      ~ValueInfo.logic_addr
      ~ValueInfo.origin_producer
      ~ValueInfo.phy_addr
      ~ValueInfo.producer
      ~ValueInfo.real_to
      ~ValueInfo.segment
      ~ValueInfo.type_key
      ~ValueInfo.user_used
      ~ValueInfo.value
   
   