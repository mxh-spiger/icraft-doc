buyibackend.PhySegment
======================

.. currentmodule:: buyibackend

.. autoclass:: PhySegment
   :members:
   :show-inheritance:
   :inherited-members:
   :special-members: __call__, __add__, __mul__

   
   
   .. rubric:: Methods

   .. autosummary::
      :nosignatures:
   
      ~PhySegment.clone
      ~PhySegment.defined
      ~PhySegment.same_as
      ~PhySegment.typeKey
      ~PhySegment.type_is
      ~PhySegment.unique
      ~PhySegment.use_count
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~PhySegment.byte_size
      ~PhySegment.hardop_map
      ~PhySegment.info_map
      ~PhySegment.memchunk
      ~PhySegment.phy_addr
      ~PhySegment.segment_type
      ~PhySegment.type_key
      ~PhySegment.user_used
   
   