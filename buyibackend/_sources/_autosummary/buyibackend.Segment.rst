buyibackend.Segment
===================

.. currentmodule:: buyibackend

.. autoclass:: Segment
   :members:
   :show-inheritance:
   :inherited-members:
   :special-members: __call__, __add__, __mul__

   
   
   .. rubric:: Methods

   .. autosummary::
      :nosignatures:
   
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~Segment.FTMP
      ~Segment.INPUT
      ~Segment.INSTR
      ~Segment.OUTPUT
      ~Segment.WEIGHT
      ~Segment.name
      ~Segment.value
   
   