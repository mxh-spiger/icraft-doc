buyibackend.ForwardInfo
=======================

.. currentmodule:: buyibackend

.. autoclass:: ForwardInfo
   :members:
   :show-inheritance:
   :inherited-members:
   :special-members: __call__, __add__, __mul__

   
   
   .. rubric:: Methods

   .. autosummary::
      :nosignatures:
   
      ~ForwardInfo.clone
      ~ForwardInfo.defined
      ~ForwardInfo.same_as
      ~ForwardInfo.typeKey
      ~ForwardInfo.type_is
      ~ForwardInfo.unique
      ~ForwardInfo.use_count
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~ForwardInfo.hardop_map
      ~ForwardInfo.idx_map
      ~ForwardInfo.type_key
      ~ForwardInfo.value_map
   
   