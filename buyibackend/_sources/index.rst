============
Icraft BuyiBackend
============

Icraft BuyiBackend是统一前向后的运行时的后端之一，BuyiBackend会将绑定在其上面的算子在QL7100AI上运算执行

BuyiBackend提供以下功能接口：

* ``compressFtmp`` ：将网络输出的中间层进行优化，可以大幅减少网络在etm上的内存占用
* ``userSetSegment`` ： 将网络某个分段使用的内存与用户申请的内存相绑定，从而可以实现多网络间的内存复用
* ``precheck``：在session部署后运行前可以调用，会检查指令与权重在etm上对应地址的数据与内存中的数据是否相同
* ``getDepolySize`` ：获取session在etm占用的总内存字节大小
* ``log``：保存BuyiBackend相关的log信息

BuyiBackend的使用基本依托于session，session的构造过程中会完成BuyiBackend的初始化，session的apply部署时也会完成BuyiBackend的部署

.. image:: ./static/buyibackend_session.png

---------------------------------------

BuyiBackend在初始化init以及部署apply的过程中会构造一些数据结构：  

* ``ValueInfo`` ：与icraft::xir::Value类的对应，包含BuyiBackend中一些补充信息
* ``HardOpInfo`` ：与icraft::xir::HardOp类的对应，包含BuyiBackend中一些补充信息
* ``LogicSegment`` ：逻辑分段类，在BuyiBackend 初始化时生成，表示对应network_view的各分段的逻辑地址相关数据
* ``PhySegment`` ：物理分段类，在BuyiBackend在apply部署后生成，表示对应network_view的各分段的真实物理地址相关数据
* ``ForwardInfo`` ：包含前向所需要的信息
  


〇 用户接口
===========

Icraft 提供了C++ API和Python API作为用户接口。

.. toctree::
   :maxdepth: 2

   C++ API reference <_cppapi/api_root>
   Python API reference <_autosummary/buyibackend>

一 快速开始
===========

1.1 构造BuyiBackend
---------------

构造BuyiBackend有两种方法：

* Session::Create时构造
* 直接构造BuyiBackend

1.1.1 Session::Create时构造
^^^^^^^^^^^^^^^^^^^^^^^^^

``C++ 示例``
"""""""""""" 
.. code-block:: cpp
   :linenos:

   auto json_file = "D:/Workspace/V3.0/SSD_BY.json";
   auto raw_file = "D:/Workspace/V3.0/SSD_BY.raw";
   auto img_file = "D:/Workspace/V3.0/ssd300.png";

   // 打开设备
   auto host_device = HostDevice::Default();
   auto buyi_device = Device::Open("axi://ql100aiu?npu=0x40000000&dma=0x80000000");

   // 加载网络
   auto network = Network::CreateFromJsonFile(json_file);
   network.loadParamsFromFile(raw_file);

   // 创建Session1
   // 该方法会根据模板参数的顺序，自动创建Backend对象
   // 首先尝试将算子绑定到BuyiBackend上，如果不支持，则绑定到HostBackend上
   auto sess1 = Session::Create<BuyiBackend, HostBackend>(network, { buyi_device, host_device });
   
   // 获取sess1所有的Backends
   auto& backends = sess1->backends;
   auto buyi_backend = backends[0].cast<BuyiBackend>();
   


``Python示例``
""""""""""""""
.. code-block:: python
   :linenos:

   # 导入必要的库
   import xir
   from xrt import *
   from host_backend import *
   from buyi_backend import *
   import unittest

   # 获取一个网络
   # Input → Conv2d → Cast → HardOp → Cast → Relu → Maxpool → Output
   network = GenerateNetwork()

   # 创建Session，按顺序传入想要绑定的后端类型
   sess1 = Session.Create([BuyiBackend, HostBackend], network.view(0), [ HostDevice.Default(), Device() ])
   sess1.apply()

   backend_bindings = sess1.backendBindings()
   self.assertTrue(backend_bindings[1].is_type(HostBackend)) # Conv2d
   self.assertTrue(backend_bindings[2].is_type(HostBackend)) # Cast
   self.assertTrue(backend_bindings[3].is_type(BuyiBackend)) # HardOp
   self.assertTrue(backend_bindings[4].is_type(BuyiBackend)) # Cast
   self.assertTrue(backend_bindings[5].is_type(HostBackend)) # Relu
   self.assertTrue(backend_bindings[6].is_type(HostBackend)) # Maxpool

   # 也可以使用CreateByOrder方法，将算子按顺序绑定到已有的Backend对象上
   sess2 = Session.CreateByOrder(network.view(0), [BuyiBackend.Init(), HostBackend.Init()], [ HostDevice.Default(), Device() ])
   buyibackend = BuyiBackend(sess2.backends[0])


1.1.2 直接构造BuyiBackend
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``C++ 示例``
"""""""""""" 
.. code-block:: cpp
   :linenos:

   auto dev_url = "axi://ql100aiu?npu=0x40000000&dma=0x80000000";
   auto dev_url_socket = "socket://ql100aiu@192.168.125.148:9981?npu=0x40000000&dma=0x80000000";
   
   //打开设备
   Device device;
   device = Device::Open(dev_url_socket);

   //加载网络
   auto network = Network::CreateFromJsonFile("E:/test/json&raw/30/seresnet_BY.json");
   network.loadParamsFromFile("E:/test/json&raw/30/seresnet_BY.raw");

   //初始化BuyiBackend
   icraft::xrt::Backend  backend = BuyiBackend::Init();
   backend.init(network, device);

   //类型转换
   auto by_backend = backend.cast<BuyiBackend>();

``Python示例``
""""""""""""""
.. code-block:: python
   :linenos:

   # 导入必要的库
   import xir
   from xrt import *
   from buyibackend import *
   import unittest

   #打开设备
   dev_url = "axi://ql100aiu?npu=0x40000000&dma=0x80000000"
   dev_url_socket = "socket://ql100aiu@192.168.125.148:9981?npu=0x40000000&dma=0x80000000"
   device = Device.Open(dev_url_socket)

   #加载网络
   network = xir.Network.CreateFromJsonFile("E:/test/json&raw/30/seresnet_BY.json")
   network.loadParamsFromFile("E:/test/json&raw/30/seresnet_BY.raw")

   #构造backend
   backend = Backend.Create(BuyiBackend, network.view(0), device)
   #类型转换
   buyibackend = BuyiBackend(backend)

1.2 网络部署的功能配置
---------------
功能配置分为在
* apply部署前配置：compressFtmp，userSetSegment
* apply部署后配置：precheck
* 前后均可配置：log，getDepolySize


1.2.1 compressFtmp 压缩中间层
^^^^^^^^^^^^^^^^^

``C++ 示例``
"""""""""""" 
.. code-block:: cpp
   :linenos:

   auto dev_url = "axi://ql100aiu?npu=0x40000000&dma=0x80000000";
   auto dev_url_socket = "socket://ql100aiu@192.168.125.148:9981?npu=0x40000000&dma=0x80000000";
   Device device;
   device = Device::Open(dev_url_socket);

   auto network = Network::CreateFromJsonFile("E:/test/json&raw/30/seresnet_BY.json");
   network.loadParamsFromFile("E:/test/json&raw/30/seresnet_BY.raw");

   icraft::xrt::Backend  backend = BuyiBackend::Init();
   backend.init(network, device);

   //调用BuyiBackend功能接口必须先将Backend类型转换成BuyiBackend
   auto by_backend = backend.cast<BuyiBackend>();

   //调用compressFtmp接口
   by_backend.compressFtmp();

   //配置完后调用apply接口部署，如果构造session，请调用session.apply
   backend.apply();
   //可以生成log，log存放在${工作目录}/logs/runtime/目录下
   by_backend.log()
   //apply部署完成后可以调用precheck进行预检，如果没问题会返回true，否则抛出异常
   by_backend.precheck()

``Python示例``
""""""""""""""
.. code-block:: python
   :linenos:

   # 导入必要的库
   import xir
   from xrt import *
   from buyibackend import *
   import unittest

   #打开设备
   dev_url = "axi://ql100aiu?npu=0x40000000&dma=0x80000000"
   dev_url_socket = "socket://ql100aiu@192.168.125.148:9981?npu=0x40000000&dma=0x80000000"
   device = Device.Open(dev_url_socket)

   #加载网络
   network = xir.Network.CreateFromJsonFile("E:/test/json&raw/30/seresnet_BY.json")
   network.loadParamsFromFile("E:/test/json&raw/30/seresnet_BY.raw")

   backend1 = Backend.Create(BuyiBackend, network.view(0), device)
   backend2 = Backend.Create(BuyiBackend, network.view(0), device)
   buyibackend1 = BuyiBackend(backend1)
   buyibackend2 = BuyiBackend(backend2)

   #对buyibackend配置优化中间层
   buyibackend2.compressFtmp()
   backend1.apply()
   #必须apply部署前配置compressFtmp才会生效
   backend2.apply()

   #这是比较便可以发现etm的存储占用 buyibackend2会少很多
   self.assertTrue(buyibackend1.getDeploySize() >= buyibackend2.getDeploySize())

1.2.2 userSetSegment
^^^^^^^^^^^^^^^^

``C++ 示例``
"""""""""""" 
.. code-block:: cpp
   :linenos:

   auto dev_url = "axi://ql100aiu?npu=0x40000000&dma=0x80000000";
   auto dev_url_socket = "socket://ql100aiu@192.168.125.148:9981?npu=0x40000000&dma=0x80000000";
   //开启设备
   Device device;
   device = Device::Open(dev_url_socket);

   //cast得到bydevice方便申请memchunk
   auto bydevice = device.cast<BuyiDevice>();

   //加载网络
   auto network = Network::CreateFromJsonFile("E:/test/json&raw/30/seresnet_BY.json");
   network.loadParamsFromFile("E:/test/json&raw/30/seresnet_BY.raw");

   //初始化buyibackend
   icraft::xrt::Backend backend1 = BuyiBackend::Init();
   icraft::xrt::Backend backend2 = BuyiBackend::Init();
   backend1.init(network, device);
   backend2.init(network, device);

   //cast得到BuyiBackend对象
   auto by_backend1 = backend1.cast<BuyiBackend>();
   auto by_backend2 = backend2.cast<BuyiBackend>();

   //部署前对by_backend2调用中间层优化
   by_backend2.compressFtmp();

   //由于两个buyibackend初始化用的是相同的网络，所以他们对应的指令数据段和权重数据段的逻辑字节大小肯定是相同的
   auto instr_bytesize = by_backend1->logic_segment_map.at(Segment::INSTR)->byte_size;
   auto weight_bytesize = by_backend1->logic_segment_map.at(Segment::WEIGHT)->byte_size;

   //虽然两个buyibackend初始化用的是相同的网络，但是by_backend2调用了中间层优化，此时两个buyibackend的中间层数据段的逻辑字节大小是不一样的
   //这里我们取较大的那个字节大小
   auto ftmp_bytesize1 = by_backend1->logic_segment_map.at(Segment::FTMP)->byte_size;
   auto ftmp_bytesize2 = by_backend2->logic_segment_map.at(Segment::FTMP)->byte_size;
   auto ftmp_bytesize = ftmp_bytesize1 > ftmp_bytesize2 ? ftmp_bytesize1 : ftmp_bytesize2;

   //向bydevice申请etm上的memchunk
   auto ftmp_chunk = bydevice.defaultMemRegion().malloc(ftmp_bytesize);
   auto weight_chunk = bydevice.defaultMemRegion().malloc(weight_bytesize);
   auto instr_chunk  = bydevice.defaultMemRegion().malloc(instr_bytesize);

   //对于相同网络的buyibackend，其权重数据是一样的，所以可以通过复用同一片空间来实现内存优化，这时只要传入将申请的memchunk以及对应的分段属性传入接口
   //不同于权重层，指令层的数据即便网络相同，但在真实执行时由于输入输出层物理地址不同，其数据是不一样的，所以不能复用
   by_backend1.userSetSegment(weight_chunk, Segment::WEIGHT);
   by_backend2.userSetSegment(weight_chunk, Segment::WEIGHT);

   //对于中间层数据，我们只要传入较大字节大小的memchunk，即便是不同网络的buyibackend也能够复用内存空间
   by_backend1.userSetSegment(ftmp_chunk, Segment::FTMP);
   by_backend2.userSetSegment(ftmp_chunk, Segment::FTMP);

   //在配置完成后进行部署
   backend1.apply();
   backend2.apply();

   //此时输出两个buyibackend的log，可以看出相应权重以及中间层的真实物理地址在同一个区域
   by_backend1.log();
   by_backend2.log();

   //通过程序也可以验证两个backend的权重，中间层段是同一个memchunk，指令段是不同的memchunk
   //对于用户自行申请的memchunk，使用完成后用户要记得去释放
   REQUIRE(by_backend1->phy_segment_map.at(Segment::INSTR)->memchunk != by_backend2->phy_segment_map.at(Segment::INSTR)->memchunk);
   REQUIRE(by_backend1->phy_segment_map.at(Segment::WEIGHT)->memchunk == by_backend2->phy_segment_map.at(Segment::WEIGHT)->memchunk);
   REQUIRE(by_backend1->phy_segment_map.at(Segment::FTMP)->memchunk == by_backend2->phy_segment_map.at(Segment::FTMP)->memchunk);

   //部署完成后进行预检
   REQUIRE(by_backend1.precheck());
   REQUIRE(by_backend2.precheck());

   //关闭设备
   Device::Close(device);

``Python示例``
""""""""""""""
.. code-block:: python
   :linenos:

   # 导入必要的库
   import xir
   from xrt import *
   from buyibackend import *
   import unittest

   dev_url = "axi://ql100aiu?npu=0x40000000&dma=0x80000000"
   dev_url_socket = "socket://ql100aiu@192.168.125.148:9981?npu=0x40000000&dma=0x80000000"
   device = Device.Open(dev_url_socket)
   bydevice = BuyiDevice(device)

   network = xir.Network.CreateFromJsonFile("E:/test/json&raw/30/seresnet_BY.json")
   network.loadParamsFromFile("E:/test/json&raw/30/seresnet_BY.raw")

   backend1 = Backend.Create(BuyiBackend, network.view(0), device)
   backend2 = Backend.Create(BuyiBackend, network.view(0), device)
   buyibackend1 = BuyiBackend(backend1)
   buyibackend2 = BuyiBackend(backend2)
   buyibackend2.compressFtmp()

   instr_bytesize = buyibackend1.logic_segment_map[Segment.INSTR].byte_size;
   weight_bytesize = buyibackend1.logic_segment_map[Segment.WEIGHT].byte_size;
   ftmp_bytesize1 = buyibackend1.logic_segment_map[Segment.FTMP].byte_size;
   ftmp_bytesize2 = buyibackend2.logic_segment_map[Segment.FTMP].byte_size;
   ftmp_bytesize = 0
   if ftmp_bytesize1 > ftmp_bytesize2:
      ftmp_bytesize = ftmp_bytesize1
   else:
      ftmp_bytesize = ftmp_bytesize2

   ftmp_chunk = bydevice.defaultMemRegion().malloc(ftmp_bytesize);
   weight_chunk = bydevice.defaultMemRegion().malloc(weight_bytesize);
   instr_chunk  = bydevice.defaultMemRegion().malloc(instr_bytesize);

   buyibackend1.userSetSegment(weight_chunk, Segment.WEIGHT);
   buyibackend2.userSetSegment(weight_chunk, Segment.WEIGHT);

   buyibackend1.userSetSegment(ftmp_chunk, Segment.FTMP);
   buyibackend2.userSetSegment(ftmp_chunk, Segment.FTMP);

   backend1.apply()
   backend2.apply()

   buyibackend1.log()
   buyibackend2.log()

   self.assertTrue(buyibackend1.phy_segment_map[Segment.INSTR].memchunk != buyibackend2.phy_segment_map[Segment.INSTR].memchunk)
   self.assertTrue(buyibackend1.phy_segment_map[Segment.WEIGHT].memchunk == buyibackend2.phy_segment_map[Segment.WEIGHT].memchunk)
   self.assertTrue(buyibackend1.phy_segment_map[Segment.FTMP].memchunk == buyibackend2.phy_segment_map[Segment.FTMP].memchunk)

   self.assertTrue(buyibackend1.precheck())
   self.assertTrue(buyibackend1.precheck())
   Device.Close(device)

二 数据结构
===========

2.1 ValueInfo
----------
``ValueInfo`` 与icraft::xir::Value类的对应，包含BuyiBackend中一些补充信息。

``ValueInfo`` 有以下属性：

* ``Value value`` ：icraft::xir::Value的引用类，获得对应的Value指针
* ``bool is_ocm = false`` ：若为true，表示对应的value数据存放在ocm上
* ``bool is_host = false`` ：若为true，表示对应的value数据存放在host端
* ``std::vector<Value> real_to`` ：real若为true, 可能包含与其共用etm为false的value
* ``Value fake_from`` ：real若为false，必定存在预期共用etm地址且为true的value
* ``uint64_t logic_addr = 0`` ：value 在etm分配的逻辑字节地址
* ``int producer = 0`` ：生成该Value的的算子的op_id
* ``int origin_producer = -1`` ：初始生成该Value的算子的op_id, 合并算子情况下与producer的值不同
* ``bool user_used = false`` ：若为true，表示存放value的memchunk为用户申请
* ``uint64_t phy_addr = 0`` ：value 在etm分配的真实物理字节地址
* ``uint64_t byte_size = 0`` ：value 在etm上占据的字节大小
* ``std::vector<int> consumer`` ：value 的consumer的op_id集合
* ``Segment segment = Segment::FTMP`` ：value 在etm上地址对应的分段类型



2.2 HardOpInfo
-------------
``HardOpInfo`` 与icraft::xir::HardOp类的对应，包含BuyiBackend中一些补充信息。

``HardOpInfo`` 有以下属性：

* ``uint64_t weights_logic_addr = 0`` ：对应HardOp的权重在etm上分配的逻辑字节地址
* ``uint64_t weights_size = 0`` ：对应HardOp的权重在etm上的字节大小
* ``uint64_t instr_logic_addr = 0`` ：对应HardOp的指令在etm上分配的逻辑字节地址
* ``uint64_t instr_logic_size = 0`` ：对应HardOp的指令在etm上分配的逻辑字节大小
* ``bool user_used = false`` ：若为true，表示对应HardOp在etm对应的memchunk为用户申请
* ``uint64_t weight_phy_addr`` ：对应HardOp的权重在etm上的真实物理字节地址
* ``uint64_t instr_phy_addr`` ：对应HardOp的指令在etm上的真实物理字节地址
* ``uint64_t instr_phy_size`` ：对应HardOp的指令在etm上的真实字节大小
* ``HardOp net_hardop`` ：对应icraft::xir::HardOp类的指针
* ``std::vector<ULL> instr_data`` ：对应HardOp执行的指令
* ``std::vector<int> merge_from`` ：如果在speedMode下，表示合并前的hardop op_id集合
* ``int sync_idx`` ：对应HardOp的同步信息


2.3 LogicSegment
-------------

``LogicSegment`` 逻辑分段类，在BuyiBackend 初始化时生成，表示对应network_view的各分段的逻辑地址相关数据。

``LogicSegment`` 有以下属性：

* ``std::map<int, ValueInfo> info_map`` ：逻辑分段包含的valueInfo信息  <v_id, valueInfo>
* ``std::map<int, HardOpInfo> hardop_map`` ：逻辑分段包含的hardOp信息  <op_id, hardopInfo>
* ``uint64_t logic_addr = 0`` ：逻辑分段在etm的逻辑字节地址
* ``uint64_t byte_size = 0`` ：逻辑分段在etm的字节大小
* ``Segment segment_type`` ：逻辑分段的分段类型

2.4 PhySegment
------------

``PhySegment`` 表示某一个内存区域上分配的一块连续内存块。``MemChunk`` 没有构造函数，所有的 ``MemChunk`` 都是由 ``MemRegion`` 的 ``malloc`` 方法分配产生的。

``PhySegment`` 有以下属性：

* ``std::map<int, ValueInfo> info_map`` ：物理分段包含的valueInfo信息  <v_id, valueInfo>
* ``std::map<int, HardOpInfo> hardop_map`` ：物理分段包含的hardOp信息  <op_id, hardopInfo>
* ``uint64_t phy_addr`` ：物理分段在etm上的真实物理字节地址
* ``uint64_t byte_size = 0`` ：物理分段在etm的字节大小
* ``Segment segment_type`` ：物理分段的分段类型
* ``MemChunk memchunk`` ：物理分段在etm上申请的memchunk
* ``bool user_used = false`` ：若为true，表示物理分段的memchunk是用户申请的

2.5 ForwardInfo
----------

``ForwardInfo`` 包含前向所需要的信息

``ForwardInfo`` 有以下属性：

* ``std::map<int, ValueInfo> value_map`` ：物理分段包含的valueInfo信息  <v_id, valueInfo>
* ``std::map<int, HardOpInfo> hardop_map`` ：network_view包含的所有hardopInfo的集合 <op_id, HardOpInfo>
* ``std::map<int, int> idx_map`` ：network_view中所有op的同步信息集合     <op_id, sync_idx>

2.6 BuyiBackend
----------

``BuyiBackend`` 表示执行在BUYI架构7100芯片的后端。



``BuyiBackend`` 除了有之前的接口外还有以下属性：

* ``std::map<uint32_t, ValueInfo> value_info`` ：包含network_view所有valueInfo的信息, <v_id, ValueInfo>
* ``ForwardInfo forward_info`` ：包含buyibackend所有前向所需要的信息
* ``std::map<Segment, LogicSegment> logic_segment_map`` ：包含buyibackend的所有逻辑分段信息，<segment_type, logicSegment>
* ``std::map<Segment, PhySegment> phy_segment_map`` ：包含buyibackend的所有物理分段信息, <segment_type, phySegment>
* ``std::list<Value> value_list`` ：所有在etm上真实分配空间的value的信息
* ``bool is_compressftmp_ = false`` ：若为true, buyibackend开启compressFtmp功能
* ``bool is_applied_ = false`` ：若为true, buyibackend已完成部署
* ``static int buyibackend_sid`` ：buyibackend的静态技术，计算当前程序下buyibackend的个数
* ``int buyibackend_id = 0`` ：当前buyibackend的id



以下以 结合icraft::xrt::Session，展示具体的接口和属性使用方法：


.. code-block:: cpp
   :linenos:
   //静态函数，通过图片以及dtype构造icraftL::xrt::Tensor
   static auto img2Tensor = [](const std::string& img_path, const TensorType& dtype) {
	   if (std::filesystem::exists(img_path) == false) {
		   throw std::logic_error("[Error in SimulatorInput::setImageFile] image file not exist, please check.");
	   }

	   cv::Mat img = cv::imread(img_path, cv::IMREAD_COLOR);
	   cv::Mat converted;
	   img.convertTo(converted, CV_32F);

	   auto& tensor_shape = dtype->shape;
	   auto& tensor_layout = dtype->layout;

	   int H = converted.rows;
	   int W = converted.cols;
	   int C = converted.channels();

	   std::vector<int64_t> output_shape = { 1, H, W, C };
	   auto output_type = TensorType(FloatType::FP32(), output_shape, tensor_layout);
	   auto output_tensor = Tensor(output_type).mallocOn(HostDevice::MemRegion());
	   auto output_tptr = (float*)output_tensor.data().cptr();
	   std::copy_n((float*)converted.data, H * W * C, output_tptr);

	   return output_tensor;
   };

   //打开device
   auto dev_url = "axi://ql100aiu?npu=0x40000000&dma=0x80000000";
	auto dev_url_socket = "socket://ql100aiu@192.168.125.77:9981?npu=0x40000000&dma=0x80000000";
	Device device;
	device = Device::Open(dev_url_socket);


	auto json_file = "E:/test/json&raw/30/nafnet16_BY_int8.json";
	auto raw_file = "E:/test/json&raw/30/nafnet16_BY_int8.raw";
	auto img_file = "E:/test/images/elephant.jpg";

   //构造加载网络
	auto network = Network::CreateFromJsonFile(json_file);
	network.loadParamsFromFile(raw_file);

   //构造session，同时构造BuyiBackend以及HostBakcend并完成算子绑定
	auto sess = Session::Create<BuyiBackend, HostBackend>(network, { device, HostDevice::Default()});
	
   //从session处得到buyi_backend
	auto buyi_backend = sess->backends[0].cast<BuyiBackend>();

   //配置中间层优化
	buyi_backend.compressFtmp();

   //运行前apply部署
	sess.apply();

   //部署完成后进行预检，输出log
	buyi_backend.precheck();
	buyi_backend.log();
	
   //通过network获得dtype并结合输入图片构造输入tensor
	auto input_dtype = network.inputOp()[0].dtype();
	auto input_tensor = img2Tensor(img_file, input_dtype);

   //执行forward前向得到输出
	auto output_tensors = sess.forward({ input_tensor });

	auto file_name = "./logs/" + network->name;
   if (!fs::is_directory(file_name) || !fs::exists(file_name)) {
	   fs::create_directories(file_name);
   }
   
   //通过buyibackend获得phy_segment_map以及value_map
   //value_map里面有前向所有valueInfo的合集
   auto phy_segment_map = buyi_backend->phy_segment_map;
   auto value_map = buyi_backend->forward_info->value_map;

   //这个循环函数的目的是将session运行过程中保存在etm上的Value全部以二进制文件的形式保存下来
   for (auto k_v : value_map) {
      //如果value的数据在host或者ocm上就跳过
	   if (k_v.second->is_host || k_v.second->is_ocm) {
		   continue;
	   }
      //获取forward_info中value相关数据
	   auto id = k_v.second->value->v_id;
	   auto byte_size = k_v.second->byte_size;
	   auto phy_addr = k_v.second->phy_addr;
	   auto mem_chunk = phy_segment_map.at(k_v.second->segment)->memchunk;
	   std::cout << "start dump v_id: " << id << std::endl;
	   std::shared_ptr<int8_t[]> temp = std::shared_ptr<int8_t[]>(new int8_t[byte_size]{ 0 });
      //将value所在memchunk对应偏移的数据保存到内存中并以二进制的形式保存下来
	   mem_chunk.read((char*)temp.get(), phy_addr - mem_chunk->begin.addr(), byte_size);
	   std::ofstream out_file(file_name + "/" + std::to_string(id) + ".ftmp", std::ios::out | std::ios::binary);
	   out_file.write((char*)temp.get(), byte_size);
	   out_file.close();
   }


三 索引
=======
* :ref:`genindex`