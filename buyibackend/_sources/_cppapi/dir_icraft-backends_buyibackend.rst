.. _dir_icraft-backends_buyibackend:


Directory buyibackend
=====================


|exhale_lsh| :ref:`Parent directory <dir_icraft-backends>` (``icraft-backends``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


*Directory path:* ``icraft-backends\buyibackend``


Files
-----

- :ref:`file_icraft-backends_buyibackend_buyibackend.h`


