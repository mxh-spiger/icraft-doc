.. _exhale_class_classicraft_1_1xrt_1_1_speed_info_node:

Class SpeedInfoNode
===================

- Defined in :ref:`file_icraft-backends_buyibackend_buyibackend.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::NodeBase< SpeedInfoNode, Node >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::SpeedInfoNode
   :project: Icraft BuyiBackend
   :members:
   :protected-members:
   :undoc-members: