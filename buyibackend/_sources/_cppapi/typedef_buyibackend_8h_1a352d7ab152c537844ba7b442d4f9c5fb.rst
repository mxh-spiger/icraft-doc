.. _exhale_typedef_buyibackend_8h_1a352d7ab152c537844ba7b442d4f9c5fb:

Typedef icraft::xrt::ITER_TYPE
==============================

- Defined in :ref:`file_icraft-backends_buyibackend_buyibackend.h`


Typedef Documentation
---------------------


.. doxygentypedef:: icraft::xrt::ITER_TYPE
   :project: Icraft BuyiBackend