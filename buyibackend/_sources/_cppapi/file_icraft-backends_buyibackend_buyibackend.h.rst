
.. _file_icraft-backends_buyibackend_buyibackend.h:

File buyibackend.h
==================

|exhale_lsh| :ref:`Parent directory <dir_icraft-backends_buyibackend>` (``icraft-backends\buyibackend``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-backends\buyibackend\buyibackend.h``)
----------------------------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-backends_buyibackend_buyibackend.h.rst





Includes
--------


- ``icraft-xir/core/data.h``

- ``icraft-xir/ops/hard_op.h``

- ``icraft-xrt/core/backend.h``

- ``icraft-xrt/core/device.h``

- ``iostream``

- ``list``

- ``map``

- ``set``

- ``vector``






Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__xir`

- :ref:`namespace_icraft__xrt`


Classes
-------


- :ref:`exhale_class_classicraft_1_1xrt_1_1_buyi_backend`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_buyi_backend_node`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_forward_info`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_forward_info_node`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_hard_op_info`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_hard_op_info_node`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_logic_segment`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_logic_segment_node`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_phy_segment`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_phy_segment_node`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_speed_info`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_speed_info_node`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_value_info`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_value_info_node`


Enums
-----


- :ref:`exhale_enum_buyibackend_8h_1a02e8200fc8b89f1431ff021142e3ead4`


Typedefs
--------


- :ref:`exhale_typedef_buyibackend_8h_1a352d7ab152c537844ba7b442d4f9c5fb`

- :ref:`exhale_typedef_buyibackend_8h_1a114b7183aabd6c95ddb1f1cb7882cb9f`

