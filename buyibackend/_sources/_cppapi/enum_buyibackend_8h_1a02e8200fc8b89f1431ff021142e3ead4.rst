.. _exhale_enum_buyibackend_8h_1a02e8200fc8b89f1431ff021142e3ead4:

Enum Segment
============

- Defined in :ref:`file_icraft-backends_buyibackend_buyibackend.h`


Enum Documentation
------------------


.. doxygenenum:: icraft::xrt::Segment
   :project: Icraft BuyiBackend