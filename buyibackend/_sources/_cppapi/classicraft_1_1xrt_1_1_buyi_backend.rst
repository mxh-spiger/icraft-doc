.. _exhale_class_classicraft_1_1xrt_1_1_buyi_backend:

Class BuyiBackend
=================

- Defined in :ref:`file_icraft-backends_buyibackend_buyibackend.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::HandleBase< BuyiBackend, Backend, BuyiBackendNode >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::BuyiBackend
   :project: Icraft BuyiBackend
   :members:
   :protected-members:
   :undoc-members: