.. _exhale_class_classicraft_1_1xrt_1_1_hard_op_info_node:

Class HardOpInfoNode
====================

- Defined in :ref:`file_icraft-backends_buyibackend_buyibackend.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::NodeBase< HardOpInfoNode, Node >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::HardOpInfoNode
   :project: Icraft BuyiBackend
   :members:
   :protected-members:
   :undoc-members: