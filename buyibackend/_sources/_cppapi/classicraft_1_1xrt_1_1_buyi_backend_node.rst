.. _exhale_class_classicraft_1_1xrt_1_1_buyi_backend_node:

Class BuyiBackendNode
=====================

- Defined in :ref:`file_icraft-backends_buyibackend_buyibackend.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::NodeBase< BuyiBackendNode, BackendNode >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::BuyiBackendNode
   :project: Icraft BuyiBackend
   :members:
   :protected-members:
   :undoc-members: