.. _exhale_class_classicraft_1_1xrt_1_1_hard_op_info:

Class HardOpInfo
================

- Defined in :ref:`file_icraft-backends_buyibackend_buyibackend.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::HandleBase< HardOpInfo, Handle, HardOpInfoNode >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::HardOpInfo
   :project: Icraft BuyiBackend
   :members:
   :protected-members:
   :undoc-members: