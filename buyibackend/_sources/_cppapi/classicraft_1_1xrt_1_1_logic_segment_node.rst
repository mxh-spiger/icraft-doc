.. _exhale_class_classicraft_1_1xrt_1_1_logic_segment_node:

Class LogicSegmentNode
======================

- Defined in :ref:`file_icraft-backends_buyibackend_buyibackend.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::NodeBase< LogicSegmentNode, Node >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::LogicSegmentNode
   :project: Icraft BuyiBackend
   :members:
   :protected-members:
   :undoc-members: