.. _exhale_class_classicraft_1_1xrt_1_1_value_info_node:

Class ValueInfoNode
===================

- Defined in :ref:`file_icraft-backends_buyibackend_buyibackend.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::NodeBase< ValueInfoNode, Node >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::ValueInfoNode
   :project: Icraft BuyiBackend
   :members:
   :protected-members:
   :undoc-members: