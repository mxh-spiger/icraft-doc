.. _exhale_class_classicraft_1_1xrt_1_1_phy_segment:

Class PhySegment
================

- Defined in :ref:`file_icraft-backends_buyibackend_buyibackend.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::HandleBase< PhySegment, Handle, PhySegmentNode >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::PhySegment
   :project: Icraft BuyiBackend
   :members:
   :protected-members:
   :undoc-members: