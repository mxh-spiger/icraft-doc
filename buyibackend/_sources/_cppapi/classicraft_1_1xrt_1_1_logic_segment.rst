.. _exhale_class_classicraft_1_1xrt_1_1_logic_segment:

Class LogicSegment
==================

- Defined in :ref:`file_icraft-backends_buyibackend_buyibackend.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::HandleBase< LogicSegment, Handle, LogicSegmentNode >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::LogicSegment
   :project: Icraft BuyiBackend
   :members:
   :protected-members:
   :undoc-members: