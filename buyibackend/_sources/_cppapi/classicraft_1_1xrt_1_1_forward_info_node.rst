.. _exhale_class_classicraft_1_1xrt_1_1_forward_info_node:

Class ForwardInfoNode
=====================

- Defined in :ref:`file_icraft-backends_buyibackend_buyibackend.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::NodeBase< ForwardInfoNode, Node >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::ForwardInfoNode
   :project: Icraft BuyiBackend
   :members:
   :protected-members:
   :undoc-members: