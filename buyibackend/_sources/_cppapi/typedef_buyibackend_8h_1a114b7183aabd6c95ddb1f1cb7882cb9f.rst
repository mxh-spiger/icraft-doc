.. _exhale_typedef_buyibackend_8h_1a114b7183aabd6c95ddb1f1cb7882cb9f:

Typedef ULL
===========

- Defined in :ref:`file_icraft-backends_buyibackend_buyibackend.h`


Typedef Documentation
---------------------


.. doxygentypedef:: ULL
   :project: Icraft BuyiBackend