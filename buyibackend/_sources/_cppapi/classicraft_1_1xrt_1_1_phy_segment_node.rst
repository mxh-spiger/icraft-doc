.. _exhale_class_classicraft_1_1xrt_1_1_phy_segment_node:

Class PhySegmentNode
====================

- Defined in :ref:`file_icraft-backends_buyibackend_buyibackend.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::NodeBase< PhySegmentNode, Node >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::PhySegmentNode
   :project: Icraft BuyiBackend
   :members:
   :protected-members:
   :undoc-members: