.. _exhale_class_classicraft_1_1xrt_1_1_forward_info:

Class ForwardInfo
=================

- Defined in :ref:`file_icraft-backends_buyibackend_buyibackend.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::HandleBase< ForwardInfo, Handle, ForwardInfoNode >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::ForwardInfo
   :project: Icraft BuyiBackend
   :members:
   :protected-members:
   :undoc-members: