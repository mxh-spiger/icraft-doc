.. _exhale_class_classicraft_1_1xrt_1_1_speed_info:

Class SpeedInfo
===============

- Defined in :ref:`file_icraft-backends_buyibackend_buyibackend.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::HandleBase< SpeedInfo, Handle, SpeedInfoNode >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::SpeedInfo
   :project: Icraft BuyiBackend
   :members:
   :protected-members:
   :undoc-members: