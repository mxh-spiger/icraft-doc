.. _exhale_class_classicraft_1_1xrt_1_1_value_info:

Class ValueInfo
===============

- Defined in :ref:`file_icraft-backends_buyibackend_buyibackend.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::HandleBase< ValueInfo, Handle, ValueInfoNode >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::ValueInfo
   :project: Icraft BuyiBackend
   :members:
   :protected-members:
   :undoc-members: