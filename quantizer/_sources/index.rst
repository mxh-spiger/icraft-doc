==================
Icraft Quantizer
==================

一、整体介绍
=============

1.1 Icraft编译流程中的量化
--------------------------

.. image:: ./_static/icraft_struct.png

Icraft的整体架构组成主要分为三部分——编译、运行时和仿真测试。按功能分为多个组件，
各组件采用统一的自定义的中间层数据结构（Icraft IR），组件与组件之间的通信通过统一格式的json文件和raw文件。

编译部分的流程包括解析、优化、量化、适配和指令生成。
解析时将框架模型转换为Icraft IR表达的模型。
优化会针对模型的算子进行等价的合并与消除以减少计算量，不会改变模型结果与精度。
量化会将网络各层的输入输出以及权重参数都转换为定点值来表示，同时支持混合精度量化，
从而在保证网络精度的基础上，大幅减少功耗和内存占用以及数据传输延迟。
指令生成会将量化后的模型进行硬件适配与编译，生成最终在AI芯片上执行的指令序列。

1.2 量化功能概述
-----------------


量化本质上是将浮点计算转化为定点计算以部署到支持定点计算的后端芯片上的过程。
为简化模型定点前向时的计算，模型参数从浮点映射为定点通常使用线性变换，也即线性量化，映射公式表示如下：

.. image:: ./_static/linear_quant.PNG

其中，x表示量化前浮点数，x_int表示量化后定点数，S表示缩放因子，Z表示零点。
当采用对称量化方案时，零点便等于0。

在对称量化方案下，量化的过程便是确定缩放因子并将浮点数转变为定点数的过程。
目前Icraft量化提供KLD、EMS和NULL等算法获取合适的缩放因子，同时支持将浮点数映射为INT8和INT16两种定点数据类型。
Icraft量化还支持选择将浮点映射为定点时的粒度，也即按层量化/按通道量化，按层量化通常指该层的参数共享同一个缩放因子，
按通道量化指的是不同输出通道上的参数共享同一个缩放因子。

在将网络中所有参数定点化成INT8/INT16后，量化组件便将模型保存成INT8/INT16格式的json&raw文件，
最后便能使用量化分析工具来验证该定点网络模型的精度。

1.3 量化流程
--------------

Icraft量化组件支持一键式的训练后量化，但其中包括多个重要步骤，因此对其分步介绍可以对量化流程有更直接的了解。
量化的流程图如下：

.. image:: ./_static/quantizer_flow.png

对输入的浮点网络进行量化主要经历了加载网络、变换输入、前向校准、测量特征图、计算归一化系数、归一化、量化参数、生成网络等步骤。
以下对每个步骤进行详细说明：



1）准备浮点网络与校准集
^^^^^^^^^^^^^^^^^^^^^^^^

Icraft量化的过程中会使用一定数量的校准集在浮点网络上进行推理，然后统计每层输入输出的数据范围作为量化
特征图时的参考，因此需首先保证浮点网络的推理结果正确，同时校准集应来自于实际测试环境中才能确保量化统计的
数据范围有实际意义。

2）加载网络
^^^^^^^^^^^^^^

从json&raw文件中加载浮点网络到内存中，在内存中表示为IR中定义的Network类型的对象。
使用该对象可以访问和改变网络中每一个算子的参数和属性等，可以方便的对网络进行归一化和量化等变换处理。

3）变换输入
^^^^^^^^^^^^^^

输入转换的目的是合并pre_scale和pre_mean参数到第一层卷积，并将pre_mean调整为128，使输入层的动态范围在（-128，127）。
该步骤严格说属于优化的内容，后续可能会被迁移到优化组件中。

4）前向校准
^^^^^^^^^^^^^

前向即对校准数据集在浮点网络上进行推断，从而获得每一层特征图的分布。根据forward_mode的不同，分为两种方式：

* 当forward_mode为image时，对指定的校准集进行推断，得到每一层的特征图。
* 当forward_mode为ftmp时，从指定的文件中加载仿真器组件预先输出的特征图文件，从而得到每一层的特征图。

当校准集太大时，一次对所有图片进行前向会对电脑的可用内存造成很大的压力。因此当电脑的内存较小时，可以使用batch对校准集进行分批前向。
比如校准集中有100张图片，指定batch = 5则会对校准集测量5次，每次测量20张图片。量化组件会对多次测量的结果取平均，作为最终的测量结果。

5）测量特征图
^^^^^^^^^^^^^^^

测量的过程即根据saturation指定的饱和方法确定每一个特征图的饱和点。

6）计算归一化系数
^^^^^^^^^^^^^^^^^^

根据测量确定的饱和点，计算每一个特征图的归一化系数。计算公式如下：

.. image:: ./_static/calc_norm.PNG

7）归一化
^^^^^^^^^^^

在测量特征图和计算归一化系数后，需要对网络的参数进行归一化处理。

根据每个特征图的归一化系数对网络进行归一化，实际操作便是算子输入和输出的归一化系数要融合到网络参数中去，
以卷积为例介绍归一化系数融合的方法：

.. image:: ./_static/conv_norm.PNG

归一化后，网络和之前的网络是完全等价的浮点网络，只是每个特征图的动态范围是相同的。
归一化操作保证了所有特征图的动态范围恰好在量化能表达的范围内，简化了量化确定截位信息时对特征图scale的考虑。

8）量化
^^^^^^^^^^

量化首先要确定特征图和网络参数的scale，其中由于归一化后所有特征图的scale为1，
因此量化的过程主要是确定网络参数的scale。
在根据saturation指定的饱和方式统计出参数的饱和点后，便能根据要量化成何种数据格式计算得到scale（其中scale参数
需要根据硬件特性进行调整，将在算子介绍中进行说明），最后根据该scale将浮点数映射为定点数。

9）生成网络
^^^^^^^^^^^^^

将量化后的网络序列化为中间层文件。

10）精度测试与分析
^^^^^^^^^^^^^^^^^^^

在获得量化后的json&raw文件后，便能通过Icraft的仿真工具来验证该量化后的模型是否满足精度要求，若不满足
还可通过调节量化的配置参数再次量化，或者采用混合精度量化直到达到精度要求。后文将对精度评估工具和
量化调优手段进行详细介绍。

二、使用说明
==============

量化组件（Icraft Quantizer）通过icraft quantize命令进行调用，后面可以跟配置文件，
也可以直接通过命令参数进行配置。
量化组件的参数可以在Icraft的toml配置文件中的[quantize]段进行配置，也可以直接在命令行通过直接配置，
还可以两者结合，命令行中配置的参数会覆盖toml文件中的对应参数。调用方式如下所示：

icraft	quantize	[<config file>]		[--<key> <value>]

2.1 参数配置
--------------

量化组件提供给用户的接口为命令行接口（CLI），用户可以通过CLI选择前向方式、饱和方式、量化粒度和量化位数等。
具体的参数配置如表所示。

.. list-table:: Table.量化参数列表
   :widths: 15 15 30 
   :header-rows: 1

   * - 参数名字
     - 数据格式
     - 说明
   * - json
     - string
     - 表示输入的中间层json文件路径 
   * - raw
     - string
     - 表示输入的中间层raw文件路径 
   * - jr_path
     - string
     - 表示输出的中间文件存放的路径
   * - target
     - string
     - 表示量化面向的后端架构（buyi）
   * - bits
     - int
     - 表示量化位数（8/16）
   * - forward_mode
     - string
     - 表示校准集的格式（image/ftmp）
   * - forward_dir
     - string
     - 表示校准集图片所在的文件夹
   * - forward_list
     - string
     - 表示校准集包含的图片列表
   * - saturation
     - string
     - 表示统计饱和点的方法（kld/ems/null）  
   * - per
     - string
     - 表示按层还是按通道量化（tensor/channel）
   * - batch
     - int（optional）
     - 将校准集分为几份，依次进行前向计算（电脑内存较小时使用），默认为1
   * - bin_num
     - int（optional）
     - 表示统计饱和点时将数据分成多少个bin，默认4096
   * - no_transinput
     - bool（optional）
     - 表示是否对前处理算子中的参数进行合并与转换，默认false
   * - no_normalize
     - bool（optional）
     - 表示是否做归一化，默认false
   * - before_relu
     - bool（optional）
     - 表示卷积输出ftmp是否用relu之前的统计，默认false
   * - ftmp_csv
     - string（optional）
     - 表示ftmp_csv的路径，从该ftmp_csv中获取ftmp的饱和点时使用
   * - raw_csv
     - string（optional）
     - 表示raw_csv的路径，从该raw_csv中获取weight的饱和点时使用
   * - decode_dll
     - string（optional）
     - 表示调用的解码图片dll的路径
   * - input_norm
     - string（optional）
     - 表示input算子的ftmp使用网络中某些ftmp的normratio
   * - mix_precision
     - string（optional）
     - 配置mix_precision= "auto"：表示自动实现混合精度量化，并按默认混合方案进行混合精度配置；
       配置mix_precision= "xxx.csv"：表示根据用户自定义的位宽配置文件实现混合精度量化。

2.2 校准集前向模式
------------------------

forward_mode参数表示量化组件测量特征图时，每一层特征图的来源是仿真器导出的结果文件（ftmp）还是直接对网络进行前向（image）得到结果。

1）image
^^^^^^^^^^^^^^

当forward_mode = image时，表示量化组件将会使用指定的校准集做前向，从而测量特征图的分布范围。此时forward_dir表示存放校准集的文件夹，
forward_list表示描述相对于forward_dir的图片路径，每行表示一个图片路径。示例配置如下：

.. image:: ./_static/image_config.PNG

其中，pic_list_coco.txt如下所示：

.. image:: ./_static/image_pic_lists.PNG

当模型存在多输入时，pic_list_coco.txt如下所示，多输入之间用分号隔开。

.. image:: ./_static/image_pic_lists_multi_input.PNG

**image模式能够简单方便的选择和使用校准集进行量化，不需要仿真器等其他组件的支持，因此推荐用户使用该模式进行量化。**

2）ftmp
^^^^^^^^^^^^^^

当forward_mode = ftmp时，表示量化组件将会根据仿真器导出的ftmp文件来测量特征图的分布范围。此时forward_dir表示存放特征图文件夹的根路径，
forward_list表示描述相对于forward_dir的特征图文件夹路径的列表文件，每行表示一个文件夹路径。示例配置如下：

.. image:: ./_static/ftmp_config.PNG

其中，yolov3_ftmp_list.txt如下所示：

.. image:: ./_static/ftmp_pic_lists.PNG

2.3 校准集分批测量
------------------------

量化需要测量每个特征图的分布，从而确定合适的量化缩放因子。校准集的作用就是使用一部分图片代表整个测试集来测量每个特征图的分布范围，
因此理论上来讲，校准集中图片的数量越多越好。但是校准集越大，量化过程需要的内存或者时间也就越多。

**考虑到量化效果，建议校准集的容量不小于50张图片。**

在对校准集做前向时，默认会一次对forward_list中指定的所有图片做前向进行测量，这会对电脑的可用内存造成很大的压力。当电脑内存比较小时，
可以使用batch对校准集分批测量。比如校准集中有50张图片，指定batch = 5则会对校准集测量5次，每次测量10张图片。量化组件会对多次测量的结果取平均，
作为最终的测量结果。

**注意：校准集的图片总量要能被batch参数整除！**

2.4 量化粒度
--------------------

对权重进行量化，首先需要通过算法确定权重的饱和点，进而得到该权重的scale参数，然后所有浮点值便按照该scale映射为定点值。
在某些网络的权重参数中，不同通道的数据动态范围差异较大，只使用一个scale值难以反映这种差异，会导致量化误差较大，
因此可以通过更改量化粒度来实现不同通道对应不同的scale值。
以卷积为例，权重的数据排布格式为（h,w,ci,co），当per = tensor时所有权重对应1个scale值；per = channel时co个通道的数据对应co个scale值。

**一般来说，per_channel量化的表现会优于per_tensor**

2.5 饱和方式
----------------------

.. image:: ./_static/saturation.png

非饱和量化，如上左图所示，会将浮点数绝对值的最大值|max|映射到定点数的最大值。而饱和量化，如右图所示，则是通过一些方法找到一个更加合适的阈值映射到定点数的最大值。
饱和的目的是去除特征图的奇异点，当浮点数据分布不够均匀时，会使量化的缩放因子偏大，由于定点数的单位精度远小于浮点数，就会让浮点数中的很多数值集中映射到定点数的某几个数字上，
严重影响了量化的精度和效果。

量化组件支持三种饱和方式：null（非饱和），kld（KL散度最小），ems（误差的平方和最小）。下面以kld为例简单进行介绍：

* ``KLD``

当saturation = kld时，量化组件会采用KL散度最小的饱和的方式测量特征图和权重的分布。
P(x)表示浮点数据分布，Q(x)表示量化以后的定点数据分布，那么其KL散度为：

.. image:: ./_static/kld.PNG

为了计算KL散度，需要分别统计浮点和定点的数据分布，以8位对称量化为例，其统计方法如下：

* 对校准数据集做前向，得到每层浮点数的数值分布，并统计|max|

* 将0~|max|分成N个bin，统计每层的特征图落到各个bin中的数目

* 遍历第128~N个bin，遍历间隔为128：
      * 以当前bin的中值作为阈值T做截断，将大于当前bin以外的bin的数目加到当前bin上，减少直接抹去这些数值带来的影响
      
      * 计算此时的概率分布P，每个bin的概率即其统计数目除以数值的总数
      
      * 创建一个新的编码，其长度为128，其元素值即P量化后的INT8数值
      
      * 因为Q分布只有128个编码，为了计算相对熵，将其扩展到和P相同的长度
      
      * 计算P和Q的相对熵

* 选择最小的相对熵对应的bin，其对应的阈值即为最终的饱和阈值

使用KLD的饱和方式时，可以通过bin_num来指定划分bin的数目，默认bin_num = 4096。
bin_num越大，划分的精度越高，但是量化占用的内存或时间也会越多。

2.6 CustomOp接口
------------------

Icraft目前已经包含丰富的算子库，但当用户想要通过自定义算子来实现某些功能时，量化组件也提供了相应的
CustomOp接口来自定义该算子的量化方式。下面分两类CustomOp分别进行介绍：

1）自定义软算子
^^^^^^^^^^^^^^^^^^

只在CPU或GPU等计算资源上完成前向的算子称为软算子，该类算子进行的是浮点计算，无需量化。

2）自定义硬算子
^^^^^^^^^^^^^^^^^

需要利用NPU或PL等计算资源来完成前向的算子称为硬算子,该类算子进行的是定点计算，需要实现如下三个量化接口函数：

1. 计算归一化系数

归一化系数指的是每一层特征图的scale，CustomOp需根据该层的数据位宽来计算输出特征图的scale。
在logger中记录有该层输出特征图的饱和点以及计算位宽，使用这两个参数便能计算得到归一化系数。

.. code-block:: cpp
  :linenos:

  ICRAFT_IMPL_FUNCTOR(BuyiNormratio) < icraft::expand::GroupNorm > ([](const icraft::expand::GroupNorm& op, QuantizerLogger& logger) {
	int bits = logger.dtype_value_[op->op_id];  //获取该算子的计算位宽，可根据实际情况修改
	auto qt_max = pow(2, bits - 1) - 1;
	for (auto oftmp : op->outputs) {
		auto oftmp_id = oftmp->v_id;
		auto sat = logger.getFtmpSat(oftmp_id);  //获取输出特征图的饱和点
		std::vector<float> nr;
		for (int i = 0; i < sat.size(); i++) {
			auto nr_value = sat[i] / qt_max;  //根据饱和点计算归一化系数
			nr.push_back(nr_value);
		}
		logger.info_.info("\tftmp id: {}, calculate normratio = {}", oftmp_id, nr[0]);
		logger.logFtmpNormratio(oftmp_id, nr);  //将输出特征图的归一化系数记录到logger中
	}
	});
   
2. 归一化

归一化是指将特征图的归一化系数融入权重参数中，使得各层特征图都符合统一分布。
以卷积为例，从logger中获取输入输出特征图的归一化系数后，按照归一化的公式将这两个系数融入参数中。

.. code-block:: cpp
  :linenos:

  ICRAFT_IMPL_FUNCTOR(BuyiNormalize) < icraft::expand::Conv2d > ([](const icraft::expand::Conv2d& op, QuantizerLogger& logger) {
  auto conv2d = op.cast<Conv2d>();
  auto processor = conv2d->compile_target->typeKey();

  auto op_type = static_cast<std::string>(conv2d->typeKey());
  logger.info_.info("Normalizing Op Id: {}, Type: {}", conv2d->op_id, op_type);
  auto iftmp_id = conv2d->inputs[0]->v_id;
  auto oftmp_id = conv2d->outputs[0]->v_id;
  auto& input_norm = logger.getFtmpNormratio(iftmp_id);  //获取输入特征图的归一化系数
  auto& output_norm = logger.getFtmpNormratio(oftmp_id);  //获取输出特征图的归一化系数
  auto weight = conv2d->inputs[1];
  auto& weight_dim = weight.tensorType()->shape;
  auto wh = weight_dim[0];
  auto ww = weight_dim[1];
  auto input_chann = weight_dim[2];
  auto output_chann = weight_dim[3];

  auto weight_data = weight.cast<Params>().data<float>();
  for (int h = 0; h < wh; h++) {
    for (int w = 0; w < ww; w++) {
      for (int i = 0; i < input_chann; i++) {
        for (int o = 0; o < output_chann; o++) {
          auto index = h * ww * input_chann * output_chann + w * input_chann * output_chann + i * output_chann + o;
          //根据输入和输出的归一化系数调整权重参数
          weight_data[index] = weight_data[index] * input_norm[i] / output_norm[o];
        }
      }
    }
  }
  auto bias = conv2d->inputs[2];
  auto bias_size = bias.tensorType().numElements();
  auto bias_data = bias.cast<Params>().data<float>();
  for (int i = 0; i < bias_size; i++) {
    //根据输出的归一化系数调整bias
    bias_data[i] /= output_norm[i];
  }
  });

3. 量化

在量化接口中需将权重参数和输出特征图都转变为量化类型，下面是转变输出特征图类型的范例。

.. code-block:: cpp
  :linenos:

  ICRAFT_IMPL_FUNCTOR(BuyiQuantize) < icraft::expand::GroupNorm > ([](const icraft::expand::GroupNorm& op, QuantizerLogger& logger) {
  auto op_id = op->op_id;
  int bits = logger.dtype_value_[op_id];  //获取该算子的计算位宽，可根据实际情况修改
  auto output_ftmps = op->outputs;
  //设置输出特征图为量化类型
  for (auto&& output_ftmp : output_ftmps) {
    auto output_ftmp_id = output_ftmp->v_id;
    auto norm = logger.getFtmpNormratio(output_ftmp_id);
    auto shape = output_ftmp.dtype()->shape;
    auto layout = output_ftmp.dtype()->layout;
    auto co = shape[-1];
    auto hard_scale = std::vector<std::pair<int, int>>(co, { 1, 0 });  //归一化后特征图的scale均为1
    auto scale = std::vector<double>(co, 1);
    auto o_type = makeQDtype(shape, layout, bits, hard_scale, scale, norm);
    output_ftmp.setDType(o_type);

    logger.logFtmpScale(output_ftmp_id, 1);
    logger.logFtmpExp(output_ftmp_id, 0);
    logger.info_.info("\tQuantizing ftmp id: {}, scale: {}, exp: {}", output_ftmp_id, 1, 0);
  }
	});

三、混合精度量化
===================

3.1 整体功能介绍
-------------------
当对网络的运行速度和精度都有较高要求时，即希望同时实现INT8的速度和INT16的精度，可以尝试使用混合精度量化。
混合精度量化通过采用特定策略选择网络中量化影响较大的算子，并使用INT16进行计算，旨在减少量化损失并提高网络的精度。
通过将关键算子使用INT16进行计算，可以在一定程度上缓解量化引入的精度损失。

混合精度量化的优势在于实现网络精度和推理速度之间的平衡。通过将一部分算子的采用高精度计算，混合精度量化可以提高整体网络的精度水平。
同时，将其他算子采用低精度进行计算，可以保证网络的推理速度，满足速度要求。

需要注意的是，混合精度量化的效果取决于选择合适的量化策略和算子。
对于量化影响较大的算子，如卷积和全连接层、或者参数不适合量化的算子等，将其计算精度提升到INT16可以明显减小量化误差，提升网络精度。
而对于其他对精度要求较低的算子，如最大池化等不涉及计算的算子，可以继续使用较低精度进行计算，以降低计算成本，保证网络推理速度。

总之，混合精度量化是一种有效的方法，通过合理选择算子和精度，可以优化网络性能，在网络速度和精度之间取得平衡。

3.2 混合精度量化流程
------------------------
混合精度量化流程是在原有的量化步骤中插入位宽配置的流程，主要包括以下步骤：准备数据集、加载网络、输入对齐、前向校准、位宽配置、量化网络以及精度测试与分析。

在混合精度量化中，位宽配置可以通过两种方式实现：

1、内置选取策略：
量化组件内置了一种选取策略，根据各层数据分布的差异，自动选择需要使用更高精度计算的层。
这种策略会分析网络中各个算子的数据以及特征，选取出需要使用INT16进行计算的算子，自动实现混合精度量化。

2、用户自定义配置：
另一种方式是用户根据自己的需求自定义配置网络中各层算子使用的计算位宽。
用户可以针对不同层的特性和精度要求，手动指定算子的计算位宽。在量化时，量化组件会根据用户的配置，实现混合精度量化。
这种方式提供了更大的灵活性，使用户可以根据具体情况对网络进行精细的控制。

无论是使用内置选取策略还是用户自定义配置，位宽配置的目的都是在混合精度量化过程中选择合适的位宽，以实现网络精度和推理速度之间的平衡。

3.3 使用说明
------------------
根据是否自定义位宽配置方案，使用接口上会有略微差异。

当使用--mix_precision=auto时，表示量化组件将根据内置策略自动分配各层算子的计算位宽，实现混合精度量化。
在这种情况下，用户无需进行额外的配置，量化组件会根据网络结构和数据分布等信息自动选择合适的位宽。

当使用--mix_precision="xx.csv"时，表示量化组件将根据用户自定义的位宽配置方案实现混合精度量化。
在这种情况下，用户需要提供一个包含位宽配置信息的CSV文件。该文件内部需要包含以下内容：

+--------+-------+--------+
|op_id   |type   |dtype   |
+========+=======+========+
|1       |conv   |INT8    |
+--------+-------+--------+
|2       |relu   |INT8    |
+--------+-------+--------+
|3       |conv   |INT16   |
+--------+-------+--------+
|4       |relu   |INT16   |
+--------+-------+--------+
|5       |conv   |INT8    |
+--------+-------+--------+
|···     |···    |···     |
+--------+-------+--------+

其中op_id需要与当前网络匹配，dtype仅支持INT8、INT16、FP32；

用户可以根据网络结构、算子特性和精度要求，在CSV文件中自行配置各个算子的位宽。
量化组件在量化过程中将根据该配置文件的信息，按照用户设定的位宽进行计算，实现混合精度量化。
通过提供自定义位宽配置方案，混合精度量化可以更好地满足用户的性能和精度需求，使用户能够灵活地控制网络中各个算子的计算精度。

另外，在使用混合精度量化时，以下是一些建议：
1、对输出层使用更高的计算位宽：
输出层通常是神经网络任务的最终结果，精度要求较高。
因此，建议对输出层使用更高的计算位宽，以确保输出结果的精度。

2、对量化前后余弦相似度低的算子使用更高的计算位宽：
在实现混合精度量化，可以通过计算量化前后各层特征图之间的余弦相似度来评估其量化对精度的影响程度。
如果发现某些算子的余弦相似度较低，即它们的量化前后结果差异较大，建议对这些算子使用更高的计算位宽，以提高它们的精度。

这些Tips旨在帮助优化混合精度量化的效果。根据具体的网络结构和任务需求，可以进一步调整和优化计算位宽的选择，以平衡精度和速度的需求。


四、量化效果分析
===================

4.1 精度损失分析指南
---------------------

量化会造成网络精度的损失，主要和以下几个方面相关：

1）量化位数
^^^^^^^^^^^^^

量化是使用较少位数的定点数的运算来表示浮点数的运算。一般来说，量化位数越高，量化的精度损失越少。
量化位数的支持一方面和量化组件有关，另一方面也取决于加速器对不同位数定点数的支持。
本量化组件支持布衣架构，支持8或16bit量化。
如果8位量化不能满足精度的需求，则可以尝试16位量化或混合精度量化

2）校准集
^^^^^^^^^^^

量化的精度损失也和选择的校准集有关。校准集的选择最好能够很好地代表真实检测/识别图片的数据分布。
因此，校准集可以从测试集或者训练集中选择。
一般情况下，校准集越大，越可能获得较好的量化效果。因此，当量化损失过大时，可以考虑增加/更换校准集。

3）饱和方法
^^^^^^^^^^^^^

饱和能够减少校准集特征图的奇异点带来的精度损失，因此相对于非饱和量化，使用饱和量化的方式能够提高量化效果。
本量化组件支持NULL、KLD和EMS的量化方式，当量化精度损失过大时，可以尝试更换饱和方式。

4）网络参数
^^^^^^^^^^^^^

量化误差和网络参数的分布有关，不同的参数分布对量化损失的容忍度不同。
当所使用的网络无论采取何种方式都不能获得很好的量化效果时，可以考虑采用量化感知训练微调网络参数，
使得网络参数更适于量化。量化感知训练的具体使用方法见《QAT用户手册》。

4.2 量化误差可视化分析
-----------------------

Icraft-Show通过运行浮点模型和量化模型并对每层的输出进行统计分析，以图形化的界面展示每层数据的量化误差。
该工具使用余弦相似度、平均绝对误差和相对欧氏距离作为量化误差的评价标准。

该工具分析完毕后通过网页展示分析结果以及模型的各项信息，网页内容包括分析结果（网页上方），网络结构（网页左下），
每层FeatureMap的数据对比（网页右中），分析结果表（网页右下）。

.. image:: ./_static/icraft_show.png

该工具的具体使用方法见《Icraft-Show使用手册》。

五、算子量化方案
================

5.1 BUYI架构
-------------

* ``Add``

**add_const**：

待补充

**add_ftmp**：
该浮点算子对应的公式是Y = aX + bY，

归一化后便是Yn = Xn*Nx*a/Nz + Yn*Ny*b/Nz，

假定I = Nx*a/Nz，J = Ny*b/Nz，均为浮点数，而实际在VPE上计算的只能是定点数，因此需将该两个值量化，

从而公式进一步转换为Yn = Xn*Iq*Si + Yn*Jq*Sj，又因为VPE中只有一次截位且位于乘加运算之后，

因此公式进一步转换为Yn = (Xn*Iq + Yn*Jq) * S；S = Si = Sj，其中S是通过I和J计算得到的scale系数。

S满足硬件p*2(-q)的格式，其中p数值范围[0,255], q数值范围[bits,2*bits]。


* ``Avgpool``


* ``Batchnorm``

由于该算子的特性，不同通道间的数据可能差异较大，因此无论"per"参数配置如何，均使用按通道量化实现。
该算子的前向使用VPE实现，量化公式如下：

Yq = (Wq * Xq + Bq) * Sw , Sw = Sb

因此要求w_scale等于b_scale，
经验表明，w对结果的影响往往更大，因此选取w_scale作为两者共同的scale

scale满足m*2(-n)的格式，其中m数值范围[0,255], n数值范围[bits,2*bits]。

* ``Concat``

**concat_const**：设定输出的normratio等于输入的normratio，然后使用该normratio对const做归一化操作，
最后在量化阶段将该const近似为Int值。

**concat_ftmp**：将输入的normratio在最后一维concat起来作为输出的normratio。

* ``Conv2d``

**conv_ftmp**：
如果是深度可分离卷积，则前向使用VPE实现，算子中的cut_scale属性用来记录VPE中的截位信息，
假定cut_scale = m*2^(-n)，其中m对应VPE中的乘法，数值范围[0,255]，n的数值范围[bits, 2*bits].

如果不是深度可分离卷积，则前向使用MPE+VPE实现，算子中的cut_scale属性记录MPE和VPE中的两次截位。
假定cut_scale = m*2^(-n)，其中m对应VPE中的乘法，数值范围[0,255]，n对应两次截位，数值范围[bits, 2*bits + 5]

需注意的是，n的下限是bits而不是0，是因为在VPE的激活模块存在默认截bits位数的操作，因此n必须大于等于bits。

假定两个输入的归一化系数是n1和n2,输出的归一化系数是n3，则cut_scale = n1 * n2 / n3，其中通过调整n3的数值保证
cut_scale满足上述的截位要求。

**conv_const**：
统计normratio和归一化操作比较常规，无需赘述。
在conv_const的情形下，使用w_scale和b_scale共同表示截位信息。

如果是深度可分离卷积，则前向使用VPE实现，w_scale需等于b_scale，这个共同的scale作为VPE中的截位。
至于如何确定这个共同的scale，优先采用w_scale，当w_scale接近零时才会采用b_scale，当两者都接近零时
则认为scale数值不重要，采用默认值1。scale采用m*2^(-n)表达，其中m的数值范围[0,255]，n的数值范围[bits, 2*bits]。

如果不是深度可分离卷积，则前向使用MPE+VPE实现，w_scale/b_scale表示MPE中的第一次截位，
b_scale表示VPE中的第二次截位。假定w_scale=a*2^(-b)，b_scale=m*2^(-n)，在优先将w_scale的浮点值近似为a和b后，需保证
a = m、0 <= n-b <= 5以及bits <= n <= 2*bits得到b_scale的近似。

需注意的是，如果是按通道量化，由于硬件的限制，MPE的不同输出通道的截位需相等，因此会在前述截位信息的基础上，
再次调整各通道的w_scale和b_scale使得第一次截位相等。

* ``Conv2dTranspose``

实现参考Conv2d

* ``Elu`` ``Gelu`` ``Hardsigmoid`` ``Hardswish``

只需统计输出的normratio，交给后端制作查找表

* ``Input``
* ``Layernorm``

只完成了统计输出normratio，交由硬算子。
后续针对该算子的优化可能实现特征图的按通道统计normratio

* ``Matmul``

**matmul_ftmp**：
参考conv_ftmp算子中非深度可分离卷积情况下的实现方式

**matmul_const**：
参考conv_const算子中非深度可分离卷积情况下的实现方式，但该算子暂不支持按通道量化

* ``Maxpool`` 

直接把输入normratio传递给输出normratio

* ``Mish``

只需统计输出的normratio，交给后端制作查找表

* ``Multiply``

**mul_ftmp**：
参考conv_ftmp算子中深度可分离卷积情况下的实现方式

**mul_const**：
参考conv_const算子中深度可分离卷积情况下的实现方式，但该算子暂不支持按通道量化

* ``MultiYolo``
* ``Output``
* ``Pad``

在归一化时，输入的normratio直接传递给输出，同时pad_value使用该normratio进行归一化操作。
针对归一化后的pad_value，将其转换为Int值并做饱和操作。

* ``PixelShuffle``

只需统计输出的normratio

* ``Region``
* ``Relu``

在量化中没有特殊操作，但在指令生成中需要获取前一个算子的b_scale用在正半轴，将该b_scale结合alpha系数
用在负半轴。

* ``Reorg``

只需统计输出的normratio

* ``Reshape``
* ``Resize``
* ``Route``

* ``Sigmoid``

待补充关于减小查找表误差的内容

* ``Silu``

只需统计输出的normratio

* ``Slice``
* ``Softmax``

* ``Split``

如果针对最后一维split，则输出的normratio也根据输入的normratio进行Split
如果针对的不是最后一维，则直接传递输入的normratio给输出。

* ``Squeeze``
* ``SSDOutput``
* ``SwapOrder``

* ``Tanh``

待补充关于减小查找表误差的内容

* ``Transpose``
* ``Upsample``
* ``Yolo``

六、索引
=========

* :ref:`genindex`