.. _exhale_class_classicraft_1_1qt_1_1_quantizer_logger:

Class QuantizerLogger
=====================

- Defined in :ref:`file_icraft-qt_core_quantizer_logger.h`


Nested Relationships
--------------------


Nested Types
************

- :ref:`exhale_struct_structicraft_1_1qt_1_1_quantizer_logger_1_1_ftmp_info`
- :ref:`exhale_struct_structicraft_1_1qt_1_1_quantizer_logger_1_1_raw_info`


Class Documentation
-------------------


.. doxygenclass:: icraft::qt::QuantizerLogger
   :project: Icraft Quantizer
   :members:
   :protected-members:
   :undoc-members: