.. _exhale_typedef_quantizer__utils_8h_1adeea4e0cf55e08c21c764f9458c9e3dd:

Typedef GetDll
==============

- Defined in :ref:`file_icraft-qt_core_quantizer_utils.h`


Typedef Documentation
---------------------


.. doxygentypedef:: GetDll
   :project: Icraft Quantizer