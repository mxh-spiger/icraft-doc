
.. _file_icraft-qt_core_quantizer_options.h:

File quantizer_options.h
========================

|exhale_lsh| :ref:`Parent directory <dir_icraft-qt_core>` (``icraft-qt\core``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-qt\core\quantizer_options.h``)
---------------------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-qt_core_quantizer_options.h.rst





Includes
--------


- ``icraft-qt/core/dllexport.h``

- ``icraft-qt/utils/enum.h``

- ``string``



Included By
-----------


- :ref:`file_icraft-qt_core_quantizer.h`




Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__qt`


Classes
-------


- :ref:`exhale_class_classicraft_1_1qt_1_1_quantizer_options`

