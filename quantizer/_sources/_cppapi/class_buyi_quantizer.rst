.. _exhale_class_class_buyi_quantizer:

Class BuyiQuantizer
===================

- Defined in :ref:`file_icraft-qt_buyi_buyi_quantizer.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public QuantizerAdapt`` (:ref:`exhale_class_class_quantizer_adapt`)


Class Documentation
-------------------


.. doxygenclass:: BuyiQuantizer
   :project: Icraft Quantizer
   :members:
   :protected-members:
   :undoc-members: