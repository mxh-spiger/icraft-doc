
.. _file_icraft-qt_core_quantizer.h:

File quantizer.h
================

|exhale_lsh| :ref:`Parent directory <dir_icraft-qt_core>` (``icraft-qt\core``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-qt\core\quantizer.h``)
-------------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-qt_core_quantizer.h.rst





Includes
--------


- ``icraft-qt/core/dllexport.h``

- ``icraft-qt/core/quantizer_adapt.h``

- ``icraft-qt/core/quantizer_logger.h``

- ``icraft-qt/core/quantizer_options.h``

- ``icraft-qt/core/quantizer_tool.h``

- ``icraft-utils/logging.h``

- ``icraft-xir/core/network.h``

- ``icraft-xir/core/operation.h``

- ``icraft-xir/core/pattern.h``



Included By
-----------


- :ref:`file_icraft-qt_buyi_buyi_adapt.h`




Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__qt`


Classes
-------


- :ref:`exhale_class_classicraft_1_1qt_1_1_quantizer`

