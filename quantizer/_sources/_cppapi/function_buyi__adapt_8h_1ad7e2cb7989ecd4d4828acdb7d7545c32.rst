.. _exhale_function_buyi__adapt_8h_1ad7e2cb7989ecd4d4828acdb7d7545c32:

Function adaptDepthScale
========================

- Defined in :ref:`file_icraft-qt_buyi_buyi_adapt.h`


Function Documentation
----------------------


.. doxygenfunction:: adaptDepthScale(std::vector<double>, std::vector<double>, int, std::string, icraft::qt::QuantizerLogger&)
   :project: Icraft Quantizer