
.. _file_icraft-qt_buyi_buyi_quantizer.h:

File buyi_quantizer.h
=====================

|exhale_lsh| :ref:`Parent directory <dir_icraft-qt_buyi>` (``icraft-qt\buyi``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-qt\buyi\buyi_quantizer.h``)
------------------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-qt_buyi_buyi_quantizer.h.rst





Includes
--------


- ``icraft-qt/core/dllexport.h``

- ``icraft-qt/core/quantizer_adapt.h``

- ``icraft-xir/core/functor.h``






Classes
-------


- :ref:`exhale_class_class_buyi_check`

- :ref:`exhale_class_class_buyi_normalize`

- :ref:`exhale_class_class_buyi_normratio`

- :ref:`exhale_class_class_buyi_quantize`

- :ref:`exhale_class_class_buyi_quantizer`

