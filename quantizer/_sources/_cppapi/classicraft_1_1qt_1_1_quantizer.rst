.. _exhale_class_classicraft_1_1qt_1_1_quantizer:

Class Quantizer
===============

- Defined in :ref:`file_icraft-qt_core_quantizer.h`


Class Documentation
-------------------


.. doxygenclass:: icraft::qt::Quantizer
   :project: Icraft Quantizer
   :members:
   :protected-members:
   :undoc-members: