
.. _file_icraft-qt_core_quantizer_tool.h:

File quantizer_tool.h
=====================

|exhale_lsh| :ref:`Parent directory <dir_icraft-qt_core>` (``icraft-qt\core``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-qt\core\quantizer_tool.h``)
------------------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-qt_core_quantizer_tool.h.rst





Includes
--------


- ``icraft-xir/core/network.h``

- ``icraft-xir/ops/conv2d.h``

- ``string``



Included By
-----------


- :ref:`file_icraft-qt_core_quantizer.h`




Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__qt`


Classes
-------


- :ref:`exhale_struct_structicraft_1_1qt_1_1_quantizer_tool_1_1_config_info`

- :ref:`exhale_class_classicraft_1_1qt_1_1_quantizer_tool`

