.. _exhale_define_dllexport_8h_1a05ff738fe4c81e0087ac8cc2b1190cf5:

Define BQ_API
=============

- Defined in :ref:`file_icraft-qt_core_dllexport.h`


Define Documentation
--------------------


.. doxygendefine:: BQ_API
   :project: Icraft Quantizer