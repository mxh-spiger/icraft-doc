.. _exhale_define_quantizer__adapt_8h_1af0105fc1fb2c3790dd407c1be77c31f7:

Define REGISTER_QUANTIZER
=========================

- Defined in :ref:`file_icraft-qt_core_quantizer_adapt.h`


Define Documentation
--------------------


.. doxygendefine:: REGISTER_QUANTIZER
   :project: Icraft Quantizer