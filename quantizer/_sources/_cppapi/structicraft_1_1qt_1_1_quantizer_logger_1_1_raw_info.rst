.. _exhale_struct_structicraft_1_1qt_1_1_quantizer_logger_1_1_raw_info:

Struct QuantizerLogger::RawInfo
===============================

- Defined in :ref:`file_icraft-qt_core_quantizer_logger.h`


Nested Relationships
--------------------

This struct is a nested type of :ref:`exhale_class_classicraft_1_1qt_1_1_quantizer_logger`.


Struct Documentation
--------------------


.. doxygenstruct:: icraft::qt::QuantizerLogger::RawInfo
   :project: Icraft Quantizer
   :members:
   :protected-members:
   :undoc-members: