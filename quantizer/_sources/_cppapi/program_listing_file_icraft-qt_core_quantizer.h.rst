
.. _program_listing_file_icraft-qt_core_quantizer.h:

Program Listing for File quantizer.h
====================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-qt_core_quantizer.h>` (``icraft-qt\core\quantizer.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/operation.h>
   #include <icraft-xir/core/network.h>
   #include <icraft-xir/core/pattern.h>
   #include <icraft-utils/logging.h>
   #include <icraft-qt/core/quantizer_logger.h>
   #include <icraft-qt/core/quantizer_options.h>
   #include <icraft-qt/core/quantizer_tool.h>
   #include <icraft-qt/core/quantizer_adapt.h>
   #include <icraft-qt/core/dllexport.h>
   
   namespace icraft::qt {
       class QT_API Quantizer {
       public:
           using FtmpMap = std::unordered_map<int, std::pair<std::vector<int>, float*>>;
   
           Quantizer(const QuantizerOptions& options, const icraft::xir::Network& network) {
               setOptions(options);            
   
               auto network_log_path = std::filesystem::path(options.log_path) / network->name;
               if (!std::filesystem::exists(network_log_path)) {
                   std::filesystem::create_directories(network_log_path);
               }
               auto log_file = network_log_path / (network->name + "_quantize.log");
               auto logger_sink = icraft::FileSink(log_file);
               auto debug_info = icraft::Logger("quantize_log", logger_sink);
               logger_.setDebugInfo(debug_info);
               logger_.setLogPath(network_log_path, network->name);
               if (!std::filesystem::exists(options.jr_path)) {
                   std::filesystem::create_directories(options.jr_path);
               }
           };
           const std::string getDllPath();
           void loadDll(const std::string path);
           QuantizerAdapt* getQuantizerAdapt();
           static void setOptions(const QuantizerOptions& options) { options_ = options; };
           static QuantizerOptions& getOptions() { return options_; };
           void setDtype(const icraft::xir::Network& network, std::string mix_precision);
           QuantizerLogger& getLogger() { return logger_; };
   
           void transInput(icraft::xir::Network& network);
           void castInNetwork(icraft::xir::Network& network, std::map<int, std::string> dtype);
           void patchOnAct(icraft::xir::Network& network);
   
           void insertCast(int& op_index, icraft::xir::Network& network, const icraft::xir::Value& iftmp, const icraft::xir::Operation& op, int index, 
               std::string i_type, std::string o_type, std::vector<int> shape, icraft::xir::Layout layout, std::string processor);
   
           void calibrate(icraft::xir::Network& network);
           void calibrateByFtmp(icraft::xir::Network& network);
           void calibrateByForward(icraft::xir::Network& network);
           void calcNormratio(icraft::xir::Network& network);
           void normalize(icraft::xir::Network& network);
           void quantize(icraft::xir::Network& network);
           void check(icraft::xir::Network& network);
   
           void measure(FtmpMap& ftmp_map);
           void measureOnChannel(Quantizer::FtmpMap& ftmp_map);
           FtmpMap loadFtmp(icraft::xir::Operation op, std::vector<std::string> file_list);        
           static std::vector<double> getScale(std::vector<float> sat, int bits);
           static std::vector<float> getBiasSat(const icraft::xir::Operation& operation);
           static std::pair<int, int> adjustScale(double scale, int bit, int shift_min, int shift_max);
           static std::pair<int, int> adjustScaleC(double scale, int base, int shift_min, int shift_max);
           static void quantizeFtmp(const icraft::xir::Operation& operation);
           static void calcNormratio(const icraft::xir::Operation& operation);
           static void passNormratio(const icraft::xir::Operation& operation);
           static void catNormratio(const icraft::xir::Operation& operation, int axis);
           static void reshapeNormratio(const icraft::xir::Operation& operation);
           //calcBNormratio´ýÐÞ¸Ä
           static void calcBNormratio(const icraft::xir::Operation& operation, bool has_bias);
   
           static std::pair<std::vector<float>, std::vector<float>> measureWeight(
               const icraft::xir::Value& weight, std::string per, const int& axis);
           static void quantizeParams(
               icraft::xir::Value& weight, icraft::xir::Value& bias,
               int bits, std::vector<std::pair<int, int>> h_w_scale, std::vector<std::pair<int, int>> h_b_scale,
               std::vector<double> w_scale, std::vector<double> b_scale);
           static void quantizeParams(icraft::xir::Value& weight, int bits, 
               std::vector<std::pair<int, int>> h_w_scale, std::vector<double> w_scale);
           static void* quantize(float* data_ptr, size_t size, int bits, int co, std::vector<double> scale);
   
           static icraft::xir::TensorType makeQDtype(icraft::xir::Array<icraft::xir::IntImm> shape, icraft::xir::Layout layout, int bits,
               std::vector<std::pair<int, int>> hard_scale, std::vector<double> scale);
           static icraft::xir::TensorType makeQDtype(icraft::xir::Array<icraft::xir::IntImm> shape, icraft::xir::Layout layout, int bits,
               std::vector<std::pair<int, int>> hard_scale, std::vector<double> scale, std::vector<float> normratio);
           void dumpJR(icraft::xir::Network& network, std::string& jr_path, std::string stage);
           void dumpFtmpLogFile();
           void dumpRawLogFile();
   
           inline static std::map<int, std::string> dtype_;
           inline static std::map<int, int> dtype_value_;
   
           inline static QuantizerLogger logger_;
           inline static QuantizerOptions options_;
           inline static QuantizerTool quantizertool_;
   
       protected:
           
           inline static std::vector<std::string> file_list_;
       private:
           void* dll_handler_;
           QuantizerAdapt* adapt_handler_;
   
           using ObserveFunction = std::function<std::pair<float, float>(const size_t&, const float*)>;
           inline static constexpr auto FTMP_SUFFIX = ".ftmp";
           static ObserveFunction getObserveFunction(const QuantizerOptions::Saturation& saturation);
   
           static void quantize_x(std::vector<float>& P, const int QUANTIZE_SIZE);
           static int kld_min(const std::vector<float>& P, const int QUANTIZE_SIZE, const int BINS_NUMBER);
           static float quant_8(const size_t& ftmp_size, const float* ftmp_data, const float& scale);
           static std::pair<float, float> absKld(const size_t& ftmp_size, const float* ftmp_data);
           static std::pair<float, float> absMinMax(const size_t& ftmp_size, const float* ftmp_data);
           static std::pair<float, float> absEms(const size_t& ftmp_size, const float* ftmp_data);
           static std::pair<float, float> quickEms(const size_t& ftmp_size, const float* ftmp_data);
           static std::pair<float, float> threeEms(const size_t& ftmp_size, const float* ftmp_data);
           static std::pair<float, float> dEms(const size_t& ftmp_size, const float* ftmp_data);
           static std::pair<float, float> nSigma(const size_t& ftmp_size, const float* ftmp_data);
       };
   }
