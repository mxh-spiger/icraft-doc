.. _exhale_class_classicraft_1_1qt_1_1_quantizer_tool:

Class QuantizerTool
===================

- Defined in :ref:`file_icraft-qt_core_quantizer_tool.h`


Nested Relationships
--------------------


Nested Types
************

- :ref:`exhale_struct_structicraft_1_1qt_1_1_quantizer_tool_1_1_config_info`


Class Documentation
-------------------


.. doxygenclass:: icraft::qt::QuantizerTool
   :project: Icraft Quantizer
   :members:
   :protected-members:
   :undoc-members: