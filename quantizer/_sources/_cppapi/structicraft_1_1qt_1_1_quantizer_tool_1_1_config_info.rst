.. _exhale_struct_structicraft_1_1qt_1_1_quantizer_tool_1_1_config_info:

Struct QuantizerTool::ConfigInfo
================================

- Defined in :ref:`file_icraft-qt_core_quantizer_tool.h`


Nested Relationships
--------------------

This struct is a nested type of :ref:`exhale_class_classicraft_1_1qt_1_1_quantizer_tool`.


Struct Documentation
--------------------


.. doxygenstruct:: icraft::qt::QuantizerTool::ConfigInfo
   :project: Icraft Quantizer
   :members:
   :protected-members:
   :undoc-members: