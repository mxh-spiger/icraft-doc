
.. _program_listing_file_icraft-qt_core_quantizer_tool.h:

Program Listing for File quantizer_tool.h
=========================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-qt_core_quantizer_tool.h>` (``icraft-qt\core\quantizer_tool.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <string>
   #include <icraft-xir/core/network.h>
   #include <icraft-xir/ops/conv2d.h>
   
   
   namespace icraft::qt {
       class QuantizerTool {
       public:
           using FtmpMap = std::unordered_map<int, std::pair<std::vector<int>, float*>>;
   
           void AutoMixCal(FtmpMap& ftmp, const icraft::xir::Operation& op);
   
           void AutoMixDecide(icraft::xir::Network& network, std::filesystem::path& log_path);
   
           void SetOpid(const int& op_id) { mix_config[op_id].op_id = op_id; };
           void SetOpType(const int& op_id, const std::string& optype) { mix_config[op_id].op_type = optype; };
           void SetVariance(const int& op_id, const float& variance) { mix_config[op_id].variance = variance; };
           void SetAmount(const int& op_id, const float& cal_amount) { mix_config[op_id].cal_amount = cal_amount; };
           void SetDtype(const int& op_id, const std::string& dtype) { mix_config[op_id].dtype = dtype; };
   
           float GetAmount(const int& op_id) { return mix_config[op_id].cal_amount; };
           float GetVariance(const int& op_id) { return mix_config[op_id].variance; };
           std::string GetDtype(const int& op_id) { return mix_config[op_id].dtype; };
   
           void dumpConfigSheet(std::string& network_name, std::filesystem::path& path);
   
       private:
           void CalVariance(FtmpMap& ftmp, const icraft::xir::Operation& op);
   
           void CalAmount(const icraft::xir::Operation& op);
   
           void DecideByVariance(icraft::xir::Network& network);
   
           bool ChangeDtype(icraft::xir::Operation op);
   
           inline static constexpr auto CONFIG_SHEET_HEADER = std::array<const char*, 5>{
                   "op_id",
                   "op_type",
                   "dtype",
                   "variance",
                   "cal_amount_percent"
           };
   
           inline static constexpr auto CONFIG_CSV_SUFFIX = "_mix_config.csv";
   
           struct ConfigInfo {
               int op_id;
               std::string op_type;
               std::string dtype;
               float variance;
               float cal_amount;
   
               operator std::string() const;
           };
   
           std::map<int, ConfigInfo> mix_config;
   
           std::vector<std::string> activate_function{ "icraft::xir::SiluNode", "icraft::xir::ReluNode", 
               "icraft::xir::EluNode", "icraft::xir::GeluNode", "icraft::xir::HardsigmoidNode", "icraft::xir::HardswishNode", 
               "icraft::xir::MishNode", "icraft::xir::PreluNode", "icraft::xir::SigmoidNode", "icraft::xir::TanhNode"};
           std::vector<std::string> multi_inputs_function{ "icraft::xir::ConcatNode", "icraft::xir::AddNode", 
               "icraft::xir::MultiplyNode" };
   
           std::string higt_bit = "INT16";
           std::string low_bit = "INT8";
   
           float cal_threshold;
       };
   }
