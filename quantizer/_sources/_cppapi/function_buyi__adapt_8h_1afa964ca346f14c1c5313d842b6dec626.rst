.. _exhale_function_buyi__adapt_8h_1afa964ca346f14c1c5313d842b6dec626:

Function adaptChannelScale
==========================

- Defined in :ref:`file_icraft-qt_buyi_buyi_adapt.h`


Function Documentation
----------------------


.. doxygenfunction:: adaptChannelScale(std::vector<std::pair<int, int>>&, std::vector<std::pair<int, int>>&, std::vector<int>, int, int, icraft::qt::QuantizerLogger&)
   :project: Icraft Quantizer