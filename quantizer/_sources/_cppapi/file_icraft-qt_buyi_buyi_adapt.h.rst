
.. _file_icraft-qt_buyi_buyi_adapt.h:

File buyi_adapt.h
=================

|exhale_lsh| :ref:`Parent directory <dir_icraft-qt_buyi>` (``icraft-qt\buyi``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-qt\buyi\buyi_adapt.h``)
--------------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-qt_buyi_buyi_adapt.h.rst





Includes
--------


- ``icraft-qt/core/quantizer.h``

- ``icraft-qt/core/quantizer_logger.h``

- ``string``

- ``tuple``

- ``vector``






Functions
---------


- :ref:`exhale_function_buyi__adapt_8h_1afa964ca346f14c1c5313d842b6dec626`

- :ref:`exhale_function_buyi__adapt_8h_1ad7e2cb7989ecd4d4828acdb7d7545c32`

- :ref:`exhale_function_buyi__adapt_8h_1a266d79d882434574966edfa4c067d893`

