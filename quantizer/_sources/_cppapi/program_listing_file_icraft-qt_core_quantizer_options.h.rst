
.. _program_listing_file_icraft-qt_core_quantizer_options.h:

Program Listing for File quantizer_options.h
============================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-qt_core_quantizer_options.h>` (``icraft-qt\core\quantizer_options.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <string>
   #include <icraft-qt/utils/enum.h>
   #include <icraft-qt/core/dllexport.h>
   
   namespace icraft::qt {
       class QT_API QuantizerOptions {
       public:
           ENUM_HPP_CLASS_DECL(ForwardMode, int,
               (ftmp)
               (image)
           )
   
               ENUM_HPP_CLASS_DECL(Saturation, int,
                   (null)
                   (kld)
                   (ems)
                   (nsigma)
               )
   
               ENUM_HPP_CLASS_DECL(Per, int,
                   (tensor)
                   (channel)
               )
   
               /*ENUM_HPP_CLASS_DECL(Target, int,
                   (QM)
                   (JDY)
                   (BUYI)
               )*/
   
               ForwardMode forward_mode;
           bool before_relu = false;
           bool no_transinput = false;
           bool no_normalize = false;
           bool debug = false;
           bool precheck = false;
           bool auto_mix = false;
           std::string forward_dir;
           std::string forward_list;
           std::string names_file;
           std::string ftmp_csv;
           std::string raw_csv;
           std::string decode_dll;
           std::string input_norm;
           std::string target;
           std::string mix_precision;
           std::string log_path;
           std::string jr_path;
           Saturation saturation;
           Per per;
           int axis = 0;
           int bits;
           int batch;
           int bin_num = 4096;
           int n;
       };
   }
