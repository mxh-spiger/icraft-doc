
.. _program_listing_file_icraft-qt_core_dllexport.h:

Program Listing for File dllexport.h
====================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-qt_core_dllexport.h>` (``icraft-qt\core\dllexport.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   
   #ifdef _WIN32
   #ifdef QD_DLL_EXPORTS
   #define QT_API _declspec(dllexport)
   #else
   #define QT_API _declspec(dllimport)
   #endif
   #else
   #define QT_API
   #endif
   
   #ifdef _WIN32
   #ifdef BQ_DLL_EXPORTS
   #define BQ_API _declspec(dllexport)
   #else
   #define BQ_API _declspec(dllimport)
   #endif
   #else
   #define BQ_API
   #endif
