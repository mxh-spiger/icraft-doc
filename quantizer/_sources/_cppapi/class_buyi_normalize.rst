.. _exhale_class_class_buyi_normalize:

Class BuyiNormalize
===================

- Defined in :ref:`file_icraft-qt_buyi_buyi_quantizer.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::OpTraitFunctor< BuyiNormalize, void(const icraft::xir::Operation &, icraft::qt::QuantizerLogger &logger)>``


Class Documentation
-------------------


.. doxygenclass:: BuyiNormalize
   :project: Icraft Quantizer
   :members:
   :protected-members:
   :undoc-members: