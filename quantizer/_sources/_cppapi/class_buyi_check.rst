.. _exhale_class_class_buyi_check:

Class BuyiCheck
===============

- Defined in :ref:`file_icraft-qt_buyi_buyi_quantizer.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::OpTraitFunctor< BuyiCheck, void(const icraft::xir::Operation &, icraft::qt::QuantizerLogger &logger)>``


Class Documentation
-------------------


.. doxygenclass:: BuyiCheck
   :project: Icraft Quantizer
   :members:
   :protected-members:
   :undoc-members: