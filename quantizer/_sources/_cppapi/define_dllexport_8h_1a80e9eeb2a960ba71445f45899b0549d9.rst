.. _exhale_define_dllexport_8h_1a80e9eeb2a960ba71445f45899b0549d9:

Define QT_API
=============

- Defined in :ref:`file_icraft-qt_core_dllexport.h`


Define Documentation
--------------------


.. doxygendefine:: QT_API
   :project: Icraft Quantizer