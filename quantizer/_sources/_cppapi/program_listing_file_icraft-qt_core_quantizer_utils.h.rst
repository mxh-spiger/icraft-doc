
.. _program_listing_file_icraft-qt_core_quantizer_utils.h:

Program Listing for File quantizer_utils.h
==========================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-qt_core_quantizer_utils.h>` (``icraft-qt\core\quantizer_utils.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <string>
   #include <iostream>
   #include <fstream>
   
   #ifdef _WIN32
   #include <windows.h>
   #else
   #include <dlfcn.h>
   #endif
   
   typedef QuantizerAdapt* (*GetDll)();
   
   namespace icraft::qt{
       class QuantizerUtils {
       public:
           template <typename T1, typename T2>
           static void transposeNhwc2cnhw(const T1* src, T2* des, const std::vector<int> dim) {
               if (dim.size() != 4) {
                   throw("ftmp error\n");
               }
               else {
                   int index_src;
                   int index_des;
                   int kn = dim[0];
                   int kh = dim[1];
                   int kw = dim[2];
                   int kc = dim[3];
                   for (int in = 0; in < kn; in++) {
                       for (int ih = 0; ih < kh; ih++) {
                           for (int iw = 0; iw < kw; iw++) {
                               for (int ic = 0; ic < kc; ic++) {
                                   index_src = in * kh * kw * kc + ih * kw * kc + iw * kc + ic;
                                   index_des = ic * kn * kh * kw + in * kh * kw + ih * kw + iw;
                                   //std::cout << "index_src: " << index_src << "index_des: " << index_des << std::endl;
                                   des[index_des] = src[index_src];
                               }
                           }
                       }
                   }
               }
           }
   
           template <typename T1, typename T2>
           static void transposeHwoi2oihw(const T1* src, T2* des, const std::vector<int> dim) {
               if (dim.size() != 4) {
                   throw("weights error\n");
               }
               else {
                   int index_src;
                   int index_des;
                   int kh = dim[0];
                   int kw = dim[1];
                   int ko = dim[2];
                   int ki = dim[3];
                   for (int ih = 0; ih < kh; ih++) {
                       for (int iw = 0; iw < kw; iw++) {
                           for (int io = 0; io < ko; io++) {
                               for (int ii = 0; ii < ki; ii++) {
                                   index_src = ih * kw * ko * ki + iw * ko * ki + io * ki + ii;
                                   index_des = io * ki * kw * kh + ii * kw * kh + ih * kw + iw;
                                   des[index_des] = src[index_src];
                               }
                           }
                       }
                   }
               }
           }
   
           template <typename T1, typename T2>
           static void transposeHwio2oihw(const T1* src, T2* des, const std::vector<int> dim) {
               if (dim.size() != 4) {
                   throw("weights error\n");
               }
               else {
                   int index_src;
                   int index_des;
                   int kh = dim[0];
                   int kw = dim[1];
                   int ki = dim[2];
                   int ko = dim[3];
                   for (int ih = 0; ih < kh; ih++) {
                       for (int iw = 0; iw < kw; iw++) {
                           for (int ii = 0; ii < ki; ii++) {
                               for (int io = 0; io < ko; io++) {
                                   index_src = ih * kw * ki * ko + iw * ki * ko + ii * ko + io;
                                   index_des = io * ki * kw * kh + ii * kw * kh + ih * kw + iw;
                                   des[index_des] = src[index_src];
                               }
                           }
                       }
                   }
               }
           }
   
           template <typename T1, typename T2>
           static void transposeOihw2hwoi(const T1* src, T2* des, const std::vector<int64_t> dim) {
               if (dim.size() != 4) {
                   throw("weights error\n");
               }
               else {
                   int index_src;
                   int index_des;
                   int ko = dim[0];
                   int ki = dim[1];
                   int kh = dim[2];
                   int kw = dim[3];
                   for (int io = 0; io < ko; io++) {
                       for (int ii = 0; ii < ki; ii++) {
                           for (int ih = 0; ih < kh; ih++) {
                               for (int iw = 0; iw < kw; iw++) {
                                   index_src = io * ki * kw * kh + ii * kw * kh + ih * kw + iw;
                                   index_des = ih * kw * ki * ko + iw * ki * ko + io * ki + ii;
                                   des[index_des] = src[index_src];
                               }
                           }
                       }
                   }
               }
           }
   
           template <typename T1, typename T2>
           static void transposeOihw2hwio(const T1* src, T2* des, const std::vector<int64_t> dim) {
               if (dim.size() != 4) {
                   throw("weights error\n");
               }
               else {
                   int index_src;
                   int index_des;
                   int ko = dim[0];
                   int ki = dim[1];
                   int kw = dim[2];
                   int kh = dim[3];
                   for (int io = 0; io < ko; io++) {
                       for (int ii = 0; ii < ki; ii++) {
                           for (int ih = 0; ih < kh; ih++) {
                               for (int iw = 0; iw < kw; iw++) {
                                   index_src = io * ki * kw * kh + ii * kw * kh + ih * kw + iw;
                                   index_des = ih * kw * ki * ko + iw * ki * ko + ii * ko + io;
                                   des[index_des] = src[index_src];
                               }
                           }
                       }
                   }
               }
           }
   
           static int fileSize(std::ifstream& file_stream) {
               file_stream.seekg(0, file_stream.end);
               auto file_size = file_stream.tellg();
               file_stream.seekg(0, file_stream.beg);
               return file_size;
           }
   
           static bool fileExists(const std::string& filename) {
               struct stat buf;
               if (stat(filename.data(), &buf) != -1) {
                   return true;
               }
               return false;
           }
   
           static void* loadDLLByPath(const std::string& path) {
               if (fileExists(path)) {
   #ifdef _WIN32
                   auto h = LoadLibraryA(path.c_str());
                   if (h == NULL) {
                       std::cerr << "load dll " << path << " error: " << std::to_string(GetLastError()) << std::endl;
                       //throw Error("Load DLL \"" + path + "\" error: " + std::to_string(GetLastError()));
                   }
                   return h;
   #else
                   auto h = dlopen(path.data(), RTLD_LAZY);
                   if (h == NULL) {
                       throw Error("Load SO \"" + path + "\" error: " + dlerror());
                   }
                   return h;
   
   #endif
               }
               //else throw FileNotExistError(path);
               else {
                   std::cerr << path << " not exit error!" << std::endl;
                   exit(-1);
               }
           }
   
           static QuantizerAdapt* getQuantizer(void* h) {
   #ifdef _WIN32
               if (h == NULL) {
                   std::cerr << "DLL is not loaded!" << std::endl;
                   //throw Error("DLL is not loaded!");
               }
               else {
                   auto getQuantizer = (GetDll)GetProcAddress((HINSTANCE)h, "GetQuantizer");
                   if (getQuantizer != NULL) {
                       return getQuantizer();
                   }
                   std::cerr << "Get process 'GetQuantizer' error!" << std::endl;
                   //throw Error("Get process \"GetUserCustom\" error: " + std::to_string(GetLastError()));
               }
   #else
               if (h == NULL) {
                   throw Error("SO is not loaded!");
               }
               else {
                   dlerror();
                   auto getUserCustom = (GetUserCustom)dlsym(h, "GetUserCustom");
                   char* error;
                   if ((error = dlerror()) == NULL) {
                       return getUserCustom(class_name.data());
                   }
                   throw Error(std::string("Get process \"GetUserCustom\" error: \n") + error);
               }
   #endif
           }
       };
   }
   
