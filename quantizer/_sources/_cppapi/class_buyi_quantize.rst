.. _exhale_class_class_buyi_quantize:

Class BuyiQuantize
==================

- Defined in :ref:`file_icraft-qt_buyi_buyi_quantizer.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::OpTraitFunctor< BuyiQuantize, void(const icraft::xir::Operation &, icraft::qt::QuantizerLogger &logger)>``


Class Documentation
-------------------


.. doxygenclass:: BuyiQuantize
   :project: Icraft Quantizer
   :members:
   :protected-members:
   :undoc-members: