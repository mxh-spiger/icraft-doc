
.. _program_listing_file_icraft-qt_buyi_buyi_adapt.h:

Program Listing for File buyi_adapt.h
=====================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-qt_buyi_buyi_adapt.h>` (``icraft-qt\buyi\buyi_adapt.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <tuple>
   #include <vector>
   #include <string>
   #include <icraft-qt/core/quantizer.h>
   #include <icraft-qt/core/quantizer_logger.h>
   
   std::tuple<std::vector<std::pair<int, int>>, std::vector<std::pair<int, int>>, std::vector<int>, int>
   adaptDepthScale(std::vector<double> w_scale, std::vector<double> b_scale, int bits, std::string per,
       icraft::qt::QuantizerLogger& logger);
   
   std::tuple<std::vector<std::pair<int, int>>, std::vector<std::pair<int, int>>, std::vector<int>, int>
   adaptScale(std::vector<double> w_scale, std::vector<double> b_scale, int bits, std::string per,
       icraft::qt::QuantizerLogger& logger);
   
   void adaptChannelScale(std::vector<std::pair<int, int>>& h_w_scale, std::vector<std::pair<int, int>>& h_b_scale,
       std::vector<int> exp_c, int exp_max, int bits, icraft::qt::QuantizerLogger& logger);
