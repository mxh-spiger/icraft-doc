
.. _program_listing_file_icraft-qt_buyi_buyi_quantizer.h:

Program Listing for File buyi_quantizer.h
=========================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-qt_buyi_buyi_quantizer.h>` (``icraft-qt\buyi\buyi_quantizer.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/functor.h>
   #include <icraft-qt/core/quantizer_adapt.h>
   #include <icraft-qt/core/dllexport.h>
   
   class BuyiQuantizer : public QuantizerAdapt {
   public:
       virtual void normratio(icraft::xir::Network& network, icraft::qt::QuantizerLogger& logger) override;
       virtual void normalize(icraft::xir::Network& network, icraft::qt::QuantizerLogger& logger) override;
       virtual void quantize(icraft::xir::Network& network, icraft::qt::QuantizerLogger& logger) override;
       virtual void check(icraft::xir::Network& network, icraft::qt::QuantizerLogger& logger) override;
   };
   
   class BuyiNormratio : public icraft::xir::OpTraitFunctor <BuyiNormratio, void(const icraft::xir::Operation&,
       icraft::qt::QuantizerLogger& logger)> {
   public:
       template <typename T>
       explicit BuyiNormratio(T t) : OpTraitFunctor(std::move(t)) {}
   
   protected:
       BQ_API static FunctorType& VTable();
   };
   
   class BuyiNormalize : public icraft::xir::OpTraitFunctor <BuyiNormalize, void(const icraft::xir::Operation&,
       icraft::qt::QuantizerLogger& logger)> {
   public:
       template <typename T>
       explicit BuyiNormalize(T t) : OpTraitFunctor(std::move(t)) {}
   
   protected:
       BQ_API static FunctorType& VTable();
   };
   
   class BuyiQuantize : public icraft::xir::OpTraitFunctor <BuyiQuantize, void(const icraft::xir::Operation&,
       icraft::qt::QuantizerLogger& logger)> {
   public:
       template <typename T>
       explicit BuyiQuantize(T t) : OpTraitFunctor(std::move(t)) {}
   
   protected:
       BQ_API static FunctorType& VTable();
   };
   
   class BuyiCheck : public icraft::xir::OpTraitFunctor <BuyiCheck, void(const icraft::xir::Operation&,
       icraft::qt::QuantizerLogger& logger)> {
   public:
       template <typename T>
       explicit BuyiCheck(T t) : OpTraitFunctor(std::move(t)) {}
   
   protected:
       BQ_API static FunctorType& VTable();
   };
