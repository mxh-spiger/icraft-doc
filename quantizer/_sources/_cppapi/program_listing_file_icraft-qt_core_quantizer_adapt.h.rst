
.. _program_listing_file_icraft-qt_core_quantizer_adapt.h:

Program Listing for File quantizer_adapt.h
==========================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-qt_core_quantizer_adapt.h>` (``icraft-qt\core\quantizer_adapt.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <vector>
   #include <icraft-xir/core/network.h>
   #include <icraft-qt/core/quantizer_logger.h>
   #include <icraft-qt/core/dllexport.h>
   
   class QT_API QuantizerAdapt {
   public:
       virtual void normratio(icraft::xir::Network& network, icraft::qt::QuantizerLogger& logger) = 0;
       virtual void normalize(icraft::xir::Network& network, icraft::qt::QuantizerLogger& logger) = 0;
       virtual void quantize(icraft::xir::Network& network, icraft::qt::QuantizerLogger& logger) = 0;
       virtual void check(icraft::xir::Network& network, icraft::qt::QuantizerLogger& logger) = 0;
   };
   
   extern "C" QT_API QuantizerAdapt * GetQuantizer();
   
   #define REGISTER_QUANTIZER(CLASS_NAME) \
   QuantizerAdapt* GetQuantizer(){         \
       return new CLASS_NAME();            \
   }
