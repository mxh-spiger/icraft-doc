
.. _file_icraft-qt_core_quantizer_adapt.h:

File quantizer_adapt.h
======================

|exhale_lsh| :ref:`Parent directory <dir_icraft-qt_core>` (``icraft-qt\core``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-qt\core\quantizer_adapt.h``)
-------------------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-qt_core_quantizer_adapt.h.rst





Includes
--------


- ``icraft-qt/core/dllexport.h``

- ``icraft-qt/core/quantizer_logger.h``

- ``icraft-xir/core/network.h``

- ``vector``



Included By
-----------


- :ref:`file_icraft-qt_buyi_buyi_quantizer.h`

- :ref:`file_icraft-qt_core_quantizer.h`




Classes
-------


- :ref:`exhale_class_class_quantizer_adapt`


Functions
---------


- :ref:`exhale_function_quantizer__adapt_8h_1ac29b91fb3dbbcad5266b4956bb40a793`


Defines
-------


- :ref:`exhale_define_quantizer__adapt_8h_1af0105fc1fb2c3790dd407c1be77c31f7`

