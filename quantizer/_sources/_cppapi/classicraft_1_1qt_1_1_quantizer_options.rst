.. _exhale_class_classicraft_1_1qt_1_1_quantizer_options:

Class QuantizerOptions
======================

- Defined in :ref:`file_icraft-qt_core_quantizer_options.h`


Class Documentation
-------------------


.. doxygenclass:: icraft::qt::QuantizerOptions
   :project: Icraft Quantizer
   :members:
   :protected-members:
   :undoc-members: