.. _exhale_class_class_buyi_normratio:

Class BuyiNormratio
===================

- Defined in :ref:`file_icraft-qt_buyi_buyi_quantizer.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::OpTraitFunctor< BuyiNormratio, void(const icraft::xir::Operation &, icraft::qt::QuantizerLogger &logger)>``


Class Documentation
-------------------


.. doxygenclass:: BuyiNormratio
   :project: Icraft Quantizer
   :members:
   :protected-members:
   :undoc-members: