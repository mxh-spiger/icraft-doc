.. _exhale_function_buyi__adapt_8h_1a266d79d882434574966edfa4c067d893:

Function adaptScale
===================

- Defined in :ref:`file_icraft-qt_buyi_buyi_adapt.h`


Function Documentation
----------------------


.. doxygenfunction:: adaptScale(std::vector<double>, std::vector<double>, int, std::string, icraft::qt::QuantizerLogger&)
   :project: Icraft Quantizer