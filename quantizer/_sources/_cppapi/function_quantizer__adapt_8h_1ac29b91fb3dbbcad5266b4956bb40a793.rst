.. _exhale_function_quantizer__adapt_8h_1ac29b91fb3dbbcad5266b4956bb40a793:

Function GetQuantizer
=====================

- Defined in :ref:`file_icraft-qt_core_quantizer_adapt.h`


Function Documentation
----------------------


.. doxygenfunction:: GetQuantizer()
   :project: Icraft Quantizer