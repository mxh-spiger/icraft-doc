.. _exhale_class_classicraft_1_1qt_1_1_quantizer_utils:

Class QuantizerUtils
====================

- Defined in :ref:`file_icraft-qt_core_quantizer_utils.h`


Class Documentation
-------------------


.. doxygenclass:: icraft::qt::QuantizerUtils
   :project: Icraft Quantizer
   :members:
   :protected-members:
   :undoc-members: