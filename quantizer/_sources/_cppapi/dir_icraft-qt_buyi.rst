.. _dir_icraft-qt_buyi:


Directory buyi
==============


|exhale_lsh| :ref:`Parent directory <dir_icraft-qt>` (``icraft-qt``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


*Directory path:* ``icraft-qt\buyi``


Files
-----

- :ref:`file_icraft-qt_buyi_buyi_adapt.h`
- :ref:`file_icraft-qt_buyi_buyi_quantizer.h`


