
.. _program_listing_file_icraft-qt_core_quantizer_logger.h:

Program Listing for File quantizer_logger.h
===========================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-qt_core_quantizer_logger.h>` (``icraft-qt\core\quantizer_logger.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <string>
   #include <iostream>
   #include <fstream>
   #include <map>
   #include <array>
   #include <vector>
   #include <filesystem>
   #include <icraft-utils/logging.h>
   
   
   namespace icraft::qt {
       class QuantizerLogger {
       public:
           QuantizerLogger() :
               info_("quantizer_log", icraft::FileSink(std::filesystem::temp_directory_path() / "temp.log")) {
           }
           struct FtmpInfo {
               std::vector<int> dims;
               std::vector<float> max;
               std::vector<float> sat;
               std::vector<float> normratio;
               float scale;
               int exp;
   
               operator std::string() const;
           };
           struct RawInfo {
               std::vector<int> dims;
               std::vector<float> max;
               std::vector<float> sat;
               std::vector<double> scale;
               std::vector<int> exp;
   
               operator std::string() const;
           };
   
           std::map<int, std::pair<std::string, std::string>> cast_type;
           std::map<int, std::string> dtype_;
           std::map<int, int> dtype_value_;
           icraft::Logger info_;
           void setDebugInfo(icraft::Logger debug_info) { info_ = debug_info; };
           void setLogPath(const std::filesystem::path& log_path, std::string network_name);
           void setVerbose(bool verbose) { verbose_ = verbose; };
           bool isVerbose() { return verbose_; }
   
           void logFtmpInfo(const int& id, const FtmpInfo& ftmp_info) { ftmp_sheet_[id] = ftmp_info; }
           void logFtmpDims(const int& id, const std::vector<int>& dims) { ftmp_sheet_[id].dims = dims; }
           void logFtmpMax(const int& id, const std::vector<float>& max) { ftmp_sheet_[id].max = max; }
           void logFtmpSat(const int& id, const std::vector<float>& sat) { ftmp_sheet_[id].sat = sat; }
           void logAvgFtmpMax(const int& id, const std::vector<float>& max, const int& batch) {
               if (ftmp_sheet_[id].max.size() == 0) {
                   for (int i = 0; i < max.size(); i++) {
                       ftmp_sheet_[id].max.push_back(max[i] / batch);
                   }
               }
               else {
                   for (int i = 0; i < max.size(); i++) {
                       ftmp_sheet_[id].max[i] += (max[i] / batch);
                   }
               }
           }
           void logAvgFtmpSat(const int& id, const std::vector<float>& sat, const int& batch) {
               if (ftmp_sheet_[id].sat.size() == 0) {
                   for (int i = 0; i < sat.size(); i++) {
                       ftmp_sheet_[id].sat.push_back(sat[i] / batch);
                   }
               }
               else {
                   for (int i = 0; i < sat.size(); i++) {
                       ftmp_sheet_[id].sat[i] += (sat[i] / batch);
                   }
               }
           }
           void logFtmpNormratio(const int& id, const std::vector<float>& normratio) { ftmp_sheet_[id].normratio = normratio; }
           void logFtmpScale(const int& id, const float& scale) { ftmp_sheet_[id].scale = scale; }
           void logFtmpExp(const int& id, const int& exp) { ftmp_sheet_[id].exp = exp; }
           const FtmpInfo& getFtmpInfo(const int& id)  const { return ftmp_sheet_.at(id); }
           std::vector<float> getFtmpMax(const int& id)  const { return ftmp_sheet_.at(id).max; }
           std::vector<float> getFtmpSat(const int& id)  const { return ftmp_sheet_.at(id).sat; }
           const std::vector<float>& getFtmpNormratio(const int& id) const { return ftmp_sheet_.at(id).normratio; }
   
           void logRawInfo(const int& id, const RawInfo& raw_info) { raw_sheet_[id] = raw_info; }
           void logRawDims(const int& id, const std::vector<int>& dims) { raw_sheet_[id].dims = dims; }
           void logRawMax(const int& id, const std::vector<float>& max) { raw_sheet_[id].max = max; }
           void logRawSat(const int& id, const std::vector<float>& sat) { raw_sheet_[id].sat = sat; }
           void logRawScale(const int& id, const std::vector<double>& scale) { raw_sheet_[id].scale = scale; }
           void logRawExp(const int& id, const std::vector<int>& exp) { raw_sheet_[id].exp = exp; }
           const RawInfo& getRawInfo(const int& id)  const { return raw_sheet_.at(id); }
           std::vector<float> getRawSat(const int& id) const { return raw_sheet_.at(id).sat; }
           std::vector<float> getRawMax(const int& id)  const { return raw_sheet_.at(id).max; }
   
           void dumpFtmpSheet();
           void dumpRawSheet();
           void loadFtmpSheet(const std::string& ftmp_csv);
           void loadRawSheet(const std::string& raw_csv);
   
       private:
           inline static constexpr auto FTMP_CSV_SUFFIX = "_ftmps.csv";
           inline static constexpr auto RAW_CSV_SUFFIX = "_raws.csv";
           inline static constexpr auto CSV_SPLIT = ",";
   
           inline static constexpr auto FTMP_SHEET_HEADER = std::array<const char*, 7>{
               "ftmp_id",
                   "size",
                   "max",
                   "sat",
                   "normratio",
                   "scale",
                   "exp"
           };
           inline static constexpr auto RAW_SHEET_HEADER = std::array<const char*, 6>{
               "raw_id",
                   "size",
                   "max",
                   "sat",
                   "scale",
                   "exp"
           };
           std::map<int, FtmpInfo> ftmp_sheet_;
           std::map<int, RawInfo> raw_sheet_;
           std::filesystem::path log_path_;
           std::string network_name_;
           bool verbose_ = false;
       };
   }
