.. _exhale_class_class_quantizer_adapt:

Class QuantizerAdapt
====================

- Defined in :ref:`file_icraft-qt_core_quantizer_adapt.h`


Inheritance Relationships
-------------------------

Derived Type
************

- ``public BuyiQuantizer`` (:ref:`exhale_class_class_buyi_quantizer`)


Class Documentation
-------------------


.. doxygenclass:: QuantizerAdapt
   :project: Icraft Quantizer
   :members:
   :protected-members:
   :undoc-members: