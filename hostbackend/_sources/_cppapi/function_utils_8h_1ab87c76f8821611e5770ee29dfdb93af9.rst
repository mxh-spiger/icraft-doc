.. _exhale_function_utils_8h_1ab87c76f8821611e5770ee29dfdb93af9:

Function icraft::hostbackend::utils::Image2Tensor(const std::string&, const xir::Value&)
========================================================================================

- Defined in :ref:`file_icraft-backends_hostbackend_utils.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::hostbackend::utils::Image2Tensor(const std::string&, const xir::Value&)
   :project: Icraft HostBackend