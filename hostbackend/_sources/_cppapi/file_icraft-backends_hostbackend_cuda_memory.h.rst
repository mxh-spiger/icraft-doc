
.. _file_icraft-backends_hostbackend_cuda_memory.h:

File memory.h
=============

|exhale_lsh| :ref:`Parent directory <dir_icraft-backends_hostbackend_cuda>` (``icraft-backends\hostbackend\cuda``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-backends\hostbackend\cuda\memory.h``)
----------------------------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-backends_hostbackend_cuda_memory.h.rst





Includes
--------


- ``icraft-backends/hostbackend/cuda/utils/utils.h``

- ``icraft-xrt/core/backend.h``



Included By
-----------


- :ref:`file_icraft-backends_hostbackend_cuda_device.h`




Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__xrt`


Classes
-------


- :ref:`exhale_class_classicraft_1_1xrt_1_1_cuda_mem_region`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_cuda_mem_region_node`

