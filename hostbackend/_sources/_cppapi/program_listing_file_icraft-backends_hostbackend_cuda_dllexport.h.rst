
.. _program_listing_file_icraft-backends_hostbackend_cuda_dllexport.h:

Program Listing for File dllexport.h
====================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-backends_hostbackend_cuda_dllexport.h>` (``icraft-backends\hostbackend\cuda\dllexport.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   
   #ifdef _WIN32
   #ifdef  CUDADEV_DLL_EXPORTS
   #define CUDADEV_DLL _declspec(dllexport)
   #else
   #define CUDADEV_DLL _declspec(dllimport)
   #endif
   #else
   #define CUDADEV_DLL
   #endif
   
