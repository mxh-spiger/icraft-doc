
.. _program_listing_file_icraft-backends_hostbackend_dllexport.h:

Program Listing for File dllexport.h
====================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-backends_hostbackend_dllexport.h>` (``icraft-backends\hostbackend\dllexport.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   
   #ifdef _WIN32
   #ifdef  HOSTBKND_DLL_EXPORTS
   #define HOSTBKND_DLL _declspec(dllexport)
   #else
   #define HOSTBKND_DLL _declspec(dllimport)
   #endif
   #else
   #define HOSTBKND_DLL
   #endif
   
