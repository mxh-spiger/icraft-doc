
.. _file_icraft-backends_hostbackend_decode_custom.h:

File decode_custom.h
====================

|exhale_lsh| :ref:`Parent directory <dir_icraft-backends_hostbackend>` (``icraft-backends\hostbackend``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-backends\hostbackend\decode_custom.h``)
------------------------------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-backends_hostbackend_decode_custom.h.rst





Includes
--------


- ``icraft-xrt/core/tensor.h``



Included By
-----------


- :ref:`file_icraft-backends_hostbackend_utils.h`




Classes
-------


- :ref:`exhale_class_class_custom_decoder`


Functions
---------


- :ref:`exhale_function_decode__custom_8h_1a199ab221e6aa20c6869f3bf8fb5da774`


Defines
-------


- :ref:`exhale_define_decode__custom_8h_1a1806ab788d6406379b257e6ff1a6a511`

- :ref:`exhale_define_decode__custom_8h_1a82087e87ec686c93353427b9e5befb20`

