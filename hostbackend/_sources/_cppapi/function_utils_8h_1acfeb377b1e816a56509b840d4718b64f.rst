.. _exhale_function_utils_8h_1acfeb377b1e816a56509b840d4718b64f:

Function icraft::hostbackend::utils::Ftmp2Tensor
================================================

- Defined in :ref:`file_icraft-backends_hostbackend_utils.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::hostbackend::utils::Ftmp2Tensor(const std::string&, const xir::Value&)
   :project: Icraft HostBackend