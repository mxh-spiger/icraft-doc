.. _dir_icraft-backends_hostbackend_cuda:


Directory cuda
==============


|exhale_lsh| :ref:`Parent directory <dir_icraft-backends_hostbackend>` (``icraft-backends\hostbackend``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


*Directory path:* ``icraft-backends\hostbackend\cuda``


Files
-----

- :ref:`file_icraft-backends_hostbackend_cuda_device.h`
- :ref:`file_icraft-backends_hostbackend_cuda_dllexport.h`
- :ref:`file_icraft-backends_hostbackend_cuda_memory.h`


