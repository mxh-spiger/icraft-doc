.. _exhale_struct_structicraft_1_1hostbackend_1_1utils_1_1_detected_box:

Struct DetectedBox
==================

- Defined in :ref:`file_icraft-backends_hostbackend_utils.h`


Struct Documentation
--------------------


.. doxygenstruct:: icraft::hostbackend::utils::DetectedBox
   :project: Icraft HostBackend
   :members:
   :protected-members:
   :undoc-members: