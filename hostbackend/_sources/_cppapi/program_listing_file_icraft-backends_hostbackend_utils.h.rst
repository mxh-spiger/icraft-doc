
.. _program_listing_file_icraft-backends_hostbackend_utils.h:

Program Listing for File utils.h
================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-backends_hostbackend_utils.h>` (``icraft-backends\hostbackend\utils.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include "icraft-backends/hostbackend/backend.h"
   #include "icraft-backends/hostbackend/dllexport.h"
   #include "icraft-backends/hostbackend/decode_custom.h"
   #include "icraft-xrt/core/tensor.h"
   
   namespace icraft::hostbackend::utils {
       struct DetectedBox {
           int id;
           float prob;
           float x, y, w, h;
       };
       HOSTBKND_DLL xrt::Tensor Image2Tensor(const std::string& img_path, int height = -1, int width = -1);
   
       HOSTBKND_DLL xrt::Tensor Image2Tensor(const std::string& img_path, const xir::Value& value);
   
       HOSTBKND_DLL xrt::Tensor Ftmp2Tensor(const std::string& ftmp_path, const xir::Value& value);
   
       HOSTBKND_DLL std::vector<xrt::Tensor> DecodeWithDecoder(const std::string& dll_path, const std::string& file_path, const std::vector<xir::TensorType>& data_type);
       
       HOSTBKND_DLL xrt::Tensor Cast(const xrt::Tensor& input, const xrt::TensorType& input_type, const xrt::TensorType& output_type, xrt::HostBackend& backend);
   
       HOSTBKND_DLL std::vector<std::vector<DetectedBox>> TensorToDetectedBoxes(xrt::Tensor tensor);
   }
