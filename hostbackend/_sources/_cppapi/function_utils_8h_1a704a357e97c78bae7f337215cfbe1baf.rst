.. _exhale_function_utils_8h_1a704a357e97c78bae7f337215cfbe1baf:

Function icraft::hostbackend::utils::Cast
=========================================

- Defined in :ref:`file_icraft-backends_hostbackend_utils.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::hostbackend::utils::Cast(const xrt::Tensor&, const xrt::TensorType&, const xrt::TensorType&, xrt::HostBackend&)
   :project: Icraft HostBackend