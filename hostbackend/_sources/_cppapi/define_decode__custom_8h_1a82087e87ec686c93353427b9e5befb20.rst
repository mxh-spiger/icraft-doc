.. _exhale_define_decode__custom_8h_1a82087e87ec686c93353427b9e5befb20:

Define REGISTER_CUSTOM_DECODER
==============================

- Defined in :ref:`file_icraft-backends_hostbackend_decode_custom.h`


Define Documentation
--------------------


.. doxygendefine:: REGISTER_CUSTOM_DECODER
   :project: Icraft HostBackend