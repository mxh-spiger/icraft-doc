
.. _file_icraft-backends_hostbackend_backend.h:

File backend.h
==============

|exhale_lsh| :ref:`Parent directory <dir_icraft-backends_hostbackend>` (``icraft-backends\hostbackend``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-backends\hostbackend\backend.h``)
------------------------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-backends_hostbackend_backend.h.rst





Includes
--------


- ``icraft-backends/hostbackend/dllexport.h``

- ``icraft-xrt/core/backend.h``

- ``icraft-xrt/core/device.h``



Included By
-----------


- :ref:`file_icraft-backends_hostbackend_utils.h`




Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__xrt`


Classes
-------


- :ref:`exhale_class_classicraft_1_1xrt_1_1_host_backend`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_host_backend_node`

