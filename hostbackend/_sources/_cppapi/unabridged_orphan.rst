:orphan:


Full API
========

Directories
***********


.. toctree::
   :maxdepth: 5

   dir_icraft-backends.rst

.. toctree::
   :maxdepth: 5

   dir_icraft-backends_hostbackend.rst

.. toctree::
   :maxdepth: 5

   dir_icraft-backends_hostbackend_cuda.rst

Files
*****


.. toctree::
   :maxdepth: 5

   file_icraft-backends_hostbackend_backend.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-backends_hostbackend_decode_custom.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-backends_hostbackend_cuda_device.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-backends_hostbackend_cuda_dllexport.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-backends_hostbackend_dllexport.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-backends_hostbackend_cuda_memory.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-backends_hostbackend_utils.h.rst
