.. _exhale_function_utils_8h_1a6a7a8b3adaf70dd20719cbdc13c67946:

Function icraft::hostbackend::utils::TensorToDetectedBoxes
==========================================================

- Defined in :ref:`file_icraft-backends_hostbackend_utils.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::hostbackend::utils::TensorToDetectedBoxes(xrt::Tensor)
   :project: Icraft HostBackend