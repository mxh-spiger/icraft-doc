.. _exhale_class_classicraft_1_1xrt_1_1_cuda_device_node:

Class CudaDeviceNode
====================

- Defined in :ref:`file_icraft-backends_hostbackend_cuda_device.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< CudaDeviceNode, DeviceNode >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::CudaDeviceNode
   :project: Icraft HostBackend
   :members:
   :protected-members:
   :undoc-members: