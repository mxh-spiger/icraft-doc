.. _exhale_class_classicraft_1_1xrt_1_1_host_backend:

Class HostBackend
=================

- Defined in :ref:`file_icraft-backends_hostbackend_backend.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< HostBackend, Backend, HostBackendNode >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::HostBackend
   :project: Icraft HostBackend
   :members:
   :protected-members:
   :undoc-members: