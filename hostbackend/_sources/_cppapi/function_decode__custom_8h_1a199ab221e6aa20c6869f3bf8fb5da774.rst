.. _exhale_function_decode__custom_8h_1a199ab221e6aa20c6869f3bf8fb5da774:

Function GetCustomDecoder
=========================

- Defined in :ref:`file_icraft-backends_hostbackend_decode_custom.h`


Function Documentation
----------------------


.. doxygenfunction:: GetCustomDecoder()
   :project: Icraft HostBackend