.. _exhale_class_class_custom_decoder:

Class CustomDecoder
===================

- Defined in :ref:`file_icraft-backends_hostbackend_decode_custom.h`


Class Documentation
-------------------


.. doxygenclass:: CustomDecoder
   :project: Icraft HostBackend
   :members:
   :protected-members:
   :undoc-members: