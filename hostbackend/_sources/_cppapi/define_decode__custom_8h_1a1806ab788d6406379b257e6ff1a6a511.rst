.. _exhale_define_decode__custom_8h_1a1806ab788d6406379b257e6ff1a6a511:

Define CUSTOM_API
=================

- Defined in :ref:`file_icraft-backends_hostbackend_decode_custom.h`


Define Documentation
--------------------


.. doxygendefine:: CUSTOM_API
   :project: Icraft HostBackend