.. _dir_icraft-backends_hostbackend:


Directory hostbackend
=====================


|exhale_lsh| :ref:`Parent directory <dir_icraft-backends>` (``icraft-backends``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


*Directory path:* ``icraft-backends\hostbackend``

Subdirectories
--------------

- :ref:`dir_icraft-backends_hostbackend_cuda`


Files
-----

- :ref:`file_icraft-backends_hostbackend_backend.h`
- :ref:`file_icraft-backends_hostbackend_decode_custom.h`
- :ref:`file_icraft-backends_hostbackend_dllexport.h`
- :ref:`file_icraft-backends_hostbackend_utils.h`


