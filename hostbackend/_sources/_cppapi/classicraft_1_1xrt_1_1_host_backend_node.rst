.. _exhale_class_classicraft_1_1xrt_1_1_host_backend_node:

Class HostBackendNode
=====================

- Defined in :ref:`file_icraft-backends_hostbackend_backend.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< HostBackendNode, BackendNode >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::HostBackendNode
   :project: Icraft HostBackend
   :members:
   :protected-members:
   :undoc-members: