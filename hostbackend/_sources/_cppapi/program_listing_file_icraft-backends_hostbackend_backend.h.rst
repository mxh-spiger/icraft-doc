
.. _program_listing_file_icraft-backends_hostbackend_backend.h:

Program Listing for File backend.h
==================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-backends_hostbackend_backend.h>` (``icraft-backends\hostbackend\backend.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xrt/core/backend.h>
   #include <icraft-xrt/core/device.h>
   #include "icraft-backends/hostbackend/dllexport.h"
   
   namespace icraft {
       namespace xrt {
           class HostBackendNode : public NodeBase<HostBackendNode, BackendNode> {
           public:
   
               HOSTBKND_DLL HostBackendNode();
   
               HOSTBKND_DLL virtual void init(const NetworkView& networkview, const Device& device) override;
   
               HOSTBKND_DLL virtual void deinit() override;
   
               HOSTBKND_DLL virtual void apply() override {};
   
               HOSTBKND_DLL virtual void view(uint64_t start_index, uint64_t end_index) override {};
   
               HOSTBKND_DLL virtual Backend fork() override;
   
               HOSTBKND_DLL virtual MergedOps autoMerge() override;
   
               ~HostBackendNode();
   
               friend class HostBackend;
   
           private:
               struct HostBkndParams;
               std::unique_ptr<HostBkndParams> params_;
               class HostBkndUtilFuncs;
               std::unique_ptr<HostBkndUtilFuncs> ufs_;
           };
   
           class HostBackend : public HandleBase<HostBackend, Backend, HostBackendNode> {
           public:
               HOSTBKND_DLL void setFakeQf(bool fakeQf);
   
               HOSTBKND_DLL bool getFakeQf() const;
   
               HOSTBKND_DLL HostBackendNode::HostBkndUtilFuncs* getUFs();
           };
       }
   }
