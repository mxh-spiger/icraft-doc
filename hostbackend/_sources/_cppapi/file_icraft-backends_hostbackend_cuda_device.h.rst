
.. _file_icraft-backends_hostbackend_cuda_device.h:

File device.h
=============

|exhale_lsh| :ref:`Parent directory <dir_icraft-backends_hostbackend_cuda>` (``icraft-backends\hostbackend\cuda``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-backends\hostbackend\cuda\device.h``)
----------------------------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-backends_hostbackend_cuda_device.h.rst





Includes
--------


- ``icraft-backends/hostbackend/cuda/dllexport.h``

- ``icraft-backends/hostbackend/cuda/memory.h``

- ``icraft-xrt/core/device.h``






Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__xrt`


Classes
-------


- :ref:`exhale_class_classicraft_1_1xrt_1_1_cuda_device`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_cuda_device_node`

