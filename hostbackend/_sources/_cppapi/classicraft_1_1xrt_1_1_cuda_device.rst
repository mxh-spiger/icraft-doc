.. _exhale_class_classicraft_1_1xrt_1_1_cuda_device:

Class CudaDevice
================

- Defined in :ref:`file_icraft-backends_hostbackend_cuda_device.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< CudaDevice, Device, CudaDeviceNode >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::CudaDevice
   :project: Icraft HostBackend
   :members:
   :protected-members:
   :undoc-members: