
.. _file_icraft-backends_hostbackend_utils.h:

File utils.h
============

|exhale_lsh| :ref:`Parent directory <dir_icraft-backends_hostbackend>` (``icraft-backends\hostbackend``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-backends\hostbackend\utils.h``)
----------------------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-backends_hostbackend_utils.h.rst





Includes
--------


- ``icraft-backends/hostbackend/backend.h``

- ``icraft-backends/hostbackend/decode_custom.h``

- ``icraft-backends/hostbackend/dllexport.h``

- ``icraft-xrt/core/tensor.h``






Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__hostbackend`

- :ref:`namespace_icraft__hostbackend__utils`


Classes
-------


- :ref:`exhale_struct_structicraft_1_1hostbackend_1_1utils_1_1_detected_box`


Functions
---------


- :ref:`exhale_function_utils_8h_1a704a357e97c78bae7f337215cfbe1baf`

- :ref:`exhale_function_utils_8h_1a4d8e44308abe110615756d8e68398d33`

- :ref:`exhale_function_utils_8h_1acfeb377b1e816a56509b840d4718b64f`

- :ref:`exhale_function_utils_8h_1a7e652666ee5a9874d9a2806583da066e`

- :ref:`exhale_function_utils_8h_1ab87c76f8821611e5770ee29dfdb93af9`

- :ref:`exhale_function_utils_8h_1a6a7a8b3adaf70dd20719cbdc13c67946`

