
.. _namespace_icraft__hostbackend__utils:

Namespace icraft::hostbackend::utils
====================================


.. contents:: Contents
   :local:
   :backlinks: none





Classes
-------


- :ref:`exhale_struct_structicraft_1_1hostbackend_1_1utils_1_1_detected_box`


Functions
---------


- :ref:`exhale_function_utils_8h_1a704a357e97c78bae7f337215cfbe1baf`

- :ref:`exhale_function_utils_8h_1a4d8e44308abe110615756d8e68398d33`

- :ref:`exhale_function_utils_8h_1acfeb377b1e816a56509b840d4718b64f`

- :ref:`exhale_function_utils_8h_1a7e652666ee5a9874d9a2806583da066e`

- :ref:`exhale_function_utils_8h_1ab87c76f8821611e5770ee29dfdb93af9`

- :ref:`exhale_function_utils_8h_1a6a7a8b3adaf70dd20719cbdc13c67946`
