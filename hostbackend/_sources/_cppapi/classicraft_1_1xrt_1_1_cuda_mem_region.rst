.. _exhale_class_classicraft_1_1xrt_1_1_cuda_mem_region:

Class CudaMemRegion
===================

- Defined in :ref:`file_icraft-backends_hostbackend_cuda_memory.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public HandleBase< CudaMemRegion, MemRegion, CudaMemRegionNode >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::CudaMemRegion
   :project: Icraft HostBackend
   :members:
   :protected-members:
   :undoc-members: