.. _exhale_class_classicraft_1_1xrt_1_1_cuda_mem_region_node:

Class CudaMemRegionNode
=======================

- Defined in :ref:`file_icraft-backends_hostbackend_cuda_memory.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public NodeBase< CudaMemRegionNode, MemRegionNode >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::CudaMemRegionNode
   :project: Icraft HostBackend
   :members:
   :protected-members:
   :undoc-members: