host\_backend.CudaDevice
========================

.. currentmodule:: host_backend

.. autoclass:: CudaDevice
   :members:
   :show-inheritance:
   :inherited-members:
   :special-members: __call__, __add__, __mul__

   
   
   .. rubric:: Methods

   .. autosummary::
      :nosignatures:
   
      ~CudaDevice.Close
      ~CudaDevice.Default
      ~CudaDevice.MemRegion
      ~CudaDevice.Open
      ~CudaDevice.check
      ~CudaDevice.clone
      ~CudaDevice.defaultMemRegion
      ~CudaDevice.defaultRegRegion
      ~CudaDevice.defined
      ~CudaDevice.getMemRegion
      ~CudaDevice.getRegRegion
      ~CudaDevice.malloc
      ~CudaDevice.readReg
      ~CudaDevice.reset
      ~CudaDevice.same_as
      ~CudaDevice.setDefaultMemRegion
      ~CudaDevice.setDefaultRegRegion
      ~CudaDevice.showStatus
      ~CudaDevice.typeKey
      ~CudaDevice.type_is
      ~CudaDevice.unique
      ~CudaDevice.use_count
      ~CudaDevice.version
      ~CudaDevice.writeReg
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~CudaDevice.type_key
   
   