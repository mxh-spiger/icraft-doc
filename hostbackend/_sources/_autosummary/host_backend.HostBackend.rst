host\_backend.HostBackend
=========================

.. currentmodule:: host_backend

.. autoclass:: HostBackend
   :members:
   :show-inheritance:
   :inherited-members:
   :special-members: __call__, __add__, __mul__

   
   
   .. rubric:: Methods

   .. autosummary::
      :nosignatures:
   
      ~HostBackend.Create
      ~HostBackend.Init
      ~HostBackend.apply
      ~HostBackend.clone
      ~HostBackend.defined
      ~HostBackend.deinit
      ~HostBackend.enableTimeProfile
      ~HostBackend.fork
      ~HostBackend.forwardOp
      ~HostBackend.getFakeQf
      ~HostBackend.getForwardFunc
      ~HostBackend.getInitFunc
      ~HostBackend.init
      ~HostBackend.initOp
      ~HostBackend.isOpSupported
      ~HostBackend.same_as
      ~HostBackend.setDevice
      ~HostBackend.setFakeQf
      ~HostBackend.setTimeElapses
      ~HostBackend.typeKey
      ~HostBackend.type_is
      ~HostBackend.unique
      ~HostBackend.use_count
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~HostBackend.device
      ~HostBackend.network_view
      ~HostBackend.time_profile
      ~HostBackend.type_key
   
   