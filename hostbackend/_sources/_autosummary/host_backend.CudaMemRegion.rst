host\_backend.CudaMemRegion
===========================

.. currentmodule:: host_backend

.. autoclass:: CudaMemRegion
   :members:
   :show-inheritance:
   :inherited-members:
   :special-members: __call__, __add__, __mul__

   
   
   .. rubric:: Methods

   .. autosummary::
      :nosignatures:
   
      ~CudaMemRegion.clone
      ~CudaMemRegion.defined
      ~CudaMemRegion.device
      ~CudaMemRegion.malloc
      ~CudaMemRegion.same_as
      ~CudaMemRegion.typeKey
      ~CudaMemRegion.type_is
      ~CudaMemRegion.unique
      ~CudaMemRegion.use_count
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~CudaMemRegion.type_key
   
   