﻿host\_backend
=============

.. automodule:: host_backend

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
      :toctree:
      :nosignatures:
   
      Cast
      DecodeWithDecoder
      Ftmp2Tensor
      Image2Tensor
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
      :toctree:
      :nosignatures:
   
      CudaDevice
      CudaMemRegion
      HostBackend
   
   

   
   
   



