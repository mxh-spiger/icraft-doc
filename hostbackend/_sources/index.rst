===================
Icraft HostBackend
===================

一 简介
===========
Icraft HostBackend(以下简称HostBackend)是Icraft XRT的计算后端之一，主要负责Host(CPU)和Cuda(NvidiaGpu)计算资源的初始化和前向计算。

从用户角度上，HostBackend模块包括HostBackend、CudaDevice、CudaMemRegion三个数据结构（因为其特殊性，HostDevice和HostMemRegion由XRT直接定义和管理），分别是XRT的Backend、Device、MemRegion在CPU和CUDA
计算资源上的具体实现。用户可以通过C++或Python的API实例化并使用HostBackend、HostDevice、CudaDevice，具体用法请见文档API说明和下文中的代码用例。

在内部实现上，HostBackend负责Icraft算子在CPU和CUDA后端硬件上的初始化和前向计算。

此外，为了方便用户和开发者，HostBackend提供了一个Utils接口，提供了若干功能函数，包括Image2Tensor、Ftmp2Tensor、Cast、DecodeWithDecoder、DecodeTensorToDetectedBoxes，使用方法请见文档API说明和下文中的代码用例。

二 接口说明
================

下面的代码说明了如何使用HostBackend、HostDevice和CudaDevice。

2.1 C++接口使用说明
--------------------

2.1.1 创建基于HostDevice或CudaDevice的Session
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: cpp
   
   auto json_file = "D:/Workspace/V3.0/SSD_BY.json";
   auto raw_file = "D:/Workspace/V3.0/SSD_BY.raw";

   // 初始化设备
   auto host_device = HostDevice::Default();
   auto cuda_device = CudaDevice::Default();

   // 加载网络
   auto network = Network::CreateFromJsonFile(json_file);
   network.loadParamsFromFile(raw_file);

   // 创建基于CudaDevice的Session, 
   // 仅限于有Cuda设备的环境里，否则无法运行;
   // 在某些无法运行在Cuda设备上的算子会自动跳转到host_device上运行；
   auto sess_cuda = Session::Create<HostBackend>(network, { cuda_device });

   // 创建基于HostDevice的Session
   auto sess_host = Session::Create<HostBackend>(network, { host_device }); 

   // 创建Session2
   // 如果不适用模板Create方法，也可以使用CreateByOrder方法
   auto sess_cuda1 = Session::CreateByOrder(network, {host_backend}, {cuda_device});
   auto sess_host1 = Session::CreateByOrder(network, {host_backend}, {host_device});
   
2.1.2 fakeQf模式
^^^^^^^^^^^^^^^^^

.. code-block:: cpp

   // 创建Backend对象
   auto host_backend = HostBackend::Init();

   // HostBackend有两个独有的接口：setFakeQf和getFakeQf。通过它们可以设置和查询HostBackend算子定点前向（数据类型非FP32的前向）是否以fake模式进行。
   // fakeQf是fake quantize forward的简称，该模式下定点前向过程中不做截位和饱和操作，
   // 因此可以直接对比HostBackend的指令前向结果和adapted模型的前向结果
   host_backend.setFakeQf(true); // 激活fakeQF模式
   bool fakeQf = host_backend.getFakeQf(); // 查询fakeQF模式
   if(fakeQf){
    printf("Xrt HostBackend FakeQf mode on.\n");
   }
   host_backend.setFakeQf(false); // 关闭fakeQF模式
   fakeQf = host_backend.getFakeQf(); // 查询fakeQF模式
   if(!fakeQf){
    printf("Xrt HostBackend FakeQf mode off.\n");
   }

2.1.3 Utils使用方法
^^^^^^^^^^^^^^^^^^^^

.. code-block:: cpp

  // Image2Tensor使用方法
  // 读取image(opencv支持的图片类型，如png,jpg,jpng等）转为xrt::Tensor
  auto img_file = "D:/Workspace/imgs/cat224.png";
  // 读取img_file路径上的图片转为Tensor，高和宽均为原图大小
  auto img_tensor_orig = Image2Tensor(img_file);
  // 读取img_file路径上的图片转为Tensor，height resize为300, width保留原图大小
  auto img_tensor_300_224 = Image2Tensor(img_file, 300, -1）
  // 读取img_file路径上的图片转为Tensor，height保留原图大小, width resize为300
  auto img_tensor_224_300 = Image2Tensor(img_file, -1, 300）

  // Ftmp2Tensor使用方法
  // 读取ftmp二进制文件转为xrt::Tensor
  auto ftmp_file = "D:/Workspace/ftmp/value0.ftmp";
  // 读取ftmp_file路径下的ftmp二进制文件转为value对应的Tensor, value为xir的Value类数据
  // 根据value创建一个Tensor，把读取自ftmp_file的数据绑定到该Tensor上
  auto v_tensor = Ftmp2Tensor(ftmp_file, value);

  // Cast使用方法
  // 输出的Tensor是输入Tensor的数据类型(int8_t, float32等)和Layout(NHWC等)
  // 从input_dtype转为output_dtype的结果。需要注意，数据类型的转换需要由data_type提供normratio。
  // auto tensor_new = Cast(tensor_old, tensor_old_dtype, tensor_new_dtype, host_backend);

  // DecodeWithDecoder使用方法
  // 将数据文件使用用户提供的CustomDecoder模块解析为HostDevice上的Xrt Tensor
  // tensor_new = DecodeWithDecoder(dll_path, file_path, data_type);

  // TensorToDetectedBoxes
  // 用于把后处理算子输出Tensor转为检测框，后面会废弃，这里不多赘述。

2.2 Python接口使用说明
----------------------


2.2.1 创建基于HostDevice或CudaDevice的Session
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: python

  import xir
  from xrt import *
  from host_backend import *
  import unittest
  import os

  json_file = "D:\debugs\hostbknd\jr\optimized\Resnet18_optimized.json"
  raw_file = "D:\debugs\hostbknd\jr\optimized\Resnet18_optimized.raw"
  img_file = "D:\debugs\hostbknd\imgs\cat224.png"

  # 初始化设备
  host_device = HostDevice.Default()
  cuda_device = CudaDevice.Default()

  # 加载网络
  network = xir.Network.CreateFromJsonFile(json_file)
  network.loadParamsFromFile(raw_file)

  # 创建基于CudaDevice的Session, 
  # 仅限于有Cuda设备的环境里，否则无法运行;
  # 在某些无法运行在Cuda设备上的算子会自动跳转到host_device上运行；
  sess_cuda = Session.Create([HostBackend], network.view(0), [cuda_device])

  # 创建基于HostDevice的Session
  sess_host = Session.Create([HostBackend], network.view(0), [host_device])

  # 创建Session2
  # 如果不适用模板Create方法，也可以使用CreateByOrder方法
  sess_cuda1 = Session.CreateByOrder(network.view(0), [HostBackend.Init()], [CudaDevice.Default()])
  sess_host1 = Session.CreateByOrder(network.view(0), [HostBackend.Init()], [HostDevice.Default()])


2.2.2 fakeQf模式
^^^^^^^^^^^^^^^^^

.. code-block:: python

  # 创建Backend对象
  host_backend = HostBackend.Init()

  # fakeQF模式
  # HostBackend有两个独有的接口：setFakeQf和getFakeQf。通过它们可以设置和查询HostBackend算子定点前向（数据类型非FP32的前向）是否以fake模式进行。
  # fakeQf模式下定点前向过程中不做截位和饱和操作，因此可以直接对比HostBackend的指令前向结果和adapted模型的前向结果
  host_bknd = HostBackend.Init()
  host_bknd.setFakeQf(True) # 激活fakeQF模式
  fakeQf = host_bknd.getFakeQf() # 查询fakeQF模式
  if fakeQf:
      print("HostBackend fakeQf mode is on.")
  host_bknd.setFakeQf(False) # 关闭fakeQF模式
  fakeQf = host_bknd.getFakeQf() # 查询fakeQF模式
  if not fakeQf:
      print("HostBackend fakeQf mode is off.")
  

2.2.3 Utils使用方法
^^^^^^^^^^^^^^^^^^^^

.. code-block:: python

  # Image2Tensor使用方法
  # 读取image(opencv支持的图片类型，如png,jpg,jpng等）转为xrt::Tensor
  img_file = "D:/Workspace/imgs/cat224.png";
  # 读取img_file路径上的图片转为Tensor，高和宽均为原图大小
  img_tensor_orig = Image2Tensor(img_file);
  # 读取img_file路径上的图片转为Tensor，height resize为300, width保留原图大小
  img_tensor_300_224 = Image2Tensor(img_file, 300, -1）
  # 读取img_file路径上的图片转为Tensor，height保留原图大小, width resize为300
  img_tensor_224_300 = Image2Tensor(img_file, -1, 300）

  # Ftmp2Tensor使用方法
  # 读取ftmp二进制文件转为xrt::Tensor
  ftmp_file = "D:/Workspace/ftmp/value0.ftmp";
  # 读取ftmp_file路径下的ftmp二进制文件转为value对应的Tensor, value为xir的Value类数据
  # 根据value创建一个Tensor，把读取自ftmp_file的数据绑定到该Tensor上
  v_tensor = Ftmp2Tensor(ftmp_file, value);

  # Cast使用方法
  # 输出的Tensor是输入Tensor的数据类型(int8_t, float32等)和Layout(NHWC等)
  # 从input_dtype转为output_dtype的结果。需要注意，数据类型的转换需要由data_type提供normratio。
  #tensor_new = Cast(tensor_old, tensor_old_dtype, tensor_new_dtype, host_backend);

  # DecodeWithDecoder使用方法
  # 将数据文件使用用户提供的CustomDecoder模块解析为HostDevice上的Xrt Tensor
  #tensor_new = DecodeWithDecoder(dll_path, file_path, data_type);

  # TensorToDetectedBoxes
  # 用于把后处理算子输出Tensor转为检测框，后面会废弃，这里不多赘述。


三 算子说明
===========
HostBackend主要实现内容是Icraft所有算子在HostBackend上的初始化、前向计算以及相关数据管理。
目前实现了所有Icraft算子在HostDevice(CPU)上的实现，由于一些约束，部分计算尚未支持CUDA，在前向计算过程中CUDA不支持时会自动回落到Host上进行计算。
下表是HostBackend支持的算子以及支持情况。

.. list-table::
   :widths: 10 18 18 50
   :header-rows: 1

   * - 算子名称
     - CPU支持
     - CUDA支持
     - 备注
   * - add 
     - √
     - √ 
     - 
   * - align_axis
     - √
     - √ 
     - 
   * - avgpool
     - √
     - 部分支持 
     - 仅在左右pad相等且上下pad相等时支持CUDA
   * - batchnorm
     - √
     - √ 
     - 
   * - cast
     - √
     - √ 
     - 
   * - concat
     - √
     - √ 
     - 
   * - conv2d
     - √
     - √ 
     - 
   * - conv2d_transpose
     - √
     - √
     - 
   * - copy 
     - √
     - √ 
     - 
   * - divide_scalar
     - √
     - √ 
     - 
   * - elu
     - √
     - √ 
     - 
   * - gelu
     - √
     - √
     - 
   * - hardop 
     - √
     - x 
     - hardop是硬件指令前向，暂不支持CUDA
   * - hardsigmoid
     - √
     - 部分支持
     - 仅当alpha参数为3时支持CUDA
   * - hardswish
     - √
     - √
     - 
   * - input
     - x 标记算子，无需计算
     - x 标记算子，无需计算
     - 

   * - layernorm
     - √
     - √ 
     - 
   * - matmul
     - √
     - √
     - 
   * - maxpool
     - √
     - √
     - 
   * - mish
     - √
     - √
     - 
   * - multiply
     - √
     - √
     - 
   * - multiyolo
     - √
     - x
     - 属于后处理算子，不支持CUDA计算
   * - output
     - x 标记算子，无需计算
     - x 标记算子，无需计算
     - 
   * - pad
     - √
     - √
     -
   * - pixel_shuffle
     - √
     - √
     - 

   * - prelu
     - 尚未支持定点前向
     - 尚未支持定点前向
     - 
   * - prune_axis
     - √
     - √
     - 
   * - region
     - √
     - x
     - 后处理算子，不支持CUDA计算
   * - relu
     - √
     - 部分支持
     - max_value != DBL_MAX 或 threshold != 0时不支持CUDA计算
   * - reorg
     - √
     - x
     - Darknet算子，暂不支持CUDA计算
   * - reshape
     - √
     - √
     - 
   * - resize
     - √
     - x
     - 通过opencv实现，暂不支持CUDA计算
   * - route
     - √
     - x
     - Darknet算子，暂不支持CUDA计算

   * - sigmoid
     - √
     - √ 
     - 
   * - silu
     - √
     - √
     - 
   * - slice
     - √
     - √
     - 
   * - softmax
     - √
     - √
     - 

   * - split
     - √
     - √ 
     - 
   * - squeeze
     - √
     - x
     - 只是维度信息的更新，不需要CUDA加速
   * - ssd_output
     - √
     - x
     - 后处理算子，暂不支持CUDA计算
   * - swap_order
     - √
     - √
     -

   * - tanh 
     - √
     - √ 
     - 
   * - transpose
     - √
     - √ 
     - 
   * - upsample
     - √
     - √ 
     - 
   * - yolo
     - √
     - x
     - 后处理算子，暂不支持CUDA计算

四 索引
=======
* :ref:`genindex`

.. toctree::
   :maxdepth: 2

   C++ API reference <_cppapi/api_root>
   Python API reference <_autosummary/host_backend>