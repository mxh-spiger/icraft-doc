==========
命令行参数
==========

一 调用方法
===========

Icraft提供了CLI命令 ``icraft`` 来支持用户完成 **解析**、 **优化** 、 **量化** 、 **适配** 、 **指令生成** 以及 **运行** 等过程。其调用方法如下：

.. card:: Icraft CLI 命令格式
    :text-align: center

    icraft <command> [<config file>] *[--<key> <value>]...*

其中 ``command`` 表示要执行的功能命令，当前支持的命令如下：

+----------+------------------------------------------------------------------------+
| 功能     | 命令                                                                   |
+==========+========================================================================+
| parse    | 解析                                                                   |
+----------+------------------------------------------------------------------------+
| optimize | 优化                                                                   |
+----------+------------------------------------------------------------------------+
| quantize | 量化                                                                   |
+----------+------------------------------------------------------------------------+
| adapt    | 适配                                                                   |
+----------+------------------------------------------------------------------------+
| generate | 指令生成                                                               |
+----------+------------------------------------------------------------------------+
| compile  | 编译，根据配置文件的参数配置依次执行解析、优化、量化、适配和指令生成四个过程 |
+----------+------------------------------------------------------------------------+
| run      | 在指定的后端上运行编译产生的中间层网络                                 |
+----------+------------------------------------------------------------------------+

例如，指令生成的命令行调用方法为 ``icraft generate [<config file>] [--<key> <value>]...``

二 参数配置
===========

CLI命令 ``icraft`` 的参数可以在配置文件中指定，也可以通过命令行参数 ``--<key> <value>`` 的方式指定。

CLI命令 ``icraft`` 的配置文件为 ``toml`` 格式。以 ``yolov5s`` 的编译配置为例，其配置文件内容如下：

.. dropdown:: yolov5s.toml
    :icon: code 

    .. code-block:: toml
        :linenos:
        :force:

        [parse]
        net_name = "yolov5s"
        framework = "pytorch"
        inputs = [ 1, 512, 960, 3]
        inputs_layout = "NHWC"
        pre_method = "nop"
        pre_scale = [ 255.0, 255.0, 255.0]
        pre_mean = [ 0.0, 0.0, 0.0]
        channel_swap = [ 2, 1, 0]
        network = "./models/YoloV5/yolov5s-v2.0-(512x960).pt"
        jr_path = "./json&raw/"
        frame_version = "1.6"

        [optimize]
        target = "BUYI"
        json = "./json&raw/yolov5s_parsed.json"
        raw = "./json&raw/yolov5s_parsed.raw"
        jr_path = "./json&raw/"

        [quantize]
        forward_mode = "image"
        saturation = "kld"
        forward_dir = "./images/datasets/yolov5s"
        forward_list = "./images/pic_lists/pic_list_yolov5s.txt"
        names_file = "./names/coco.names"
        bits = 8
        json = "./json&raw/yolov5s_optimized.json"
        raw = "./json&raw/yolov5s_optimized.raw"
        jr_path = "./json&raw/"
        per = "tensor"
        target = "buyi"

        [adapt]
        target = "BUYI"
        json = "./json&raw/yolov5s_quantized.json"
        raw = "./json&raw/yolov5s_quantized.raw"
        jr_path = "./json&raw/"

        [generate]
        json = "./json&raw/yolov5s_adapted.json"
        raw = "./json&raw/yolov5s_adapted.raw"
        jr_path = "./json&raw/"
        log_path = "./logs/"

        [run]
        json = "json&raw/yolov5s_BY.json"
        raw = "json&raw/yolov5s_BY.raw"
        input = "./images/test/test_640x640.jpg"
        backends = "BY,socket://ql100aiu@192.168.125.148:9981;Host"
        dump_format = "HQB"
        log_io = true

如上所示， 在toml文件中，方括号括起来的部分，比如 ``[parse]`` 和 ``[generate]`` 等表示一个小节， icraft使用一个小节来配置一个命令。小节的名称，即 ``parse`` 和 ``generate`` 等表示被配置的命令，小节中使用 ``=`` 表达的键值对表示该命令的参数的键和值。例如，当执行 ``icraft parse yolov5s.toml`` 时， ``icraft`` 会使用 ``[parse]`` 这一小节的键值对来配置 ``parse`` 命令。

除了使用配置文件外，CLI命令 ``icraft`` 还支持直接使用命令行来指定参数，比如上文中的 ``icraft parse yolov5s.toml`` ，等价于以下命令：

.. code-block:: bash
    :force:

    icraft parse                                        \
    --net_name  yolov5s                                 \
    --framework  pytorch                                \
    --inputs  1,512,960,3                               \
    --inputs_layout NHWC                                \
    --pre_method nop                                    \
    --pre_scale 255.0,255.0,255.0                       \
    --pre_mean 0.0,0.0,0.0                              \
    --channel_swap 2,1,0                                \
    --network ./models/YoloV5/yolov5s-v2.0-(512x960).pt \
    --jr_path "./json&raw/"                             \
    --frame_version 1.6                                 

CLI命令还支持配置文件和命令行参数混用，在混用时，命令行参数中的配置将覆盖配置文件中的同名参数，比如 ``icraft parser yolov5s.toml --frame_version 1.9`` 会将配置文件中的 ``frame_version`` 改为 ``1.9`` 。

.. note:: 
    ``compile`` 命令需要根据配置文件中各个命令的配置依次调用 ``parse`` , ``optimize`` , ``quantize`` , ``adapt`` , ``generate`` 等过程，因此不支持使用命令行来指定参数。

三 参数列表
===========

目前， ``icraft`` 支持的命令及其参数如下：

3.1 parse
---------

.. list-table:: icraft parse 参数说明表
   :widths: 25 25 60
   :header-rows: 1

   * - 参数名称
     - 数据格式
     - 说明
   * - net_name
     - string
     - 表示网络模型的名字
   * - network
     - string
     - 表示网络模型文件的路径，通常与weight（参数文件）配合使用。部分框架的网络模型和参数在同一个文件，该情况下，仅需配置network即可
   * - weights
     - string
     - 表示参数文件的路径，与network配合使用
   * - jr_path
     - string
     - 表示解析组件输出文件的存放路径
   * - log_path
     - string
     - 可选，表示解析组件执行过程输出的log文件的存放路径，省略则放到默认路径"./.icraft/logs/"下
   * - framework
     - string
     - 产生网络的框架，仅支持Pytorch、Onnx、Darknet
   * - frame_version
     - string
     - 框架版本号，因为只有PytorchParser支持多个版本，因此该参数仅在framework=Pytorch时需要配置，默认1.9
   * - inputs
     - string
     - 表示网络输入的尺寸，因为网络存在多输入的情况，因此需要给每个输入配置输入尺寸，以";"分隔，每个输入内部以","区分维度
   * - inputs_layout
     - string
     - 表示网络输入的layout，因为网络存在多输入的情况，因此需要给每个输入配置layout，以";"分隔，在cnn领域，推荐配置"NHWC"，对于维度含义不确定的情况，推荐"*C",'*'的输入为input.dim.size-1，表示采用深度学习框架的排布
   * - pre_method
     - string
     - 表示网络前处理，因为网络存在多输入的情况，因此需要给每个输入配置前处理，以";"分隔，当某个输入没有前处理时，需要用nop占位
   * - pre_mean
     - string
     - 表示网络前处理参数pre_mean，因为网络存在多输入的情况，因此需要给每个输入配置前处理，以";"分隔，当某个输入没有前处理参数pre_mean，需要用nop占位
   * - pre_scale
     - string
     - 表示网络前处理参数pre_scale，因为网络存在多输入的情况，因此需要给每个输入配置前处理，以";"分隔，当某个输入没有前处理参数pre_scale，需要用nop占位
   * - channel_swap
     - string
     - 表示网络前处理参数channel_swap，因为网络存在多输入的情况，因此需要给每个输入配置前处理，以";"分隔，当某个输入没有前处理参数channel_swap，需要用nop占位

3.2 optimize
------------

.. list-table:: icraft optimize参数列表
   :widths: 15 15 30 
   :header-rows: 1

   * - 参数名字
     - 数据格式
     - 说明
   * - json
     - string类型
     - 表示输入的中间层json文件路径
   * - raw
     - string类型
     - 表示输入的中间层raw文件路径，与json参数配合使用，json+raw、path+network的组合仅可存在一组
   * - path
     - string类型
     - 表示路径，通常与network配合使用，表示网络路径
   * - network
     - string类型
     - 表示网络名称，通常与path配合使用，表示网络路径，json+raw、path+network的组合仅可存在一组
   * - jr_path
     - string类型
     - 表示优化后网络的存放路径
   * - log_path
     - string类型
     - 可选，表示log文件的存放路径，省略则放到默认路径"./.icraft/logs/"下
   * - pass_on
     - string类型
     - 可选，通过配置该参数，可以打开icraft内置pass或自定义pass，以”，“分隔，pass_on所打开的自定义pass将优先于内置pass执行
   * - pass_off
     - string类型
     - 可选，通过配置该参数，可以关掉icraft内置pass，以”，“分隔。pass_on和pass_off不可包含同一个pass
   * - custom_config
     - string类型
     - 可选，用于描述文件路径，该文件用于向自定义pass传递参数。
   * - no_integral
     - implicit类型
     - 默认为false，该参数通常与pass_on配合使用，表明将忽略icraft内置pass，仅调用pass_on所描述的pass选项

3.3 quantize
------------
.. list-table:: icraft quantize参数列表
   :widths: 15 15 30 
   :header-rows: 1

   * - 参数名字
     - 数据格式
     - 说明
   * - json
     - string
     - 表示输入的中间层json文件路径 
   * - raw
     - string
     - 表示输入的中间层raw文件路径 
   * - jr_path
     - string
     - 表示输出的中间文件存放的路径
   * - target
     - string
     - 表示量化面向的后端架构（buyi）
   * - bits
     - int
     - 表示量化位数（8/16）
   * - forward_mode
     - string
     - 表示校准集的格式（image/ftmp）
   * - forward_dir
     - string
     - 表示校准集图片所在的文件夹
   * - forward_list
     - string
     - 表示校准集包含的图片列表
   * - saturation
     - string
     - 表示统计饱和点的方法（kld/ems/null）  
   * - per
     - string
     - 表示按层还是按通道量化（tensor/channel）
   * - batch
     - int（optional）
     - 将校准集分为几份，依次进行前向计算（电脑内存较小时使用），默认为1
   * - bin_num
     - int（optional）
     - 表示统计饱和点时将数据分成多少个bin，默认4096
   * - no_transinput
     - bool（optional）
     - 表示是否对前处理算子中的参数进行合并与转换，默认false
   * - no_normalize
     - bool（optional）
     - 表示是否做归一化，默认false
   * - before_relu
     - bool（optional）
     - 表示卷积输出ftmp是否用relu之前的统计，默认false
   * - ftmp_csv
     - string（optional）
     - 表示ftmp_csv的路径，从该ftmp_csv中获取ftmp的饱和点时使用
   * - raw_csv
     - string（optional）
     - 表示raw_csv的路径，从该raw_csv中获取weight的饱和点时使用
   * - decode_dll
     - string（optional）
     - 表示调用的解码图片dll的路径
   * - input_norm
     - string（optional）
     - 表示input算子的ftmp使用网络中某些ftmp的normratio
   * - mix_precision
     - string（optional）
     - 配置mix_precision= "auto"：表示自动实现混合精度量化，并按默认混合方案进行混合精度配置；
       配置mix_precision= "xxx.csv"：表示根据用户自定义的位宽配置文件实现混合精度量化。

3.4 adapt
---------

.. list-table:: icraft adapt参数列表
   :widths: 15 15 30 
   :header-rows: 1

   * - 参数名字
     - 数据格式
     - 说明
   * - json
     - string类型
     - 表示输入的中间层json文件路径
   * - raw
     - string类型
     - 表示输入的中间层raw文件路径，与json参数配合使用，json+raw、path+network的组合仅可存在一组
   * - path
     - string类型
     - 表示路径，通常与network配合使用，表示网络路径
   * - network
     - string类型
     - 表示网络名称，通常与path配合使用，表示网络路径，json+raw、path+network的组合仅可存在一组
   * - jr_path
     - string类型
     - 表示优化后网络的存放路径
   * - log_path
     - string类型
     - 可选，表示log文件的存放路径，省略则放到默认路径"./.icraft/logs/"下
   * - pass_on
     - string类型
     - 可选，通过配置该参数，可以打开icraft内置pass或自定义pass，以”，“分隔，pass_on所打开的自定义pass将优先于内置pass执行
   * - pass_off
     - string类型
     - 可选，通过配置该参数，可以关掉icraft内置pass，以”，“分隔。pass_on和pass_off不可包含同一个pass
   * - custom_config
     - string类型
     - 可选，用于描述文件路径，该文件用于向自定义pass传递参数。
   * - no_integral
     - implicit类型
     - 默认为false，该参数通常与pass_on配合使用，表明将忽略icraft内置pass，仅调用pass_on所描述的pass选项

3.5 generate
------------

.. list-table:: icraft generate参数列表
   :widths: 15 15 30 
   :header-rows: 1

   * - 参数名字
     - 数据格式
     - 说明
   * - json
     - string
     - 输入的中间层JSON文件(adapted阶段) 
   * - raw
     - string
     - 输入的中间层RAW文件(adapted阶段) 
   * - jr_path
     - string
     - 输出的中间文件存放的路径
   * - log_path
     - string（optional）
     - 表示log文件的存放路径，省略则为默认路径 
   * - path 
     - string（optional）
     - 表示路径，通常与network配合使用，表示网络路径
   * - network  
     - string（optional）
     - 表示网络名称，通常与path配合使用，表示网络路径，json+raw、path+network的组合仅可存在一组  
   * - ddr_base
     - int（optional）
     - 表示预留的地址空间，默认预留64x64 bytes
   * - rows（optional）
     - int 1~4（optional）
     - 表示AI CORE采用的PE阵列数量（行），默认4
   * - cols（optional）
     - int 1~4（optional）
     - 表示AI CORE采用的PE阵列数量（列），默认4
   * - xlmopt
     - int（optional）
     - 表示对ifm的优化，默认3，最高级优化
   * - klmopt
     - int（optional）
     - 表示对kernenl的优化，默认3，最高级优化  
   * - icropt
     - int（optional）
     - 表示对指令的优化，默认1，打开优化
   * - ocmopt
     - int（optional） -1/0/1/2/3
     - 表示选择ocm的优化方案，默认-1;-1表示遍历方案1和2选得分较高的方案；0表示关闭ocm优化；1表示选择方案1，2表示选择方案2，3表示选择方案3
   * - version
     - bool（optional）
     - 表示是否打印版本信息，默认不打印
   * - device_info 
     - string（optional）
     - 表示fpga硬算子所需的硬件信息，通常为一组寄存器信息

3.6 run
-------

.. list-table:: icraft 参数
   :widths: 15 15 30 
   :header-rows: 1

   * - 参数
     - 数据格式
     - 说明
   * - json
     - string
     - 待运行的json文件路径
   * - raw
     - string
     - 待运行的raw文件路径
   * - jr_path
     - string
     - 待运行的json和raw文件路径
   * - network
     - string
     - 待运行的json和raw文件名
   * - input
     - string
     - 输入文件名：支持图片或ftmp；支持多输入
   * - log_path
     - string（optional）
     - log存放路径，默认为.logs；完整路径为log_path/network/\*.log
   * - backends
     - string
     - 指定后端，默认为Host
   * - dump_format
     - string（optional）
     - 指定dump特征图使用的格式；支持SFB/SFT/SQB/SQT/HQB/HQT等；默认为空，即不dump
   * - dump_ftmp
     - string（optional）
     - 指定dump哪些特征图；默认为空，即dump所有特征图
   * - fake_qf
     - bool（optional）
     - 打开fake_qf模式，仅针对HostBackend有效
   * - compress
     - bool（optional）
     - 打开内存压缩模式，仅针对BYBackend有效
   * - log_time
     - bool（optional）
     - 记录每个算子的执行时间信息到屏幕和xrun_io.log文件中
   * - log_io
     - bool（optional）
     - 记录每个算子的输入输出信息到屏幕和xrun_time.log文件中