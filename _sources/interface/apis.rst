=============
API Reference
=============

XIR
===
Icraft-XIR 提供了一系列API，来对中间层网络进行增删查改等操作：

- :external+xir:doc:`XIR C++ API Reference<_cppapi/api_root>`

- :external+xir:doc:`XIR Python API Reference <_autosummary/xir>`

XRT
===
Icraft-XRT 提供了一系列API，来将中间层网络部署到指定后端，并控制前向过程：

- :external+xrt:doc:`XRT C++ API Reference<_cppapi/api_root>`

- :external+xrt:doc:`XRT Python API Reference <_autosummary/xrt>`

HostBackend
===========
Icraft-HostBackend 提供了一系列API，控制网络在HostBackend的前向行为：

- :external+hostbackend:doc:`HostBackend C++ API Reference<_cppapi/api_root>`

- :external+hostbackend:doc:`HostBackend Python API Reference <_autosummary/host_backend>`

BuyiBackend
===========
Icraft-BuyiBackend 提供了一系列API，控制网络在BuyiBackend的前向行为：

- :external+buyibackend:doc:`BuyiBackend C++ API Reference<_cppapi/api_root>`

- :external+buyibackend:doc:`BuyiBackend Python API Reference <_autosummary/buyibackend>`