=====
安装
=====

``Icraft`` 目前提供 ``Windows10`` 、 ``Ubuntu20.04-aarch64`` 两种模式的安装包。


一 Windows10
=============

安装
----

双击 ``Icraft Setup.exe`` 即可开始安装。

安装程序将在 ``C盘根目录`` 创建 ``Icraft`` 文件夹，然后在此文件夹内存放当前版本的 ``Icraft`` ，例如 ``CLI v3.0.0`` 。

安装程序会创建名为 ``CLI`` 的软链接，链接到刚刚安装的 ``CLI v3.0.0`` 文件夹上，并将该软链接加入环境变量中的 ``PATH`` 项。

如果已经存在指向其它版本的软链接，安装程序会删除它后再执行上一步操作，以保证环境变量指向的总是最新安装的一版。

安装程序会在安装成功后向环境变量中 ``ICRAFT_VERSION`` 项添加当前版本号，例如 ``v3.0.0`` 。

如果在安装前发现 ``ICRAFT_VERSION`` 中已经存在当前版本号了，则拒绝安装，并提示用户已存在该版本。

安装程序会向环境变量中 ``CMAKE_PREFIX_PATH`` 项添加值 ``C:\Icraft\CLI\lib\cmake\``

卸载
----

每个版本目录下都有一个 ``uninst.exe`` 卸载程序，双击即可开始卸载该版本。

卸载时程序将从环境变量中 ``ICRAFT_VERSION`` 项删除当前版本号，所以未按正常流程卸载导致此变量有残留，可能导致之后无法正常安装，此时只需手动删除即可。

卸载程序会删除当前CLI软链接，如果发现环境变量中 ``ICRAFT_VERSION`` 项还有其它版本存在，则会再次创建名为CLI的软链接，链接到上一版安装的 ``CLI`` 文件夹上。

PythonAPI
----------

Icraft在Windows10下提供基于python3.8的扩展包，名为 ``icraft-3.0.0-cp38-none-win_amd64.whl`` 。

与其它python扩展包相同，icraft扩展包的安装与卸载命令如下：

.. code-block:: bash

    pip install icraft-3.0.0-cp38-none-win_amd64.whl
    pip uninstall icraft


二 Ubuntu20.04-aarch64
=======================

安装
----

使用以下命令为安装包设置权限

.. code-block:: bash

    chmod 777 Icraft_3.0.0_arm64.deb

使用以下命令即可开始安装

.. code-block:: bash

    sudo dpkg -i Icraft_3.0.0_arm64.deb

安装程序将在 ``/opt`` 目录创建 ``Icraft`` 文件夹，然后在此文件夹内存放当前版本的 ``Icraft`` ，例如 ``CLI v3.0.0`` 。

安装程序会修改以下环境变量：
* 为 ``PATH`` 项添加值 ``/opt/Icraft/CLIv3.0.0/bin/``
* 为 ``LD_LIBRARY_PATH`` 项添加值 ``/opt/Icraft/CLIv3.0.0/lib/``
* 为 ``CMAKE_PREFIX_PATH`` 项添加值 ``/opt/Icraft/CLIv3.0.0/lib/cmake/``

以上修改会存放到 ``/etc/profile.d/Icraft.sh`` 文件内，目前需要手动执行以下命令才会生效

.. code-block:: bash

    source /etc/profile

如果在安装前发现 ``/opt/Icraft/CLIv3.0.0`` 文件夹已存在，则拒绝安装，并提示用户已存在该版本。

卸载
----

使用以下命令即可开始卸载

.. code-block:: bash

    dpkg -r Icraft

卸载程序会删除文件 ``/etc/profile.d/Icraft.sh`` 。

如果在卸载完成后发现 ``/opt/Icraft`` 目录下还有其它版本存在，则会将上述三个环境变量项设置为该版本。

PythonAPI
----------

Icraft在Ubuntu20.04-aarch64下提供基于python3.8的扩展包，名为 ``icraft-3.0.0-cp38-none-any.whl`` 。

与其它python扩展包相同，icraft扩展包的安装与卸载命令如下：

.. code-block:: bash

    pip install icraft-3.0.0-cp38-none-any.whl
    pip uninstall icraft