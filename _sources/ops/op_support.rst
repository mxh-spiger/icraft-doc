============
框架支持列表
============

目前，Icraft平台支持的框架有：

- Pytoch，支持pytorch1.9.0、pytorch2.0.1两个版本的原生网络模型文件（.pt格式）
- Darknet，支持Darknet框架原生网络模型；
- Onnx，仅支持Paddle框架保存为onnx（opset=11）格式的模型文件；

Pytorch
=======
========================    ====================    =====================================================
    Pytorch Op                  Icraft Op               支持情况                                          
========================    ====================    =====================================================
    Adaptiveavgpool2d           Avgpool                 不支持：

                                                        - input_h%output_h!=0 || input_w%output_w!=0

    Add                         Add                     √ 

    Avgpool2d                   Avgpool                 不支持：
    
                                                        - count_include_pad=false，icraft强制修改为true 
                                                        - divisor_override < 0          

    Batchnorm                   Batchnorm               √                                                 

    Cat                         Concat                  √                                       

    Conv                        Conv2d                  不支持：
                        
                                                        - padding mode!=zeros的其他pad形式                      

    ConvTranspose2d            Conv2dTranspose         不支持：  
               
                                                        - group != 1  
                                                        - padding_mode!='zeros'               

    Chunk                       Split                   √                                    

    Flatten                     Reshape                 √  

    Linear                      InnerProduct            √

    Maxpool2d                   Maxpool                 不支持:
    
                                                        - return_indices

    Mean                        Avgpool                 不支持：
    
                                                        - ifm_dim.size()!=4 || dim!=[2,3] 

    Relu                        Relu                    √ 

    Relu6                       Relu                    √ 

    LeakyRelu                   Relu                    √

    Reshape                     Reshape                 不支持：
    
                                                        - 最后一维参与reshape：支持ofm的最后一维不为 32、16 整数倍（8bit、16bit）
  
    Softmax                     Softmax                 √

    Split                       Split                   √

    Squeeze                     Squeeze                 √

    Upsample 系列               Upsample                不支持：
    
                                                        - time=h_size/ifm_height，不支持time的质因数>22   

    View                        Reshape                 不支持：
    
                                                        - 最后一维参与reshape：支持ofm的最后一维不为 32、16 整数倍（8bit、16bit）
    
    multiply                    multiply                √

    mish                        Mish                    √

    prelu                       Prelu                   不支持：
    
                                                        - 不支持 alpha 为常数和 vector 外的情况

    sigmoid                     Sigmoid                 √

    silu                        Silu                    √

    elu                         Elu                     √

    tanh                        Tanh                    √

    hardsigmoid                 Hardsigmoid             √

    hardtanh                    Hardtanh                √

    hardswish                   Hardswish               √

    pad                         Pad                     不支持：
    
                                                        - input的排布为NHWC或NCHW且pad常数外的其他情况

    abs                         Abs                     √

    Divide_scalar               Divide_scalar           不支持：
    
                                                        - 除数不为常数标量

    expand                      Expand                  √

    expand_as                   Expand                  √

    layernorm                   Layernorm               √

    normalize                   normalize               √

    pixel_shuffle               pixel_shuffle           √

    select                      Slice                   √

    a [ :, 1:2 ]                Slice                   √

    lstmcell                    小粒度算子集合           √

    lstm                        小粒度算子集合           √
    
    grucell                     小粒度算子集合           √

    gru                         小粒度算子集合           √
========================    ====================    =====================================================

.. note::
    表格中列 ``支持情况`` 根据算子的特性，罗列了各算子不支持的参数配置。

PaddlePaddle(ONNX)
==================
========================    ====================    =====================================================
    Paddle Op                   Icraft Op               支持情况                                          
========================    ====================    =====================================================
    AdaptiveAvgPool2D           Avgpool                 不支持：

                                                        - input_h%output_h!=0 || input_w%output_w!=0
    
    AvgPool2D                   Avgpool                 √                                                   

    Batchnorm                   Batchnorm               √                                                                                     

    Conv2D                      Conv2d                  不支持：
                        
                                                        - padding mode!=zeros的其他pad形式                                                   

    Conv2DTranspose             Conv2dTranspose         不支持：  
               
                                                        - group != 1  
                                                        - padding_mode!='zeros'
                                                        - padding="SAME"，padding="SAME" 时，OnnxRuntime 与 paddle 的前向表现不一致，parser 目前是与 OnnxRuntime 对齐的                                                                                

    Flatten                     Reshape                 不支持：
    
                                                        - 最后一维参与reshape：支持ofm的最后一维不为 32、16 整数倍（8bit、16bit） 

    GELU                        Gelu                    √

    Hardwish                    Hardwish                √

    LeakyReLU                   Relu                    √

    Linear                      InnerProduct            √

    MaxPool2D                   Maxpool                 √

    Pad2D                       Pad                     √ 

    PReLU                       Prelu                   √ 

    Relu                        Relu                    √ 

    Relu6                       Relu                    √ 

    Sigmoid                     Sigmoid                 √ 

    Silu                        Silu                    √                     
  
    Softmax                     Softmax                 √

    Tanh                        Tanh                    √ 

    UpsamplingBilinear2D        Upsample                不支持：
    
                                                        - time=h_size/ifm_height，不支持time的质因数>22  

    UpsamplingNearest2D         Upsample                不支持：
    
                                                        - time=h_size/ifm_height，不支持time的质因数>22  

    ZeroPad2D                   Pad                     √ 

    reshape                     Reshape                 不支持：
    
                                                        - 最后一维参与reshape：支持ofm的最后一维不为 32、16 整数倍（8bit、16bit）

    slice                       Slice                   √

    squeeze                     Squeeze                 √

    split                       Split                   √

    strided_slice               Slice                   √                
                                                      
    unsqueeze                   Reshape                 √     

    add                         Add                     √

    bmm                         Innerproduct            √

    concat                      Concat                  √

    divide                      Divide_scalar           不支持：
    
                                                        - 除数不为常数标量         

    matmul                      Matmul                  √                   
    
    multiply                    multiply                √

    abs                         Abs                     √

    layernorm                   Layernorm               √

    pixel_shuffle               pixel_shuffle           √

    lstmcell                    小算子集合               √
                                         
========================    ====================    =====================================================

.. note::
    表格中列 ``支持情况`` 根据算子的特性，罗列了各算子不支持的参数配置。

Darknet
=======
========================   =========================    =====================================================
    Darknet Op                  Icraft Op                   支持情况                                          
========================   =========================    =====================================================
    Avgpool                     Avgpool                     √     

    Local_avgpool
 (相当于普通 avgpool)           Avgpool                      √                                                

    Cat                         Concat                      √                                       

    Convolution                 Conv2d                      不支持：
                            
                                                            - activation不为以下三种：leaky，relu，linear                    

    conv2d_transpose            Conv2dTranspose             √                 

    Chunk                       Split                       √                                    

    Flatten                     Reshape                     √  

    Connected                   InnerProduct                不支持：
                            
                                                            - activation不为以下三种：leaky，relu，linear

    Maxpool                     Maxpool                     √

    Reorg                       Reorg                       √

    Route                   - 表连接关系，没有算子；
                            - Concat，多输入情况，
                              且没有groups参数；
                            - Route，多输入情况，
                              有 groups 参数；               √ 

    Relu6                       Relu                        √ 

    LeakyRelu                   Relu                        √

    Shortcut                    Eltwise                     不支持：
                            
                                                            - activation不为以下三种：leaky，relu，linear
  
    Softmax                     Softmax                     √

    Upsample                    Upsample                    √  

    Yolo                        Yolo                        √
========================   =========================    =====================================================

.. note::
    表格中列 ``支持情况`` 根据算子的特性，罗列了各算子不支持的参数配置。