===============
Icraft 组件总览
===============

.. grid:: 2
    :gutter: 3

    .. grid-item-card:: XIR 用户手册

        :external+xir:doc:`XIR用户手册 <index>`

        See :ref:`xir <xir:genindex>` for the XIR documentation.

    .. grid-item-card:: XRT 用户手册

        :external+xrt:doc:`XRT用户手册 <index>`

        See :ref:`xrt <xrt:genindex>` for the XRT documentation

    .. grid-item-card:: CLI 用户手册

        :external+cli:doc:`CLI 用户手册 <index>`

    .. grid-item-card:: 解析用户手册

        :external+parse:doc:`解析用户手册 <index>`

        See :ref:`parse <parse:genindex>` for the parse documentation

    .. grid-item-card:: 量化用户手册

        :external+quantizer:doc:`量化用户手册 <index>`

        See :ref:`quantizer <quantizer:genindex>` for the quantizer documentation

    .. grid-item-card:: 适配用户手册

        :external+adapt:doc:`适配用户手册 <index>`

        See :ref:`adapt <adapt:genindex>` for the adapt documentation

    .. grid-item-card:: 指令生成用户手册

        :external+codegen:doc:`指令生成用户手册 <index>`

        See :ref:`codegen <codegen:genindex>` for the CodeGen documentation

    .. grid-item-card:: 运行用户手册

        :external+icraft-run:doc:`运行用户手册 <index>`

    .. grid-item-card:: HostBackend 用户手册

        :external+hostbackend:doc:`HostBackend 用户手册 <index>`

        See :ref:`hostbackend <hostbackend:genindex>` for the HostBackend documentation

    .. grid-item-card:: BuyiBackend 用户手册

        :external+buyibackend:doc:`BuyiBackend 用户手册 <index>`

        See :ref:`buyibackend <buyibackend:genindex>` for the BuyiBackend documentation

    .. grid-item-card:: SocketQL100AIU 用户手册

        :external+socketql100aiu:doc:`SocketQL100AIU 用户手册 <index>`

    .. grid-item-card:: AXIQL100AIU 用户手册

        :external+axiql100aiu:doc:`AXIQL100AIU 用户手册 <index>`

    .. grid-item-card:: Icraft-Show 用户手册

        :external+show:doc:`Icraft-Show 用户手册 <index>`