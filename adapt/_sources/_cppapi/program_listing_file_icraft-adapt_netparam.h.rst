
.. _program_listing_file_icraft-adapt_netparam.h:

Program Listing for File netparam.h
===================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-adapt_netparam.h>` (``icraft-adapt\netparam.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <string>
   namespace icraft {
       namespace adapter {
           struct NetParam {
               std::string json;                   
               std::string raw;                    
               std::string jr_path;                
               std::string log_path;               
               std::string function;               
               std::string custom_config = "";     
               std::string ini_path;               
               std::string pass_on;                
               std::string pass_off;               
               bool debug_mode = false;            
               bool no_integral = false;           
   
               NetParam(const std::string& _json, const std::string& _raw, const std::string& _jr_path,
                   const std::string& _log_path, const std::string& _function,
                   const std::string& _custom_config = "",const std::string& _ini_path = "", 
                   const std::string& _pass_on = "",const std::string& _pass_off = "", 
                   const bool _debug_mode = false, const bool _no_integral = false) {
                   json = _json;
                   raw = _raw;
                   jr_path = _jr_path;
                   log_path = _log_path;
                   function = _function;
                   custom_config = _custom_config;
                   ini_path = _ini_path;
                   pass_on = _pass_on;
                   pass_off = _pass_off;
                   debug_mode = _debug_mode;
                   no_integral = _no_integral;
               }
               NetParam() {};
           };
       }
   }
