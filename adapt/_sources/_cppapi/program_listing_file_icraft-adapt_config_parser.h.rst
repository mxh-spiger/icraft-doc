
.. _program_listing_file_icraft-adapt_config_parser.h:

Program Listing for File config_parser.h
========================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-adapt_config_parser.h>` (``icraft-adapt\config_parser.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <vector>
   #include <icraft-utils/logging.h>
   #include <icraft-xir/utils/dll_utils.h>
   #include <icraft-xir/utils/string_utils.h>
   #include "icraft-adapt/netparam.h"
   #include "ini.h"
   
   namespace icraft::adapter {
       class ConfigParser {
       public:
           static std::vector<std::pair<std::string, std::string>> getPassInfor(NetParam& net_param){
               auto adapte_sequence = parse(net_param);
               return adapte_sequence;
           }
   
       private:
           enum class OnOff {
               ON = 0,
               OFF = 1
           };
   
           static inline std::map<std::string, OnOff> onoff_map_ =
           {
               { "ON",  OnOff::ON },
               { "OFF", OnOff::OFF }
           };
           static inline std::map<std::string, int> onoff =
           {
               { "ON", static_cast<int>(OnOff::ON) },
               { "OFF", static_cast<int>(OnOff::OFF) }
           };
           static inline std::map<std::string, bool> bool_map_ =
           {
               { "TRUE", true },
               { "FALSE", false }
           };
   
           static std::vector<std::pair<std::string, std::string>> parse(NetParam& net_param) {
               std::vector<std::pair<std::string, OnOff>> default_sequence;
               net_param.ini_path = (net_param.ini_path == "") ? icraft::xir::utils::detail::GetDLLDir()+
                   ("/"+net_param.function + ".ini") : net_param.ini_path;
               if (!net_param.no_integral) default_sequence = defaultConfigParse(net_param.ini_path);
               auto pass_on = split(net_param.pass_on);
               auto pass_off = split(net_param.pass_off);
               configCheck(pass_on, pass_off);
               auto merged_sequence = mergeAdaptSequence(default_sequence, pass_on, pass_off);
               std::vector<std::pair<std::string, std::string>> adapt_sequence;
               adapt_sequence.insert(adapt_sequence.end(), merged_sequence.begin(), merged_sequence.end());
   
               return adapt_sequence;
           }
   
           static void configCheck(const std::vector<std::string>& pass_on, const std::vector<std::string>& pass_off) {
               for (auto pass : pass_on) {
                   auto it = std::find(pass_off.begin(), pass_off.end(), pass);
                   if (it != pass_off.end())
                       ICRAFT_LOG(EXCEPT) << pass << " can not be configed in pass_on and pass_off at the same time.";
               }
           }
   
           static std::vector<std::pair<std::string, std::string>> mergeAdaptSequence(
               const std::vector<std::pair<std::string, OnOff>>& default_sequence,
               const std::vector<std::string>& pass_on, const std::vector<std::string>& pass_off) {
               std::vector<std::pair<std::string, OnOff>> merged_sequence{ default_sequence.begin(), default_sequence.end() };
   
               int index = 0;
               for (auto pass : pass_on) {
                   auto defualt_it = std::find_if(merged_sequence.begin(), merged_sequence.end(), [&](std::pair<std::string, OnOff>& item) {
                       return item.first == pass;
                       });
                   auto en = OnOff::ON;
                   if (defualt_it != merged_sequence.end())
                       defualt_it->second = en;
                   else
                       merged_sequence.insert(merged_sequence.begin() + (index++), { pass, en });
               }
   
               for (auto pass : pass_off) {
                   auto defualt_it = std::find_if(merged_sequence.begin(), merged_sequence.end(), [&](std::pair<std::string, OnOff>& item) {
                       return item.first == pass;
                       });
                   auto en = OnOff::OFF;
                   if (defualt_it != merged_sequence.end())
                       defualt_it->second = en;
                   else
                       merged_sequence.insert(merged_sequence.begin(), { pass, en });
               }
   
               std::vector<std::pair<std::string, std::string>> adapt_sequence;
               auto dll_dir = xir::utils::detail::GetDLLDir();
   
               for (auto seq : merged_sequence) {
                   auto en = seq.second;
                   if (en == OnOff::ON) {
                       auto it_default = std::find_if(default_sequence.begin(), default_sequence.end(), [&]
                       (const std::pair<std::string, OnOff> item) {
                               return item.first == seq.first;
                           });
                       std::filesystem::path adapter_name;
                       auto dll_file = seq.first + ".dll";
                       //icraft_pass
                       if (it_default != default_sequence.end()) adapter_name = std::filesystem::path(dll_dir) / dll_file;
                       else
                       {
                           //search env_path first, bin second
                           auto custom_dirs_env = getenv("ICRAFT_CUSTOM_DIR");
                           if (custom_dirs_env != nullptr) {
                               auto custom_dirs = xir::utils::split(custom_dirs_env, ";");
                               for (auto&& custom_dir : custom_dirs) {
                                   auto dll_path = std::filesystem::path(custom_dir) / dll_file;
                                   if (std::filesystem::exists(dll_path)) {
                                       adapter_name = dll_path;
                                       break;
                                   }
                               }
                           }
                           //else adapter_name = std::filesystem::path(dll_dir) / dll_file;
                           if (adapter_name.empty()) adapter_name = std::filesystem::path(dll_dir) / dll_file;
                       }
   
                       if (std::filesystem::exists(adapter_name)) adapt_sequence.push_back({ adapter_name.string(), seq.first });
                       else {
                           ICRAFT_LOG(WARNING) << dll_file << " not existed. This may cause error if pass not registerd in xir already.";
                           adapt_sequence.push_back({ "File not found", seq.first });
                       }
                   }
   
               }
               return adapt_sequence;
           }
   
           /*不支持文件中一个pass多次配置，不好确定pass的位置*/
           static std::vector<std::pair<std::string, OnOff>> defaultConfigParse(const std::string& default_config) {
               std::vector<std::pair<std::string, OnOff>> default_sequence;
               std::vector<std::string> on_sequence;
               std::vector<std::string> off_sequence;
               ini::IniFile ini_file;
               ini_file.load(default_config);
   
               for (auto& it : ini_file) {
                   if (it.first == "Adapter") {
                       auto sequence = it.second;
                       for (auto itt : sequence) {
                           auto en_str = itt.second.as<std::string>();
                           auto pass = itt.first;
                           std::for_each(en_str.begin(), en_str.end(), [](char& c) { c = ::toupper(c); });
                           auto it_map = onoff.find(en_str);
                           if (it_map == onoff.end()) ICRAFT_LOG(EXCEPT) << "optionnal_config: only on and off supported";
                           auto defualt_it = std::find_if(default_sequence.begin(), default_sequence.end(), [&](std::pair<std::string, OnOff>& item) {
                               return item.first == pass;
                               });
                           auto en = onoff_map_[en_str];
                           if (defualt_it != default_sequence.end())
                               ICRAFT_LOG(EXCEPT) << pass << " can not be configed twice.";
                           else
                               default_sequence.push_back({ pass, en });
                       }
                   }
               }
               return default_sequence;
           }
   
           static std::vector<std::string> split(const std::string& seq, const std::string& delim = ",") {
               std::string::size_type pos;
               std::vector<std::string> result;
               auto str = seq;
               if (str == "")
                   return result;
               str += delim;
               int size = str.size();
               for (int i = 0; i < size; i++) {
                   pos = str.find(delim, i);
                   if (pos < size) {
                       std::string s = str.substr(i, pos - i);
                       result.push_back(s);
                       i = pos + delim.size() - 1;
                   }
               }
               return result;
           }
       };
   }
