
.. _program_listing_file_icraft-adapt_adapter_api.h:

Program Listing for File adapter_api.h
======================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-adapt_adapter_api.h>` (``icraft-adapt\adapter_api.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/network.h>
   #include <icraft-utils/logging.h>
   #include <icraft-xir/core/pass.h>
   #include "config_parser.h"
   #include "dllexport.h"
   #include "netparam.h"
   #include "log.h"
   
   namespace icraft {
       namespace adapter {
           ADAPTER_API icraft::xir::Network excutePass(icraft::xir::Network& network, 
               const std::vector<std::pair<std::string, std::string>>& passes, const NetParam& net_param, const int loop=0);
           
           ADAPTER_API icraft::xir::Network excuteAdapterPass(icraft::xir::Network& network, 
               const std::vector<std::pair<std::string, std::string>>& passes, const NetParam& net_param);
   
           ADAPTER_API icraft::xir::Network excuteOptimizerPass(icraft::xir::Network& network, 
               std::vector<std::pair<std::string, std::string>>& passes, const NetParam& net_param);
   
           ADAPTER_API icraft::xir::Network makeNetwork(const std::string& json, const std::string& raw);
   
           ADAPTER_API void dumpNetwork(const icraft::xir::Network& network, const NetParam& net_param);
   
           ADAPTER_API void optimizeNetwork(NetParam& net_param);
   
           ADAPTER_API void adaptNetwork(NetParam& net_param);
       }
   }
