.. _dir_icraft-adapt:


Directory icraft-adapt
======================


*Directory path:* ``icraft-adapt``


Files
-----

- :ref:`file_icraft-adapt_adapter_api.h`
- :ref:`file_icraft-adapt_buyilayout.h`
- :ref:`file_icraft-adapt_config_parser.h`
- :ref:`file_icraft-adapt_dllexport.h`
- :ref:`file_icraft-adapt_ini.h`
- :ref:`file_icraft-adapt_log.h`
- :ref:`file_icraft-adapt_netparam.h`
- :ref:`file_icraft-adapt_version.h`


