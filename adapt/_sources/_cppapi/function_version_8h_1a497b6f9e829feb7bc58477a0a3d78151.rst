.. _exhale_function_version_8h_1a497b6f9e829feb7bc58477a0a3d78151:

Function icraft::adapter::CommitID
==================================

- Defined in :ref:`file_icraft-adapt_version.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::adapter::CommitID()
   :project: Icraft Optimizer\Adapter