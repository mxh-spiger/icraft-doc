.. _exhale_function_adapter__api_8h_1a9ad6be0ea71e26005b49bf4674f3ed48:

Function icraft::adapter::excuteAdapterPasses
=============================================

- Defined in :ref:`file_icraft-adapt_adapter_api.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::adapter::excuteAdapterPasses(icraft::xir::Network&, std::vector<std::pair<std::string, std::string>>&, const std::string&, const NetParam&)
   :project: Icraft Optimizer\Adapter