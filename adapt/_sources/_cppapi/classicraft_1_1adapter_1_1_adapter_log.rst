.. _exhale_class_classicraft_1_1adapter_1_1_adapter_log:

Class AdapterLog
================

- Defined in :ref:`file_icraft-adapt_log.h`


Class Documentation
-------------------


.. doxygenclass:: icraft::adapter::AdapterLog
   :project: Icraft Optimizer\Adapter
   :members:
   :protected-members:
   :undoc-members: