.. _exhale_function_adapter__api_8h_1aaad868c1909bb610fde38065724d4d90:

Function icraft::adapter::dumpNetwork
=====================================

- Defined in :ref:`file_icraft-adapt_adapter_api.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::adapter::dumpNetwork(const icraft::xir::Network&, const NetParam&, const std::string&)
   :project: Icraft Optimizer\Adapter