.. _exhale_function_adapter__api_8h_1ab5a8759c81b2c77afa5d8a4a8985f9ae:

Function icraft::adapter::excutePass
====================================

- Defined in :ref:`file_icraft-adapt_adapter_api.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::adapter::excutePass(icraft::xir::Network&, std::pair<std::string, std::string>&, std::string)
   :project: Icraft Optimizer\Adapter