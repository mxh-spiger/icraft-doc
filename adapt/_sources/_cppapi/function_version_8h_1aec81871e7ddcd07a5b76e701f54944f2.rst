.. _exhale_function_version_8h_1aec81871e7ddcd07a5b76e701f54944f2:

Function icraft::adapter::MainVersion
=====================================

- Defined in :ref:`file_icraft-adapt_version.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::adapter::MainVersion()
   :project: Icraft Optimizer\Adapter