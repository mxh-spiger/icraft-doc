.. _exhale_class_classicraft_1_1adapter_1_1_config_parser:

Class ConfigParser
==================

- Defined in :ref:`file_icraft-adapt_config_parser.h`


Class Documentation
-------------------


.. doxygenclass:: icraft::adapter::ConfigParser
   :project: Icraft Optimizer\Adapter
   :members:
   :protected-members:
   :undoc-members: