.. _exhale_function_version_8h_1a234487798ed47026f728ac29b136cf0e:

Function icraft::adapter::IsModified
====================================

- Defined in :ref:`file_icraft-adapt_version.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::adapter::IsModified()
   :project: Icraft Optimizer\Adapter