
.. _file_icraft-adapt_log.h:

File log.h
==========

|exhale_lsh| :ref:`Parent directory <dir_icraft-adapt>` (``icraft-adapt``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-adapt\log.h``)
-----------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-adapt_log.h.rst





Includes
--------


- ``icraft-utils/logging.h``



Included By
-----------


- :ref:`file_icraft-adapt_adapter_api.h`




Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__adapter`


Classes
-------


- :ref:`exhale_class_classicraft_1_1adapter_1_1_adapter_log`

