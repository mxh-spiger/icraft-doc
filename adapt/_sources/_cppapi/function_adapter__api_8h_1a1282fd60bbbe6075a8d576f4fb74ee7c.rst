.. _exhale_function_adapter__api_8h_1a1282fd60bbbe6075a8d576f4fb74ee7c:

Function icraft::adapter::initLog
=================================

- Defined in :ref:`file_icraft-adapt_adapter_api.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::adapter::initLog(std::string&, const std::string&, const std::string&, const icraft::LogLevel&)
   :project: Icraft Optimizer\Adapter