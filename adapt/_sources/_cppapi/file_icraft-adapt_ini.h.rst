
.. _file_icraft-adapt_ini.h:

File ini.h
==========

|exhale_lsh| :ref:`Parent directory <dir_icraft-adapt>` (``icraft-adapt``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-adapt\ini.h``)
-----------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-adapt_ini.h.rst





Includes
--------


- ``algorithm``

- ``fstream``

- ``icraft-utils/logging.h``

- ``istream``

- ``map``

- ``sstream``

- ``stdexcept``



Included By
-----------


- :ref:`file_icraft-adapt_config_parser.h`




Namespaces
----------


- :ref:`namespace_ini`


Classes
-------


- :ref:`exhale_class_classini_1_1_ini_field`

- :ref:`exhale_class_classini_1_1_ini_file`


Typedefs
--------


- :ref:`exhale_typedef_ini_8h_1a1d8810acf5294507a68c2b6ecf5aaed8`

