.. _exhale_function_version_8h_1a96c8adafd6a10fbafa925be8d8b51716:

Function icraft::adapter::CommitSinceTag
========================================

- Defined in :ref:`file_icraft-adapt_version.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::adapter::CommitSinceTag()
   :project: Icraft Optimizer\Adapter