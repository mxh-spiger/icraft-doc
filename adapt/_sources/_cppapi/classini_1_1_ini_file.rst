.. _exhale_class_classini_1_1_ini_file:

Class IniFile
=============

- Defined in :ref:`file_icraft-adapt_ini.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public std::map< std::string, IniSection >``


Class Documentation
-------------------


.. doxygenclass:: ini::IniFile
   :project: Icraft Optimizer\Adapter
   :members:
   :protected-members:
   :undoc-members: