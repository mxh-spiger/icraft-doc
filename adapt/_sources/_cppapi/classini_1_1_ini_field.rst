.. _exhale_class_classini_1_1_ini_field:

Class IniField
==============

- Defined in :ref:`file_icraft-adapt_ini.h`


Class Documentation
-------------------


.. doxygenclass:: ini::IniField
   :project: Icraft Optimizer\Adapter
   :members:
   :protected-members:
   :undoc-members: