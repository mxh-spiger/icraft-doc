.. _exhale_function_adapter__api_8h_1a96645b1abbde278988e9f7a02e9998a0:

Function icraft::adapter::initLog
=================================

- Defined in :ref:`file_icraft-adapt_adapter_api.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::adapter::initLog(std::string&, const std::string&, const std::string&, const icraft::LogLevel&)
   :project: Icraft Optimizer\Adapter