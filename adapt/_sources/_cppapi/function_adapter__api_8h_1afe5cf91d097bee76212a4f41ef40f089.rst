.. _exhale_function_adapter__api_8h_1afe5cf91d097bee76212a4f41ef40f089:

Function icraft::adapter::excutePass
====================================

- Defined in :ref:`file_icraft-adapt_adapter_api.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::adapter::excutePass(icraft::xir::Network&, const std::vector<std::pair<std::string, std::string>>&, const NetParam&, const int)
   :project: Icraft Optimizer\Adapter