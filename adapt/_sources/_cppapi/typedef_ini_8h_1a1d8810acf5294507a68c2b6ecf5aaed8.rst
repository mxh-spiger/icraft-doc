.. _exhale_typedef_ini_8h_1a1d8810acf5294507a68c2b6ecf5aaed8:

Typedef ini::IniSection
=======================

- Defined in :ref:`file_icraft-adapt_ini.h`


Typedef Documentation
---------------------


.. doxygentypedef:: ini::IniSection
   :project: Icraft Optimizer\Adapter