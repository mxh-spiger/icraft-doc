
.. _program_listing_file_icraft-adapt_version.h:

Program Listing for File version.h
==================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-adapt_version.h>` (``icraft-adapt\version.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include "dllexport.h"
   #include "icraft-adapt/_git_version.h"
   #include <string>
   
   namespace icraft::adapter {
       ADAPTER_API std::string_view MainVersion();
   
       ADAPTER_API std::string_view FullVersion();
   
       ADAPTER_API uint64_t MajorVersionNum();
   
       ADAPTER_API uint64_t MinorVersionNum();
   
       ADAPTER_API uint64_t PatchVersionNum();
   
       ADAPTER_API uint64_t CommitSinceTag();
   
       ADAPTER_API std::string_view CommitID();
   
       ADAPTER_API bool IsModified();
   
       ADAPTER_API std::string_view BuildTime();
   }
