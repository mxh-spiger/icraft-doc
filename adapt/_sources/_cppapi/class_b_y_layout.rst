.. _exhale_class_class_b_y_layout:

Class BYLayout
==============

- Defined in :ref:`file_icraft-adapt_buyilayout.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public OpTraitFunctor< BYLayout, Array< Operation >(const Operation &, const icraft::Logger &)>``


Class Documentation
-------------------


.. doxygenclass:: BYLayout
   :project: Icraft Optimizer\Adapter
   :members:
   :protected-members:
   :undoc-members: