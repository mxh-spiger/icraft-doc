.. _exhale_function_adapter__api_8h_1ad17408341db44672c7ff76ccbdb0d129:

Function icraft::adapter::getPassInfor
======================================

- Defined in :ref:`file_icraft-adapt_adapter_api.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::adapter::getPassInfor(const NetParam&, const std::string&, const std::string&, const std::string&, const std::string&)
   :project: Icraft Optimizer\Adapter