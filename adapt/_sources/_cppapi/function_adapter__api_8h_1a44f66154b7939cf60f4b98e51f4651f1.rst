.. _exhale_function_adapter__api_8h_1a44f66154b7939cf60f4b98e51f4651f1:

Function icraft::adapter::excutePass
====================================

- Defined in :ref:`file_icraft-adapt_adapter_api.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::adapter::excutePass(icraft::xir::Network&, std::pair<std::string, std::string>&, const std::string&)
   :project: Icraft Optimizer\Adapter