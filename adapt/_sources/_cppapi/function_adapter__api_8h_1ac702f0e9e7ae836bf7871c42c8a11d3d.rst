.. _exhale_function_adapter__api_8h_1ac702f0e9e7ae836bf7871c42c8a11d3d:

Function icraft::adapter::excuteAdapterPass
===========================================

- Defined in :ref:`file_icraft-adapt_adapter_api.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::adapter::excuteAdapterPass(icraft::xir::Network&, const std::vector<std::pair<std::string, std::string>>&, const NetParam&)
   :project: Icraft Optimizer\Adapter