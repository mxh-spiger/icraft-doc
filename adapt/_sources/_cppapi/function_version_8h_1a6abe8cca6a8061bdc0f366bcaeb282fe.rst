.. _exhale_function_version_8h_1a6abe8cca6a8061bdc0f366bcaeb282fe:

Function icraft::adapter::BuildTime
===================================

- Defined in :ref:`file_icraft-adapt_version.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::adapter::BuildTime()
   :project: Icraft Optimizer\Adapter