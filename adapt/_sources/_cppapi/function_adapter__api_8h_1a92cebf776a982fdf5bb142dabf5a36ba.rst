.. _exhale_function_adapter__api_8h_1a92cebf776a982fdf5bb142dabf5a36ba:

Function icraft::adapter::makeNetwork
=====================================

- Defined in :ref:`file_icraft-adapt_adapter_api.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::adapter::makeNetwork(const std::string&, const std::string&)
   :project: Icraft Optimizer\Adapter