
.. _program_listing_file_icraft-adapt_log.h:

Program Listing for File log.h
==============================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-adapt_log.h>` (``icraft-adapt\log.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-utils/logging.h>
   
   namespace icraft {
       namespace adapter {
           class AdapterLog {
               public:
               static icraft::Logger initLog(const std::string& log_path, const icraft::LogLevel& level = icraft::LogLevel::DEBUG) {
                   auto stdout_sink = icraft::StdoutSink();
                   auto stderr_sink = icraft::StderrSink();
                   auto file_sink = icraft::FileSink(log_path);
                   //auto logger = icraft::Logger("stdout", { stdout_sink, stderr_sink, file_sink });
                   // auto logger = icraft::Logger(log_path, { file_sink });
                   auto logger = icraft::Logger(log_path, { file_sink });
                   stdout_sink.setColor(icraft::LogLevel::INFO, icraft::LogColor::Foregroud(icraft::ColorName::WHITE));
                   stderr_sink.setColor(icraft::LogLevel::FATAL, icraft::LogColor(icraft::ColorName::WHITE, icraft::ColorName::BLACK));
                   //file_sink.setPattern(">>>>>[%D %T] [%n]-[%L] %v<<<<<");
                   file_sink.setPattern(">>>>>[%D %T] %v<<<<<");
                   file_sink.setLevel(level);
                   return logger;
               }
           };
       }
   }
