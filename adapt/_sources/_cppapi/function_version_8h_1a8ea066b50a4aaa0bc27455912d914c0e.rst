.. _exhale_function_version_8h_1a8ea066b50a4aaa0bc27455912d914c0e:

Function icraft::adapter::PatchVersionNum
=========================================

- Defined in :ref:`file_icraft-adapt_version.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::adapter::PatchVersionNum()
   :project: Icraft Optimizer\Adapter