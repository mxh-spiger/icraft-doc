
.. _program_listing_file_icraft-adapt_dllexport.h:

Program Listing for File dllexport.h
====================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-adapt_dllexport.h>` (``icraft-adapt\dllexport.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #ifdef _WIN32
   #ifdef ADAPTER_API_EXPORTS
   #define ADAPTER_API _declspec(dllexport)
   #else
   #define ADAPTER_API _declspec(dllimport)
   #endif
   #else
   #define ADAPTER_API_EXPORTS
   #endif
