
.. _program_listing_file_icraft-adapt_buyilayout.h:

Program Listing for File buyilayout.h
=====================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-adapt_buyilayout.h>` (``icraft-adapt\buyilayout.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #include <icraft-xir/core/operation.h>
   #include <icraft-xir/core/functor.h>
   #include "dllexport.h"
   
   using namespace icraft::xir;
   class BYLayout : public OpTraitFunctor<BYLayout, Array<Operation>(const Operation&, const icraft::Logger&)> {
   public:
       template <typename T>
       explicit BYLayout(T t) : OpTraitFunctor(std::move(t)) {}
   
   protected:
       ADAPTER_API static FunctorType& VTable();
   };
