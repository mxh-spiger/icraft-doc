.. _exhale_function_adapter__api_8h_1a67afb4738f6bc333a9749e38bdb5e542:

Function icraft::adapter::adaptNetwork
======================================

- Defined in :ref:`file_icraft-adapt_adapter_api.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::adapter::adaptNetwork(NetParam&)
   :project: Icraft Optimizer\Adapter