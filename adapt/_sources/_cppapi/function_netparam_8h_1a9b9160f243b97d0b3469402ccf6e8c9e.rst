.. _exhale_function_netparam_8h_1a9b9160f243b97d0b3469402ccf6e8c9e:

Function icraft::adapter::getPassInfor1
=======================================

- Defined in :ref:`file_icraft-adapt_netparam.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::adapter::getPassInfor1(const NetParam&, const std::string&, std::string&, const std::string&, const std::string&, const std::string&)
   :project: Icraft Optimizer\Adapter