.. _exhale_function_adapter__api_8h_1ae8765867c8ebd5f9e9ea27de955d25cf:

Function icraft::adapter::excuteAdapterPasses
=============================================

- Defined in :ref:`file_icraft-adapt_adapter_api.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::adapter::excuteAdapterPasses(icraft::xir::Network&, std::vector<std::pair<std::string, std::string>>&, const std::string&, icraft::Logger&, const NetParam&)
   :project: Icraft Optimizer\Adapter