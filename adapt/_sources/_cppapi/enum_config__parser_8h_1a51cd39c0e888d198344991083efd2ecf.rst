.. _exhale_enum_config__parser_8h_1a51cd39c0e888d198344991083efd2ecf:

Enum OnOff
==========

- Defined in :ref:`file_icraft-adapt_config_parser.h`


Enum Documentation
------------------


.. doxygenenum:: icraft::adapter::OnOff
   :project: Icraft Optimizer\Adapter