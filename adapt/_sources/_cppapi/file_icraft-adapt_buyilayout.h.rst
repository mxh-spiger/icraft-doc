
.. _file_icraft-adapt_buyilayout.h:

File buyilayout.h
=================

|exhale_lsh| :ref:`Parent directory <dir_icraft-adapt>` (``icraft-adapt``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-adapt\buyilayout.h``)
------------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-adapt_buyilayout.h.rst





Includes
--------


- ``dllexport.h`` (:ref:`file_icraft-adapt_dllexport.h`)

- ``icraft-xir/core/functor.h``

- ``icraft-xir/core/operation.h``






Namespaces
----------


- :ref:`namespace_icraft__xir`


Classes
-------


- :ref:`exhale_class_class_b_y_layout`

