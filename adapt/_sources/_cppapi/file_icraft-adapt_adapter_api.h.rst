
.. _file_icraft-adapt_adapter_api.h:

File adapter_api.h
==================

|exhale_lsh| :ref:`Parent directory <dir_icraft-adapt>` (``icraft-adapt``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-adapt\adapter_api.h``)
-------------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-adapt_adapter_api.h.rst





Includes
--------


- ``config_parser.h`` (:ref:`file_icraft-adapt_config_parser.h`)

- ``dllexport.h`` (:ref:`file_icraft-adapt_dllexport.h`)

- ``icraft-utils/logging.h``

- ``icraft-xir/core/network.h``

- ``icraft-xir/core/pass.h``

- ``log.h`` (:ref:`file_icraft-adapt_log.h`)

- ``netparam.h`` (:ref:`file_icraft-adapt_netparam.h`)






Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__adapter`


Functions
---------


- :ref:`exhale_function_adapter__api_8h_1a67afb4738f6bc333a9749e38bdb5e542`

- :ref:`exhale_function_adapter__api_8h_1a9a754f321b8d073f2563a7359b9ec810`

- :ref:`exhale_function_adapter__api_8h_1ac702f0e9e7ae836bf7871c42c8a11d3d`

- :ref:`exhale_function_adapter__api_8h_1a69c8da2a5892e463c2608bedd71c3823`

- :ref:`exhale_function_adapter__api_8h_1afe5cf91d097bee76212a4f41ef40f089`

- :ref:`exhale_function_adapter__api_8h_1a92cebf776a982fdf5bb142dabf5a36ba`

- :ref:`exhale_function_adapter__api_8h_1ac9012a15e7a463286423c92047d25043`

