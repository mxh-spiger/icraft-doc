.. _exhale_function_adapter__api_8h_1a9a754f321b8d073f2563a7359b9ec810:

Function icraft::adapter::dumpNetwork
=====================================

- Defined in :ref:`file_icraft-adapt_adapter_api.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::adapter::dumpNetwork(const icraft::xir::Network&, const NetParam&)
   :project: Icraft Optimizer\Adapter