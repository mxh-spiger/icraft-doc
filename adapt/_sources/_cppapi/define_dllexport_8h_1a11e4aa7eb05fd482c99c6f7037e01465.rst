.. _exhale_define_dllexport_8h_1a11e4aa7eb05fd482c99c6f7037e01465:

Define ADAPTER_API_EXPORTS
==========================

- Defined in :ref:`file_icraft-adapt_dllexport.h`


Define Documentation
--------------------


.. doxygendefine:: ADAPTER_API_EXPORTS
   :project: Icraft Optimizer\Adapter