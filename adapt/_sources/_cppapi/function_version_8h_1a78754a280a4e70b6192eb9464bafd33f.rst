.. _exhale_function_version_8h_1a78754a280a4e70b6192eb9464bafd33f:

Function icraft::adapter::FullVersion
=====================================

- Defined in :ref:`file_icraft-adapt_version.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::adapter::FullVersion()
   :project: Icraft Optimizer\Adapter