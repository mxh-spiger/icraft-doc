.. _exhale_function_adapter__api_8h_1a69c8da2a5892e463c2608bedd71c3823:

Function icraft::adapter::excuteOptimizerPass
=============================================

- Defined in :ref:`file_icraft-adapt_adapter_api.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::adapter::excuteOptimizerPass(icraft::xir::Network&, std::vector<std::pair<std::string, std::string>>&, const NetParam&)
   :project: Icraft Optimizer\Adapter