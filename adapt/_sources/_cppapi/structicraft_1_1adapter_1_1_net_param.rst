.. _exhale_struct_structicraft_1_1adapter_1_1_net_param:

Struct NetParam
===============

- Defined in :ref:`file_icraft-adapt_netparam.h`


Struct Documentation
--------------------


.. doxygenstruct:: icraft::adapter::NetParam
   :project: Icraft Optimizer\Adapter
   :members:
   :protected-members:
   :undoc-members: