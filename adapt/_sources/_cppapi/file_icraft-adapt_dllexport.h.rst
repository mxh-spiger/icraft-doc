
.. _file_icraft-adapt_dllexport.h:

File dllexport.h
================

|exhale_lsh| :ref:`Parent directory <dir_icraft-adapt>` (``icraft-adapt``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-adapt\dllexport.h``)
-----------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-adapt_dllexport.h.rst







Included By
-----------


- :ref:`file_icraft-adapt_adapter_api.h`

- :ref:`file_icraft-adapt_buyilayout.h`

- :ref:`file_icraft-adapt_version.h`




Defines
-------


- :ref:`exhale_define_dllexport_8h_1a11e4aa7eb05fd482c99c6f7037e01465`

