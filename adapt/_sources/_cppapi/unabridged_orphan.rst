:orphan:


Full API
========

Directories
***********


.. toctree::
   :maxdepth: 5

   dir_icraft-adapt.rst

Files
*****


.. toctree::
   :maxdepth: 5

   file_icraft-adapt_adapter_api.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-adapt_buyilayout.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-adapt_config_parser.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-adapt_dllexport.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-adapt_ini.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-adapt_log.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-adapt_netparam.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-adapt_version.h.rst
