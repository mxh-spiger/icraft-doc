.. _exhale_function_version_8h_1a70a34ef712f6bee084e34fa3136a2241:

Function icraft::adapter::MinorVersionNum
=========================================

- Defined in :ref:`file_icraft-adapt_version.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::adapter::MinorVersionNum()
   :project: Icraft Optimizer\Adapter