Icraft编译网络，需经历icraft-parse -> icraft-optimize -> icraft-quantize -> icraft-adapt -> icraft-codegen。
其中icraft-optimize和icraft-adapt从组成结构到应用方式都高度相似，因此本文将两个组件放到一起来介绍。

一 icraft-optimize
===================
icraft-parse -> **icraft-optimize** -> icraft-quantize -> icraft-adapt -> icraft-codegen

顾名思义，icraft-optimize完成的工作是优化网络结构，或达到减小运算量的目的，或达到减小带宽的目的云云，
每一个优化选项就是一个pass。

1.1 组件结构
------------
该组件由两部分组成：

- icraft-optimize.exe
- icraft.XxPass.dll

1.1.1 icraft-optimize.exe
^^^^^^^^^^^^^^^^^^^^^^^^^
icraft-optimize.exe作为组件的顶层，主要负责结合用户的配置和内嵌配置调用各pass选项，需要说明的是，组件内部的各pass选项是循环调度的，
循环调度的结束标志是当前循环下网络结构不再发生改变。

参数说明
~~~~~~~~

- json，string类型，表示输入的中间层json文件路径。
- raw，string类型，表示输入的中间层raw文件路径，与json参数配合使用，json+raw、path+network的组合仅可存在一组。
- path，string类型，表示路径，通常与network配合使用，表示网络路径。
- network，string类型，表示网络名称，通常与path配合使用，表示网络路径，json+raw、path+network的组合仅可存在一组。
- jr_path, string类型，表示优化后网络的存放路径。
- log_path，string类型，可选，表示log文件的存放路径，省略则为默认路径。
- pass_on，string类型，可选，通过配置该参数，可以打开icraft内置pass或自定义pass，以”，“分隔，pass_on所打开的自定义pass将优先于内置pass执行。
- pass_off，string类型，可选，通过配置该参数，可以关掉icraft内置pass，以”，“分隔。pass_on和pass_off不可包含同一个pass。
- custom_config，string类型，可选，用于描述文件路径，该文件用于向自定义pass传递参数。
- no_integral, implicit类型，默认为false，该参数通常与pass_on配合使用，表明将忽略icraft内置pass，仅调用pass_on所描述的pass选项。

应用示例
~~~~~~~~

- 编译网络：icraft compile ./yolov5.toml，compile命令会一次调用icraft-parse -> **icraft-optimize** -> icraft-quantize -> icraft-adapt -> icraft-codegen
- 优化网络：icraft optimize ./yolov5.toml，optimize命令会调用**icraft-optimize**对网络进行优化，所调用的pass由yolov5.toml中[optimize]下的参数决定。

1.1.2 icraft.xxPass.dll
^^^^^^^^^^^^^^^^^^^^^^^
icraft.xxPass为具体的优化pass选项，该单元为一系列的dll文件。Optimizer中的pass选项采用xir提供的pass接口来统一实现。
pass中用到接口以及模式匹配请参见:`Icraft XIR设计说明 <https://persistent-donut-283.notion.site/Icraft-XIR-aabc179ffa114314b5a832bcac2b930a>`_。
Optimizer共有多个Pass,下面列举各Pass的功能说明：

icraft.ConvertBigKernel2SmallKernelPass
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
功能描述：该Pass的功能是将大尺寸的卷积核划分成多个小尺寸的卷积核，以实现硬件上的多线程并行计算加速。优化前后网络结构改变如下

.. image:: .//_static//BK2SK.png

icraft.ConvertBigPool2SmallPoolPass
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
功能描述：该pass将大尺寸的平均池化划分为多个小尺寸的平均池化，目的是实现多线程的并行加速。
优化前后网络结构改变与ConvertBigKernel2SmallKernelPass基本一致，区别是本pass划分的是平均池化算子而不是Conv2d算子。

icraft.ConvertDimFreeOpLayoutPass
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
功能描述：该pass将调整一些维度不敏感的算子的计算顺序，包括add和multiply两种算子。调整的结构是transpose-add和transpose-multiply两种结
构，经过调整后add和multiply将被转移至transpose前计算,便于transpose算子跟后续算子的合并，减小计算量。

.. image:: .//_static//DOL.png

可优化的网络：yolov4_mish，se_resnet等

icraft.ConvertDivideScalar2MultiplyPass
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
功能描述：该pass将除数为固定值的除法转换成乘除数的倒数的乘法。

icraft.ConvertInnerProduct2Conv2dPass
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
功能描述：该pass将reshape+matmul转换为conv2d+reshape。

约束条件：reshape中input.layout=NHWC

可优化的网络：resnet等

icraft.ConvertMultiplyAdd2BatchnormPass
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
功能描述：该pass将multiply+add转换为batchnorm。

约束条件：要求multiply和add算子的ifm0.dim=4,ifm1为vector的param。

可优化的网络：nafnet16等


icraft.ConvertRoutePass
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
功能描述：该pass将route算子转换成split算子。Darknet框架下route算子的功能可用split算子替代，为简化后端所需支持的算子库，用split替换。

icraft.ConvertSqueeze2ReshapePass
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
功能描述：该pass的功能是将squeeze算子转换成reshape算子，简化后端所需支持的算子库。

icraft.ConvertUpsampleBili2NearestPass
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
功能描述：该pass将输入ftmp尺寸为1*1的双线性插值改为最邻近插值，在输入ftmp尺寸为1*1时，最邻近插值与双线性插值是等效的，但是最邻近插值计算更快。

icraft.DemergeTransposePass
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
功能描述：受后端组件限制，要求transpose算子每次只能对两个维度的数据进行重排。该pass正是基于上述限制采用transpose拆分级联的方式进行算子的等效，
从而达到适配后端组件的目的。后端组件的限制为：1）参与重排的维度只有两个维度；特定情况下，为了减少级联次数，可以考虑合并维度，例如NX0X1X2X3->NX1X2X0X3，可考虑将X1X2合并；
2）仅面上的维度参与重排，重排的两个维度可以不相邻；3）通道参与重排，重排到通道的维度必须先移动到与通道相邻的位置。

限制条件：该pass与MergeMultipleTransposePass是相反的功能，因此该pass会在其他pass循环调用结束后单独调用一次。

icraft.ExpandParamPass
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
功能描述：该pass将根据add、multiply算子功能实现相应的数据维度扩展。

- multiply_ftmp(add_ftmp)扩充为相同数量的维度，例如input0.dim=[1,32,23,112]，input1.dim=[1,112]，经过该pass，input0.dim=[1,32,23,112]，input1.dim=[1,1,1,112]，仅扩充维度数量。
- multiply_const(add_const)，根据expand的规则，针对ftmp的输入，仅扩充维度；针对params的输入，在扩充为度的基础上，扩充最后一维。
  例如input0.dim=[1,112]，input1.dim=[1,20,1]，其中input0为ftmp，input1为params，经过该pass,input0.dim=[1,1,112]，input1.dim=[1,20,112]。
  optimizer提前将param的最后一维扩充好，简化后端实现；但是又没有扩充param的其他维度，是为了减小数据传输量，提高效率。

icraft.MergeAdd2Conv2dPass
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
功能描述：该pass将1）满足条件的add+conv2d合并为conv2d，要求add算子有一个输入为常数的vector，如果该输入原本为常数标量，会在ExpandParamPass中扩展为vector，满足合并条件；
2）满足条件的conv2d+add合并为conv2d，要求add算子有一个输入为常数的vector，如果该输入原本为常数标量，会在ExpandParamPass中扩展为vector，满足合并条件。

icraft.MergeBatchnormConv2dPass
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
功能描述：该pass将batchnorm+conv2d合并为conv2d，合并后的计算结果与合并前的会有略微差别，如果出现精度下降的情况可以将本pass关闭。

icraft.MergeBatchnormMatmulPass
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
功能描述：该pass将batchnorm+matmul合并为matmul，此处的batchnorm是layernorm=layernorm+batchnorm拆分出来的，该pass有待考虑，batchnorm必须是4维的，但是此处很可能不是4维。

icraft.MergeConv2dBatchnormPass
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
功能描述：该pass将conv2d+batchnorm合并为conv2d，合并后的计算结果与合并前的会有略微差别，如果出现精度下降的情况可以将本pass关闭。

icraft.MergeMulConst2InnerProductPass
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
功能描述：该pass将乘数是常数标量的乘法融合到内积中，其目的是减小网络计算量；合并的方式是将Multiply的参数加到InnerProduct的weights中。

icraft.MergeMultipleConcatPass
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
功能描述：该pass的功能是将多个级联且没有其他分支的concat算子合并成一个，合并的好处在于简化图结构，便于后续优化。
如果存在其他分支，则不合并。

icraft.MergeMultipleReshapePass
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
功能描述：该pass是将多个级联且没有其他分支的reshape算子合并成一个，合并的好处在于简化图结构。
如果存在其他分支，则不合并。

icraft.MergeMultipleSlicePass
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
功能描述：该pass将多个级联且没有其他分支的slice算子合并成一个，合并的好处在于减少算子数量，减少运行时间。
如果存在其他分支，则不合并。

icraft.MergeMultipleTransposePass
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
功能描述：该pass将多个级联且没有其他分支的transpose算子合并成一个，合并的好处在于减少算子数量，减少运行时间。
如果存在其他分支，则不合并。如果ftmp经过多个transpose后输出的ftmp的layout和输入ftmp一致，则删除所有的级联的transpose。

icraft.MergeTranspose2InnerProductPass
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
功能描述：该pass的功能是将Transpose融合到Innerproduct(Matmul_const)里，融合的结构是transpose-reshape-matmul，
融合后transpose消失，仅保留reshape和matmul，减小网络计算量。
约束条件：transpose算子搬移的维度经过reshape为最后一维，全用来做内积运算。

icraft.OpParamConstraintBYPass
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
功能描述：该pass参考硬件设备对算子的支持情况，配置算子的target。配置规则为：BuyiTarget、FpgaTarget优先，
上述两种target不支持的情况，才考虑HostTarget。

下表针对target进行了所支持算子的汇总。

.. list-table:: 表1 target所支持算子的汇总
   :widths: 25 60
   :header-rows: 1

   * - Target
     - 算子名称
   * - BuyiTarget
     - Conv2d、Hardswish、Relu、Gelu、Silu、Mish、Sigmoid、Tanh、Elu、Maxpool、Avgpool、Concat、Upsample、Matmul、Batchnorm、Reshape
       PixelShuffle、Squeeze、Split、Add、Multiply、Slice、Pad、Copy、Transpose、Conv2dTranspose
   * - FpgaTarget
     - Cast、Layernorm
   * - HostTarget
     - Conv2d、Hardswish、Relu、Gelu、Silu、Mish、Sigmoid、Tanh、Elu、Maxpool、Avgpool、Concat、Upsample、Matmul、Batchnorm、Reshape
       PixelShuffle、Squeeze、Split、Add、Multiply、Slice、Pad、Copy、Transpose、Conv2dTranspose、Layernorm、Cast、Reorg

需要说明的是，因为一些原因影响，上表中BuyiTarget所支持的算子是有限制的。下表列出了icraft算子在BuyiTarget中的约束，如果算子参数在约束限制内，则将该算子的target配置为HostTarget。

.. list-table:: 表2 BuyiTarget中算子约束
   :widths: 25 60
   :header-rows: 1

   * - 算子名称
     - 算子约束
   * - Conv2d 
     - 卷积核长乘宽小于2048
   * - Conv2dTranspose
     - 卷积核长乘宽小于2048
   * - Slice （待扩充）
     - slice的步长必须等于1  
   * - Upsample
     - BICUBIC模式不支持
   * - Upsample
     - 输入ftmp的维度仅支持四维或五维
   * - Upsample
     - 放大倍数是整数且面积放大倍数小于512.即 (ofm_w / ifm_w) * (ofm_h / ifm_h)<512  
   * - Pad
     - pad的数值仅支持0；不支持其他数值，即padding mode = CONSTANT  
   * - Multiply
     - 算子的两个输入的维度数量相同, 且通道数（最后一维）相同  



icraft.RemoveUselessAvgpoolPass
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
功能描述：移除无用的Avgpool，即池化的长宽，步长均为1，上下左右的pad都是0，膨胀系数为0的平均池化，
ftmp经过这样的池化数值是不会改变，因此可以直接删除该算子。

icraft.RemoveUselessBranchPass
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
功能描述：移除无用的分支，即一些从网络中分出但是没有连接到输出的无效分支，本pass会将该分支完全移除。

icraft.RemoveUselessMaxpoolPass
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
功能描述：移除无用的Maxpool，即池化的长宽，步长均为1，上下左右的pad都是0，膨胀系数为0的平均池化，
ftmp经过这样的池化数值是不会改变，因此可以直接删除该算子。

icraft.RemoveUselessReshapePass
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
功能描述：移除无用的Reshape，即输入输出ftmp的尺寸一致，layout也一致的Reshape，
ftmp经过这种算子不发生变化因此可以直接删除该算子。

icraft.RemoveUselessTranspose1Pass
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
功能描述：将仅合并HW维度的Transpose-Reshape-Transpose结构简化为Reshape，主要用于Transformer中，示意图如下

.. image:: .//_static//TRT.png

icraft.RemoveUselessTransposePass
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
功能描述：将连续交换维度而且只有一个维度为1的多个级联的transpose合并成一个Reshape。

icraft.ReplaceAvgpoolParamPass
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
功能描述：将Avgpool中电路不支持的count_include_pad = false的模式改成count_include_pad = true。

icraft.ReplaceUpsampleParamPass
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
功能描述：Nearest模式的上采样不支持非整数倍的上采样，因此遇到这种情况本pass会将Nearest换成Bilinear模式。

icraft.SplitLayernormPass
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
功能描述：该pass拆分layernorm，layernorm -> layernorm + batchnorm（mul+add），便于适配硬件的计算。

icraft.SplitUpsamplePass
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
功能描述：出于硬件的限制，双线性插值上采样倍率仅能支持到22倍，因此超过22倍的整数倍上采样会被拆分成多个级联的上采样算子实现。
但是在使用时要注意，这种拆分只有在上采样的Align Corner参数为True时才完全等效，
而在Align Corner参数为false时，二者会有一定的误差。优化前后网络结构示意图如下：

.. image:: .//_static//SU.png

icraft.TuningPadPass
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
功能描述：调整Maxpool,Avgpool,Conv2d算子的Pad值，使算子的输出ftmp规整。

icraft.TurnDimFreeOpLocationPass
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
功能描述：调整维度不敏感的算子的位置，维度不敏感的算子包括:Sigmoid,Gelu,Relu,Hardswish,Prelu,Silu,Tanh,Hardsigmoid
可以调整的算子包括：Reshape，transpose
当这两种算子相连时，就可以把维度不敏感的算子转移至Reshape或transpose后进行计算，方便后续的算子合并。

应用网络：se_resnet

.. image:: .//_static//DFO.png

icraft.TurnOpOrder1Pass
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
功能描述：调整conv2d-concat的结构，将原来由concat合并的两个卷积分支拆分回两个分支以便于后续的融合算子等优化，示意图如下

.. image:: .//_static//oporder1.png






二 icraft-adapt
================

icraft-adapt完成的主要工作是基于后端组件进行网络的适配，当然由于icraft-adapt所处的编译环节，
我们也将部分适合优化量化后网络的pass放置到了icraft-adapt中。因此icraft-adapt组件也是由一系列的内置pass组成。

2.1 组件结构
------------

该组件由两部分组成：

- icraft-adapt.exe
- icraft.XxPass.dll

2.1.1 icraft-adapt.exe
^^^^^^^^^^^^^^^^^^^^^^

icraft-adapt.exe作为组件的顶层，主要负责结合用户的配置和内嵌配置调用各pass选项，组件内部的各pass选项是循环调度的，
循环调度的结束标志是当前循环下网络结构不再发生改变。

参数说明
~~~~~~~~

- json，string类型，表示输入的中间层json文件路径。
- raw，string类型，表示输入的中间层raw文件路径，与json参数配合使用，json+raw、path+network的组合仅可存在一组。
- path，string类型，表示路径，通常与network配合使用，表示网络路径。
- network，string类型，表示网络名称，通常与path配合使用，表示网络路径，json+raw、path+network的组合仅可存在一组。
- jr_path, string类型，表示优化后网络的存放路径。
- log_path，string类型，可选，表示log文件的存放路径，省略则为默认路径。
- pass_on，string类型，可选，通过配置该参数，可以打开icraft内置pass或自定义pass，以”，“分隔，pass_on所打开的自定义pass将优先于内置pass执行。
- pass_off，string类型，可选，通过配置该参数，可以关掉icraft内置pass，以”，“分隔。pass_on和pass_off不可包含同一个pass。
- custom_config，string类型，可选，用于描述文件路径，该文件用于向自定义pass传递参数。
- no_integral, implicit类型，默认为false，该参数通常与pass_on配合使用，表明将忽略icraft内置pass，仅调用pass_on所描述的pass选项。

应用示例
~~~~~~~~

- 编译网络：icraft compile ./yolov5.toml，compile命令会一次调用icraft-parse -> icraft-optimize -> icraft-quantize -> **icraft-adapt** -> icraft-codegen
- 优化网络：icraft adapt ./yolov5.toml，adapt命令会调用**icraft-adapt**对网络进行优化，所调用的pass由yolov5.toml中[adapt]下的参数决定。

2.1.2 icraft.xxPass.dll
^^^^^^^^^^^^^^^^^^^^^^^
icraft.xxPass为具体的优化pass选项，该单元为一系列的dll文件。Adapter中的pass选项采用xir提供的pass接口来统一实现。
pass中用到接口以及模式匹配请参见:`Icraft XIR设计说明 <https://persistent-donut-283.notion.site/Icraft-XIR-aabc179ffa114314b5a832bcac2b930a>`_。
Adapter共有多个Pass,下面列举各Pass的功能说明：

icraft.AddCopy2LayernormPass
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

功能描述：buyi_target要求，layernorm的最后一维必须是32的整数倍，对于不满足32整数倍的，添加copy+concat来补充通道以满足硬件要求。

icraft.AddCopy2noInsOpPass
~~~~~~~~~~~~~~~~~~~~~~~~~~

功能描述：部分算子如reshape、split在buyi_target上为无指令算子，从而能够优化网络的计算量。但当多个无指令算子连续出现时，无指令算子序列的处理比较复杂，
因此，该pass在连续的无指令算子之间添加copy算子，便于后算组件处理。

icraft.BuyiLayoutPass
~~~~~~~~~~~~~~~~~~~~~

功能描述：icraft3.0针对BuyiTarget进行网络编译，该pass的任务就是调整网络数据排布（我们称之为layout），以适应硬件的运算。针对BuyiTarget进行layout，
我们既要实现ftmp的layout，也要实现param的layout。需要说明的是，layout的实现是一个非常繁琐的过程，或为了便于分级维护，或为了量化方便，或为了优化，
我们将整个layout的过程分散实现在了多个pass中。这其中，BuyiLayoutPass毫无疑问是最主要的一个，该pass忽略网络前后结构，从算子的角度出发，
针对每一个算子进行具体的layout。因此layout过程中，需要考虑前后结构的实现，都独立出去自成一个pass了。下面，我们将以算子为切入点，展开layout的介绍以及
相关前后铺垫的介绍。

add_layout
""""""""""
- 前级pass：ExpandParamPass、ExpandFtmpPass，将算子ifm.tensorType()->shape.size()调整到一致，ifm.tensorType()->shape无需一致。
- layout方式：1）判断c是否一致，不一致报错推出（目前不支持添加conv2d调成一致的动作）；2）判断C的位置是一致的，不一致则采用transpose移动到一致。
  3）根据ifm.tensorType()获取ofm.tensorType()。
- 后级pass：无。

avgpool_layout
""""""""""""""
- 前级pass：ConvertBigPool2SmallPoolPass，将大kernel的pool算子拆分为小kernel算子的级联；
- layout方式：1）判断C的位置，如果不满足NCHWc的排布顺序，添加transpose算子移动C到指定位置。2）根据ifm.tensorType()获取ofm.tensorType()。
- 后级pass：无。

batchnorm_layout
""""""""""""""""
- 前级pass：无。
- layout方式：1）判断C的位置，如果不满足NCHWc的排布顺序，添加transpose算子移动C到指定位置。2）根据ifm.tensorType()获取ofm.tensorType()。
- 后级pass：无。

cast_layout
""""""""""""
- 前级pass：无。
- layout方式：cast是唯一一个所有compile_target=host也要考虑layout的算子。1）compile_target==FpgaTarget，目前考虑8<->16bit的转换，
  应用与混合精度，根据target_type获取ofm.tensorType()；2）compile_target==HostTarget，考虑软硬件数据的转换，这里既要考虑数据类型的转换，
  也要考虑数据排布的转换，还要考虑补通道和去通道的问题。

  - soft -> hard, cast -> align_axis + cast  (其中，align_axis负责给数据补通道，cast负责完成数据类型的转换和数据排布的转换)，
    cast实现硬件排布时，C所在的位置时固定到了index=1的位置；
  - hard -> soft，cast -> cast + prune_axis （其中，cast负责完成数据类型的转换和数据排布的转换，prune_axis负责去掉无效通道）。
  - 后续有可能对上述步骤进行硬件加速。
  
- 后级pass：MergeTranspose2CastPass。如前所述，soft -> hard将C的索引固化到了index=1的位置，这个位置可能不满足后续算子的需求，需要添加
  tranpose来移动C的位置，MergeTranspose2CastPass用来将移动C的transpose合并到cast中，实现cast算子根据后续算子的需求来配置C的位置，减小
  网络计算量。

concat_layout
""""""""""""""
- 前级pass：无。
- layout方式：layout中，axis参数需要记录的是硬件对应的concat维度。
  
  - ifms中存在param：
  
    - axis = -1，根据所有的ftmp输入来统计c，针对ftmp添加conv2d来同步c；针对param进行补通道和重排，从而达到同步c的目的，修改axis=C_index；
      根据ofm.mergedAxisDistr，考虑添加conv2d来合并无效通道，调节为合适的c；
    - axis != -1，根据c对param进行补通道和重排，从而达到同步c的目的，因为添加了C维度，需要根据硬件维度更新axis；

  - ifms全为ftmp：

    - 目前，为支持ifms.layout不相同的情况，后续考虑添加transpose；
    - axis = -1，根据所有的ftmp输入来统计c，针对ftmp添加conv2d来同步c；修改axis=C_index；根据ofm.mergedAxisDistr，考虑添加conv2d来合并无效通道，调节为合适的c；
    - axis != -1，因为添加了C维度，需要根据硬件维度更新axis；

- 后级pass：无。

conv2d_layout
""""""""""""""
- 前级pass：PreProcessConv2dFtmpPass，该pass针对con2d_ftmp中ifm1进行重排，由HWIO -> HWOI（O中的无效通道保留）。
- layout方式：

  - conv2d_const

    - group = 1：根据weight和storage_type确定i、o；根据i、o进行weight的补通道和重排；获取ofm.tensorType()；
    - group = I*i；根据weight和storage_type确定i、o；根据i、o进行weight的补通道和重排；获取ofm.tensorType()；
    - 1 < group < I*i：根据weight、storage_type、group确定i、o；添加conv2d调整ifm.i;获取ofm.tensorType()；添加conv2d合并无效通道；

  - con2d_ftmp

    - group = 1 || I*i：将HWIOi变为OIHWoi；HWIOi->Reshape(HWIOoi)->Transpose(IOHWoi)->Transpose(OIHWoi);获取ofm.tensorType()；
    - 1 < group < I*i：考虑用concat_const针对ifm1（HWOI）进行IO的补通道；将HWIOi变为OIHWoi；
      HWIOi->Reshape(HWIOoi)->Transpose(IOHWoi)->Transpose(OIHWoi);获取ofm.tensorType()；后续添加conv2d合并无效通道。

- 后级pass：无。

conv2dtranspose_layout
""""""""""""""""""""""
- 前级pass：无
- layout方式：同于conv2d_const；
- 后级pass：无。

copy_layout
""""""""""""
- 前级pass：无
- layout方式：ofm.tensorType() = ifm.tensorType().clone()；
- 后级pass：无。

dividescalar_layout
""""""""""""""""""""
- 前级pass：ConvertDivideScalar2MultiplyPass将dividescalar替换为mul。
- layout方式：在layout中看不到该算子，忽略。
- 后级pass：无。
  
elu_layout
""""""""""""""
- 前级pass：无
- layout方式：ofm.tensorType() = ifm.tensorType().clone()；
- 后级pass：无。

gelu_layout
""""""""""""""
- 前级pass：无
- layout方式：ofm.tensorType() = ifm.tensorType().clone()；
- 后级pass：无。
  
hardsigmoid_layout
"""""""""""""""""""
- 前级pass：无
- layout方式：ofm.tensorType() = ifm.tensorType().clone()；
- 后级pass：无。

hardswish_layout
""""""""""""""""""
- 前级pass：无
- layout方式：ofm.tensorType() = ifm.tensorType().clone()；
- 后级pass：无。

input_layout
""""""""""""""
- 前级pass：无
- layout方式：该算子在3.0中compile_target = host，无需实现layout。
- 后级pass：无。

layernorm_layout
""""""""""""""""
- 前级pass：PreProcessLayernormPass

  .. image:: .//_static//PreProcessLayernormPass.png
- layout方式：仅针对layernorm算子进行layout实现，ofm.tensorType() = ifm.tensorType().clone()。PreProcessLayernormPass拆分出的算子，在其他算子自身的layout中实现。
  需要补充的是：PreProcessLayernormPass拆分出来的transpose算子在做最后两维翻转时，要保留最后一维的无效通道。
- 后级pass：无。

matmul_layout
""""""""""""""
- 前级pass：PreProcessMatmulPass，将**IO重排为**OI；
- layout方式：
  - matmul_const:根据ifm和ofm，确定weight的io；针对i对weight进行输入的补通道，o无需补，因为硬件可以自定的补输出通道；将weight从**IO重排为**OI。
  - matmul_ftmp:依赖PreProcessMatmulPass实现重排，获取ofm.tensorType()。

maxpool_layout
""""""""""""""
- 前级pass：ConvertBigPool2SmallPoolPass，将大kernel的pool算子拆分为小kernel算子的级联；
- layout方式：1）判断C的位置，如果不满足NCHWc的排布顺序，添加transpose算子移动C到指定位置。2）根据ifm.tensorType()获取ofm.tensorType()。
- 后级pass：无。

mish_layout
""""""""""""""""""
- 前级pass：无
- layout方式：ofm.tensorType() = ifm.tensorType().clone()；
- 后级pass：无。

multiply_layout
""""""""""""""""
- 前级pass：ExpandParamPass、ExpandFtmpPass，将算子ifm.tensorType()->shape.size()调整到一致，ifm.tensorType()->shape无需一致。
- layout方式：1）判断c是否一致，不一致报错推出（目前不支持添加conv2d调成一致的动作）；2）判断C的位置是一致的，不一致则采用transpose移动到一致。
  3）根据ifm.tensorType()获取ofm.tensorType()。
- 后级pass：无。

output_layout
""""""""""""""
- 前级pass：无
- layout方式：该算子在3.0中compile_target = host，无需实现layout。
- 后级pass：无。

pad_layout
""""""""""""""""""
- 前级pass：无
- layout方式：根据ofm获取ofm.tensorType()。
- 后级pass：无。

pixelshuffle_buyilayout
""""""""""""""""""""""""
- 前级pass：ConvertTranspose2PixelShufflePass，将一系列的小算子合并为pixelshuffle。
- layout方式：根据ofm获取ofm.tensorType()。
- 后级pass：ConvertPixelShuffle2TransposePass。pixelshuffle在硬件上是用reshape+transpose拼搭出来的，
  因此该pass会针对layout后的pixelshuffle进行拆分，以适配硬件的计算。

prelu_layout
""""""""""""""""""
- 前级pass：无
- layout方式：ofm.tensorType() = ifm.tensorType().clone()；
- 后级pass：无。

region_layout
""""""""""""""
- 前级pass：无
- layout方式：该算子在3.0中compile_target = host，无需实现layout。
- 后级pass：无。

relu_layout
""""""""""""""""""
- 前级pass：无
- layout方式：ofm.tensorType() = ifm.tensorType().clone()；
- 后级pass：无。

reorg_layout
""""""""""""""
- 前级pass：无
- layout方式：该算子在3.0中compile_target = host，无需实现layout。
- 后级pass：无。

reshape_layout
""""""""""""""
- 前级pass：无
- layout方式：添加transpose算子将C移动到倒数第二维，此时，我们认为恢复到了软件排布。
  硬件对reshape的理解，是除c维度外，其他维度随意拼接组合。因此要求i_c=o_c，不满足的情况下同通过添加conv2d来添加无效通道。
- 后级pass：无。

resize_layout
""""""""""""""
- 前级pass：无
- layout方式：前处理算子，该算子在3.0中compile_target = host，无需实现layout。
- 后级pass：无。

sigmoid_layout
""""""""""""""""""
- 前级pass：无
- layout方式：ofm.tensorType() = ifm.tensorType().clone()；
- 后级pass：无。

silu_layout
""""""""""""""""""
- 前级pass：无
- layout方式：ofm.tensorType() = ifm.tensorType().clone()；
- 后级pass：无。

slice_layout
""""""""""""
- 前级pass：无。
- layout方式：
  - 通道不切，根据ofm获取ofm.tensorType();instrgen支持一次切通道外的多个维度；
  - 仅切通道，考虑添加conv2d将要切出来的部分变成n*c；根据ofm获取ofm.tensorType()，因此instrgen看到的slice是切C;
  - 同时切通道和其他维度，需要对slice进行级联，待实现在新的pass里。
- 后级pass：ConvertSlice2SplitPass，在不需要搬移数据的场景，将slice算子转换为split，减小网络运行时间(暂时未实现这个优化)。

softmax_layout
""""""""""""""""""
- 待实现，考虑同于layernorm；

split_layout
""""""""""""
- 前级pass：无。
- layout方式：
  - axis=-1，针对多个输出，找到合适的c，根据c采用conv2d对ifm进行补通道，从而满足后续split不搬移数据的实现方式；
  - axis!=-1，无需特殊操作。
- 后级pass：ConvertSplit2SlicePass，在需要搬移数据的场景，将split换成slice算子，后端统一支持slice来搬移数据。

swaporder_layout
""""""""""""""""
- 前级pass：无
- layout方式：前处理算子，该算子在3.0中compile_target = host，无需实现layout。
- 后级pass：无。

tanh_layout
""""""""""""""""""
- 前级pass：无
- layout方式：ofm.tensorType() = ifm.tensorType().clone()；
- 后级pass：无。

transpose_layout
""""""""""""""""""""
- 前级pass：DemergeTransposePass，该pass负责将transpose做拆分，拆分为instrgen能够支持的形式；
- layout方式：因为前级pass的处理，此处只需考虑:
  
  - 最后一维参与重排，需要将另一个需要重排的维度先搬移到倒数第二维，然后做最后两维的重排；
    考虑到硬件可以实现重排时去无效通道，layout实现出来的网络结构必须与硬件对齐，且可以仿真，因此采用多算子来等效替换这个实现，
    instrgen编辑指令时需要做该子图重新合并为transpose算子。

    .. image:: .//_static//transpose_buyilayout.png

  - 最后一维不参与重排。
  
- 后级pass：无。

upsample_layout
""""""""""""""""
- 前级pass：SplitUpsamplePass；
- layout方式：根据ifm.tensorType()获取ofm.tensorType()；
- 后级pass：无。

icraft.ConvertPixelShuffle2TransposePass
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
- 功能描述：该pass根据pixelshuffle的layout情况，将pixelshuffle拆分为了transpose+reshape的组合，适配后端组件的实现。
  也就是说，在instrgen端是看不到pixelshuffle算子的。

icraft.CopyFtmpPass
~~~~~~~~~~~~~~~~~~~
- 功能描述：concat算子在buyi_target上为无指令算子，该pass针对同一个pass作为多个concat输入的情况，添加copy算子。

ConvertSplit2SlicePass
~~~~~~~~~~~~~~~~~~~~~~
- 前置pass：BuyiLayoutPass，针对split（axis!=-1）算子进行layout；
- 功能描述：将axis不等于第一个非1维度的场景，将split替换为slice算子，如是，后端组件的split算子不再考虑搬移数据，搬移数据的动作由slice完成。

三 硬件适配
===========

3.1 Add
--------
- 适配硬件需求：codegen要求 1）add（add_ftmp、add_const）算子的输入维度数量必须相同；
  2）如果其中一个输入为vector（无论该输入为ftmp还是param），则该输入不需要广播，codegen自动广播；
  3）如果输入需要广播，param则提前广播好，ftmp则添加expand算子。

- 相关Pass
  - ExpandParamPass：适配add_const，用于广播add_const下的ftmp和param；
  
    .. image:: .//_static//ExpandParamPass.PNG

  - ExpandFtmpPass：适配add_ftmp，用于广播add_ftmp下的ftmp；
  
    .. image:: .//_static//ExpandFtmpPass.PNG

  - SplitExpandPass:ExpandParamPass以及ExpandFtmpPass添加的expand是不考虑后端组件对expand的支持方式的，因此统一用SplitExpandPass来做expand的级联。

3.2 Multiply
------------
- 适配硬件需求：codegen要求 1）multiply（multiply_ftmp、multiply_const）算子的输入维度数量必须相同；
  2）如果其中一个输入为vector（无论该输入为ftmp还是param），则该输入不需要广播，codegen自动广播；
  3）如果输入需要广播，param则提前广播好，ftmp则添加expand算子。

- 相关Pass
  - ExpandParamPass：适配multiply_const，用于广播multiply_const下的ftmp和param；
  
    .. image:: .//_static//ExpandParamPass.PNG

  - ExpandFtmpPass：适配multiply_ftmp，用于广播multiply_ftmp下的ftmp；
  
    .. image:: .//_static//ExpandFtmpPass.PNG

  - SplitExpandPass:ExpandParamPass以及ExpandFtmpPass添加的expand是不考虑后端组件对expand的支持方式的，因此统一用SplitExpandPass来做expand的级联。

3.3 Matmul
------------
- 适配硬件需求：codegen要求 1）matmul（matmul_ftmp、matmul_const）算子的输入维度数量必须相同；
  2）如果其中一个输入只有两维（无论该输入为ftmp还是param），则该输入不需要广播，codegen自动广播；
  3）如果输入需要广播，param则提前广播好，ftmp则添加expand算子。

- 相关Pass
  - ExpandParamPass：适配matmul_const，用于广播matmul_const下的ftmp和param；
  
    .. image:: .//_static//ExpandParamPass.PNG

  - ExpandFtmpPass：适配matmul_ftmp，用于广播matmul_ftmp下的ftmp；
  
    .. image:: .//_static//ExpandFtmpPass.PNG

  - SplitExpandPass:ExpandParamPass以及ExpandFtmpPass添加的expand是不考虑后端组件对expand的支持方式的，因此统一用SplitExpandPass来做expand的级联。

3.4 Softmax
------------
- 适配硬件要求：硬件要求softmax的维度必须排在倒数第二维，且重排后最后一维必须是当前精度下最大cu的整数倍。

  .. image:: .//_static//PreProcessSoftmaxPass.PNG

- 相关pass：
  
  - PreProcessSoftmaxPass，用于添加transpose重排最后两个维度，且在改transpose上打上tag：fix_cu，表示改transpose_layout的新cu必须为指定的cu，而非最小cu。
  - BuyiLayoutPass，用于对PreProcessSoftmaxPass添加的算子逐个做layout实现。
  
- 补充：该算子的layout前向，因为softmax作为数据重排，因此需要在layout前向中做逆重排。

3.5 Layernorm
--------------
- 适配硬件要求：硬件要求layernorm维度必须排在倒数第二维，且重排后最后一维必须是当前精度下最大cu的整数倍。

  .. image:: .//_static//PreProcessLayernormPass.PNG

- 相关pass：
  
  - PreProcessLayernormPass，用于添加transpose重排最后两个维度。目前没有项softmax一样做到最大的cu。
  - BuyiLayoutPass，用于对PreProcessLayernormPass添加的算子逐个做layout实现。
  
- 补充：该算子的layout前向，因为layernorm作为数据重排，因此需要在layout前向中做逆重排。

3.6 Matmul
----------
- 适配硬件要求：硬件要求matmul_ftmp的ifm1的数据需要翻转重排，例如(a,b,c,d)->(a,b,d,c)。
- 相关pass：
  
  - PreProcessMatmulPass，用于添加transpose重排最后两个维度。
  - BuyiLayoutPass，用于对PreProcessLayernormPass添加的算子逐个做layout实现。
  

四 索引
=======
* :ref:`genindex`

