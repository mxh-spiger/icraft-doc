.. _exhale_struct_structicraft_1_1parser_1_1_net_param:

Struct NetParam
===============

- Defined in :ref:`file_icraft-parserapi_netparam.h`


Struct Documentation
--------------------


.. doxygenstruct:: icraft::parser::NetParam
   :project: Icraft Parser
   :members:
   :protected-members:
   :undoc-members: