.. _exhale_struct_structicraft_1_1parser_1_1_net_params:

Struct NetParams
================

- Defined in :ref:`file_icraft-parserapi_netparams.h`


Struct Documentation
--------------------


.. doxygenstruct:: icraft::parser::NetParams
   :project: Icraft Parser
   :members:
   :protected-members:
   :undoc-members: