.. _exhale_struct_structicraft_1_1parser_1_1_pre_method_info:

Struct PreMethodInfo
====================

- Defined in :ref:`file_icraft-parserapi_netparam.h`


Struct Documentation
--------------------


.. doxygenstruct:: icraft::parser::PreMethodInfo
   :project: Icraft Parser
   :members:
   :protected-members:
   :undoc-members: