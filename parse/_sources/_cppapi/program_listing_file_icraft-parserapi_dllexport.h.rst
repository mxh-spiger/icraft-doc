
.. _program_listing_file_icraft-parserapi_dllexport.h:

Program Listing for File dllexport.h
====================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-parserapi_dllexport.h>` (``icraft-parserapi\dllexport.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #ifdef _WIN32
   #ifdef PARSER_DLL_EXPORTS
   #define PARSER_API _declspec(dllexport)
   #else
   #define PARSER_API _declspec(dllimport)
   #endif
   #else
   #define PARSER_DLL_EXPORTS
   #endif
