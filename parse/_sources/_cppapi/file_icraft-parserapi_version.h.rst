
.. _file_icraft-parserapi_version.h:

File version.h
==============

|exhale_lsh| :ref:`Parent directory <dir_icraft-parserapi>` (``icraft-parserapi``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-parserapi\version.h``)
-------------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-parserapi_version.h.rst





Includes
--------


- ``string``






Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__parser`


Functions
---------


- :ref:`exhale_function_version_8h_1a6abe8cca6a8061bdc0f366bcaeb282fe`

- :ref:`exhale_function_version_8h_1a497b6f9e829feb7bc58477a0a3d78151`

- :ref:`exhale_function_version_8h_1a96c8adafd6a10fbafa925be8d8b51716`

- :ref:`exhale_function_version_8h_1a78754a280a4e70b6192eb9464bafd33f`

- :ref:`exhale_function_version_8h_1a234487798ed47026f728ac29b136cf0e`

- :ref:`exhale_function_version_8h_1aec81871e7ddcd07a5b76e701f54944f2`

- :ref:`exhale_function_version_8h_1a54db24f732df50a1b1ecde8e3eae4431`

- :ref:`exhale_function_version_8h_1a70a34ef712f6bee084e34fa3136a2241`

- :ref:`exhale_function_version_8h_1a8ea066b50a4aaa0bc27455912d914c0e`

