.. _exhale_function_version_8h_1a70a34ef712f6bee084e34fa3136a2241:

Function icraft::parser::MinorVersionNum
========================================

- Defined in :ref:`file_icraft-parserapi_version.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::parser::MinorVersionNum()
   :project: Icraft Parser