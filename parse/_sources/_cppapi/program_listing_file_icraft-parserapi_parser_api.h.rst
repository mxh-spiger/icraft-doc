
.. _program_listing_file_icraft-parserapi_parser_api.h:

Program Listing for File parser_api.h
=====================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-parserapi_parser_api.h>` (``icraft-parserapi\parser_api.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-parserapi/dllexport.h>
   #include <icraft-parserapi/netparams.h>
   
   namespace icraft {
       namespace parser {
           PARSER_API int parse(const NetParams& net_params);
       }
   }
