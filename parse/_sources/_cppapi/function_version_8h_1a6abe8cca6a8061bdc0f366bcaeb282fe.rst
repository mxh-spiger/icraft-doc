.. _exhale_function_version_8h_1a6abe8cca6a8061bdc0f366bcaeb282fe:

Function icraft::parser::BuildTime
==================================

- Defined in :ref:`file_icraft-parserapi_version.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::parser::BuildTime()
   :project: Icraft Parser