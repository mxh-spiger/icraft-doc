.. _exhale_function_version_8h_1a8ea066b50a4aaa0bc27455912d914c0e:

Function icraft::parser::PatchVersionNum
========================================

- Defined in :ref:`file_icraft-parserapi_version.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::parser::PatchVersionNum()
   :project: Icraft Parser