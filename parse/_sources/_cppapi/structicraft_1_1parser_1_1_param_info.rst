.. _exhale_struct_structicraft_1_1parser_1_1_param_info:

Struct ParamInfo
================

- Defined in :ref:`file_icraft-parserapi_netparam.h`


Struct Documentation
--------------------


.. doxygenstruct:: icraft::parser::ParamInfo
   :project: Icraft Parser
   :members:
   :protected-members:
   :undoc-members: