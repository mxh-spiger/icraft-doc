.. _exhale_function_parser__api_8h_1af8c8c5b170538a9f88aef892c3442201:

Function icraft::parser::parse
==============================

- Defined in :ref:`file_icraft-parserapi_parser_api.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::parser::parse(const NetParams&)
   :project: Icraft Parser