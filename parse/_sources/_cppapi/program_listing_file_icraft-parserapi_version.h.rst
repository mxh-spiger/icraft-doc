
.. _program_listing_file_icraft-parserapi_version.h:

Program Listing for File version.h
==================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-parserapi_version.h>` (``icraft-parserapi\version.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <string>
   //#include "dllexport.h"
   
   namespace icraft::parser {
       std::string_view MainVersion();
   
       std::string_view FullVersion();
   
       uint64_t MajorVersionNum();
   
       uint64_t MinorVersionNum();
   
       uint64_t PatchVersionNum();
   
       uint64_t CommitSinceTag();
   
       std::string_view CommitID();
   
       bool IsModified();
   
       std::string_view BuildTime();
   }
