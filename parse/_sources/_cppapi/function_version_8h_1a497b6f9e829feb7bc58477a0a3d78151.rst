.. _exhale_function_version_8h_1a497b6f9e829feb7bc58477a0a3d78151:

Function icraft::parser::CommitID
=================================

- Defined in :ref:`file_icraft-parserapi_version.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::parser::CommitID()
   :project: Icraft Parser