.. _exhale_function_version_8h_1a234487798ed47026f728ac29b136cf0e:

Function icraft::parser::IsModified
===================================

- Defined in :ref:`file_icraft-parserapi_version.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::parser::IsModified()
   :project: Icraft Parser