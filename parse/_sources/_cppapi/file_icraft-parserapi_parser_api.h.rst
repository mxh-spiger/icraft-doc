
.. _file_icraft-parserapi_parser_api.h:

File parser_api.h
=================

|exhale_lsh| :ref:`Parent directory <dir_icraft-parserapi>` (``icraft-parserapi``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-parserapi\parser_api.h``)
----------------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-parserapi_parser_api.h.rst





Includes
--------


- ``icraft-parserapi/dllexport.h``

- ``icraft-parserapi/netparams.h``






Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__parser`


Functions
---------


- :ref:`exhale_function_parser__api_8h_1af8c8c5b170538a9f88aef892c3442201`

