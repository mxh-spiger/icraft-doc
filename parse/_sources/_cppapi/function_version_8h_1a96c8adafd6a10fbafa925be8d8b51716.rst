.. _exhale_function_version_8h_1a96c8adafd6a10fbafa925be8d8b51716:

Function icraft::parser::CommitSinceTag
=======================================

- Defined in :ref:`file_icraft-parserapi_version.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::parser::CommitSinceTag()
   :project: Icraft Parser