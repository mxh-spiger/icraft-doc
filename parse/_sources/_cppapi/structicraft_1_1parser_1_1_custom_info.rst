.. _exhale_struct_structicraft_1_1parser_1_1_custom_info:

Struct CustomInfo
=================

- Defined in :ref:`file_icraft-parserapi_netparam.h`


Struct Documentation
--------------------


.. doxygenstruct:: icraft::parser::CustomInfo
   :project: Icraft Parser
   :members:
   :protected-members:
   :undoc-members: