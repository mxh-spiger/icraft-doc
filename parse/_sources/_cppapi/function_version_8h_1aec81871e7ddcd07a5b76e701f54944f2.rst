.. _exhale_function_version_8h_1aec81871e7ddcd07a5b76e701f54944f2:

Function icraft::parser::MainVersion
====================================

- Defined in :ref:`file_icraft-parserapi_version.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::parser::MainVersion()
   :project: Icraft Parser