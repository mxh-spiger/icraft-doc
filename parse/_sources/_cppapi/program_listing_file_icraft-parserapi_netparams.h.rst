
.. _program_listing_file_icraft-parserapi_netparams.h:

Program Listing for File netparams.h
====================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-parserapi_netparams.h>` (``icraft-parserapi\netparams.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include<string>
   #include<array>
   #include<vector>
   #include<map>
   #include<memory>
   #include<icraft-parserapi/netparam.h>
   
   namespace icraft {
       namespace parser {
           struct NetParams {
               std::string net_name;                               
               std::string framework;                              
               std::string network;                                
               std::string jr_path;                                
               std::string log_path;                               
               std::string weights;                                
               std::map<int, std::string> pre_method;              
               std::string frame_version;                          
               std::map<int, std::vector<float>> pre_scale;        
               std::map<int, std::vector<float>> pre_mean;         
               std::map<int, std::vector<int>> channel_swap;       
               std::map<int, std::vector<int>> input;              
               std::map<int, std::string> input_layout;            
           };
   
           NetParam transferNetParam(const NetParams& net_params) {
               std::string custom_op;//file: .ini
               std::string ex_param;//file: .ini
               std::vector<CustomInfo> custom_infos;
               std::vector<ExParamInfo> exparam_infos;
               PreMethodInfo pre_method_infor;
               std::vector<std::tuple<std::string, std::string, std::string>> custom_dlls;
               std::string post_method = "nop";
   
               NetParam net_param{ net_params.net_name, net_params.framework, custom_infos,exparam_infos,net_params.network,
               net_params.jr_path, net_params.log_path, net_params.weights, net_params.pre_method,pre_method_infor, post_method,
               net_params.frame_version, net_params.pre_scale, net_params.pre_mean, net_params.channel_swap, 
               net_params.input, custom_dlls,net_params.input_layout };
               return net_param;
           }
       }
   }
