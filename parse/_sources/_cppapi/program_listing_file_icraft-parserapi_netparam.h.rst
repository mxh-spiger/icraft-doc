
.. _program_listing_file_icraft-parserapi_netparam.h:

Program Listing for File netparam.h
===================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-parserapi_netparam.h>` (``icraft-parserapi\netparam.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include<string>
   #include<array>
   #include<vector>
   #include<map>
   #include<memory>
   
   namespace icraft {
       namespace parser {
           struct CustomInfo {
               std::string type;                                             
               std::string dll_name;                                         
               std::string class_name;                                       
               int start_index;                                              
               int end_index;                                                
               std::vector<std::string> inputs;                              
               std::vector<std::string> outputs;                             
               std::vector<std::pair<std::string, std::string>> params;      
               bool instantiated = false;                                    
           };
           struct PreMethodInfo { 
               std::string dll_name;                            
               std::string class_name;                          
           };
   
           struct ParamInfo {
               std::string param_name;                           
               std::vector<int> dim;                             
               std::vector<float> data;                          
           };
           struct ExParamInfo {
               std::string name;                                  
               int start_index;                                   
               int end_index;                                     
               int param_num;                                     
               std::vector<ParamInfo> param_infos;                
           };
           struct NetParam {
               std::string net_name;                                                                  
               std::string framework;                                                                 
               std::vector<CustomInfo> custom_infos;                                                  
               std::vector<ExParamInfo> exparam_infos;                                                
               std::string network;                                                                   
               std::string jr_path;                                                                   
               std::string log_path;                                                                  
               std::string weights;                                                                   
               std::map<int, std::string> pre_method;                                                 
               PreMethodInfo pre_method_infor;                                                        
               std::string post_method;                                                               
               std::string frame_version;                                                             
               std::map<int, std::vector<float>> pre_scale;                                           
               std::map<int, std::vector<float>> pre_mean;                                            
               std::map<int, std::vector<int>> channel_swap;                                          
               std::map<int, std::vector<int>> input;                                                 
               std::vector<std::tuple<std::string, std::string, std::string>> custom_dlls;            
               std::map<int, std::string> input_layout;                                               
           };                                                                                         
       }                                                                                             
   }
