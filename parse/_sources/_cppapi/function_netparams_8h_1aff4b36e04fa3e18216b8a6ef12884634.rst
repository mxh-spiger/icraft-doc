.. _exhale_function_netparams_8h_1aff4b36e04fa3e18216b8a6ef12884634:

Function icraft::parser::transferNetParam
=========================================

- Defined in :ref:`file_icraft-parserapi_netparams.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::parser::transferNetParam(const NetParams&)
   :project: Icraft Parser