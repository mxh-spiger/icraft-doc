.. _exhale_struct_structicraft_1_1parser_1_1_ex_param_info:

Struct ExParamInfo
==================

- Defined in :ref:`file_icraft-parserapi_netparam.h`


Struct Documentation
--------------------


.. doxygenstruct:: icraft::parser::ExParamInfo
   :project: Icraft Parser
   :members:
   :protected-members:
   :undoc-members: