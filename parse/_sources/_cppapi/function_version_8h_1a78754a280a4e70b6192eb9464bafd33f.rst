.. _exhale_function_version_8h_1a78754a280a4e70b6192eb9464bafd33f:

Function icraft::parser::FullVersion
====================================

- Defined in :ref:`file_icraft-parserapi_version.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::parser::FullVersion()
   :project: Icraft Parser