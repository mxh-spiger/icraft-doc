
.. _file_icraft-xrt_core_tensor.h:

File tensor.h
=============

|exhale_lsh| :ref:`Parent directory <dir_icraft-xrt_core>` (``icraft-xrt\core``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-xrt\core\tensor.h``)
-----------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-xrt_core_tensor.h.rst





Includes
--------


- ``icraft-xir/core/data.h``

- ``icraft-xir/core/data_type.h``

- ``icraft-xrt/base/dllexport.h``

- ``icraft-xrt/core/device.h``



Included By
-----------


- :ref:`file_icraft-xrt_core_backend.h`




Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__xrt`


Classes
-------


- :ref:`exhale_class_classicraft_1_1xrt_1_1_tensor`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_tensor_node`

