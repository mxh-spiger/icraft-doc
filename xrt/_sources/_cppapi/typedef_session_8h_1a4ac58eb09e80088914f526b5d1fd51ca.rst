.. _exhale_typedef_session_8h_1a4ac58eb09e80088914f526b5d1fd51ca:

Typedef icraft::xrt::TimeProfileResults
=======================================

- Defined in :ref:`file_icraft-xrt_core_session.h`


Typedef Documentation
---------------------


.. doxygentypedef:: icraft::xrt::TimeProfileResults
   :project: Icraft XRT