.. _exhale_class_classicraft_1_1xrt_1_1_buyi_device:

Class BuyiDevice
================

- Defined in :ref:`file_icraft-xrt_dev_buyi_device.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::HandleBase< BuyiDevice, Device, BuyiDeviceNode >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::BuyiDevice
   :project: Icraft XRT
   :members:
   :protected-members:
   :undoc-members: