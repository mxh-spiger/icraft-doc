.. _exhale_class_classicraft_1_1xrt_1_1_device:

Class Device
============

- Defined in :ref:`file_icraft-xrt_core_device.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::VirtualBase< Device, Handle, DeviceNode >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::Device
   :project: Icraft XRT
   :members:
   :protected-members:
   :undoc-members: