.. _exhale_function_device_8h_1ac256b08b4de939f08f66ef2da848cd8e:

Function icraft::xrt::operator+(uint64_t, const MemPtr&)
========================================================

- Defined in :ref:`file_icraft-xrt_core_device.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xrt::operator+(uint64_t, const MemPtr&)
   :project: Icraft XRT