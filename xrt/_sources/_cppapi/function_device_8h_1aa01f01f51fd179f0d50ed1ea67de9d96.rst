.. _exhale_function_device_8h_1aa01f01f51fd179f0d50ed1ea67de9d96:

Function icraft::xrt::operator<
===============================

- Defined in :ref:`file_icraft-xrt_core_device.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xrt::operator<(const MemPtr&, const MemPtr&)
   :project: Icraft XRT