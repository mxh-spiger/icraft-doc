.. _exhale_class_classicraft_1_1xrt_1_1_mem_chunk_node:

Class MemChunkNode
==================

- Defined in :ref:`file_icraft-xrt_core_device.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::NodeBase< MemChunkNode, Node >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::MemChunkNode
   :project: Icraft XRT
   :members:
   :protected-members:
   :undoc-members: