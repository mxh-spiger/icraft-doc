.. _exhale_class_classicraft_1_1xrt_1_1_tensor_node:

Class TensorNode
================

- Defined in :ref:`file_icraft-xrt_core_tensor.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::NodeBase< TensorNode, Node >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::TensorNode
   :project: Icraft XRT
   :members:
   :protected-members:
   :undoc-members: