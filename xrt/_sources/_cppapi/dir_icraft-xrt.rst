.. _dir_icraft-xrt:


Directory icraft-xrt
====================


*Directory path:* ``icraft-xrt``

Subdirectories
--------------

- :ref:`dir_icraft-xrt_base`
- :ref:`dir_icraft-xrt_core`
- :ref:`dir_icraft-xrt_dev`
- :ref:`dir_icraft-xrt_utils`



