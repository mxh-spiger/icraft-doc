.. _exhale_class_classicraft_1_1xrt_1_1_host_device_node:

Class HostDeviceNode
====================

- Defined in :ref:`file_icraft-xrt_dev_host_device.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::NodeBase< HostDeviceNode, DeviceNode >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::HostDeviceNode
   :project: Icraft XRT
   :members:
   :protected-members:
   :undoc-members: