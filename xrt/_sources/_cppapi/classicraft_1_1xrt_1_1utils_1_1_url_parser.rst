.. _exhale_class_classicraft_1_1xrt_1_1utils_1_1_url_parser:

Class UrlParser
===============

- Defined in :ref:`file_icraft-xrt_core_device.h`


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::utils::UrlParser
   :project: Icraft XRT
   :members:
   :protected-members:
   :undoc-members: