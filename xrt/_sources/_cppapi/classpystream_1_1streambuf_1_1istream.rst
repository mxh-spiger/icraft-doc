.. _exhale_class_classpystream_1_1streambuf_1_1istream:

Class streambuf::istream
========================

- Defined in :ref:`file_icraft-xrt_utils_pystreambuf.h`


Nested Relationships
--------------------

This class is a nested type of :ref:`exhale_class_classpystream_1_1streambuf`.


Inheritance Relationships
-------------------------

Base Type
*********

- ``public istream``


Derived Type
************

- ``public istream`` (:ref:`exhale_struct_structpystream_1_1istream`)


Class Documentation
-------------------


.. doxygenclass:: pystream::streambuf::istream
   :project: Icraft XRT
   :members:
   :protected-members:
   :undoc-members: