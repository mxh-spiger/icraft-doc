
.. _file_icraft-xrt_core_device.h:

File device.h
=============

|exhale_lsh| :ref:`Parent directory <dir_icraft-xrt_core>` (``icraft-xrt\core``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-xrt\core\device.h``)
-----------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-xrt_core_device.h.rst





Includes
--------


- ``cstdint``

- ``functional``

- ``icraft-xir/base/map.h``

- ``icraft-xir/base/string.h``

- ``icraft-xir/core/reflection.h``

- ``icraft-xrt/base/dllexport.h``

- ``vector``



Included By
-----------


- :ref:`file_icraft-xrt_core_tensor.h`

- :ref:`file_icraft-xrt_dev_buyi_device.h`

- :ref:`file_icraft-xrt_dev_host_device.h`




Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__xrt`

- :ref:`namespace_icraft__xrt__utils`


Classes
-------


- :ref:`exhale_class_classicraft_1_1xrt_1_1_device`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_device_node`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_device_registry`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_mem_chunk`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_mem_chunk_node`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_mem_ptr`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_mem_ptr_node`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_mem_region`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_mem_region_node`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_reg_region`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_reg_region_node`

- :ref:`exhale_class_classicraft_1_1xrt_1_1utils_1_1_url_parser`


Enums
-----


- :ref:`exhale_enum_device_8h_1af974064fefd7dd52b2dccb8e242769c7`


Functions
---------


- :ref:`exhale_function_device_8h_1ae1a38cb593f243cb21e611d484537628`

- :ref:`exhale_function_device_8h_1ac256b08b4de939f08f66ef2da848cd8e`

- :ref:`exhale_function_device_8h_1a288b7ee752d26955ddfe0921eebc7906`

- :ref:`exhale_function_device_8h_1a8b74090dfb3075d41129de3976a24270`

- :ref:`exhale_function_device_8h_1aa01f01f51fd179f0d50ed1ea67de9d96`

- :ref:`exhale_function_device_8h_1adc331dcd1e69bc979cf076cbda4e8580`

- :ref:`exhale_function_device_8h_1ae9e08156e1225760d595f320483e0d7b`

- :ref:`exhale_function_device_8h_1a2089c7a554ac1b86c3c9944d353251d5`

- :ref:`exhale_function_device_8h_1a01ca57d0ab88d81862d5d5210f92222f`


Typedefs
--------


- :ref:`exhale_typedef_device_8h_1a85bf30b5045c0b4dfd06d1d7fa0177db`

