.. _exhale_class_classicraft_1_1xrt_1_1_reg_region_node:

Class RegRegionNode
===================

- Defined in :ref:`file_icraft-xrt_core_device.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::NodeBase< RegRegionNode, Node >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::RegRegionNode
   :project: Icraft XRT
   :members:
   :protected-members:
   :undoc-members: