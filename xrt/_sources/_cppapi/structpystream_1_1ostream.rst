.. _exhale_struct_structpystream_1_1ostream:

Struct ostream
==============

- Defined in :ref:`file_icraft-xrt_utils_pystreambuf.h`


Inheritance Relationships
-------------------------

Base Types
**********

- ``private streambuf_capsule`` (:ref:`exhale_struct_structpystream_1_1streambuf__capsule`)
- ``public streambuf::ostream`` (:ref:`exhale_class_classpystream_1_1streambuf_1_1ostream`)


Struct Documentation
--------------------


.. doxygenstruct:: pystream::ostream
   :project: Icraft XRT
   :members:
   :protected-members:
   :undoc-members: