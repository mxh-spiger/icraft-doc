.. _exhale_class_classicraft_1_1xrt_1_1_device_node:

Class DeviceNode
================

- Defined in :ref:`file_icraft-xrt_core_device.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::NodeBase< DeviceNode, Node >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::DeviceNode
   :project: Icraft XRT
   :members:
   :protected-members:
   :undoc-members: