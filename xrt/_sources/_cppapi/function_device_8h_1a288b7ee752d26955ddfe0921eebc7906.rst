.. _exhale_function_device_8h_1a288b7ee752d26955ddfe0921eebc7906:

Function icraft::xrt::operator-(const MemPtr&, uint64_t)
========================================================

- Defined in :ref:`file_icraft-xrt_core_device.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xrt::operator-(const MemPtr&, uint64_t)
   :project: Icraft XRT