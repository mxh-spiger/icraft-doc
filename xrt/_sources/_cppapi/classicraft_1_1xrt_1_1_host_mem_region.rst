.. _exhale_class_classicraft_1_1xrt_1_1_host_mem_region:

Class HostMemRegion
===================

- Defined in :ref:`file_icraft-xrt_dev_host_device.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::HandleBase< HostMemRegion, MemRegion, HostMemRegionNode >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::HostMemRegion
   :project: Icraft XRT
   :members:
   :protected-members:
   :undoc-members: