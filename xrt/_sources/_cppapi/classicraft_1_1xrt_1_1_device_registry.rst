.. _exhale_class_classicraft_1_1xrt_1_1_device_registry:

Class DeviceRegistry
====================

- Defined in :ref:`file_icraft-xrt_core_device.h`


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::DeviceRegistry
   :project: Icraft XRT
   :members:
   :protected-members:
   :undoc-members: