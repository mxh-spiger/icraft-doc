
.. _file_icraft-xrt_utils_timer.h:

File timer.h
============

|exhale_lsh| :ref:`Parent directory <dir_icraft-xrt_utils>` (``icraft-xrt\utils``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-xrt\utils\timer.h``)
-----------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-xrt_utils_timer.h.rst





Includes
--------


- ``chrono``






Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__xrt`

- :ref:`namespace_icraft__xrt__utils`


Classes
-------


- :ref:`exhale_class_classicraft_1_1xrt_1_1utils_1_1_timer`

