.. _exhale_function_device_8h_1adc331dcd1e69bc979cf076cbda4e8580:

Function icraft::xrt::operator<=
================================

- Defined in :ref:`file_icraft-xrt_core_device.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xrt::operator<=(const MemPtr&, const MemPtr&)
   :project: Icraft XRT