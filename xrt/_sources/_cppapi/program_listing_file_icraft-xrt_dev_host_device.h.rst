
.. _program_listing_file_icraft-xrt_dev_host_device.h:

Program Listing for File host_device.h
======================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xrt_dev_host_device.h>` (``icraft-xrt\dev\host_device.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xrt/core/device.h>
   
   namespace icraft::xrt {
       class HostMemRegionNode : public NodeBase<HostMemRegionNode, MemRegionNode> {
       private:
           virtual void init() override {};
   
           virtual void deinit() override {};
   
           XRT_DLL virtual void read(char* dest, const MemPtr& src, uint64_t byte_size) const override;
   
           XRT_DLL virtual void write(const MemPtr& dest, char* src, uint64_t byte_size) const override;
   
           XRT_DLL virtual MemChunk malloc(uint64_t byte_size, bool auto_free) const override;
   
           XRT_DLL virtual MemChunk malloc(MemPtr begin, uint64_t byte_size, FChunkDeleter deleter, bool auto_free) const override;
   
           XRT_DLL virtual void free(const MemPtr& src) const override;
   
           XRT_DLL virtual void memcpy(const MemPtr& dest, const MemPtr& src, uint64_t byte_size) const override;
       };
   
       class HostMemRegion : public HandleBase<HostMemRegion, MemRegion, HostMemRegionNode> {};
   
       class HostDeviceNode : public NodeBase<HostDeviceNode, DeviceNode> {
       public:
           ICRAFT_DECLARE_DEVICE_NODE(HostDeviceNode) {
               ICRAFT_DEFAULT_MEM_REGION_FIELD(host, HostMemRegion);
           }
   
           virtual void init() override {};
   
           virtual void deinit() override {};
   
           virtual void reset(int level) override {};
   
           virtual bool check(int level) const override { return true; };
   
           virtual void showStatus(int level) const override {};
   
           virtual std::unordered_map<std::string, std::string> version() const override { 
               return {};
           };
       };
   
       class HostDevice : public HandleBase<HostDevice, Device, HostDeviceNode> {
       public:
           XRT_DLL static HostDevice& Default();
   
           XRT_DLL static HostMemRegion& MemRegion();
       };
   }
