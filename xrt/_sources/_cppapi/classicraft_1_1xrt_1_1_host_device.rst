.. _exhale_class_classicraft_1_1xrt_1_1_host_device:

Class HostDevice
================

- Defined in :ref:`file_icraft-xrt_dev_host_device.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::HandleBase< HostDevice, Device, HostDeviceNode >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::HostDevice
   :project: Icraft XRT
   :members:
   :protected-members:
   :undoc-members: