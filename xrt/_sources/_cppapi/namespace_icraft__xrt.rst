
.. _namespace_icraft__xrt:

Namespace icraft::xrt
=====================


.. contents:: Contents
   :local:
   :backlinks: none





Namespaces
----------


- :ref:`namespace_icraft__xrt__utils`


Classes
-------


- :ref:`exhale_class_classicraft_1_1xrt_1_1_buyi_device`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_buyi_device_node`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_device`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_device_node`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_device_registry`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_host_device`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_host_device_node`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_host_mem_region`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_host_mem_region_node`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_mem_chunk`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_mem_chunk_node`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_mem_ptr`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_mem_ptr_node`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_mem_region`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_mem_region_node`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_reg_region`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_reg_region_node`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_session`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_session_node`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_tensor`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_tensor_node`


Enums
-----


- :ref:`exhale_enum_device_8h_1af974064fefd7dd52b2dccb8e242769c7`


Functions
---------


- :ref:`exhale_function_device_8h_1ae1a38cb593f243cb21e611d484537628`

- :ref:`exhale_function_device_8h_1ac256b08b4de939f08f66ef2da848cd8e`

- :ref:`exhale_function_device_8h_1a288b7ee752d26955ddfe0921eebc7906`

- :ref:`exhale_function_device_8h_1a8b74090dfb3075d41129de3976a24270`

- :ref:`exhale_function_device_8h_1aa01f01f51fd179f0d50ed1ea67de9d96`

- :ref:`exhale_function_device_8h_1adc331dcd1e69bc979cf076cbda4e8580`

- :ref:`exhale_function_device_8h_1ae9e08156e1225760d595f320483e0d7b`

- :ref:`exhale_function_device_8h_1a2089c7a554ac1b86c3c9944d353251d5`


Typedefs
--------


- :ref:`exhale_typedef_device_8h_1a85bf30b5045c0b4dfd06d1d7fa0177db`

- :ref:`exhale_typedef_session_8h_1a4ac58eb09e80088914f526b5d1fd51ca`
