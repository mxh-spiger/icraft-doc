.. _exhale_class_classicraft_1_1xrt_1_1_mem_region:

Class MemRegion
===============

- Defined in :ref:`file_icraft-xrt_core_device.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::VirtualBase< MemRegion, Handle, MemRegionNode >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::MemRegion
   :project: Icraft XRT
   :members:
   :protected-members:
   :undoc-members: