.. _dir_icraft-xrt_base:


Directory base
==============


|exhale_lsh| :ref:`Parent directory <dir_icraft-xrt>` (``icraft-xrt``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


*Directory path:* ``icraft-xrt\base``


Files
-----

- :ref:`file_icraft-xrt_base_dllexport.h`


