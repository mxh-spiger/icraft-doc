
.. _program_listing_file_icraft-xrt_core_backend.h:

Program Listing for File backend.h
==================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xrt_core_backend.h>` (``icraft-xrt\core\backend.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   #include <icraft-xir/core/data.h>
   #include <icraft-xir/core/network.h>
   #include <icraft-xrt/core/tensor.h>
   #include <icraft-xir/core/operation.h>
   #include <string_view>
   #include <unordered_map>
   
   namespace icraft::xrt {
       using icraft::xir::Operation;
       using icraft::xir::NetworkView;
   
       class Backend;
       using FInitOp = std::function<void(const xir::Operation&, Backend)>;
       using FForwardOp = std::function<std::vector<Tensor>(const xir::Operation&, const std::vector<Tensor>&, Backend)>;
   
       using MergedOp = std::pair<Operation, std::vector<Operation>>;
       using MergedOps = std::vector<MergedOp>;
   
       class BackendNode;
   
       class Backend : public VirtualBase<Backend, Handle, BackendNode> {
       public:
           XRT_DLL std::optional<FInitOp> getInitFunc(const Operation& op) const;
   
           XRT_DLL std::optional<FForwardOp> getForwardFunc(const Operation& op) const;
   
           XRT_DLL bool isOpSupported(const Operation& op) const;
   
           XRT_DLL void initOp(const Operation& op);
   
           XRT_DLL std::vector<Tensor> forwardOp(const Operation& op, const std::vector<Tensor>& inputs);
   
           XRT_DLL void init(const NetworkView& network_view, const Device& device);
   
           XRT_DLL void deinit();
   
           XRT_DLL void apply();
   
           XRT_DLL void enableTimeProfile(bool enable);
   
           XRT_DLL void setTimeElapses(int64_t op_id, double memcpy_time, double hard_time);
   
           XRT_DLL void view(uint64_t start_index, uint64_t end_index);
   
           XRT_DLL MergedOps autoMerge();
   
           XRT_DLL Backend fork();
   
           XRT_DLL Backend& setDevice(Device device);
   
           template <typename BackendType>
           static BackendType Create(const NetworkView& network_view, const Device& device) {
               auto backend = BackendType::Init();
               backend.init(network_view, device);
               return backend;
           }
       };
   
       class BackendNode : public Node {
       public:
           NetworkView network_view;       
           Device device;                  
           bool time_profile = false;      
   
       private:
           std::unordered_map<int64_t, std::tuple<double, double>> time_elapses_;
   
           virtual void accept(AttrVisitor& visitor) override {}
   
           virtual void init(const NetworkView& network_view, const Device& device) = 0;
   
           virtual void deinit() = 0;
   
           virtual void apply() = 0;
   
           virtual void view(uint64_t start_index, uint64_t end_index) = 0;
   
           virtual MergedOps autoMerge() = 0;
   
           virtual Backend fork() = 0;
   
           friend class Backend;
           friend class Session;
       };
   
       using FOpConstraint = std::function<bool(const xir::Operation&, Backend)>;
       class BackendOpRegistry {
       public:
           class Manager;
   
           XRT_DLL BackendOpRegistry& set_init(FInitOp f);
   
           XRT_DLL BackendOpRegistry& set_forward(FForwardOp f);
   
           XRT_DLL BackendOpRegistry& set_constraint(FOpConstraint f);
   
           XRT_DLL static BackendOpRegistry& Register(std::string_view backend, std::string_view op, bool can_override = false);
   
           XRT_DLL static bool Remove(std::string_view backend, std::string_view op);
   
           XRT_DLL static std::optional<FInitOp> GetInitFunc(std::string_view backend, std::string_view op);
   
           XRT_DLL static std::optional<FForwardOp> GetForwardFunc(std::string_view backend, std::string_view op);
   
           XRT_DLL static std::optional<FOpConstraint> GetConstraintFunc(std::string_view backend, std::string_view op);
   
           XRT_DLL std::vector<std::vector<std::string_view>> ListNames();
   
       private:
           FInitOp finit_;
           FForwardOp fforward_;
           FOpConstraint fconstraint_;
       };
   
       template <
           typename OpType, 
           typename BackendType,
           typename = typename std::enable_if_t<
           std::is_base_of_v<Operation, OpType>>,
           typename = typename std::enable_if_t<
           std::is_base_of_v<Backend, BackendType>>
       >
       class BackendOpRegisterHelper {
       public:
           BackendOpRegisterHelper() 
               : registry_(BackendOpRegistry::Register(BackendType::NodeType::type_key, OpType::NodeType::type_key))
               {}
   
           BackendOpRegisterHelper& set_init(std::function<void(const OpType&, BackendType)> func) {
               auto f = [=](const xir::Operation& op, Backend backend) {
                   func(op.cast<OpType>(), backend.cast<BackendType>());
               };
               registry_.set_init(f);
               return *this;
           }
   
           BackendOpRegisterHelper& set_forward(
               std::function<std::vector<Tensor>(const OpType&, const std::vector<Tensor>&, BackendType)> func
           ) {
               auto f = [=](const xir::Operation& op, const std::vector<Tensor>& inputs, Backend backend) {
                   return func(op.cast<OpType>(), inputs, backend.cast<BackendType>());
               };
               registry_.set_forward(f);
               return *this;
           }
   
           BackendOpRegisterHelper& set_constraint(
               std::function<bool(const OpType&, BackendType)> func
           ) {
               auto f = [=](const xir::Operation& op, Backend backend) {
                   return func(op.cast<OpType>(), backend.cast<BackendType>());
               };
               registry_.set_constraint(f);
               return *this;
           }
       private:
           BackendOpRegistry& registry_;
       };
   }
   
   #define ICRAFT_ADD_OP_TO_BACKEND_IMPL_VAR_DEF           \
     static ICRAFT_ATTRIBUTE_UNUSED auto __add_op_to_backend_impl
   
   #define ICRAFT_ADD_OP_TO_BACKEND(OpType, BackendType)   \
   ICRAFT_STR_CONCAT(ICRAFT_ADD_OP_TO_BACKEND_IMPL_VAR_DEF, __COUNTER__) = \
   icraft::xrt::BackendOpRegisterHelper<OpType, BackendType>()
   
