.. _exhale_class_classicraft_1_1xrt_1_1_mem_region_node:

Class MemRegionNode
===================

- Defined in :ref:`file_icraft-xrt_core_device.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::NodeBase< MemRegionNode, Node >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::MemRegionNode
   :project: Icraft XRT
   :members:
   :protected-members:
   :undoc-members: