.. _exhale_function_device_8h_1a01ca57d0ab88d81862d5d5210f92222f:

Template Function icraft::xrt::utils::WaitUntil
===============================================

- Defined in :ref:`file_icraft-xrt_core_device.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xrt::utils::WaitUntil(predicate, std::chrono::duration<Rep, Period>)
   :project: Icraft XRT