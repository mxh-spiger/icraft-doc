.. _dir_icraft-xrt_utils:


Directory utils
===============


|exhale_lsh| :ref:`Parent directory <dir_icraft-xrt>` (``icraft-xrt``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


*Directory path:* ``icraft-xrt\utils``


Files
-----

- :ref:`file_icraft-xrt_utils_pystreambuf.h`
- :ref:`file_icraft-xrt_utils_timer.h`


