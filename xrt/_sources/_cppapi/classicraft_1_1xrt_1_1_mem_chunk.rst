.. _exhale_class_classicraft_1_1xrt_1_1_mem_chunk:

Class MemChunk
==============

- Defined in :ref:`file_icraft-xrt_core_device.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::HandleBase< MemChunk, Handle, MemChunkNode >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::MemChunk
   :project: Icraft XRT
   :members:
   :protected-members:
   :undoc-members: