
.. _file_icraft-xrt_core_session.h:

File session.h
==============

|exhale_lsh| :ref:`Parent directory <dir_icraft-xrt_core>` (``icraft-xrt\core``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-xrt\core\session.h``)
------------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-xrt_core_session.h.rst





Includes
--------


- ``icraft-xir/core/network.h``

- ``icraft-xrt/core/backend.h``






Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__xrt`


Classes
-------


- :ref:`exhale_class_classicraft_1_1xrt_1_1_session`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_session_node`


Typedefs
--------


- :ref:`exhale_typedef_session_8h_1a4ac58eb09e80088914f526b5d1fd51ca`

