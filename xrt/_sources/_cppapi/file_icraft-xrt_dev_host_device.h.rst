
.. _file_icraft-xrt_dev_host_device.h:

File host_device.h
==================

|exhale_lsh| :ref:`Parent directory <dir_icraft-xrt_dev>` (``icraft-xrt\dev``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-xrt\dev\host_device.h``)
---------------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-xrt_dev_host_device.h.rst





Includes
--------


- ``icraft-xrt/core/device.h``






Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__xrt`


Classes
-------


- :ref:`exhale_class_classicraft_1_1xrt_1_1_host_device`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_host_device_node`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_host_mem_region`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_host_mem_region_node`

