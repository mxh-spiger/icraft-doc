.. _exhale_class_classicraft_1_1xrt_1_1_buyi_device_node:

Class BuyiDeviceNode
====================

- Defined in :ref:`file_icraft-xrt_dev_buyi_device.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::NodeBase< BuyiDeviceNode, DeviceNode >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::BuyiDeviceNode
   :project: Icraft XRT
   :members:
   :protected-members:
   :undoc-members: