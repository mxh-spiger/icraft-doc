
.. _namespace_pystream:

Namespace pystream
==================


A stream buffer getting data from and putting data into a Python file object. 




.. contents:: Contents
   :local:
   :backlinks: none




Detailed Description
--------------------

The aims are as follow:



- Given a C++ function acting on a standard stream, e.g.

   voidread_inputs(std::istream&input){
...
input>>something>>something_else;
}


   and given a piece of Python code which creates a file-like object, to be able to pass this file object to that C++ function, e.g.

   importgzip
gzip_file_obj=gzip.GzipFile(...)
read_inputs(gzip_file_obj)


   and have the standard stream pull data from and put data into the Python file object.

- When Python ``read_inputs()`` returns, the Python object is able to continue reading or writing where the C++ code left off.

- Operations in C++ on mere files should be competitively fast compared to the direct use of ``std::fstream``.



**Motivation** 



- the standard Python library offer of file-like objects (files, compressed files and archives, network, ...) is far superior to the offer of streams in the C++ standard library and Boost C++ libraries.

- i/o code involves a fair amount of text processing which is more efficiently prototyped in Python but then one may need to rewrite a time-critical part in C++, in as seamless a manner as possible.



**Usage** 

This is 2-step:



- a trivial wrapper function

   usingboost_adaptbx::python::streambuf;
voidread_inputs_wrapper(:ref:`exhale_class_classpystream_1_1streambuf`&input)
{
:ref:`exhale_class_classpystream_1_1streambuf_1_1istream`is(input);
read_inputs(is);
}

def("read_inputs",read_inputs_wrapper);


   which has to be written every time one wants a Python binding for such a C++ function.

- the Python side

   fromboost.pythonimportstreambuf
read_inputs(streambuf(python_file_obj=obj,buffer_size=1024))


   ``buffer_size`` is optional. See also: ``default_buffer_size`` 



Note: references are to the C++ standard (the numbers between parentheses at the end of references are margin markers). 





Classes
-------


- :ref:`exhale_struct_structpystream_1_1istream`

- :ref:`exhale_struct_structpystream_1_1ostream`

- :ref:`exhale_struct_structpystream_1_1streambuf__capsule`

- :ref:`exhale_class_classpystream_1_1streambuf`

- :ref:`exhale_class_classpystream_1_1streambuf_1_1istream`

- :ref:`exhale_class_classpystream_1_1streambuf_1_1ostream`
