.. _dir_icraft-xrt_dev:


Directory dev
=============


|exhale_lsh| :ref:`Parent directory <dir_icraft-xrt>` (``icraft-xrt``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


*Directory path:* ``icraft-xrt\dev``


Files
-----

- :ref:`file_icraft-xrt_dev_buyi_device.h`
- :ref:`file_icraft-xrt_dev_host_device.h`


