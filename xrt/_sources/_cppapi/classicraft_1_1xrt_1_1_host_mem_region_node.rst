.. _exhale_class_classicraft_1_1xrt_1_1_host_mem_region_node:

Class HostMemRegionNode
=======================

- Defined in :ref:`file_icraft-xrt_dev_host_device.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::NodeBase< HostMemRegionNode, MemRegionNode >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::HostMemRegionNode
   :project: Icraft XRT
   :members:
   :protected-members:
   :undoc-members: