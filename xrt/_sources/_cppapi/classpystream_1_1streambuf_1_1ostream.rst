.. _exhale_class_classpystream_1_1streambuf_1_1ostream:

Class streambuf::ostream
========================

- Defined in :ref:`file_icraft-xrt_utils_pystreambuf.h`


Nested Relationships
--------------------

This class is a nested type of :ref:`exhale_class_classpystream_1_1streambuf`.


Inheritance Relationships
-------------------------

Base Type
*********

- ``public ostream``


Derived Type
************

- ``public ostream`` (:ref:`exhale_struct_structpystream_1_1ostream`)


Class Documentation
-------------------


.. doxygenclass:: pystream::streambuf::ostream
   :project: Icraft XRT
   :members:
   :protected-members:
   :undoc-members: