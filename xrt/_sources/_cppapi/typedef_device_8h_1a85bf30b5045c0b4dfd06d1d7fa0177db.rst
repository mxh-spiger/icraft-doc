.. _exhale_typedef_device_8h_1a85bf30b5045c0b4dfd06d1d7fa0177db:

Typedef icraft::xrt::FChunkDeleter
==================================

- Defined in :ref:`file_icraft-xrt_core_device.h`


Typedef Documentation
---------------------


.. doxygentypedef:: icraft::xrt::FChunkDeleter
   :project: Icraft XRT