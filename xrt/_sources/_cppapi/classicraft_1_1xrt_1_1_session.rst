.. _exhale_class_classicraft_1_1xrt_1_1_session:

Class Session
=============

- Defined in :ref:`file_icraft-xrt_core_session.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::HandleBase< Session, Handle, SessionNode >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::Session
   :project: Icraft XRT
   :members:
   :protected-members:
   :undoc-members: