.. _exhale_struct_structpybind11_1_1detail_1_1type__caster_3_01std_1_1ostream_01_4:

Template Struct type_caster< std::ostream >
===========================================

- Defined in :ref:`file_icraft-xrt_utils_pystreambuf.h`


Struct Documentation
--------------------


.. doxygenstruct:: pybind11::detail::type_caster< std::ostream >
   :project: Icraft XRT
   :members:
   :protected-members:
   :undoc-members: