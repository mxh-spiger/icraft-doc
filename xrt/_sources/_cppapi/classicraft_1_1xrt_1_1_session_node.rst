.. _exhale_class_classicraft_1_1xrt_1_1_session_node:

Class SessionNode
=================

- Defined in :ref:`file_icraft-xrt_core_session.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::NodeBase< SessionNode, Node >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::SessionNode
   :project: Icraft XRT
   :members:
   :protected-members:
   :undoc-members: