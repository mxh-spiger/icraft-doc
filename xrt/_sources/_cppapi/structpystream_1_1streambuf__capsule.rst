.. _exhale_struct_structpystream_1_1streambuf__capsule:

Struct streambuf_capsule
========================

- Defined in :ref:`file_icraft-xrt_utils_pystreambuf.h`


Inheritance Relationships
-------------------------

Derived Types
*************

- ``private istream`` (:ref:`exhale_struct_structpystream_1_1istream`)
- ``private ostream`` (:ref:`exhale_struct_structpystream_1_1ostream`)


Struct Documentation
--------------------


.. doxygenstruct:: pystream::streambuf_capsule
   :project: Icraft XRT
   :members:
   :protected-members:
   :undoc-members: