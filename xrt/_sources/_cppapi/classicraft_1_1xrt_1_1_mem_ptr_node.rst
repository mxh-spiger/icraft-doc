.. _exhale_class_classicraft_1_1xrt_1_1_mem_ptr_node:

Class MemPtrNode
================

- Defined in :ref:`file_icraft-xrt_core_device.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::NodeBase< MemPtrNode, Node >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::MemPtrNode
   :project: Icraft XRT
   :members:
   :protected-members:
   :undoc-members: