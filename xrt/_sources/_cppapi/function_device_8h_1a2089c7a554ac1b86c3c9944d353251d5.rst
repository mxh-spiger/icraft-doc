.. _exhale_function_device_8h_1a2089c7a554ac1b86c3c9944d353251d5:

Function icraft::xrt::operator>=
================================

- Defined in :ref:`file_icraft-xrt_core_device.h`


Function Documentation
----------------------


.. doxygenfunction:: icraft::xrt::operator>=(const MemPtr&, const MemPtr&)
   :project: Icraft XRT