
.. _file_icraft-xrt_core_backend.h:

File backend.h
==============

|exhale_lsh| :ref:`Parent directory <dir_icraft-xrt_core>` (``icraft-xrt\core``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-xrt\core\backend.h``)
------------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-xrt_core_backend.h.rst





Includes
--------


- ``icraft-xir/core/data.h``

- ``icraft-xir/core/network.h``

- ``icraft-xir/core/operation.h``

- ``icraft-xrt/core/tensor.h``

- ``string_view``

- ``unordered_map``



Included By
-----------


- :ref:`file_icraft-xrt_core_session.h`



