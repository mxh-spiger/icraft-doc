.. _exhale_enum_device_8h_1af974064fefd7dd52b2dccb8e242769c7:

Enum PtrType
============

- Defined in :ref:`file_icraft-xrt_core_device.h`


Enum Documentation
------------------


.. doxygenenum:: icraft::xrt::PtrType
   :project: Icraft XRT