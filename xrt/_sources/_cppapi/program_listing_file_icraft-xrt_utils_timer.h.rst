
.. _program_listing_file_icraft-xrt_utils_timer.h:

Program Listing for File timer.h
================================

|exhale_lsh| :ref:`Return to documentation for file <file_icraft-xrt_utils_timer.h>` (``icraft-xrt\utils\timer.h``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS

.. code-block:: cpp

   #pragma once
   
   #include <chrono>
   
   namespace icraft::xrt::utils {
       class Timer {
       public:
           void tik() {
               start = std::chrono::high_resolution_clock::now();
           }
   
           void tok() {
               end = std::chrono::high_resolution_clock::now();
           }
   
           template <typename T>
           double elapsed() const {
               return std::chrono::duration<double, typename T::period>(end - start).count();
           }
   
           std::chrono::nanoseconds duration() const {
               return end - start;
           }
   
       private:
           std::chrono::time_point<std::chrono::high_resolution_clock> start, end;
       };
   }
