
.. _file_icraft-xrt_dev_buyi_device.h:

File buyi_device.h
==================

|exhale_lsh| :ref:`Parent directory <dir_icraft-xrt_dev>` (``icraft-xrt\dev``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


.. contents:: Contents
   :local:
   :backlinks: none

Definition (``icraft-xrt\dev\buyi_device.h``)
---------------------------------------------


.. toctree::
   :maxdepth: 1

   program_listing_file_icraft-xrt_dev_buyi_device.h.rst





Includes
--------


- ``cstdint``

- ``icraft-xrt/core/device.h``






Namespaces
----------


- :ref:`namespace_icraft`

- :ref:`namespace_icraft__xrt`


Classes
-------


- :ref:`exhale_class_classicraft_1_1xrt_1_1_buyi_device`

- :ref:`exhale_class_classicraft_1_1xrt_1_1_buyi_device_node`

