.. _exhale_class_classicraft_1_1xrt_1_1_reg_region:

Class RegRegion
===============

- Defined in :ref:`file_icraft-xrt_core_device.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::HandleBase< RegRegion, Handle, RegRegionNode >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::RegRegion
   :project: Icraft XRT
   :members:
   :protected-members:
   :undoc-members: