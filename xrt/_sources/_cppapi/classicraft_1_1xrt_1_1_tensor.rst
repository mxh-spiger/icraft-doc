.. _exhale_class_classicraft_1_1xrt_1_1_tensor:

Class Tensor
============

- Defined in :ref:`file_icraft-xrt_core_tensor.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::HandleBase< Tensor, Handle, TensorNode >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::Tensor
   :project: Icraft XRT
   :members:
   :protected-members:
   :undoc-members: