.. _dir_icraft-xrt_core:


Directory core
==============


|exhale_lsh| :ref:`Parent directory <dir_icraft-xrt>` (``icraft-xrt``)

.. |exhale_lsh| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS


*Directory path:* ``icraft-xrt\core``


Files
-----

- :ref:`file_icraft-xrt_core_backend.h`
- :ref:`file_icraft-xrt_core_device.h`
- :ref:`file_icraft-xrt_core_session.h`
- :ref:`file_icraft-xrt_core_tensor.h`


