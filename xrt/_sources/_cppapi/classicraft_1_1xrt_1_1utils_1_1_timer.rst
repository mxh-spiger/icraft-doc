.. _exhale_class_classicraft_1_1xrt_1_1utils_1_1_timer:

Class Timer
===========

- Defined in :ref:`file_icraft-xrt_utils_timer.h`


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::utils::Timer
   :project: Icraft XRT
   :members:
   :protected-members:
   :undoc-members: