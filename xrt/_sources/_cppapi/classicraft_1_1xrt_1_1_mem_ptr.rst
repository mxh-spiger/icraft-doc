.. _exhale_class_classicraft_1_1xrt_1_1_mem_ptr:

Class MemPtr
============

- Defined in :ref:`file_icraft-xrt_core_device.h`


Inheritance Relationships
-------------------------

Base Type
*********

- ``public icraft::xir::HandleBase< MemPtr, Handle, MemPtrNode >``


Class Documentation
-------------------


.. doxygenclass:: icraft::xrt::MemPtr
   :project: Icraft XRT
   :members:
   :protected-members:
   :undoc-members: