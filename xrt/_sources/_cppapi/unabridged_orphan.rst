:orphan:


Full API
========

Directories
***********


.. toctree::
   :maxdepth: 5

   dir_icraft-xrt.rst

.. toctree::
   :maxdepth: 5

   dir_icraft-xrt_base.rst

.. toctree::
   :maxdepth: 5

   dir_icraft-xrt_core.rst

.. toctree::
   :maxdepth: 5

   dir_icraft-xrt_dev.rst

.. toctree::
   :maxdepth: 5

   dir_icraft-xrt_utils.rst

Files
*****


.. toctree::
   :maxdepth: 5

   file_icraft-xrt_core_backend.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xrt_dev_buyi_device.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xrt_core_device.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xrt_base_dllexport.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xrt_dev_host_device.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xrt_utils_pystreambuf.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xrt_core_session.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xrt_core_tensor.h.rst

.. toctree::
   :maxdepth: 5

   file_icraft-xrt_utils_timer.h.rst
